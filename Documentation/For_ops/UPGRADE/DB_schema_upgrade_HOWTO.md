# Database schema upgrade HOWTO

In case of database schema modification from one version to another one, you will have to do `pgdump` 
and place the result in `config/SQL/COMPTOIR_DB_create_tables_and_procedures.sql`.

This if fine.

BUT, as this file is meant to be run by a non-privileged user (comptoir), you'll have to remove all 
SQL commands requiring privileges. As a record, those commands are all placed in 
`config/SQL/COMPTOIR_DB_create_database_and_set_ownership.sql` (which is run as root).

So you'll have to *manually* remove the following statements :

* at the beginning of the file:
    * all statements before the `\connect comptoir` statement, including the `\connect comptoir` statement 
    * statement `CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;`
    * statement `COMMENT ON EXTENSION plpgsql
                 IS 'PL/pgSQL procedural language';`
* at the end of the file, the four statements:
    `REVOKE ALL ON SCHEMA public FROM PUBLIC;
    REVOKE ALL ON SCHEMA public FROM postgres;
    GRANT ALL ON SCHEMA public TO postgres;
    GRANT ALL ON SCHEMA public TO PUBLIC`

   
