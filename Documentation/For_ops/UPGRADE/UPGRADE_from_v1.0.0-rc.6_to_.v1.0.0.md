# How to upgrade from v1.0.0-rc.6 to v1.0.0

## 0. Pre-requisites: Have the suitable database

* a database `comptoir`
* a user `comptoir`, owner of the database `comptoir`

Verify it with:

```shell
psql -U comptoir -W comptoir
```

@@@TODO how to verify comptoir has effectively the ownership on the database ???

## 1. Stop Apache

```
service apache2 stop
```

## 2. COMPTOIR-SRV Update source code

As a regular user (say "comptoir"):

```shell
cd /home/comptoir/Comptoir-srv
git fetch --prune
git checkout v1.0.0
```

## 3. Migrate DB schema

```shell
cd /home/comptoir/Comptoir-srv
./bin/cake migrations migrate -t 20161213103642
```

## 4. Define email settings for notifications

Edit `/home/comptoir/Comptoir-srv/config/app.php`, and set `username` and `password`:

```
'EmailTransport' => [
           'default' => [
               'host' => 'in-v3.mailjet.com',
               'port' => 587,
               'username' => 'my-username',
               'password' => 'my-password',
               'className' => 'Smtp',
               'tls' => true
           ],
       ],
```

## 5. COMPTOIR-WEB Update source code

As a regular user (say "comptoir"):

```
cd /home/comptoir/Comptoir-web
git fetch --prune
git checkout v1.0.0
```

## 6. URL comptoir web

In `/home/comptoir/Comptoir-srv/config/paths.php`, define value of `COMPTOIR_WEB_HOST`

## 7. Reset i18n

```
cd /home/comptoir/Comptoir-srv
rm -rf ./tmp/cache/persistent/myapp_cake_core_translations_*
```

## 8. Set Log file

As a regular user (say "comptoir"):

```
touch /home/comptoir/Comptoir-srv/logs/crud.log
```

## 9. Set UNIX permissions

As user **root**:

```shell
cd /home/comptoir/Comptoir-srv/
sudo chown -R comptoir.www-data webroot logs tmp
sudo chmod -R o-w *
for i in webroot logs tmp; do
    sudo find $i -type d -exec sudo chmod 775 {} \;
    sudo find $i -type f -exec sudo chmod 664 {} \;
done
```

## 10. Start Apache

```
service apache2 start
```
