# How to upgrade from v2.12.0 to v2.13.0

## 1) First backup (DB + images files)

```bash
# (1) user comptoir
sudo su comptoir

# First backup (DB + images files)
/home/comptoir/Comptoir-srv/bin/COMPTOIR_export_DB_data_AND_images.sh
/home/comptoir/Comptoir-srv/bin/COMPTOIR_export_DB_structure_only.sh
ls -lh /home/comptoir/Comptoir-EXPORT/
```

## 2) Preloading vendor directory

```bash
# (1) use the user "comptoir"
sudo su comptoir

# Preloading vendor
mkdir /home/comptoir/tmp_migration
cd /home/comptoir/tmp_migration
git clone https://gitlab.adullact.net/Comptoir/Comptoir-srv.git
mv Comptoir-srv/ Comptoir_tmp_update_vendor
cd Comptoir_tmp_update_vendor/
git checkout origin/main
git log --decorate --oneline --graph --all
composer check-platform-reqs
composer validate
composer install
```

## 3) Activate maintenance site

see:
[www-maintenance-comptoir](https://gitlab.adullact.net/Adullact-prive/comptoir-prive/www-maintenance-comptoir)
(private repository)

```bash
# (1) use your user

# Activate maintenance site
sudo a2dissite 25-https_comptoir-du-libre.org.conf
sudo a2ensite 30-503-MAINTENANCE_https_comptoir-du-libre.org.conf
sudo service apache2 reload

    # on your computer, the following command lines
    # must retrun an HTTP 500 response
    curl -v https://comptoir-du-libre.org/
    curl -v https://comptoir-du-libre.org/notFoundPage
```

## 4) Backup before migration (DB + images files + main directory)

```bash
# (1) user comptoir
sudo su comptoir

# Second backup (DB + images files)
/home/comptoir/Comptoir-srv/bin/COMPTOIR_export_DB_data_AND_images.sh
/home/comptoir/Comptoir-srv/bin/COMPTOIR_export_DB_structure_only.sh
ls -lh /home/comptoir/Comptoir-EXPORT/

# Backup main directory
cd  /home/comptoir/
DATE=$(date +"%Y.%m.%d_%Hh%M")
tar -czvf "Comptoir-srv_BACKUP_${DATE}.tar.gz" Comptoir-srv
```

## 5) Update source code

```bash
# (1) user comptoir
sudo su comptoir

# Update source code
cd /home/comptoir/Comptoir-srv
rm -rvf vendor/
git remote -v
git remote update -p
git checkout origin/main
git log --decorate --oneline --graph --all
```

## 6) Update vendor directory + clean cache

```bash
# (1) user comptoir
sudo su comptoir

# Update vendor directory (see step 2 "Preloading vendor directory")
cd /home/comptoir/Comptoir-srv
rm -rvf vendor/
mv -v ../tmp_migration/Comptoir_tmp_update_vendor/vendor ./

# Clean cache
cd /home/comptoir/Comptoir-srv
bin/cake cache clear_all
```

## 7) Apply migration of the database (new tables, adding fields, ...)

The following changes must be recorded in the [production version tracking](https://gitlab.adullact.net/Adullact-prive/comptoir-prive/Exploitation-Comptoir/-/blob/master/Suivi-versions-PROD.md) file.

```bash
# (1) user comptoir
sudo su comptoir

# Apply migration of the database (new tables, adding fields, ...)
cd /home/comptoir/Comptoir-srv
bin/cake migrations migrate

# Now you can apply additional changes in SQL with pgsl
# ---> nothing to do here for this version
```


## 8) Update config files

The following changes must be recorded in the [production version tracking](https://gitlab.adullact.net/Adullact-prive/comptoir-prive/Exploitation-Comptoir/-/blob/master/Suivi-versions-PROD.md) file.

nothing to do here for this version

```bash
# (1) user comptoir
sudo su comptoir

# Apply additional changes in config files
cd /home/comptoir/Comptoir-srv
vim config/comptoir.php # ---> nothing to do here for this version
vim config/app.php  # ---> nothing to do here for this version
```

## 9) Enable the main website

see:
[www-maintenance-comptoir](https://gitlab.adullact.net/Adullact-prive/comptoir-prive/www-maintenance-comptoir)
(private repository)

```bash
# (1) use your user

# Enable the main website
sudo a2dissite 30-503-MAINTENANCE_https_comptoir-du-libre.org.conf
sudo a2ensite 25-https_comptoir-du-libre.org.conf
sudo service apache2 reload

    # on your computer, the first following command line
    # must return an HTTP 200 response code
    # and the second return an HTTP 404 response code.
    curl -v https://comptoir-du-libre.org/
    curl -v https://comptoir-du-libre.org/notFoundPage
```

## 10) Backup after upgraded

```bash
# (1) user comptoir
sudo su comptoir

# Backup after upgraded (DB + images files)
/home/comptoir/Comptoir-srv/bin/COMPTOIR_export_DB_data_AND_images.sh
/home/comptoir/Comptoir-srv/bin/COMPTOIR_export_DB_structure_only.sh
ls -lh /home/comptoir/Comptoir-EXPORT/
```


## 11) Clean up

```bash
# (1) user comptoir
sudo su comptoir

#  Clean up
cd /home/comptoir/
rm -rvf tmp_migration/
rm -v Comptoir-srv_BACKUP_*.tar.gz
```
