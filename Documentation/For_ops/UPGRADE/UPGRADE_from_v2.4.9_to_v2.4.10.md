# How to upgrade from v2.4.9 to v2.4.10

## WARNING: new recommended upgrade path

The new recommended upgrade path consists in:

* exporting data
* installing Comptoir on a new instance
* adjusting configuration
* importing data
* configure weekly data export

## 1) Export data

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_export_DB_data_AND_images.sh
```

## 2) Save `config/comptoir.php`

```shell
cd /home/comptoir/Comptoir-srv && \
    cp config/comptoir.php config/comptoir.php-$(date +%Y-%m-%d_%kh%M)
```

## 3) Install Comptoir on a new instance

(just do it :) )


## 4) Update `config/comptoir.php`

1. `cd /home/comptoir/Comptoir-srv/config && cp comptoir.default.php comptoir.php`
1. Re-add customized values from the old `comptoir.php` into the new one (like the Piwik stanza or Pick of the month)

## 5) Import data

Use `bin/COMPTOIR_import_DB_data_AND_images.sh` (see help to use it)

## 6) Remove temporary i18n files

```shell
sudo rm -f /home/comptoir/Comptoir-srv/tmp/cache/persistent/*
```

## 7) Configure weekly export

Create file `/etc/cron.d/comptoir` with content:

```
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# Automated backup of application
# Each sunday at 6 o'clock.
0 6     * * 7   comptoir        test -x /home/comptoir/Comptoir-srv/bin/COMPTOIR_export_DB_data_AND_images.sh && /home/comptoir/Comptoir-srv/bin/COMPTOIR_export_DB_data_AND_images.sh
0 7     * * 7   comptoir        find /home/comptoir/Comptoir-EXPORT/SAVE_COMPTOIR_* -daystart -mtime +90 -delete
```
