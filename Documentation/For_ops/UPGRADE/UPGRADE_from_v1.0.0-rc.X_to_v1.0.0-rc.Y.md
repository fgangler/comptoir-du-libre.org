# How to upgrade from v1.0.0-rc.X to v1.0.0-rc.Y (with Y>X)

## 0. Pre-requisites: Have the suitable database

* a database `comptoir`
* a user `comptoir`, owner of the database `comptoir`

Verify it with:

```shell
psql -U comptoir -W comptoir
```

@@@TODO how to verify comptoir has effectively the ownership on the database ???

## 0a. Stop Apache

```
service apache2 stop
```

## 0b. Git clone

As user `comptoir`, do:

```
git clone https://gitlab.adullact.net/Comptoir/Comptoir-srv.git
git clone https://gitlab.adullact.net/Comptoir/Comptoir-web.git
```

## 0c. Reinstall app from new dir

--> See INSTALL.md

* composer install x2
* set perms
* copy app.php from manivelle-srv

## 1. Save DB data *AND* structure

Run the following commands as regular user (johndoe...):

```sh
cd /home/comptoir/Comptoir-srv
bin/COMPTOIR_export_DB_data_AND_images.sh
bin/COMPTOIR_export_DB_structure_only.sh
```

(When prompted, type in the password for the SQL user `comptoir`)

## 2. DB: drop tables

```sh
COMPTOIR_SRV_DIR="/home/comptoir/Comptoir-srv"
bin/COMPTOIR_purge_DB_drop_tables_and_procedures.sh -d "${COMPTOIR_SRV_DIR}"
```

(When prompted, type in the password for the SQL user `comptoir`)

## 3. DB: re-create tables

```sh
COMPTOIR_SRV_DIR="/home/comptoir/Comptoir-srv"
COMPTOIR_TIMESTAMP="2016-10-18-16h10m31" # <---- the timestamp of the export you just made, see in /tmp/
bin/COMPTOIR_create_DB_tables_and_procedures.sh -d "${COMPTOIR_SRV_DIR}"
```

## 4. Add content

As a regular user with **sudo abilities** (johndoe but NOT comptoir)

```sh
COMPTOIR_SRV_DIR="/home/comptoir/Comptoir-srv"
COMPTOIR_TIMESTAMP="2016-10-18-16h10m31"
bin/COMPTOIR_import_DB_data_AND_images.sh -t "${COMPTOIR_TIMESTAMP}" -d "${COMPTOIR_SRV_DIR}"
```

## 5. Migrate DB schema

```shell
cd /home/comptoir/Comptoir-srv
sudo -u comptoir bin/cake migrations migrate
```

## 6. Update source code

```shell
cd /home/comptoir/Comptoir-srv
sudo -u comptoir git pull
```

## 7. Start Apache

```
service apache2 start
```
