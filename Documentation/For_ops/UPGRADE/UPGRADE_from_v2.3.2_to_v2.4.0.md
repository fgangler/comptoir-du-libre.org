# How to upgrade from v2.3.2 to v2.4.0

## Save `config/comptoir.php`

```shell
cd /home/comptoir/Comptoir-srv && \
    cp config/comptoir.php config/comptoir.php-$(date +%Y-%m-%d_%kh%M)
```

## Grab source code

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv && \
    git fetch -p && \
    git checkout "v2.4.0"
```

## Update `config/comptoir.php`

1. `cd /home/comptoir/Comptoir-srv/config && cp comptoir.default.php comptoir.php`
1. Re-add customized values from the old `comptoir.php` into the new one (like the Piwik stanza)

## Remove temporary i18n files

```shell
sudo rm -f /home/comptoir/Comptoir-srv/tmp/cache/persistent/*
```

## Migrate Database

```shell
cd /home/comptoir/Comptoir-srv &&\
composer update &&\
composer require alt3/cakephp-swagger:dev-master &&\
bin/cake migrations migrate -t 20170113155603 &&\
bin/cake migrations migrate -t 20170113162044
```
