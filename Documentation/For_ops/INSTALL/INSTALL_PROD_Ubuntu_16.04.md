# COMPTOIR-SRV install

<!-- =============================================================================================================== -->
<!-- ===== Prerequisites =========================================================================================== -->
<!-- =============================================================================================================== -->

## Prerequisites: Ubuntu packages

As `root` user, do:

```shell
apt-get install \
    libicu55 \
    zlib1g \
    zip \
    unzip \
    git \
    apache2 \
    php \
    php-intl \
    libapache2-mod-php \
    postgresql \
    php-pgsql \
    php-mbstring \
    php-zip
```

php-zip extension used by composer and  must be enabled for php-cli,
but it is not necessary to enable it for php-apache.

## Prerequisites: PHP > timezone + max_upload

As `root` user, do:

```shell
for i in apache2 cli; do
    echo "date.timezone = \"Europe/Paris\"" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
    echo "upload_max_filesize = 2M" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
done
```

## Prerequisites: Install Composer

As `root` user, do:

```shell
php -r "readfile('https://getcomposer.org/installer');" \
    | php -- --install-dir=/usr/local/bin --filename=composer
```

## Prerequisites: Remove useless packages

As `root` user, do:

```shell
apt-get purge --auto-remove \
    zlib1g
```

## Prerequisites: configure locale en_US and fr_FR

As `root` user, do:

```shell
dpkg-reconfigure locales
```

...and select the following locales:

* `fr_FR.UTF8`
* `en_US.UTF8`

To avoid error messages about local variable:

```shell
sudo vi /etc/environment
```

Add a new line:
```
LC_ALL="en_US.UTF-8"
```

## Postgres authentication

As `root` user, in `/etc/postgresql/9.5/main/pg_hba.conf`, replace

```
       local   all             all                                     peer
```

by

```
       local   all             all                                     password
```

and restart Postgres

```shell
sudo service postgresql restart
```

## Create system user

As `root` user, do:

```shell
useradd -d /home/comptoir/ -m -s /bin/bash comptoir
```

<!-- =============================================================================================================== -->
<!-- ===== Installation ============================================================================================ -->
<!-- =============================================================================================================== -->

## Installation Comptoir **SRV**

As `root` user, do:

```shell
su - comptoir
cd /home/comptoir/
git clone https://gitlab.adullact.net/Comptoir/Comptoir-srv.git
cd Comptoir-srv && git checkout v1.0.0
```

## POSTGRESQL Create user

As `root` user, do:

```shell
sudo -u postgres psql
```

Adjust password to your needs:

```postgresql
CREATE USER comptoir WITH PASSWORD 'comptoir';
```

## POSTGRESQL Set .pgpass file

Create the `.pgpass` file for user `comptoir`. As user `comptoir`, do:

```shell
PGPASSFILE=/home/comptoir/.pgpass
touch "${PGPASSFILE}"
chmod 0600 "${PGPASSFILE}"
cat >"${PGPASSFILE}" <<EOF
# hostname:port:database:username:password
localhost:5432:comptoir:comptoir:my-sql-password-for-user-comptoir-TO-BE-CHANGED
EOF
```

Don't forget to adjust password (used for the SQL user `comptoir`).

## POSTGRESQL Create DB and set ownership

As `root` user, do:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_create_DB_database_and_set_ownership.sh -d /home/comptoir/Comptoir-srv
```

Test it:

```shell
psql -U comptoir -W comptoir
```

## POSTGRESQL Create tables and procedures

As any user, do:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_create_DB_tables_and_procedures.sh -d /home/comptoir/Comptoir-srv
```

<!-- =============================================================================================================== -->
<!-- ===== Configuration============================================================================================ -->
<!-- =============================================================================================================== -->

## POSTGRESQL Insert content + binary files

Let say the archived content is:

1. compound of the two following files:
    * `SAVE_COMPTOIR_2017-01-10-18h05m44_Data_only.sql.bz2`
    * `SAVE_COMPTOIR_2017-01-10-18h05m44_Dir_Files.tar.bz2`
1. files which are located in `/home/comptoir/Comptoir-EXPORT/` directory

To import content, run as `comptoir` user:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_import_DB_data_AND_images.sh \
    -d /home/comptoir/Comptoir-srv \
    -t 2017-01-10-18h05m44
```

/!\ Caution:

* Files **must** be in `/home/comptoir/Comptoir-EXPORT/' folder.
* The `-t` parameter is the **exact** timestamp copied/pasted from the filenames.

## Comptoir: Composer install

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv/ \
&& /usr/local/bin/composer install --no-dev \
&& sed -i "s/'debug' => true/'debug' => false/" config/app.php
```

## Comptoir: configuration > app.php

As user `comptoir`, edit `config/app.php` and set the values for:

* Section `EmailTransport`: `password`
* Section `Datasource/Default`: DB credential

## Comptoir: configuration > comptoir.php

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv/ \
&& cp config/comptoir.default.php config/comptoir.php
```

### Comptoir.php >> Piwik

Then edit `config/comptoir.php` and set the values for:

* the Piwik stanza (or any analytics HTML code)

### Comptoir.php >> PickOfTheMonth

The array `Categories => PickOfTheMonth` contain the four ids of softwares you want see in the section `Pick of the month` on the home page.

```
Default ids [   27, // Authentik
                49, // Maarch Courrier
                9,  // Asqatasun
                23  // OpenADS ]
```

## Set UNIX permissions

As `root` user, do

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_import_set_unix_permissions.sh \
    -d /home/comptoir/Comptoir-srv \
```

<!-- =============================================================================================================== -->
<!-- ===== Virtual hosts =========================================================================================== -->
<!-- =============================================================================================================== -->

## APACHE Vhost creation "comptoir-srv"

Let say want to have Comptoir-SRV available at URL `srv.comptoir-du-libre.org`

Create the file `/etc/apache2/sites-available/comptoir-srv.conf` and add the following content:

```apacheconfig
<VirtualHost *:80>
    ServerName srv.comptoir-du-libre.org
    ServerAdmin webmaster@localhost
    DocumentRoot /home/comptoir/Comptoir-srv/webroot/

    ErrorLog ${APACHE_LOG_DIR}/srv.comptoir-du-libre.org.log
    CustomLog ${APACHE_LOG_DIR}/srv.comptoir-du-libre.org.log combined

    <Directory "/home/comptoir/Comptoir-srv/webroot/">
        AllowOverride All
    </Directory>

    <Location />
        Require all granted
    </Location>
</VirtualHost>
```

## APACHE enable mod_rewrite and new vhosts

As `root` user, do:

```shell
a2enmod rewrite
a2ensite srv.comptoir-du-libre.org comptoir-du-libre.org
service apache2 reload
```

### APACHE enable mod_deflate (optional)

Enable gzip compression (mod_deflate) for css, js, html and txt files.
```shell
sudo a2enmod deflate
service apache2 reload
```

### APACHE enable mod_headers (optional)

Allows the addition of HTTP security headers to be put in the vhost.
```shell
sudo a2enmod headers
vim /etc/apache2/sites-available/comptoir-srv.conf
service service apache2 reload
```

```apache
# File : /etc/apache2/sites-available/comptoir-srv.conf

  <IfModule mod_headers.c>

     # Don't allow any pages to be framed - Defends against CSRF
     Header set X-Frame-Options DENY

     # prevent mime based attacks
     Header set X-Content-Type-Options "nosniff"

     # Turn on IE8-IE9 XSS prevention tools
     Header set X-XSS-Protection "1; mode=block"

     # Referrer-Policy HTTP header
     Header set Referrer-Policy "strict-origin-when-cross-origin"

     # Content-Security-Policy (CSP)
     Header set Content-Security-Policy   "default-src 'none'; frame-ancestors 'none'; base-uri 'none'; font-src 'self'; form-action 'self'; img-src 'self' ; script-src 'self' 'unsafe-inline' ; style-src 'self' 'unsafe-inline';  "

     # HTTP Strict Transport Security (HSTS)
     # ---> Only connect to this site via HTTPS for the next six months (recommended)
     Header set Strict-Transport-Security: max-age=15768000

            # ---> for hstspreload.org
            # Header set Strict-Transport-Security "max-age=31536000; includeSubDomains; preload"
  </IfModule>
```



## 5. Test

* Comptoir-SRV with: [http://srv.comptoir-du-libre.org/api/v1/softwares.json](http://srv.comptoir-du-libre.org/api/v1/softwares.json)

## 6. Install HTTPS

... with Let's Encrypt

## 7. Functionnal Testing

### Manually

* Verify a user can recover its lots password

### Automatically

@@@TODO

## 8. Details host-specific

* automatic export
* automatic backup
