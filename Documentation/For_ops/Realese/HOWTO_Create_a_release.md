# Create a release for Comptoir-srv

## Tasks to be done BEFORE releasing

* [ ] CHANGELOG: grab all merge request that were merged since previous release, select the most interesting ones and quickly describe them according to KeepAChangelog.com
* [ ] CODE: change the version in `config/bootstrap.php`
* [ ] DOC: create the upgrade doc in `Documentation/For_ops/UPGRADE`
* [ ] CHANGELOG: as a final step, set the date of the release

## Merge, tag and push

Once the code in `develop`  is ready, merge into `main` like follows:

```sh
git checkout main
MYTAG="vX.Y.Z"
git merge develop --no-verify --no-ff -m "Comptoir $MYTAG"
git tag -a $MYTAG -m "$MYTAG"
git push origin main
git push origin $MYTAG
```

## Tasks to be done just AFTER releasing

### Gitlab:

* [ ] Update release notes of tag in Gitlab
* [ ] Milestone: Set the end-date of milestone as the date of the version tag
* [ ] Milestone: Close this milestone

### Piwik / Matomo

On the production instance, modify `config/comptoir.php`, and add at the end of the file the following:

```html
<!-- Matomo / Embedding the tracker after the load event -->
<script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    function embedTrackingCode(){
        var u="https://<serverStat>/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '<id_website>']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    }
    if (window.addEventListener){ window.addEventListener("load", embedTrackingCode, false); }
    else if (window.attachEvent){ window.attachEvent("onload", embedTrackingCode);           }
    else                        { embedTrackingCode();                                       }
</script>
<!-- End Matomo Code -->
```
the parameters `<serverStat>` and `<id_website>` must be replaced by the production values.


### Pick of the month

Eventually, change the selection of the month in `config/comptoir.php`, entry `PickOfTheMonth`
