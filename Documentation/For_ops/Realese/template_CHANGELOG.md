## Changelog template

## Versioning

We tend to follow the [semantic versioning](http://semver.org/) recommendations.

## Template

```
## 2.x.y     (unreleased)

### Added

### Changed

### Fixed

### Security

```
