# API

This **API** provides list of all **free software**
on [comptoir-du-libre.org](https://comptoir-du-libre.org/fr/) website.

- [CHANGELOG](./CHANGELOG.md) of this API
- **License** of data returned by this API:
  > [CC0-1.0 - Creative Commons Zero v1.0 Universal](https://spdx.org/licenses/CC0-1.0.html)

## Documentation

For each software, this **API** provides:
- Web site,
- Repository,
- Software license,
- _Comptoir-du-libre.org_ ID and i18n URLs,
- External resources:
  - **Wikipedia** (software i18n slugs and URLs),
  - **WikiData** (software ID and URL),
  - **FramaLibre** (software slug and URL),
  - **CNLL** (software ID and URL),
  - **SILL** (software ID and i18n URLs),
- Providers:
  - Provider name,
  - Provider website,
  - _Comptoir-du-libre.org_ ID and URLs,


Example:
```json
{
  "api_documentation": {
    "version": "1.2.1",
    "changelog": "https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/tree/develop/Documentation/API/CHANGELOG.md",
    "documentation": "https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/tree/develop/Documentation/API/",
    "deprecated": {}
  },
  "data_documentation": {},
  "data_statistics": {},
  "data_date": "2022-12-07T16:19:40+00:00",
  "softwares": [
    {
      "id": 72,
      "name": "7-zip",
      "url": "https://comptoir-du-libre.org/fr/softwares/72",
      "i18n_url": {
        "fr": "https://comptoir-du-libre.org/fr/softwares/72",
        "en": "https://comptoir-du-libre.org/en/softwares/72"
      },
      "license": "GNU LGPL",
      "created": "2017-02-07T22:46:57+00:00",
      "modified": "2017-02-07T22:46:58+00:00",
      "external_resources": {
        "website": "http://www.7-zip.org/",
        "repository": "https://github.com/pornel/7z.git",
        "framalibre": {},
        "sill":  {},
        "wikidata":  {},
        "wikipedia": {}
      },
      "provider": {}
```
