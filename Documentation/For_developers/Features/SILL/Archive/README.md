# SILL (Socle Interministériel de Logiciels Libres)

* https://sill.etalab.gouv.fr/fr/software
  * src: https://github.com/etalab/sill.etalab.gouv.fr
  * data: https://github.com/DISIC/sill
  * consolidated data: https://etalab.github.io/sill-data/
- SILL 2020
  - [fichier CSV](https://github.com/DISIC/sill/blob/master/2020/sill-2020.csv)
  - [fichier JSON](https://etalab.github.io/sill-data/sill.json) généré à partir du fichier CSV + extraction de données de wikidata

------------------------

- Tag `SILL-2020` ---> [ID 258](https://comptoir-du-libre.org/fr/tags/258/software)
- Tag `SILL-2019` ---> [ID 137](https://comptoir-du-libre.org/fr/tags/137/software)
- Tag `SILL-2018` ---> [ID 27](https://comptoir-du-libre.org/fr/tags/27/software)
- Tag `SILL-2017` ---> [ID 29](https://comptoir-du-libre.org/fr/tags/29/software)


## Update `SILL-2020`  tag
The `SILL-2020`  tag must exist beforehand in comptoir app.
```php
cd 2020
php prepare_SQL.php
       # load sill-2020.csv from https://github.com/DISIC/sill
       # generate a SQL file to update the "SILL-2020" tag
```

```sql
psql <base>

    -- the "SILL-2020" tag must exist beforehand in comptoir app
    DELETE FROM softwares_tags WHERE tag_id = 258
    INSERT INTO softwares_tags (software_id, tag_id) VALUES (33, 258);
    ...
    \q

rm -v .psql_history
```
