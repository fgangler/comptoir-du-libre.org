<?php

# the "SILL-<year>" tags must exist beforehand in comptoir app
$tagIds = [];
$tagIds[2017] = 29;   // Tag "SILL-2017" --> https://comptoir-du-libre.org/fr/tags/29/software
$tagIds[2018] = 27;   // Tag "SILL-2018" --> https://comptoir-du-libre.org/fr/tags/27/software
$tagIds[2019] = 137;  // Tag "SILL-2019" --> https://comptoir-du-libre.org/fr/tags/137/software
$tagIds[2020] = 258;  // Tag "SILL-2020" --> https://comptoir-du-libre.org/fr/tags/258/software
$tagIds[2021] = 474;  // Tag "SILL-2021" --> https://comptoir-du-libre.org/fr/tags/474/software
$tagIds[2022] = 578;  // Tag "SILL-2022" --> https://comptoir-du-libre.org/fr/tags/578/software


# data source
$url = "https://git.sr.ht/~etalab/sill/blob/master/sill.csv";
    # Legacy data source
    # $url = "https://raw.githubusercontent.com/DISIC/sill/master/2020/sill-2020.csv";


/////////////////////////////////////////////////////////////////////////

// get the source file
$fileName = 'sill.csv';
$dataSrc = file_get_contents($url);
file_put_contents($fileName, $dataSrc);  /* [0] => ID
                                            [1] => nom
                                            [2] => fonction
                                            [3] => poste_agent
                                            [4] => annees
                                            [5] => statut
                                            [6] => parent
                                            [7] => public
                                            [8] => support
                                            [9] => similaire-a
                                            [10] => wikidata
                                            [11] => comptoir-du-libre
                                            [12] => licence
                                            ...
                                            [21] => version_max       */

// column numbers of the csv file
$numberOfFileFields = 22;
$colId=0;
$colName=1;
$colYears = 4;
$colWikidata = 10;
$colComptoir = 11;

//  file processing
$row = 1;
$years = [];
$noIds = [];
if (($handle = fopen($fileName, "r")) !== false) {
    while (($data = fgetcsv($handle, 1000, ",")) !== false) {
        $num = count($data);
        echo "\nline $row:"; // DEBUG print_r($data); exit();

        // check the number of fields in the line
        if ($num !== $numberOfFileFields) {
            throw new Exception("the number of fields ($num) on this line is invalid ($numberOfFileFields).");
        }

        // check the 1st line of the file
        if ($row === 1) {
            if (trim($data[$colYears]) !== 'annees') {
                throw new Exception("column $colYears is not 'annees'");
            }
            if (trim($data[$colWikidata]) !== 'wikidata') {
                throw new Exception("column $colWikidata is not 'wikidata'");
            }
            if (trim($data[$colComptoir]) !== 'comptoir-du-libre') {
                throw new Exception("column $colComptoir is not 'comptoir-du-libre'");
            }
        }

        // line processing
        if ($row !== 1 && !empty($data[$colYears]) && !empty($data[$colComptoir])) {
            $softwareId   = $data[$colId];
            $softwareName = $data[$colName];
            $idComptoir = $data[$colComptoir];
            $idWikidata = $data[$colWikidata];
            $lineYears = explode(';', $data[$colYears]);
            foreach ($lineYears as $year) {
                $year = trim($year);
                if ($year !== '') {
                    $year = (int) $year;
                    $years[$year][$idComptoir] = $idWikidata;
                }
            }
            echo " idSILL->$softwareId / idComptoir->$idComptoir / idWikidata->$idWikidata -----> $softwareName";
        } else {
            $softwareId   = $data[$colId];
            $softwareName = $data[$colName];
            echo " -----> $softwareName";
            $noIds[$row] = $data;
        }
        $row++;
    }
    fclose($handle);
}
$row--;

// Created SQL update files
foreach ($years as $year => $Ids) {
    ksort($Ids);
    if (isset($tagIds[$year])) {
        $tagId = $tagIds[$year];
        $sql = "DELETE FROM softwares_tags WHERE tag_id = '$tagId'; \n";
        foreach ($Ids as $idComptoir => $idWikidata) {
            $idComptoir = (int) $idComptoir;
            $sql .= "INSERT INTO softwares_tags (software_id, tag_id) VALUES ('$idComptoir', '$tagId'); \n";
        }
        file_put_contents("SQL_maj_tagSILL_$year.sql", $sql);
    }
}


echo "\n\n";
echo "\n------> ". ($row - 1)   . " src";
echo "\n------> ". (count($noIds) - 1). " NO id comptoir";
foreach ($years as $year => $Ids) {
    echo "\n------> ". count($Ids) . " software for SILL-$year";
    // print_r($Ids);
}
echo "\n\n";

$fp = fopen("sill.csv_NO.idComptoir.csv", 'w');
fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) )); // UTF-8 BOM
foreach ($noIds as $fields) {
    fputcsv($fp, $fields);
}
fclose($fp);
