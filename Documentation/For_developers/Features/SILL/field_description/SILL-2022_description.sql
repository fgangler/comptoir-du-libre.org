-- Tag SILL-2022 ---> [ID 578]
UPDATE public.tags
SET name = 'SILL',
    description_i18n_fr = '<p>
                     Co-construit par des communautés d’agents publics informaticiens de
                     l’État (les référents « SILL »), le <a href="https://code.gouv.fr/sill">socle interministériel des
                     logiciels libres</a> est le catalogue de référence des logiciels libres
                     recommandés par l’État.  Tous ces logiciels sont utilisés dans un
                     cadre de production dans au moins l’une des administrations
                     contributrices.
                 </p>
                 <p>
                     Son périmètre - ne couvrant pas la totalité du système d’information
                     de l’État - classe les logiciels par thématiques : poste de travail,
                     gestion de parc, exploitation de serveurs et de bases de données et
                     environnements de développement.
                 </p>
                 <p>
                     Sous forme de site web,
                     il permet d’identifier rapidement le
                     logiciel et la version minimale recommandée (toute administration
                     est libre d’utiliser une version plus récente).  Il est également
                     possible de consulter l’état d’avancement du statut d’un logiciel,
                     selon qu’il est en cours d’observation ou recommandé.
                 </p>'
WHERE id = 578;
