UPDATE softwares SET framalibre = NULL, wikipedia_en = NULL, wikipedia_fr = NULL;
-- Comtpoir: 9 -> Asqatasun /  WidiData: Q24026504 ---> FR Asqatasun | EN Asqatasun
UPDATE softwares SET framalibre = 'asqatasun' WHERE id = 9;

-- Comtpoir: 11 -> XWiki /  WidiData: Q526699 ---> FR XWiki | EN XWiki
UPDATE softwares SET wikipedia_fr = 'XWiki' WHERE id = 11;
UPDATE softwares SET wikipedia_en = 'XWiki' WHERE id = 11;
UPDATE softwares SET framalibre = 'xwiki' WHERE id = 11;

-- Comtpoir: 22 -> XiVO /  WidiData: Q3570950 ---> FR XiVO | EN XiVO
UPDATE softwares SET framalibre = 'xivo' WHERE id = 22;

-- Comtpoir: 25 -> BlueMind /  WidiData: Q2907192 ---> FR BlueMind | EN BlueMind
UPDATE softwares SET wikipedia_fr = 'BlueMind_(logiciel)' WHERE id = 25;

-- Comtpoir: 33 -> LibreOffice /  WidiData: Q10135 ---> FR LibreOffice | EN LibreOffice
UPDATE softwares SET wikipedia_fr = 'LibreOffice' WHERE id = 33;
UPDATE softwares SET wikipedia_en = 'LibreOffice' WHERE id = 33;

-- Comtpoir: 35 -> GLPI /  WidiData: Q3104570 ---> FR Gestion libre de parc informatique | EN GLPi
UPDATE softwares SET wikipedia_fr = 'Gestionnaire_Libre_de_Parc_Informatique' WHERE id = 35;
UPDATE softwares SET wikipedia_en = 'GLPi' WHERE id = 35;
UPDATE softwares SET framalibre = 'glpi' WHERE id = 35;

-- Comtpoir: 36 -> SPIP /  WidiData: Q1536426 ---> FR Spip | EN SPIP
UPDATE softwares SET wikipedia_fr = 'SPIP_(logiciel)' WHERE id = 36;
UPDATE softwares SET wikipedia_en = 'SPIP' WHERE id = 36;
UPDATE softwares SET framalibre = 'spip' WHERE id = 36;

-- Comtpoir: 38 -> WordPress /  WidiData: Q13166 ---> FR WordPress | EN WordPress
UPDATE softwares SET wikipedia_fr = 'WordPress' WHERE id = 38;
UPDATE softwares SET wikipedia_en = 'WordPress' WHERE id = 38;
UPDATE softwares SET framalibre = 'wordpress' WHERE id = 38;

-- Comtpoir: 42 -> LimeSurvey /  WidiData: Q1514978 ---> FR LimeSurvey | EN LimeSurvey
UPDATE softwares SET wikipedia_fr = 'LimeSurvey' WHERE id = 42;
UPDATE softwares SET wikipedia_en = 'LimeSurvey' WHERE id = 42;

-- Comtpoir: 47 -> Dolibarr ERP CRM /  WidiData: Q2076540 ---> FR Dolibarr | EN Dolibarr
UPDATE softwares SET wikipedia_fr = 'Dolibarr' WHERE id = 47;
UPDATE softwares SET wikipedia_en = 'Dolibarr' WHERE id = 47;
UPDATE softwares SET framalibre = 'dolibarr-erp-crm' WHERE id = 47;

-- Comtpoir: 50 -> KeePass /  WidiData: Q762660 ---> FR KeePass | EN KeePass
UPDATE softwares SET wikipedia_fr = 'KeePass' WHERE id = 50;
UPDATE softwares SET wikipedia_en = 'KeePass' WHERE id = 50;
UPDATE softwares SET framalibre = 'keepass' WHERE id = 50;

-- Comtpoir: 51 -> VeraCrypt /  WidiData: Q19911889 ---> FR VeraCrypt | EN VeraCrypt
UPDATE softwares SET wikipedia_fr = 'VeraCrypt' WHERE id = 51;
UPDATE softwares SET wikipedia_en = 'VeraCrypt' WHERE id = 51;
UPDATE softwares SET framalibre = 'veracrypt' WHERE id = 51;

-- Comtpoir: 52 -> FreeFileSync /  WidiData: Q18150468 ---> FR FreeFileSync | EN FreeFileSync
UPDATE softwares SET wikipedia_fr = 'FreeFileSync' WHERE id = 52;
UPDATE softwares SET wikipedia_en = 'FreeFileSync' WHERE id = 52;
UPDATE softwares SET framalibre = 'freefilesync' WHERE id = 52;

-- Comtpoir: 55 -> Notepad++ /  WidiData: Q2033 ---> FR Notepad++ | EN Notepad++
UPDATE softwares SET wikipedia_fr = 'Notepad%2B%2B' WHERE id = 55;
UPDATE softwares SET wikipedia_en = 'Notepad%2B%2B' WHERE id = 55;
UPDATE softwares SET framalibre = 'notepad++' WHERE id = 55;

-- Comtpoir: 56 -> Greenshot /  WidiData: Q1544791 ---> FR Greenshot | EN Greenshot
UPDATE softwares SET wikipedia_fr = 'Greenshot' WHERE id = 56;
UPDATE softwares SET wikipedia_en = 'Greenshot' WHERE id = 56;

-- Comtpoir: 57 -> Sumatra PDF /  WidiData: Q479808 ---> FR Sumakou | EN Sumatra PDF
UPDATE softwares SET wikipedia_fr = 'Sumatra_PDF' WHERE id = 57;
UPDATE softwares SET wikipedia_en = 'Sumatra_PDF' WHERE id = 57;
UPDATE softwares SET framalibre = 'sumatra-pdf' WHERE id = 57;

-- Comtpoir: 58 -> BlueGriffon /  WidiData: Q885580 ---> FR BlueGriffon | EN BlueGriffon
UPDATE softwares SET wikipedia_fr = 'BlueGriffon' WHERE id = 58;
UPDATE softwares SET wikipedia_en = 'BlueGriffon' WHERE id = 58;
UPDATE softwares SET framalibre = 'bluegriffon' WHERE id = 58;

-- Comtpoir: 59 -> ProjectLibre /  WidiData: Q580985 ---> FR ProjectLibre | EN ProjectLibre
UPDATE softwares SET wikipedia_fr = 'ProjectLibre' WHERE id = 59;
UPDATE softwares SET wikipedia_en = 'ProjectLibre' WHERE id = 59;
UPDATE softwares SET framalibre = 'projectlibre' WHERE id = 59;

-- Comtpoir: 60 -> QGIS /  WidiData: Q1329181 ---> FR QGIS | EN QGIS
UPDATE softwares SET wikipedia_fr = 'QGIS' WHERE id = 60;
UPDATE softwares SET wikipedia_en = 'QGIS' WHERE id = 60;
UPDATE softwares SET framalibre = 'qgis' WHERE id = 60;

-- Comtpoir: 61 -> Freeplane /  WidiData: Q3028675 ---> FR Freeplane | EN Freeplane
UPDATE softwares SET wikipedia_fr = 'Freeplane' WHERE id = 61;
UPDATE softwares SET wikipedia_en = 'Freeplane' WHERE id = 61;
UPDATE softwares SET framalibre = 'freeplane' WHERE id = 61;

-- Comtpoir: 62 -> VLC /  WidiData: Q171477 ---> FR VLC media player | EN VLC media player
UPDATE softwares SET wikipedia_fr = 'VLC_media_player' WHERE id = 62;
UPDATE softwares SET wikipedia_en = 'VLC_media_player' WHERE id = 62;
UPDATE softwares SET framalibre = 'vlc' WHERE id = 62;

-- Comtpoir: 63 -> NVDA /  WidiData: Q1116614 ---> FR NonVisual Desktop Access | EN NonVisual Desktop Access
UPDATE softwares SET wikipedia_fr = 'NonVisual_Desktop_Access' WHERE id = 63;
UPDATE softwares SET wikipedia_en = 'NonVisual_Desktop_Access' WHERE id = 63;

-- Comtpoir: 64 -> FileZilla /  WidiData: Q300662 ---> FR FileZilla | EN FileZilla
UPDATE softwares SET wikipedia_fr = 'FileZilla' WHERE id = 64;
UPDATE softwares SET wikipedia_en = 'FileZilla' WHERE id = 64;
UPDATE softwares SET framalibre = 'filezilla' WHERE id = 64;

-- Comtpoir: 67 -> GIMP /  WidiData: Q8038 ---> FR GIMP | EN GIMP
UPDATE softwares SET wikipedia_fr = 'GIMP' WHERE id = 67;
UPDATE softwares SET wikipedia_en = 'GIMP' WHERE id = 67;
UPDATE softwares SET framalibre = 'gimp' WHERE id = 67;

-- Comtpoir: 68 -> Inkscape /  WidiData: Q8041 ---> FR Inkscape | EN Inkscape
UPDATE softwares SET wikipedia_fr = 'Inkscape' WHERE id = 68;
UPDATE softwares SET wikipedia_en = 'Inkscape' WHERE id = 68;
UPDATE softwares SET framalibre = 'inkscape' WHERE id = 68;

-- Comtpoir: 70 -> Avidemux /  WidiData: Q790931 ---> FR Avidemux | EN Avidemux
UPDATE softwares SET wikipedia_fr = 'Avidemux' WHERE id = 70;
UPDATE softwares SET wikipedia_en = 'Avidemux' WHERE id = 70;

-- Comtpoir: 72 -> 7-zip /  WidiData: Q215051 ---> FR 7-Zip | EN 7-Zip
UPDATE softwares SET wikipedia_fr = '7-Zip' WHERE id = 72;
UPDATE softwares SET wikipedia_en = '7-Zip' WHERE id = 72;
UPDATE softwares SET framalibre = '7-zip' WHERE id = 72;

-- Comtpoir: 74 -> DVDstyler /  WidiData: Q4117800 ---> FR DVDStyler | EN DVDStyler
UPDATE softwares SET wikipedia_en = 'DVDStyler' WHERE id = 74;

-- Comtpoir: 76 -> Scribus /  WidiData: Q8295 ---> FR Scribus | EN Scribus
UPDATE softwares SET wikipedia_fr = 'Scribus' WHERE id = 76;
UPDATE softwares SET wikipedia_en = 'Scribus' WHERE id = 76;
UPDATE softwares SET framalibre = 'scribus' WHERE id = 76;

-- Comtpoir: 79 -> Grammalecte /  WidiData: Q17629243 ---> FR Grammalecte | EN Grammalecte
UPDATE softwares SET wikipedia_fr = 'Grammalecte' WHERE id = 79;

-- Comtpoir: 80 -> Thunderbird /  WidiData: Q483604 ---> FR Mozilla Thunderbird | EN Mozilla Thunderbird
UPDATE softwares SET wikipedia_fr = 'Mozilla_Thunderbird' WHERE id = 80;
UPDATE softwares SET wikipedia_en = 'Mozilla_Thunderbird' WHERE id = 80;
UPDATE softwares SET framalibre = 'thunderbird' WHERE id = 80;

-- Comtpoir: 82 -> Firefox ESR /  WidiData: Q698 ---> FR Mozilla Firefox | EN Mozilla Firefox
UPDATE softwares SET wikipedia_fr = 'Mozilla_Firefox' WHERE id = 82;
UPDATE softwares SET wikipedia_en = 'Firefox' WHERE id = 82;
UPDATE softwares SET framalibre = 'firefox' WHERE id = 82;

-- Comtpoir: 83 -> OCS Inventory /  WidiData: Q1135744 ---> FR OCS Inventory | EN OCS Inventory
UPDATE softwares SET wikipedia_fr = 'OCS_Inventory' WHERE id = 83;
UPDATE softwares SET wikipedia_en = 'OCS_Inventory' WHERE id = 83;

-- Comtpoir: 87 -> TYPO3 /  WidiData: Q618512 ---> FR TYPO3 | EN TYPO3
UPDATE softwares SET wikipedia_fr = 'TYPO3' WHERE id = 87;
UPDATE softwares SET wikipedia_en = 'TYPO3' WHERE id = 87;

-- Comtpoir: 98 -> OPENLDAP /  WidiData: Q682918 ---> FR OpenLDAP | EN OpenLDAP
UPDATE softwares SET wikipedia_fr = 'OpenLDAP' WHERE id = 98;
UPDATE softwares SET wikipedia_en = 'OpenLDAP' WHERE id = 98;

-- Comtpoir: 101 -> LemonLDAP NG /  WidiData: Q3229380 ---> FR LemonLDAP::NG | EN LemonLDAP::NG
UPDATE softwares SET wikipedia_fr = 'LemonLDAP::NG' WHERE id = 101;
UPDATE softwares SET framalibre = 'lemonldapng' WHERE id = 101;

-- Comtpoir: 105 -> Pentaho /  WidiData: Q644841 ---> FR Pentaho | EN Pentaho
UPDATE softwares SET wikipedia_en = 'Pentaho' WHERE id = 105;

-- Comtpoir: 109 -> Apache JMeter™ /  WidiData: Q616885 ---> FR JMeter | EN Apache JMeter
UPDATE softwares SET wikipedia_fr = 'Apache_JMeter' WHERE id = 109;
UPDATE softwares SET wikipedia_en = 'Apache_JMeter' WHERE id = 109;

-- Comtpoir: 110 -> Zimbra /  WidiData: Q203485 ---> FR Zimbra | EN Zimbra
UPDATE softwares SET wikipedia_fr = 'Zimbra' WHERE id = 110;
UPDATE softwares SET wikipedia_en = 'Zimbra' WHERE id = 110;

-- Comtpoir: 112 -> Roundcube /  WidiData: Q1769269 ---> FR Roundcube | EN Roundcube
UPDATE softwares SET wikipedia_fr = 'Roundcube' WHERE id = 112;
UPDATE softwares SET wikipedia_en = 'Roundcube' WHERE id = 112;

-- Comtpoir: 113 -> Proxmox Virtual Environment PVE /  WidiData: Q344376 ---> FR Proxmox VE | EN Proxmox Virtual Environment
UPDATE softwares SET wikipedia_fr = 'Proxmox_VE' WHERE id = 113;
UPDATE softwares SET wikipedia_en = 'Proxmox_Virtual_Environment' WHERE id = 113;
UPDATE softwares SET framalibre = 'proxmox-virtual-environnement' WHERE id = 113;

-- Comtpoir: 117 -> NextCloud /  WidiData: Q25874683 ---> FR Nextcloud | EN Nextcloud
UPDATE softwares SET wikipedia_fr = 'Nextcloud' WHERE id = 117;
UPDATE softwares SET wikipedia_en = 'Nextcloud' WHERE id = 117;
UPDATE softwares SET framalibre = 'nextcloud' WHERE id = 117;

-- Comtpoir: 121 -> Zim /  WidiData: Q203374 ---> FR Zim | EN Zim Desktop Wiki
UPDATE softwares SET wikipedia_fr = 'Zim-wiki' WHERE id = 121;
UPDATE softwares SET wikipedia_en = 'Zim_(software)' WHERE id = 121;
UPDATE softwares SET framalibre = 'zim' WHERE id = 121;

-- Comtpoir: 122 -> MediaWiki /  WidiData: Q83 ---> FR MediaWiki | EN MediaWiki
UPDATE softwares SET wikipedia_fr = 'MediaWiki' WHERE id = 122;
UPDATE softwares SET wikipedia_en = 'MediaWiki' WHERE id = 122;
UPDATE softwares SET framalibre = 'mediawiki' WHERE id = 122;

-- Comtpoir: 123 -> PostgreSQL /  WidiData: Q192490 ---> FR PostgreSQL | EN PostgreSQL
UPDATE softwares SET wikipedia_fr = 'PostgreSQL' WHERE id = 123;
UPDATE softwares SET wikipedia_en = 'PostgreSQL' WHERE id = 123;
UPDATE softwares SET framalibre = 'postgresql' WHERE id = 123;

-- Comtpoir: 125 -> GeoNature /  WidiData: Q109038131 ---> FR GeoNature | EN GeoNature
UPDATE softwares SET wikipedia_fr = 'GeoNature' WHERE id = 125;

-- Comtpoir: 132 -> Blender /  WidiData: Q173136 ---> FR Blender | EN Blender
UPDATE softwares SET wikipedia_fr = 'Blender' WHERE id = 132;
UPDATE softwares SET wikipedia_en = 'Blender_(software)' WHERE id = 132;
UPDATE softwares SET framalibre = 'blender' WHERE id = 132;

-- Comtpoir: 138 -> FOG Project /  WidiData: Q111132714 ---> FR FOG | EN 
UPDATE softwares SET wikipedia_fr = 'FOG_(logiciel)' WHERE id = 138;

-- Comtpoir: 139 -> SOGo /  WidiData: Q1523357 ---> FR SOGo | EN SOGo
UPDATE softwares SET wikipedia_fr = 'SOGo' WHERE id = 139;
UPDATE softwares SET wikipedia_en = 'SOGo' WHERE id = 139;

-- Comtpoir: 140 -> PeerTube /  WidiData: Q50938515 ---> FR PeerTube | EN PeerTube
UPDATE softwares SET wikipedia_fr = 'PeerTube' WHERE id = 140;
UPDATE softwares SET wikipedia_en = 'PeerTube' WHERE id = 140;
UPDATE softwares SET framalibre = 'peertube' WHERE id = 140;

-- Comtpoir: 142 -> samba /  WidiData: Q1830735 ---> FR Samba | EN Samba
UPDATE softwares SET wikipedia_fr = 'Samba_(informatique)' WHERE id = 142;
UPDATE softwares SET wikipedia_en = 'Samba_(software)' WHERE id = 142;

-- Comtpoir: 148 -> Kanboard /  WidiData: Q106199750 ---> FR  | EN Kanboard
UPDATE softwares SET wikipedia_en = 'Kanboard' WHERE id = 148;

-- Comtpoir: 159 -> Squid /  WidiData: Q841783 ---> FR Squid | EN Squid cache
UPDATE softwares SET wikipedia_fr = 'Squid_(logiciel)' WHERE id = 159;
UPDATE softwares SET wikipedia_en = 'Squid_(software)' WHERE id = 159;

-- Comtpoir: 169 -> PDFSam basic /  WidiData: Q3898451 ---> FR PDF Split and Merge | EN PDF Split and Merge
UPDATE softwares SET wikipedia_fr = 'PDF_Split_and_Merge' WHERE id = 169;
UPDATE softwares SET wikipedia_en = 'PDF_Split_and_Merge' WHERE id = 169;
UPDATE softwares SET framalibre = 'pdf-sam' WHERE id = 169;

-- Comtpoir: 170 -> Eclipse /  WidiData: Q82268 ---> FR Eclipse | EN Eclipse
UPDATE softwares SET wikipedia_fr = 'Eclipse_(projet)' WHERE id = 170;
UPDATE softwares SET wikipedia_en = 'Eclipse_(software)' WHERE id = 170;

-- Comtpoir: 173 -> OpenJDK /  WidiData: Q1461054 ---> FR OpenJDK | EN OpenJDK
UPDATE softwares SET wikipedia_fr = 'OpenJDK' WHERE id = 173;
UPDATE softwares SET wikipedia_en = 'OpenJDK' WHERE id = 173;

-- Comtpoir: 175 -> Git /  WidiData: Q186055 ---> FR Git | EN Git
UPDATE softwares SET wikipedia_fr = 'Git' WHERE id = 175;
UPDATE softwares SET wikipedia_en = 'Git' WHERE id = 175;

-- Comtpoir: 176 -> Maven /  WidiData: Q139941 ---> FR Apache Maven | EN Apache Maven
UPDATE softwares SET wikipedia_fr = 'Apache_Maven' WHERE id = 176;
UPDATE softwares SET wikipedia_en = 'Apache_Maven' WHERE id = 176;

-- Comtpoir: 179 -> NPM /  WidiData: Q7067518 ---> FR npm | EN npm
UPDATE softwares SET wikipedia_fr = 'Npm' WHERE id = 179;
UPDATE softwares SET wikipedia_en = 'Npm' WHERE id = 179;

-- Comtpoir: 181 -> SonarQube /  WidiData: Q541691 ---> FR SonarQube | EN SonarQube
UPDATE softwares SET wikipedia_fr = 'SonarQube' WHERE id = 181;
UPDATE softwares SET wikipedia_en = 'SonarQube' WHERE id = 181;

-- Comtpoir: 182 -> JaCoCo /  WidiData: Q6109486 ---> FR JaCoCo | EN JaCoCo
UPDATE softwares SET wikipedia_en = 'Java_code_coverage_tools' WHERE id = 182;

-- Comtpoir: 183 -> JUnit /  WidiData: Q847675 ---> FR JUnit | EN JUnit
UPDATE softwares SET wikipedia_fr = 'JUnit' WHERE id = 183;
UPDATE softwares SET wikipedia_en = 'JUnit' WHERE id = 183;

-- Comtpoir: 186 -> SoapUI /  WidiData: Q2296314 ---> FR SoapUI | EN SoapUI
UPDATE softwares SET wikipedia_fr = 'SoapUI' WHERE id = 186;
UPDATE softwares SET wikipedia_en = 'SoapUI' WHERE id = 186;

-- Comtpoir: 187 -> Jenkins /  WidiData: Q7491312 ---> FR Jenkins | EN Jenkins
UPDATE softwares SET wikipedia_fr = 'Jenkins_(logiciel)' WHERE id = 187;
UPDATE softwares SET wikipedia_en = 'Jenkins_(software)' WHERE id = 187;

-- Comtpoir: 191 -> TestLink /  WidiData: Q248384 ---> FR TestLink | EN TestLink
UPDATE softwares SET wikipedia_en = 'TestLink' WHERE id = 191;

-- Comtpoir: 193 -> Chromium /  WidiData: Q48524 ---> FR Chromium | EN Chromium
UPDATE softwares SET wikipedia_fr = 'Chromium' WHERE id = 193;
UPDATE softwares SET wikipedia_en = 'Chromium_(web_browser)' WHERE id = 193;
UPDATE softwares SET framalibre = 'chromium' WHERE id = 193;

-- Comtpoir: 194 -> H2 /  WidiData: Q1562670 ---> FR H2 | EN H2
UPDATE softwares SET wikipedia_fr = 'H2_(base_de_donn%C3%A9es)' WHERE id = 194;
UPDATE softwares SET wikipedia_en = 'H2_(database)' WHERE id = 194;

-- Comtpoir: 197 -> Redmine /  WidiData: Q1149365 ---> FR Redmine | EN Redmine
UPDATE softwares SET wikipedia_fr = 'Redmine' WHERE id = 197;
UPDATE softwares SET wikipedia_en = 'Redmine' WHERE id = 197;

-- Comtpoir: 198 -> Gitlab /  WidiData: Q16639197 ---> FR GitLab | EN GitLab
UPDATE softwares SET wikipedia_fr = 'GitLab' WHERE id = 198;
UPDATE softwares SET wikipedia_en = 'GitLab' WHERE id = 198;
UPDATE softwares SET framalibre = 'gitlab' WHERE id = 198;

-- Comtpoir: 201 -> Elastic Search /  WidiData: Q3050461 ---> FR Elasticsearch | EN Elasticsearch
UPDATE softwares SET wikipedia_fr = 'Elasticsearch' WHERE id = 201;
UPDATE softwares SET wikipedia_en = 'Elasticsearch' WHERE id = 201;

-- Comtpoir: 202 -> Kibana /  WidiData: Q21156460 ---> FR Kibana | EN Kibana
UPDATE softwares SET wikipedia_fr = 'Kibana' WHERE id = 202;
UPDATE softwares SET wikipedia_en = 'Kibana' WHERE id = 202;

-- Comtpoir: 203 -> LogStash /  WidiData: Q17620247 ---> FR Logstash | EN Logstash
UPDATE softwares SET wikipedia_fr = 'Logstash' WHERE id = 203;

-- Comtpoir: 204 -> Apache SolR /  WidiData: Q2858103 ---> FR Apache Solr | EN Apache Solr
UPDATE softwares SET wikipedia_fr = 'Apache_Solr' WHERE id = 204;
UPDATE softwares SET wikipedia_en = 'Apache_Solr' WHERE id = 204;

-- Comtpoir: 205 -> Tomcat /  WidiData: Q507430 ---> FR Apache Tomcat | EN Apache Tomcat
UPDATE softwares SET wikipedia_fr = 'Apache_Tomcat' WHERE id = 205;
UPDATE softwares SET wikipedia_en = 'Apache_Tomcat' WHERE id = 205;

-- Comtpoir: 206 -> MariaDB /  WidiData: Q787177 ---> FR MariaDB | EN MariaDB
UPDATE softwares SET wikipedia_fr = 'MariaDB' WHERE id = 206;
UPDATE softwares SET wikipedia_en = 'MariaDB' WHERE id = 206;
UPDATE softwares SET framalibre = 'mariadb' WHERE id = 206;

-- Comtpoir: 207 -> PostGIS /  WidiData: Q1569955 ---> FR PostGIS | EN PostGIS
UPDATE softwares SET wikipedia_fr = 'PostGIS' WHERE id = 207;
UPDATE softwares SET wikipedia_en = 'PostGIS' WHERE id = 207;

-- Comtpoir: 210 -> Centreon /  WidiData: Q2946121 ---> FR Centreon | EN Centreon
UPDATE softwares SET wikipedia_fr = 'Centreon' WHERE id = 210;

-- Comtpoir: 211 -> ClamAV /  WidiData: Q852000 ---> FR ClamAV | EN Clam AntiVirus
UPDATE softwares SET wikipedia_fr = 'ClamAV' WHERE id = 211;
UPDATE softwares SET wikipedia_en = 'Clam_AntiVirus' WHERE id = 211;

-- Comtpoir: 212 -> Apache /  WidiData: Q11354 ---> FR Apache HTTP Server | EN Apache HTTP Server
UPDATE softwares SET wikipedia_fr = 'Apache_HTTP_Server' WHERE id = 212;
UPDATE softwares SET wikipedia_en = 'Apache_HTTP_Server' WHERE id = 212;

-- Comtpoir: 213 -> Nginx /  WidiData: Q306144 ---> FR NGINX | EN nginx
UPDATE softwares SET wikipedia_fr = 'NGINX' WHERE id = 213;
UPDATE softwares SET wikipedia_en = 'Nginx' WHERE id = 213;

-- Comtpoir: 214 -> Varnish /  WidiData: Q1602447 ---> FR Varnish | EN Varnish
UPDATE softwares SET wikipedia_fr = 'Varnish' WHERE id = 214;
UPDATE softwares SET wikipedia_en = 'Varnish_(software)' WHERE id = 214;

-- Comtpoir: 216 -> PostFix /  WidiData: Q1121334 ---> FR Postfix | EN Postfix
UPDATE softwares SET wikipedia_fr = 'Postfix' WHERE id = 216;
UPDATE softwares SET wikipedia_en = 'Postfix_(software)' WHERE id = 216;

-- Comtpoir: 217 -> CUPS /  WidiData: Q868171 ---> FR Common Unix Printing System | EN Common UNIX Printing System
UPDATE softwares SET wikipedia_fr = 'Common_Unix_Printing_System' WHERE id = 217;
UPDATE softwares SET wikipedia_en = 'CUPS' WHERE id = 217;

-- Comtpoir: 218 -> Docker /  WidiData: Q15206305 ---> FR Docker | EN Docker
UPDATE softwares SET wikipedia_fr = 'Docker_(logiciel)' WHERE id = 218;
UPDATE softwares SET wikipedia_en = 'Docker_(software)' WHERE id = 218;

-- Comtpoir: 219 -> Kubernetes /  WidiData: Q22661306 ---> FR Kubernetes | EN Kubernetes
UPDATE softwares SET wikipedia_fr = 'Kubernetes' WHERE id = 219;
UPDATE softwares SET wikipedia_en = 'Kubernetes' WHERE id = 219;

-- Comtpoir: 220 -> KVM /  WidiData: Q377539 ---> FR Kernel-based Virtual Machine | EN Kernel-based Virtual Machine
UPDATE softwares SET wikipedia_fr = 'Kernel-based_Virtual_Machine' WHERE id = 220;
UPDATE softwares SET wikipedia_en = 'Kernel-based_Virtual_Machine' WHERE id = 220;

-- Comtpoir: 221 -> OpenStack /  WidiData: Q1470293 ---> FR OpenStack | EN OpenStack
UPDATE softwares SET wikipedia_fr = 'OpenStack' WHERE id = 221;
UPDATE softwares SET wikipedia_en = 'OpenStack' WHERE id = 221;

-- Comtpoir: 222 -> OpenNebula CE /  WidiData: Q1164484 ---> FR OpenNebula | EN OpenNebula
UPDATE softwares SET wikipedia_fr = 'OpenNebula' WHERE id = 222;
UPDATE softwares SET wikipedia_en = 'OpenNebula' WHERE id = 222;

-- Comtpoir: 223 -> Ceph /  WidiData: Q4035967 ---> FR Ceph | EN Ceph
UPDATE softwares SET wikipedia_fr = 'Ceph' WHERE id = 223;
UPDATE softwares SET wikipedia_en = 'Ceph_(software)' WHERE id = 223;

-- Comtpoir: 224 -> SaltStack /  WidiData: Q7405800 ---> FR Salt | EN Salt
UPDATE softwares SET wikipedia_fr = 'Salt_(logiciel)' WHERE id = 224;
UPDATE softwares SET wikipedia_en = 'Salt_(software)' WHERE id = 224;

-- Comtpoir: 225 -> Ansible /  WidiData: Q2852503 ---> FR Ansible | EN Ansible
UPDATE softwares SET wikipedia_fr = 'Ansible_(logiciel)' WHERE id = 225;
UPDATE softwares SET wikipedia_en = 'Ansible_(software)' WHERE id = 225;

-- Comtpoir: 226 -> HashiCorp Consul /  WidiData: Q28709844 ---> FR Consul | EN Consul
UPDATE softwares SET wikipedia_en = 'Consul_(software)' WHERE id = 226;

-- Comtpoir: 227 -> Rudder /  WidiData: Q16673801 ---> FR Rudder | EN Rudder
UPDATE softwares SET wikipedia_fr = 'Rudder_(logiciel)' WHERE id = 227;
UPDATE softwares SET wikipedia_en = 'Rudder_(software)' WHERE id = 227;

-- Comtpoir: 229 -> RunDeck /  WidiData: Q56317793 ---> FR Rundeck | EN Rundeck
UPDATE softwares SET wikipedia_fr = 'Rundeck' WHERE id = 229;

-- Comtpoir: 230 -> Swarm /  WidiData: Q7653883 ---> FR Swarm | EN Swarm
UPDATE softwares SET wikipedia_en = 'Swarm_(simulation)' WHERE id = 230;

-- Comtpoir: 237 -> HAProxy /  WidiData: Q5628948 ---> FR HAProxy | EN HAProxy
UPDATE softwares SET wikipedia_en = 'HAProxy' WHERE id = 237;

-- Comtpoir: 238 -> NodeJS /  WidiData: Q756100 ---> FR Node.js | EN Node.js
UPDATE softwares SET wikipedia_fr = 'Node.js' WHERE id = 238;
UPDATE softwares SET wikipedia_en = 'Node.js' WHERE id = 238;
UPDATE softwares SET framalibre = 'nodejs' WHERE id = 238;

-- Comtpoir: 239 -> CentOS /  WidiData: Q207542 ---> FR CentOS | EN CentOS
UPDATE softwares SET wikipedia_fr = 'CentOS' WHERE id = 239;
UPDATE softwares SET wikipedia_en = 'CentOS' WHERE id = 239;
UPDATE softwares SET framalibre = 'centos' WHERE id = 239;

-- Comtpoir: 240 -> EOLE Ubuntu /  WidiData: Q3046024 ---> FR EOLE | EN EOLE
UPDATE softwares SET wikipedia_fr = 'EOLE_(Linux)' WHERE id = 240;

-- Comtpoir: 241 -> Debian /  WidiData: Q7715973 ---> FR Debian | EN Debian
UPDATE softwares SET wikipedia_fr = 'Debian' WHERE id = 241;
UPDATE softwares SET wikipedia_en = 'Debian' WHERE id = 241;
UPDATE softwares SET framalibre = 'debian-gnulinux' WHERE id = 241;

-- Comtpoir: 242 -> Syslog-ng /  WidiData: Q467865 ---> FR Syslog-ng | EN syslog-ng
UPDATE softwares SET wikipedia_fr = 'Syslog-ng' WHERE id = 242;
UPDATE softwares SET wikipedia_en = 'Syslog-ng' WHERE id = 242;

-- Comtpoir: 243 -> BareOS /  WidiData: Q25387280 ---> FR Bareos | EN Bareos
UPDATE softwares SET wikipedia_fr = 'Bareos' WHERE id = 243;

-- Comtpoir: 249 -> PrimTux /  WidiData: Q27024590 ---> FR PrimTux | EN PrimTux
UPDATE softwares SET wikipedia_fr = 'PrimTux' WHERE id = 249;
UPDATE softwares SET framalibre = 'primtux' WHERE id = 249;

-- Comtpoir: 250 -> Moodle /  WidiData: Q190434 ---> FR Moodle | EN Moodle
UPDATE softwares SET wikipedia_fr = 'Moodle' WHERE id = 250;
UPDATE softwares SET wikipedia_en = 'Moodle' WHERE id = 250;
UPDATE softwares SET framalibre = 'moodle' WHERE id = 250;

-- Comtpoir: 252 -> Chamilo /  WidiData: Q423221 ---> FR Chamilo | EN Chamilo
UPDATE softwares SET wikipedia_fr = 'Chamilo' WHERE id = 252;
UPDATE softwares SET wikipedia_en = 'Chamilo' WHERE id = 252;
UPDATE softwares SET framalibre = 'chamilo' WHERE id = 252;

-- Comtpoir: 253 -> SambaÉdu /  WidiData: Q16675432 ---> FR SambaÉdu | EN SambaEdu
UPDATE softwares SET wikipedia_fr = 'SambaEdu' WHERE id = 253;

-- Comtpoir: 261 -> WAPT Community /  WidiData: Q50829216 ---> FR WAPT | EN WAPT
UPDATE softwares SET wikipedia_fr = 'Wapt_(logiciel)' WHERE id = 261;
UPDATE softwares SET framalibre = 'wapt' WHERE id = 261;

-- Comtpoir: 264 -> Drupal /  WidiData: Q170855 ---> FR Drupal | EN Drupal
UPDATE softwares SET wikipedia_fr = 'Drupal' WHERE id = 264;
UPDATE softwares SET wikipedia_en = 'Drupal' WHERE id = 264;
UPDATE softwares SET framalibre = 'drupal' WHERE id = 264;

-- Comtpoir: 268 -> Decidim /  WidiData: Q20105575 ---> FR Decidim | EN Decidim
UPDATE softwares SET wikipedia_fr = 'Decidim' WHERE id = 268;

-- Comtpoir: 279 -> Symfony /  WidiData: Q1322933 ---> FR Symfony | EN Symfony
UPDATE softwares SET wikipedia_fr = 'Symfony' WHERE id = 279;
UPDATE softwares SET wikipedia_en = 'Symfony' WHERE id = 279;

-- Comtpoir: 281 -> Sympa /  WidiData: Q1850168 ---> FR Sympa | EN Sympa
UPDATE softwares SET wikipedia_fr = 'Sympa_(informatique)' WHERE id = 281;
UPDATE softwares SET wikipedia_en = 'Sympa' WHERE id = 281;

-- Comtpoir: 282 -> Discourse /  WidiData: Q15054354 ---> FR Discourse | EN Discourse
UPDATE softwares SET wikipedia_fr = 'Discourse' WHERE id = 282;
UPDATE softwares SET wikipedia_en = 'Discourse_(software)' WHERE id = 282;

-- Comtpoir: 283 -> Mastodon /  WidiData: Q27986619 ---> FR Mastodon | EN Mastodon
UPDATE softwares SET wikipedia_fr = 'Mastodon_(r%C3%A9seau_social)' WHERE id = 283;
UPDATE softwares SET wikipedia_en = 'Mastodon_(social_network)' WHERE id = 283;
UPDATE softwares SET framalibre = 'mastodon' WHERE id = 283;

-- Comtpoir: 285 -> OBS Studio /  WidiData: Q21707789 ---> FR Open Broadcaster Software | EN OBS Studio
UPDATE softwares SET wikipedia_fr = 'Open_Broadcaster_Software' WHERE id = 285;
UPDATE softwares SET wikipedia_en = 'OBS_Studio' WHERE id = 285;
UPDATE softwares SET framalibre = 'obs-studio' WHERE id = 285;

-- Comtpoir: 286 -> K-9 Mail /  WidiData: Q14565723 ---> FR K-9 Mail | EN K-9 Mail
UPDATE softwares SET wikipedia_fr = 'K-9_Mail' WHERE id = 286;
UPDATE softwares SET wikipedia_en = 'K-9_Mail' WHERE id = 286;
UPDATE softwares SET framalibre = 'k-9-mail' WHERE id = 286;

-- Comtpoir: 288 -> uBlock Origin /  WidiData: Q19881587 ---> FR uBlock Origin | EN uBlock Origin
UPDATE softwares SET wikipedia_fr = 'UBlock_Origin' WHERE id = 288;
UPDATE softwares SET wikipedia_en = 'UBlock_Origin' WHERE id = 288;
UPDATE softwares SET framalibre = 'ublock-origin' WHERE id = 288;

-- Comtpoir: 291 -> MuPDF /  WidiData: Q6930567 ---> FR MuPDF | EN MuPDF
UPDATE softwares SET wikipedia_en = 'MuPDF' WHERE id = 291;
UPDATE softwares SET framalibre = 'mupdf' WHERE id = 291;

-- Comtpoir: 300 -> draw.io /  WidiData: Q59339175 ---> FR draw.io | EN draw.io
UPDATE softwares SET wikipedia_en = 'Diagrams.net' WHERE id = 300;

-- Comtpoir: 301 -> KEYCLOAK /  WidiData: Q42916195 ---> FR Keycloak | EN Keycloak
UPDATE softwares SET wikipedia_fr = 'Keycloak' WHERE id = 301;
UPDATE softwares SET wikipedia_en = 'Keycloak' WHERE id = 301;

-- Comtpoir: 304 -> Acceleo /  WidiData: Q2822666 ---> FR Acceleo | EN Acceleo
UPDATE softwares SET wikipedia_fr = 'Acceleo' WHERE id = 304;
UPDATE softwares SET wikipedia_en = 'Acceleo' WHERE id = 304;

-- Comtpoir: 312 -> Vault /  WidiData: Q4052619 ---> FR Vault | EN Vault
UPDATE softwares SET wikipedia_en = 'Vault_(version_control_system)' WHERE id = 312;

-- Comtpoir: 320 -> Freecad /  WidiData: Q251322 ---> FR FreeCAD | EN FreeCAD
UPDATE softwares SET wikipedia_fr = 'FreeCAD' WHERE id = 320;
UPDATE softwares SET wikipedia_en = 'FreeCAD' WHERE id = 320;
UPDATE softwares SET framalibre = 'freecad' WHERE id = 320;

-- Comtpoir: 323 -> Solution logicielle Vitam /  WidiData: Q65130365 ---> FR Programme Vitam | EN Vitam Program
UPDATE softwares SET wikipedia_fr = 'Programme_Vitam' WHERE id = 323;

-- Comtpoir: 325 -> Tuleap /  WidiData: Q3541946 ---> FR Tuleap | EN Tuleap
UPDATE softwares SET wikipedia_fr = 'Tuleap' WHERE id = 325;
UPDATE softwares SET wikipedia_en = 'Tuleap_(project_management)' WHERE id = 325;
UPDATE softwares SET framalibre = 'tuleap' WHERE id = 325;

-- Comtpoir: 332 -> JOPLIN /  WidiData: Q97175797 ---> FR Joplin | EN Joplin
UPDATE softwares SET wikipedia_fr = 'Joplin_(logiciel)' WHERE id = 332;
UPDATE softwares SET wikipedia_en = 'Joplin_(software)' WHERE id = 332;
UPDATE softwares SET framalibre = 'joplin' WHERE id = 332;

-- Comtpoir: 348 -> Koha /  WidiData: Q1114959 ---> FR Koha | EN Koha
UPDATE softwares SET wikipedia_fr = 'Koha_(logiciel)' WHERE id = 348;
UPDATE softwares SET wikipedia_en = 'Koha_(software)' WHERE id = 348;
UPDATE softwares SET framalibre = 'koha' WHERE id = 348;

-- Comtpoir: 349 -> Element.io /  WidiData: Q47004029 ---> FR Element | EN Element
UPDATE softwares SET wikipedia_fr = 'Element_(logiciel)' WHERE id = 349;
UPDATE softwares SET wikipedia_en = 'Element_(software)' WHERE id = 349;
UPDATE softwares SET framalibre = 'riotim' WHERE id = 349;

-- Comtpoir: 355 -> Zammad /  WidiData: Q71389626 ---> FR  | EN Zammad
UPDATE softwares SET wikipedia_en = 'Zammad' WHERE id = 355;

-- Comtpoir: 356 -> Jekyll /  WidiData: Q17067385 ---> FR Jekyll | EN Jekyll
UPDATE softwares SET wikipedia_fr = 'Jekyll_(logiciel)' WHERE id = 356;
UPDATE softwares SET wikipedia_en = 'Jekyll_(software)' WHERE id = 356;

-- Comtpoir: 358 -> Matomo /  WidiData: Q34162 ---> FR Matomo | EN Matomo
UPDATE softwares SET wikipedia_fr = 'Matomo_(logiciel)' WHERE id = 358;
UPDATE softwares SET wikipedia_en = 'Matomo_(software)' WHERE id = 358;
UPDATE softwares SET framalibre = 'piwik' WHERE id = 358;

-- Comtpoir: 361 -> Yunohost /  WidiData: Q15971793 ---> FR YunoHost | EN YunoHost
UPDATE softwares SET wikipedia_fr = 'YunoHost' WHERE id = 361;
UPDATE softwares SET framalibre = 'yunohost' WHERE id = 361;

-- Comtpoir: 364 -> DBeaver /  WidiData: Q28956992 ---> FR DBeaver | EN DBeaver
UPDATE softwares SET wikipedia_fr = 'DBeaver' WHERE id = 364;
UPDATE softwares SET wikipedia_en = 'DBeaver' WHERE id = 364;
UPDATE softwares SET framalibre = 'dbeaver' WHERE id = 364;

-- Comtpoir: 365 -> Redis /  WidiData: Q2136322 ---> FR Redis | EN Redis
UPDATE softwares SET wikipedia_fr = 'Redis' WHERE id = 365;
UPDATE softwares SET wikipedia_en = 'Redis' WHERE id = 365;

-- Comtpoir: 366 -> RStudio /  WidiData: Q4798119 ---> FR RStudio | EN RStudio
UPDATE softwares SET wikipedia_fr = 'RStudio' WHERE id = 366;
UPDATE softwares SET wikipedia_en = 'RStudio' WHERE id = 366;
UPDATE softwares SET framalibre = 'rstudio' WHERE id = 366;

-- Comtpoir: 368 -> Archifiltre /  WidiData: Q77064547 ---> FR Archifiltre | EN Archifiltre
UPDATE softwares SET wikipedia_fr = 'Archifiltre' WHERE id = 368;

-- Comtpoir: 373 -> Grafana /  WidiData: Q43399271 ---> FR Grafana | EN Grafana
UPDATE softwares SET wikipedia_fr = 'Grafana' WHERE id = 373;
UPDATE softwares SET wikipedia_en = 'Grafana' WHERE id = 373;

-- Comtpoir: 375 -> ProjeQtOr /  WidiData: Q17501773 ---> FR ProjeQtOr | EN ProjeQtOr
UPDATE softwares SET wikipedia_en = 'ProjeQtOr' WHERE id = 375;
UPDATE softwares SET framalibre = 'projeqtor' WHERE id = 375;

-- Comtpoir: 376 -> OpenIO SDS /  WidiData: Q30348631 ---> FR  | EN OpenIO
UPDATE softwares SET wikipedia_en = 'OpenIO' WHERE id = 376;

-- Comtpoir: 379 -> OpenShot Video Editor /  WidiData: Q769346 ---> FR OpenShot Video Editor | EN OpenShot Video Editor
UPDATE softwares SET wikipedia_fr = 'OpenShot_Video_Editor' WHERE id = 379;
UPDATE softwares SET wikipedia_en = 'OpenShot' WHERE id = 379;
UPDATE softwares SET framalibre = 'openshot' WHERE id = 379;

-- Comtpoir: 380 -> Opale /  WidiData: Q91312803 ---> FR Opale | EN Opale
UPDATE softwares SET framalibre = 'opale' WHERE id = 380;

-- Comtpoir: 384 -> BigBlueButton /  WidiData: Q4904858 ---> FR BigBlueButton | EN BigBlueButton
UPDATE softwares SET wikipedia_fr = 'BigBlueButton' WHERE id = 384;
UPDATE softwares SET wikipedia_en = 'BigBlueButton' WHERE id = 384;

-- Comtpoir: 385 -> Exodus-Privacy (standalone) /  WidiData: Q60348651 ---> FR Exodus Privacy | EN Exodus Privacy
UPDATE softwares SET wikipedia_fr = 'Exodus_Privacy' WHERE id = 385;

-- Comtpoir: 387 -> OpenFisca /  WidiData: Q64777688 ---> FR OpenFisca | EN OpenFisca
UPDATE softwares SET wikipedia_fr = 'OpenFisca' WHERE id = 387;

-- Comtpoir: 390 -> JOSM /  WidiData: Q12877 ---> FR JOSM | EN JOSM
UPDATE softwares SET wikipedia_fr = 'Java_OpenStreetMap_Editor' WHERE id = 390;
UPDATE softwares SET wikipedia_en = 'JOSM' WHERE id = 390;

-- Comtpoir: 392 -> Kdenlive /  WidiData: Q572226 ---> FR Kdenlive | EN Kdenlive
UPDATE softwares SET wikipedia_fr = 'Kdenlive' WHERE id = 392;
UPDATE softwares SET wikipedia_en = 'Kdenlive' WHERE id = 392;
UPDATE softwares SET framalibre = 'kdenlive' WHERE id = 392;

-- Comtpoir: 396 -> Open edX /  WidiData: Q2106448 ---> FR edX | EN edX
UPDATE softwares SET wikipedia_fr = 'EdX' WHERE id = 396;
UPDATE softwares SET wikipedia_en = 'EdX' WHERE id = 396;

-- Comtpoir: 408 -> Mattermost /  WidiData: Q55478510 ---> FR Mattermost | EN Mattermost
UPDATE softwares SET wikipedia_fr = 'Mattermost' WHERE id = 408;
UPDATE softwares SET wikipedia_en = 'Mattermost' WHERE id = 408;

-- Comtpoir: 418 -> Apache Superset /  WidiData: Q68705831 ---> FR Apache Superset | EN Apache Superset
UPDATE softwares SET wikipedia_fr = 'Apache_Superset' WHERE id = 418;
UPDATE softwares SET wikipedia_en = 'Apache_Superset' WHERE id = 418;

-- Comtpoir: 419 -> uPortal /  WidiData: Q3547301 ---> FR uPortal | EN uPortal
UPDATE softwares SET wikipedia_fr = 'UPortal' WHERE id = 419;
UPDATE softwares SET wikipedia_en = 'UPortal' WHERE id = 419;

-- Comtpoir: 421 -> ONLYOFFICE Docs /  WidiData: Q1582500 ---> FR OnlyOffice | EN OnlyOffice
UPDATE softwares SET wikipedia_fr = 'OnlyOffice' WHERE id = 421;
UPDATE softwares SET wikipedia_en = 'OnlyOffice' WHERE id = 421;
UPDATE softwares SET framalibre = 'onlyoffice' WHERE id = 421;

-- Comtpoir: 447 -> GCompris /  WidiData: Q1147042 ---> FR GCompris | EN GCompris
UPDATE softwares SET wikipedia_fr = 'GCompris' WHERE id = 447;
UPDATE softwares SET wikipedia_en = 'GCompris' WHERE id = 447;
UPDATE softwares SET framalibre = 'gcompris' WHERE id = 447;

-- Comtpoir: 467 -> Dokiel /  WidiData: Q112063447 ---> FR Dokiel | EN Dokiel
UPDATE softwares SET framalibre = 'dokiel' WHERE id = 467;

-- Comtpoir: 468 -> Topaze /  WidiData: Q112062753 ---> FR Topaze | EN Topaze
UPDATE softwares SET framalibre = 'topaze' WHERE id = 468;

-- Comtpoir: 470 -> Composer /  WidiData: Q15252222 ---> FR Composer | EN Composer
UPDATE softwares SET wikipedia_fr = 'Composer_(logiciel)' WHERE id = 470;
UPDATE softwares SET wikipedia_en = 'Composer_(software)' WHERE id = 470;

-- Comtpoir: 471 -> PHPUnit /  WidiData: Q1663673 ---> FR PHPUnit | EN PHPUnit
UPDATE softwares SET wikipedia_fr = 'PHPUnit' WHERE id = 471;
UPDATE softwares SET wikipedia_en = 'PHPUnit' WHERE id = 471;

-- Comtpoir: 473 -> KiCad /  WidiData: Q942363 ---> FR KiCad | EN KiCad
UPDATE softwares SET wikipedia_fr = 'KiCad' WHERE id = 473;
UPDATE softwares SET wikipedia_en = 'KiCad' WHERE id = 473;

-- Comtpoir: 489 -> Python /  WidiData: Q28865 ---> FR Python | EN Python
UPDATE softwares SET wikipedia_fr = 'Python_(langage)' WHERE id = 489;
UPDATE softwares SET wikipedia_en = 'Python_(programming_language)' WHERE id = 489;

-- Comtpoir: 496 -> Zotero /  WidiData: Q226915 ---> FR Zotero | EN Zotero
UPDATE softwares SET wikipedia_fr = 'Zotero' WHERE id = 496;
UPDATE softwares SET wikipedia_en = 'Zotero' WHERE id = 496;

-- Comtpoir: 497 -> Dovecot /  WidiData: Q742820 ---> FR Dovecot | EN Dovecot
UPDATE softwares SET wikipedia_fr = 'Dovecot' WHERE id = 497;
UPDATE softwares SET wikipedia_en = 'Dovecot_(software)' WHERE id = 497;

-- Comtpoir: 499 -> JabRef /  WidiData: Q1676802 ---> FR JabRef | EN JabRef
UPDATE softwares SET wikipedia_fr = 'JabRef' WHERE id = 499;
UPDATE softwares SET wikipedia_en = 'JabRef' WHERE id = 499;
UPDATE softwares SET framalibre = 'jabref' WHERE id = 499;

-- Comtpoir: 501 -> ILIAS /  WidiData: Q1623223 ---> FR ILIAS | EN ILIAS
UPDATE softwares SET wikipedia_fr = 'ILIAS' WHERE id = 501;
UPDATE softwares SET wikipedia_en = 'ILIAS' WHERE id = 501;

-- Comtpoir: 503 -> MkDocs /  WidiData: Q102565362 ---> FR MkDocs | EN MkDocs
UPDATE softwares SET wikipedia_en = 'MkDocs' WHERE id = 503;

-- Comtpoir: 504 -> GDL - GNU Data Language /  WidiData: Q929932 ---> FR GNU Data Language | EN GNU Data Language
UPDATE softwares SET wikipedia_fr = 'GNU_Data_Language' WHERE id = 504;
UPDATE softwares SET wikipedia_en = 'GNU_Data_Language' WHERE id = 504;

-- Comtpoir: 505 -> Groff /  WidiData: Q288684 ---> FR Groff | EN groff
UPDATE softwares SET wikipedia_fr = 'Groff_(langage)' WHERE id = 505;
UPDATE softwares SET wikipedia_en = 'Groff_(software)' WHERE id = 505;

-- Comtpoir: 507 -> KDE Plasma 5 /  WidiData: Q17363870 ---> FR KDE Plasma 5 | EN KDE Plasma 5
UPDATE softwares SET wikipedia_en = 'KDE_Plasma_5' WHERE id = 507;

-- Comtpoir: 509 -> Kate /  WidiData: Q261933 ---> FR Kate | EN Kate
UPDATE softwares SET wikipedia_fr = 'Kate_(logiciel)' WHERE id = 509;
UPDATE softwares SET wikipedia_en = 'Kate_(text_editor)' WHERE id = 509;
UPDATE softwares SET framalibre = 'kate' WHERE id = 509;

-- Comtpoir: 512 -> Bitwarden /  WidiData: Q56319818 ---> FR Bitwarden | EN Bitwarden
UPDATE softwares SET wikipedia_fr = 'Bitwarden' WHERE id = 512;
UPDATE softwares SET wikipedia_en = 'Bitwarden' WHERE id = 512;
UPDATE softwares SET framalibre = 'bitwarden' WHERE id = 512;

-- Comtpoir: 513 -> Homebrew /  WidiData: Q11223506 ---> FR Homebrew | EN Homebrew
UPDATE softwares SET wikipedia_fr = 'Homebrew_(gestionnaire_de_paquets)' WHERE id = 513;
UPDATE softwares SET wikipedia_en = 'Homebrew_(package_manager)' WHERE id = 513;

-- Comtpoir: 514 -> Clojure /  WidiData: Q51798 ---> FR Clojure | EN Clojure
UPDATE softwares SET wikipedia_fr = 'Clojure' WHERE id = 514;
UPDATE softwares SET wikipedia_en = 'Clojure' WHERE id = 514;

-- Comtpoir: 517 -> Vim /  WidiData: Q131382 ---> FR Vim | EN Vim
UPDATE softwares SET wikipedia_fr = 'Vim' WHERE id = 517;
UPDATE softwares SET wikipedia_en = 'Vim_(text_editor)' WHERE id = 517;
UPDATE softwares SET framalibre = 'vim' WHERE id = 517;

-- Comtpoir: 519 -> Deno /  WidiData: Q65070939 ---> FR Deno | EN Deno
UPDATE softwares SET wikipedia_fr = 'Deno_(logiciel)' WHERE id = 519;
UPDATE softwares SET wikipedia_en = 'Deno_(software)' WHERE id = 519;

-- Comtpoir: 520 -> R /  WidiData: Q206904 ---> FR R | EN R
UPDATE softwares SET wikipedia_fr = 'R_(langage)' WHERE id = 520;
UPDATE softwares SET wikipedia_en = 'R_(programming_language)' WHERE id = 520;
UPDATE softwares SET framalibre = 'r' WHERE id = 520;

-- Comtpoir: 522 -> PlantUML /  WidiData: Q18346546 ---> FR PlantUML | EN PlantUML
UPDATE softwares SET wikipedia_fr = 'PlantUML' WHERE id = 522;
UPDATE softwares SET wikipedia_en = 'PlantUML' WHERE id = 522;

-- Comtpoir: 523 -> SuiteCRM /  WidiData: Q16294867 ---> FR SuiteCRM | EN SuiteCRM
UPDATE softwares SET wikipedia_en = 'SuiteCRM' WHERE id = 523;
UPDATE softwares SET framalibre = 'suitecrm' WHERE id = 523;

-- Comtpoir: 524 -> MinIO /  WidiData: Q28956397 ---> FR Minio | EN MinIO
UPDATE softwares SET wikipedia_en = 'MinIO' WHERE id = 524;

-- Comtpoir: 525 -> GanttProject /  WidiData: Q1493695 ---> FR GanttProject | EN GanttProject
UPDATE softwares SET wikipedia_fr = 'GanttProject' WHERE id = 525;
UPDATE softwares SET framalibre = 'ganttproject' WHERE id = 525;

-- Comtpoir: 526 -> Angular /  WidiData: Q28925578 ---> FR Angular | EN Angular
UPDATE softwares SET wikipedia_fr = 'Angular' WHERE id = 526;
UPDATE softwares SET wikipedia_en = 'Angular_(web_framework)' WHERE id = 526;

-- Comtpoir: 527 -> Apache Atlas /  WidiData: Q28915124 ---> FR Apache Atlas | EN Apache Atlas
UPDATE softwares SET wikipedia_fr = 'Apache_Atlas' WHERE id = 527;

-- Comtpoir: 528 -> Podman /  WidiData: Q70876440 ---> FR podman | EN podman
UPDATE softwares SET wikipedia_fr = 'Podman' WHERE id = 528;

-- Comtpoir: 529 -> Cherrytree /  WidiData: Q105029197 ---> FR Cherrytree | EN CherryTree
UPDATE softwares SET framalibre = 'cherrytree' WHERE id = 529;

-- Comtpoir: 530 -> Flask /  WidiData: Q289281 ---> FR Flask | EN Flask
UPDATE softwares SET wikipedia_fr = 'Flask_(framework)' WHERE id = 530;
UPDATE softwares SET wikipedia_en = 'Flask_(web_framework)' WHERE id = 530;

-- Comtpoir: 531 -> G'MIC /  WidiData: Q21041255 ---> FR G'MIC | EN G'MIC
UPDATE softwares SET wikipedia_fr = 'G%27MIC' WHERE id = 531;
UPDATE softwares SET wikipedia_en = 'G%27MIC' WHERE id = 531;

-- Comtpoir: 534 -> Raspberry Pi OS /  WidiData: Q890474 ---> FR Raspberry Pi OS | EN Raspberry Pi OS
UPDATE softwares SET wikipedia_fr = 'Raspberry_Pi_OS' WHERE id = 534;
UPDATE softwares SET wikipedia_en = 'Raspberry_Pi_OS' WHERE id = 534;

-- Comtpoir: 537 -> Bonita /  WidiData: Q1076128 ---> FR Bonita | EN Bonita
UPDATE softwares SET wikipedia_fr = 'Bonitasoft' WHERE id = 537;
UPDATE softwares SET wikipedia_en = 'Bonita_BPM' WHERE id = 537;

-- Comtpoir: 540 -> Taiga /  WidiData: Q22909254 ---> FR Taiga | EN Taiga
UPDATE softwares SET wikipedia_en = 'Taiga_(project_management)' WHERE id = 540;

-- Comtpoir: 541 -> Chocolatey /  WidiData: Q16988239 ---> FR Chocolatey | EN Chocolatey
UPDATE softwares SET wikipedia_en = 'Chocolatey' WHERE id = 541;

-- Comtpoir: 542 -> LaTeX /  WidiData: Q5310 ---> FR LaTeX | EN LaTeX
UPDATE softwares SET wikipedia_fr = 'LaTeX' WHERE id = 542;
UPDATE softwares SET wikipedia_en = 'LaTeX' WHERE id = 542;

-- Comtpoir: 547 -> GNU Octave /  WidiData: Q223679 ---> FR GNU Octave | EN GNU Octave
UPDATE softwares SET wikipedia_fr = 'GNU_Octave' WHERE id = 547;
UPDATE softwares SET wikipedia_en = 'GNU_Octave' WHERE id = 547;
UPDATE softwares SET framalibre = 'gnu-octave' WHERE id = 547;

-- Comtpoir: 548 -> GNU TeXmacs /  WidiData: Q1464634 ---> FR GNU TeXmacs | EN GNU TeXmacs
UPDATE softwares SET wikipedia_fr = 'GNU_TeXmacs' WHERE id = 548;
UPDATE softwares SET wikipedia_en = 'GNU_TeXmacs' WHERE id = 548;

-- Comtpoir: 549 -> OCaml /  WidiData: Q212587 ---> FR OCaml | EN OCaml
UPDATE softwares SET wikipedia_fr = 'OCaml' WHERE id = 549;
UPDATE softwares SET wikipedia_en = 'OCaml' WHERE id = 549;

-- Comtpoir: 550 -> FreeFEM /  WidiData: Q5499643 ---> FR FreeFem++ | EN FreeFem++
UPDATE softwares SET wikipedia_fr = 'FreeFem%2B%2B' WHERE id = 550;
UPDATE softwares SET wikipedia_en = 'FreeFem%2B%2B' WHERE id = 550;

-- Comtpoir: 552 -> Arduino IDE /  WidiData: Q55080330 ---> FR Arduino IDE | EN Arduino IDE
UPDATE softwares SET wikipedia_en = 'Arduino_IDE' WHERE id = 552;
UPDATE softwares SET framalibre = 'arduino' WHERE id = 552;

-- Comtpoir: 558 -> Sugarizer /  WidiData: Q114097965 ---> FR Sugarizer | EN Sugarizer
UPDATE softwares SET framalibre = 'sugarizer' WHERE id = 558;

-- Comtpoir: 562 -> GNU Emacs /  WidiData: Q1252773 ---> FR GNU Emacs | EN GNU Emacs
UPDATE softwares SET wikipedia_fr = 'GNU_Emacs' WHERE id = 562;
UPDATE softwares SET wikipedia_en = 'GNU_Emacs' WHERE id = 562;
UPDATE softwares SET framalibre = 'gnu-emacs' WHERE id = 562;

-- Comtpoir: 564 -> Duplicati /  WidiData: Q1266534 ---> FR Duplicati | EN Duplicati
UPDATE softwares SET wikipedia_fr = 'Duplicati' WHERE id = 564;
UPDATE softwares SET wikipedia_en = 'Duplicati' WHERE id = 564;
UPDATE softwares SET framalibre = 'duplicati' WHERE id = 564;

-- Comtpoir: 575 -> PacketFence /  WidiData: Q3360235 ---> FR PacketFence | EN PacketFence
UPDATE softwares SET wikipedia_fr = 'PacketFence' WHERE id = 575;
UPDATE softwares SET wikipedia_en = 'PacketFence' WHERE id = 575;
