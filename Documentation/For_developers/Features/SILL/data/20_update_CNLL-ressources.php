<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//define('_ENV_TYPE', 'dev');
//define('_ENV_TYPE', 'prod');
define('_DATA_DIR', './data_cnll');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
# data source
$urlComptoir = "https://comptoir-du-libre.org/public/export/comptoir-du-libre_export_v1.json";
$urlCnllPage = "https://annuaire.cnll.fr/solutions/";
$urlCnllJson = "https://annuaire.cnll.fr/api/prestataires-sill.json";


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Get remote data
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // get Comptoir file

if (!is_dir(_DATA_DIR) && !mkdir(_DATA_DIR, 0750, true)) {
    throw new \RuntimeException(sprintf('Directory "%s" was not created', _DATA_DIR));
}
        file_put_contents(_DATA_DIR . '/src_'.basename($urlComptoir), file_get_contents($urlComptoir));
        file_put_contents(_DATA_DIR . '/src_cnll_'.basename($urlCnllJson), file_get_contents($urlCnllJson));
        file_put_contents(_DATA_DIR . '/src_cnll_'.basename($urlCnllPage).'.html', file_get_contents($urlCnllPage));

// Extract Comtpoir data
////////////////////////////////////////////////////////////////////////////////
$jsonData = json_decode(file_get_contents(_DATA_DIR . '/src_'.basename($urlComptoir)));
$comptoirJson = $jsonData->softwares;
$comptoirData  = []; // Key : SILL_<sillId>
$comptoirData2 = []; // Key : NAME_<comptoirSoftwareName>
foreach ($comptoirJson as $key => $comptoirSofware) {
    $comptoirId = (int) trim($comptoirSofware->id);
    $comptoirName = trim($comptoirSofware->name);
    $comptoirSlug = strtolower($comptoirName);
    if (isset($comptoirSofware->external_resources->sill->id)) {
        $comptoirSillId = (int) trim($comptoirSofware->external_resources->sill->id);
        $comptoirData["SILL_$comptoirSillId"]['comptoir_sill_id'] = $comptoirSillId;
        $comptoirData["SILL_$comptoirSillId"]['comptoir_id'] = "$comptoirId";
        $comptoirData["SILL_$comptoirSillId"]['comptoir_name'] = "$comptoirName";
    }
    $comptoirData2["NAME_$comptoirSlug"]['comptoir_id'] = "$comptoirId";
    $comptoirData2["NAME_$comptoirSlug"]['comptoir_name'] = "$comptoirName";
} // DEBUG  print_r($comptoirData); print_r($comptoirData2); exit();


// Extract CNLL - "Solutions" web page
////////////////////////////////////////////////////////////////////////////////
$htmlRaw = file_get_contents(_DATA_DIR . '/src_cnll_'.basename($urlCnllPage).'.html');
libxml_use_internal_errors(true); // Disable libxml errors and allow user to fetch error information as needed
$htmlDom = new DOMDocument;
$htmlDom->loadHTML($htmlRaw); // see comment : https://www.php.net/manual/en/domdocument.loadhtml.php#95463
libxml_use_internal_errors(false); // Enable libxml errors and allow user to fetch error information as needed
$links = $htmlDom->getElementsByTagName('a');
$cnllSofwares = array();
foreach ($links as $link) {
    $linkHref = trim($link->getAttribute('href'));
    if (!str_starts_with($linkHref, '/solutions/') || $linkHref === '/solutions/') {
        continue;
    }
    $childNodes = $link->childNodes;
    foreach ($childNodes as $child) {
        if ($child->nodeName === 'h2') {
            // DEBUG echo "$linkHref ---> $child->nodeType:$child->nodeName --> $child->nodeValue  \n";
            $cnllSofwareName = trim($child->nodeValue);
        }
    }
    $cnllId = str_replace('/solutions/', '', $linkHref);
    $cnllSofwares["$cnllSofwareName"] = [
        'matching_type' =>  '',
        'comptoir_id' =>  '',
        'comptoir_name' =>  '',
        'cnll_name' => "$cnllSofwareName",
        'cnll_id' =>  str_replace('/solutions/', '', $linkHref),
        'cnll_sill_id' => '',
        'cnll_href' => "$linkHref",
        'cnll_url' => "https://annuaire.cnll.fr/solutions/$cnllId",
        'cnll_providers' => [],
    ];
    $jsonSoftwareNameSlug = strtolower($cnllSofwareName);
    if (isset($comptoirData2["NAME_$jsonSoftwareNameSlug"])) {
        $comptoirId = $comptoirData2["NAME_$jsonSoftwareNameSlug"]['comptoir_id'];
        $comptoirName = $comptoirData2["NAME_$jsonSoftwareNameSlug"]['comptoir_name'];
        $cnllSofwares["$cnllSofwareName"]['matching_type'] = 'CNLL-html_SOFTWARE-NAME';
        $cnllSofwares["$cnllSofwareName"]['comptoir_id']  = $comptoirId;
        $cnllSofwares["$cnllSofwareName"]['comptoir_name'] = $comptoirName;
    }
} // DEBUG print_r($cnllSofwares); echo count($cnllSofwares) ."\n"; exit();

// Extract CNLL - JSON data + compute Comptoir data
////////////////////////////////////////////////////////////////////////////////
$jsonData = json_decode(file_get_contents(_DATA_DIR . '/src_cnll_'.basename($urlCnllJson)));
foreach ($jsonData as $key => $cnllSofware) {
    $jsonSoftwareName = trim($cnllSofware->nom);
    $jsonSoftwareNameSlug = strtolower($jsonSoftwareName);
    $jsonSoftwareSill = '';
    if (isset($cnllSofwares["$jsonSoftwareName"])) {
        if (isset($cnllSofware->sill_id) && !empty($cnllSofware->sill_id)) {
            $jsonSoftwareSill = trim($cnllSofware->sill_id);
            $cnllSofwares["$jsonSoftwareName"]['cnll_sill_id'] = "$jsonSoftwareSill";
            if (isset($comptoirData["SILL_$jsonSoftwareSill"])) {
                $comptoirId = $comptoirData["SILL_$jsonSoftwareSill"]['comptoir_id'];
                $comptoirName = $comptoirData["SILL_$jsonSoftwareSill"]['comptoir_name'];
                $cnllSofwares["$jsonSoftwareName"]['matching_type'] = 'CNLL-json_SILL-ID';
                $cnllSofwares["$jsonSoftwareName"]['comptoir_id']  = $comptoirId;
                $cnllSofwares["$jsonSoftwareName"]['comptoir_name'] = $comptoirName;
            }
        } // DEBUG echo "$jsonSoftwareName --> SILL $jsonSoftwareSill --> COMPTOIR $comptoirId - $comptoirName\n";

        foreach ($cnllSofware->prestataires as $providerKey => $provider) {
            $cnllSofwares["$jsonSoftwareName"]['cnll_providers']["$provider->nom"]['name'] = "$provider->nom";
            $cnllSofwares["$jsonSoftwareName"]['cnll_providers']["$provider->nom"]['siren'] = "$provider->siren";
            $cnllSofwares["$jsonSoftwareName"]['cnll_providers']["$provider->nom"]['url'] = "$provider->url";
        }
    }
} // DEBUG print_r($jsonData);

//ksort($cnllSofwares,  SORT_NATURAL);
ksort($cnllSofwares, SORT_FLAG_CASE | SORT_NATURAL);
    //    [Symfony] => Array (
    //        [comptoir_id]         => 279
    //        [comptoir_name]       => Symfony
    //        [cnll_name]           => Symfony
    //        [cnll_id]             => 193
    //        [cnll_sill_id]        => 247
    //        [cnll_href]           => /solutions/193
    //        [cnll_url]            => https://annuaire.cnll.fr/solutions/193
    //        [cnll_providers] => Array ([SMILE] => Array ( [name] => SMILE
    //                                                      [siren] => 378615363
    //                                                      [url] => https://annuaire.cnll.fr/societes/378615363

$cnllCsv = '';
$cnllCsvOnlyWithComptoirId = '';
$csvLineNumber = 1;
$sqlMajAuto = [];
$sqlMajNotFound = [];
foreach ($cnllSofwares as $row) {
    $matchingType = $row['matching_type'] ;
    $comptoirId = $row['comptoir_id'] ;
    $comptoirName = $row['comptoir_name'] ;
    $cnllName =  $row['cnll_name'] ;
    $cnllId = $row['cnll_id'] ;
    $cnllSillId = $row['cnll_sill_id'] ;
    echo "-- Comtpoir: $comptoirId -> $comptoirName / CNLL: $cnllId ->  $cnllName / SILL: $cnllSillId ";
    echo " ---> [ $matchingType ]\n";

    $sqlUpdate = '';
    $sqlUpdate .= "-- Matching type: $matchingType \n";
    $sqlUpdate .= "-- Comtpoir:      $comptoirId ---> $comptoirName\n";
    $sqlUpdate .= "-- CNLL:          $cnllId --->  $cnllName\n";
    $sqlUpdate .= "-- SILL:          $cnllSillId\n";
    $sqlUpdate .= "UPDATE softwares SET cnll = '$cnllId' WHERE id = $comptoirId;\n";

    $csvLine = '';
    $csvLine .= "\"". $matchingType         ."\";";
    $csvLine .= "\"". $comptoirId           ."\";";
    $csvLine .= "\"". $comptoirName         ."\";";
    $csvLine .= "\"". $cnllName             ."\";";
    $csvLine .= "\"". $cnllId               ."\";";
    $csvLine .= "\"". $cnllSillId           ."\";";
    $csvLine .= "\"". $row['cnll_href']     ."\";";
    $csvLine .= "\"". $row['cnll_url']      ."\";";
    $csvLine .= "\n";
    $csvLineNumber++;
    $cnllCsv .=  $csvLine;
    if (!empty($row['comptoir_id'])) {
        $cnllCsvOnlyWithComptoirId .=  $csvLine;
        $sqlMajAuto["comptoirId_$comptoirId"] = "$sqlUpdate";
    } else {
        $sqlMajNotFound["cnllName_$cnllName"] = "$sqlUpdate";
    }
}

ksort($sqlMajAuto, SORT_NATURAL);
array_unshift($sqlMajAuto, "UPDATE softwares SET cnll = NULL;\n\n");
file_put_contents(_DATA_DIR ."/output_SQL_maj_softwareCNLL.sql", implode("\n", $sqlMajAuto));

ksort($sqlMajNotFound, SORT_NATURAL);
file_put_contents(_DATA_DIR ."/output_SQL_maj_softwareCNLL_withoutComptoirID.sql", implode("\n", $sqlMajNotFound));

$cnllCsvHead  = '';
$cnllCsvHead .= "\"matching_type\";";
$cnllCsvHead .= "\"comptoir_ID\";";
$cnllCsvHead .= "\"comptoir_NAME\";";
$cnllCsvHead .= "\"cnll_NAME\";";
$cnllCsvHead .= "\"cnll_ID\";";
$cnllCsvHead .= "\"cnll_SILL_ID\";";
$cnllCsvHead .= "\"cnll_RELATIVE_URL\";";
$cnllCsvHead .= "\"cnll_URL\";";
$cnllCsvHead .= "\n";
// print_r($cnllSofwares); echo count($cnllSofwares) ."\n";
file_put_contents(
    _DATA_DIR ."/output_cnll_only-with_comptoirId.csv",
    "$cnllCsvHead" . "$cnllCsvOnlyWithComptoirId"
);
file_put_contents(
    _DATA_DIR ."/output_cnll_with_comptoirId.csv",
    "$cnllCsvHead" . "$cnllCsv"
);
file_put_contents(
    _DATA_DIR ."/output_cnll_with_comptoirId.json",
    json_encode($cnllSofwares, JSON_PRETTY_PRINT)
);
