-- taxonomys_softwares table
-- -------------------------------------------------------------------
-- DELETE FROM public.taxonomys_softwares;

-- MÉTIERS -----------------------------------------------------------------------------
----------------------------------------------------------------------------------------

-- ID 11 : Métiers > Administration générale / RH / Finances
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (11, 43, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (11, 130, null, now(), now());

-- ID 16 : Métiers > Culture / Patrimoine
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (16, 124, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (16, 129, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (16, 331, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (16, 348, null, now(), now());

-- ID 18 : Métiers > Famille / Enfance / Éducation / Scolaire
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (18, 111, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (18, 249, null, now(), now());

-- ID 13 : Métiers > Régalien (État civil, cimetière, élections, sécurité publique...)
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (13, 15, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (13, 17, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (13, 40, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (13, 18, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (13, 5, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (13, 24, null, now(), now());

-- ID 14 : Métiers > Santé / Social
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (14, 16, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (14, 115, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (14, 352, null, now(), now());

-- ID 17 : Métiers > Sport
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (17, 111, null, now(), now());

-- ID 15 : Métiers > Transport
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (15, 13, null, now(), now());

-- ID 12 : Métiers > Urbanisme / Espace public / Environnement
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (12, 23, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (12, 388, null, now(), now());

-- ID 19 : Métiers > Vie économique
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (19, 277, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (19, 387, null, now(), now());


-- OUTILLAGE -----------------------------------------------------------------------------
------------------------------------------------------------------------------------------

-- ID 9 : Outillage > Civic-tech
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (9, 268, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (9, 317, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (9, 382, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (9, 387, null, now(), now());

-- ID 8 : Outillage > Courrier
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (8, 49, null, now(), now());

-- ID 6 : Outillage > GED - Gestion Électronique de Documents
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (6, 29, null, now(), now());

-- ID 4 : Outillage > GRU - Gestion de la Relation Usager
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (4, 26, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (4, 136, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (4, 382, null, now(), now());

-- ID 7 : Outillage > SAE (archivage)
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (7, 3, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (7, 131, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (7, 323, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (7, 368, null, now(), now());

-- ID 5 : Outillage > SIG - Système d'information Géographique
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (5, 60, null, now(), now());

-- ID 10 : Outillage > Signature
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (10, 4, null, now(), now());


-- GENERIQUE -----------------------------------------------------------------------------
------------------------------------------------------------------------------------------

-- ID 20 : Générique > Bureautique
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (20, 33, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (20, 57, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (20, 79, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (20, 82, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (20, 80, null, now(), now());

-- ID 21 : Générique > Web
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (21, 38, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (21, 36, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (21, 140, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (21, 282, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (21, 356, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (21, 358, null, now(), now());

-- ID 22 : Générique > Infrastructure
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (22, 98, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (22, 123, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (22, 212, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (22, 218, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (22, 255, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (22, 374, null, now(), now());

-- ID 23 : Générique > Development
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (23, 9, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (23, 163, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (23, 171, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (23, 172, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (23, 175, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (23, 198, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (23, 279, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (23, 364, null, now(), now());

-- ID 24 : Générique > Multimedia
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (24, 62, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (24, 67, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (24, 68, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (24, 69, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (24, 140, null, now(), now());
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified) VALUES (24, 285, null, now(), now());


