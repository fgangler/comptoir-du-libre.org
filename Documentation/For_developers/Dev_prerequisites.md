# Dev prerequisites for Comptoir

## 0. Verify prerequisites Ubuntu 18.04

Checks that PHP and extensions versions match the platform requirements of the installed packages:

```bash
composer check-platform-reqs
```

Documentation: https://getcomposer.org/doc/03-cli.md#check-platform-reqs

List php versions available on the host:

```bash
sudo update-alternatives --list php
```

Display the default php version:

```bash
sudo update-alternatives --display php
```

If suitable PHP version is available, enable it:

```bash
sudo update-alternatives --set php /usr/bin/php7.0
```

## 1. Install correct PHP version + extensions

```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt install    \
    php7.0          \
    php7.0-intl     \
    php7.0-mbstring \
    php7.0-curl     \
    php7.0-xml      \
    php7.0-dom
```

## 2. Enabled the PHP version needed by comptoir app

```bash
sudo update-alternatives --set php /usr/bin/php7.0
```

and verify again with:

```bash
composer check-platform-reqs
```
