# How to upgrade the CakePhp's version

## When you have to upgrade the cakePhp version:

We assume that you are at the `/root` of the project and as regular user. 

- Change the version in the '/composer.json' file.
- Update the '/composer.lock' file by: `composer update`
- Then update the application by "re-installing" it with composer: `/usr/local/bin/composer --no-progress install`


You can check the current CakePhp version at the end of 'vendor/cakephp/cakephp/VERSION.txt':

```
////////////////////////////////////////////////////////////////////////////////////////////////////
// +--------------------------------------------------------------------------------------------+ //
// CakePHP Version
//
// Holds a static string representing the current version of CakePHP
//
// CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
// Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
//
// Licensed under The MIT License
// Redistributions of files must retain the above copyright notice.
//
// @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
// @link          http://cakephp.org
// @since         CakePHP(tm) v 0.2.9
// @license       http://www.opensource.org/licenses/mit-license.php MIT License
// +--------------------------------------------------------------------------------------------+ //
////////////////////////////////////////////////////////////////////////////////////////////////////
3.4.0
```
