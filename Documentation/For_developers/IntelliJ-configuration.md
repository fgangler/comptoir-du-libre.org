# IntelliJ-configuration

## Global

* We follow [PSR2](https://www.php-fig.org/psr/psr-2/)

## Configure: Code Style

1. Preferences (CTRL-ALT-S) > Editor > Code Style > PHP
1. Identify link "set from..." (top right part of the window)
1. Choose: predefined Style (PSR1/PSR2)

## Configure: commit

In the Commit dialog (CTRL-K), please verify the following checkboxes:

* `Code reformat`: **NOT** checked
* `Rearrange code`: checked 
* `Optimize imports`: checked 
* `Perform code analysis`: checked 
* `Check TODO`: checked 
* `Cleanup`: checked 

## Code Reformat

* Do **NOT** use code reformat (CTRL-ALT-L) as generates conflits with PSR2
* Prefer using Show reformat Dialog (SHIFT-CTRL-ALT-L): tick the 3 checkboxes 
    * Optimize imports
    * Rearrange code
    * Cleanup code
    
## Configure: Code Sniffer

### Code Sniffer Path

* Goto Settings (CTRL-ALT-S) > Languages & Frameworks > PHP > Quality tools
* Click Code Sniffer
* Select the absolute path to `phpcs` binary. 
* **Important**: in order to maintain consistency across developers configurations, please use the binary located in `vendor/bin/`

### Code Sniffer embedded with Code Inspection    

* Goto Settings (CTRL-ALT-S) > Editor > Inspection
* Select PHP > Quality tools > PHP Code Sniffer: tick the entry
* Fulfill the following settings:
    * Severity: weak warning / in all scopes
    * Check files with extensions: `php,js,css,inc`
    * Tick "Show warning as" + select "Warnings"
    * Tick Show sniff name
    * Coding Standard: choose "Custom", then select `.phpcs.xml` at the root of our project (which PSR2 minus some tests due to CakePHP itself)
    
## Configure: Mess Detector

### Mess Detector Path

* Goto Settings (CTRL-ALT-S) > Languages & Frameworks > PHP > Quality tools
* Click Mess Detector
* Select the absolute path to `phpmd` binary. 
* **Important**: in order to maintain consistency across developers configurations, please use the binary located in `vendor/bin/`

### Mess Detector embedded with Code Inspection

* Goto Settings (CTRL-ALT-S) > Editor > Inspection
* Select PHP > Qualitity tools > PHP Mess Detector: tick the entry
* Choose: Severity: weak warning / in all scopes
* Tick the following checkboxes:
    * Code size rules
    * Design rules
    * Naming rules
    * Ununsed rules
