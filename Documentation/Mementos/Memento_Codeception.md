# Memento Codeception

## Documentation codeception

You can find more [codeception's documention](https://codeception.com/docs/01-Introduction)
About [Cakephp/Codecepetion](https://github.com/cakephp/codeception/)
[List of actions](https://codeception.com/docs/modules/PhpBrowser) to acceptance tests


## Error to install Cakephp/Codeception

If you have an error when you try to install Cakephp/Codeception for the first time, error can looks like:

```
...
Problem 1
    - facebook/php-sdk dev-master requires ext-curl * -> the requested PHP extension curl is missing from your system.
    ...
```

That means you need `php-curl`:

```
sudo apt-get install php-curl
sudo service apache2 restart
composer require --dev cakephp/codeception:dev-master && composer run-script post-install-cmd
```

## Verify configuration

In the Comptoir du Libre project check if:

In /config/app.php, section 'EmailTransport', 'className' should be set on 'Debug' if you haven't smtp.
``` 
  'className' => 'Debug',
``` 

In /tests/Acceptance.suite.yml, set the right url (especially if you use vagrant)
```
- PhpBrowser:
            url: http://localhost:8080/fr
```

## Error to generate new tests 


If you follow the gettingSarted or QuickStart documentation of Codeception, there is some differences you have to pay 
attention because of cake.

If this error happens:

```
In Configuration.php line 293:
                                   
  Suite acceptance was not loaded 
```

That's because of the naming convention of cake. `acceptance` is `Acceptance` in cake!
You have to write:

```
vendor/bin/codecept generate:cest Acceptance NameTest
```

## To run the tests

At the root of the project do:

```
vendor/bin/codecept run
```

To run only one test:

```
vendor/bin/codecept run Acceptance UserPersonCheckActionsCest::declareAsUserOfSoftware
```

To see steps during tests:

```
vendor/bin/codecept run --steps
```
