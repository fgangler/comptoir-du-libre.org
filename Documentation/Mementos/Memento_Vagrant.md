# Memento Vagrant

## To install vagrant

Download it on: https://www.vagrantup.com/downloads.html

## Basic commands 

(To be executed in the directory containing the vagrantfile)
```
vagrant up
vagrant ssh
vagrant destroy
```

Quicker:
```
vagrant destroy -f && vagrant up
```

To directly set the local langage in the vagrantfile:

ENV["LC_ALL"] = "en_US.UTF-8"

Vagrant.configure("2") do |config|

## More documentation

More documentation on: https://www.vagrantup.com/docs/index.html
