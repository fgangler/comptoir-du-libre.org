# Memento Gitlab CI Runner

## How to test a CI job locally

From https://docs.gitlab.com/ce/ci/examples/php.html/#testing-things-locally

Pre-requisite: have a [GitLab Runner](https://docs.gitlab.com/runner/) installed locally

```
gitlab-runner exec docker job-name
```
