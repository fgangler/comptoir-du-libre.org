# 1. Minimum needed in your php class to build a connector. W.I.P.

## 1.1 Objective

Explain as well as possible how to build a correct connector for Manivelle.

## 1.2 Needs

* Your principal php class need to extends AbstractConnectorQuery.


## 1.3 Methods needed

### 1.3.1 Request (\Cake\Network\Request) [GET]


Shall return an metaProject like this one :

```
"meta_project"=> [
                "project_array"=> [
                    [
                   "forge"=> "GitHub",
                    "name"=> "tdd-tetris-tutorial-themakingof",
                    "url"=> "https://github.com/marianomdq/tdd-tetris-tutorial-themakingof",
                    "creationDate"=> "2014-03-15T21:06:55Z",
                    "lastUpdatedDate"=> "2014-04-24T04:34:22Z",
                    "description"=> "Learning repository for tdd-tetris-tutorial"
                    ]
                ],
               "total_count"=>1
            ],

            "order"=> "desc",
            "limit"=> 25,
            "description"=> "Not specified",
            "name"=> "GitHub",
            "url_connector"=> "https://github.com/",
            "url_connector_api"=> "https://api.github.com/",
            "request"=> "tdd-tetris-tutorial-themakingof",
            "request_error"=> null,
            "success"=> false/true,
            "message"=> "A String ",
            "error"=> null];
```

## 2. Notes 

As always nothing to say but a lot to add.
