# How to create a connector available in Manivelle ?

## Objective

All forges can not be developed by our teams. It is therefore obvious that the desire takes you to add your own connector.

To do this, follow the steps below.

## Step #1  : Create a plugin as usual

To create a plugin I recommend to you to use the cake console with the bake command.

Go to your app folder and use the cake console like this :

```
bin/cake bake plugin ForgeQueryXxx

```
Please note that your plugin's name shall start by ForgeQuery.

## Step #2 : autoload the plugin into your app

I recommend to use composer.

Please enter this command :

```
composer dumpautoload

```
## Step #3 Create your connector

* Create a php class in src/Model/Entity of your plugin folder.
* Your class shall extends AbstractConnectorQuery.
* Your class shall override the request method.


## Step #4 Test if your plugin is available

To check if your plugin is available for Manivelle. follow this steps.

* First call forgeList command
    * If the forgeList command return to you an json containing the name of your plugin then your plugin was correctly loaded in cakeApp.

* Next call request command
    * If you can see your `connector` in the `forges` table then Manivelle has been able to use your `connector`.Please note that there is not necessarily the results for your connector `results`' table.


## Notes

Please if you have any questions about `"how to build a plugin ?"` check the cake documentation else go to PhpClassStrucutre file to build your connector.
