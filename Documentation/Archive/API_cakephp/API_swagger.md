# Configurer Swagger dans un projet CakePHP

## Installation de Swagger

Comme utilisateur "superdupont" (développeur), et depuis le dossier du projet CakePHP (contenant webroot, log, tmp...), faire :

```shell
composer require alt3/cakephp-swagger:~1.0
bin/cake plugin load Alt3/Swagger --routes
```

## Créer le fichier de configuration

Créer `config/swagger.php` et y ajouter :

```php
<?php
use Cake\Core\Configure;

return [
    'Swagger' => [
        'ui' => [
            'title' => 'ALT3 Swagger',
            'validator' => true,
            'api_selector' => true,
            'route' => '/swagger/',
            'schemes' => ['http', 'https']
        ],
        'docs' => [
            'crawl' => Configure::read('debug'),
            'route' => '/swagger/docs/',
            'cors' => [
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET, POST',
                'Access-Control-Allow-Headers' => 'X-Requested-With'
            ]
        ],
        'library' => [
            'api' => [
                'include' => ROOT . DS . 'src',
                'exclude' => [
                    '/Editor/'
                ]
            ],
            'editor' => [
                'include' => [
                    ROOT . DS . 'src' . DS . 'Controller' . DS . 'AppController.php',
                    ROOT . DS . 'src' . DS . 'Controller' . DS . 'Editor',
                    ROOT . DS . 'src' . DS . 'Model'
                ]
            ]
        ]
    ]
];
```

## (Optimisation locale / à changer par la véritable action)

Depuis le dossier du projet CakePHP, modifier le fichier `tmp/cache/cakephp_swagger_api.json` comme ceci:

```php
'docs' => [
            'crawl' => false,
```

## Mise à jour des permissions

S'assurer que le fichier `cakephp_swagger_api.json` est accessible en écriture par l'utilisateur du serveur web
(typiquement `www-data`) et le développeur (disons `superdupont`).

Exemple:

Si `superdupont` fait parti du groupe `comptoir`, alors faire

```shell
chown www-data:comptoir cakephp_swagger_api.json
chmod 664 cakephp_swagger_api.json
```

## Utiliser Swagger

Dans le projet Comptoir, ouvrir le fichier `Documentation/Api/v1/swagger.yml`
Copier le contenue dans la partie gauche de http://editor.swagger.io/#/
Convertir le yaml en json : `-> file -> download JSON`
Copier le contenue JSON dans le fichier `Comptoir-srv/tmp/cache/persistent/cakephp_swagger_api.json`
Ce fichier est utilisé pour générer la page http://comptoir-srv.local/swagger

Pour continuer la documentation, continuer le fichier yaml sur http://editor.swagger.io/#/

Sur la page http://comptoir-srv.local/swagger mettre dans le swagger : http://comptoir-srv.local/swagger/docs/api

Si il y a des erreurs, enlever les exemples du fichier `Comptoir-srv/tmp/cache/persistent/cakephp_swagger_api.json`.

 Cf :   https://github.com/swagger-api/swagger-ui/issues/2369
        https://github.com/swagger-api/swagger-ui/pull/2469


