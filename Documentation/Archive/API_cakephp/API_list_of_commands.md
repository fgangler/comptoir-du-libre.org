# COMPTOIR API: List of commands

## URL pattern

URL pattern is like http://srv.comptoir-du-libre.org/api/v1/COMMAND where COMMAND is the called method.

## List for user

* http://srv.comptoir-du-libre.org/api/v1/users
* http://srv.comptoir-du-libre.org/api/v1/users.json
* http://srv.comptoir-du-libre.org/api/v1/user-types
* http://srv.comptoir-du-libre.org/api/v1/user-types.json

## List for softwares

* http://srv.comptoir-du-libre.org/api/v1/softwares
* `LicenceTypes`
* `Licences`
* `RawMetricsSoftwares`
* http://srv.comptoir-du-libre.org/api/v1/reviews
* http://srv.comptoir-du-libre.org/api/v1/screenshots

## List for relationships

* http://srv.comptoir-du-libre.org/api/v1/relationships
* `RelationshipsSoftware`
* `RelationshipsSoftwareUsers` http://srv.comptoir-du-libre.org/api/v1/relationships-softwares-users
* `RelationshipsUsers`
* `RelationshipsTypes`
