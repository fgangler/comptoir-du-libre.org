# Error management structure


* FORMAT  : application/json

* Structure  :

```
{
    "success": false,
    "error_code": "error code",
    "message": {
        "name": {
            "RULE": "Validation failed : The corresponding error message in positive form. Some help to resolve it."
        },
        "help": "Please check the manual at http:\/\/manivelle.adullact.org\/manual"
    }
}
```

## Guidelines to write messages

* Have a positive message (even when it is an error)
* Tell the user what to do ("get him back to the trail")
