
# [GET/] Method/[none]

## Objective

The forgeList method give the list of all available connectors in json format.

## Ressource url

* /api/v1/forgeList

## Ressource informations

* Response formats : JSON
* Requires authentication? : No
* Rate limited ? : No yet

##  Input

No input available yet.

## Output

The forgeList command returns a json, each item are composed of  :
 * id : id of the connector (corresponding to plugin number);
 * name : the name of the connector (ex : GitHub);
 * url : url of the connector web site;
 * url_api : url of the reachable api.

## Examples

* /api/v1/forgeList

```
"connectors": [
        {
            "id": 1,
            "name": "Adullact",
            "url": "http:\/\/adullact.net\/",
            "url_api": "http:\/\/adullact.net\/"
        },
        {
            "id": 2,
            "name": "GitHub",
            "url": "https:\/\/github.com\/",
            "url_api": "https:\/\/api.github.com\/"
        }
    ]
```

## Error codes


No error code implemeted yet

## Notes

Nothing to say. If you need some help please contact mickael.pastor@adullact.org
