# Branch develop cake 3.4 aborted

See the [history](gitHistory_TAG_develop_cake.3.4_abort.md) of
the `develop_cake.3.4_abort` tag and the commits backport
 to the `develop_cake.3.3_reloaded` tag.

## Tags
  - [`develop_cake.3.4_abort`](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/tags/develop_cake.3.4_abort) tag
  - [`develop_cake.3.3_reloaded_START`](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/tags/develop_cake.3.3_reloaded_START) tag
  - [`develop_cake.3.3_reloaded`](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/tags/develop_cake.3.3_reloaded) tag

------------

see also: #754
