# Changelog

Tous les changements notables de ce projet sont documentés dans ce fichier.

Le format s'appuie sur [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
et le projet suit [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


----------

## 2.13.2_DEV     (unreleased)

### Added

### Changed

### Fixed

### Security


----------

## [2.13.1](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.13.1) - 2023-10-10

### Changed

- [#973](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/973) Gitlab: use "main" branch instead of "master" branch
- [#980](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/issues/980) Update SILL URL : use `code.gouv.fr/sill/` instead of `sill.etalab.gouv.fr`

### Fixed

- [API 1.2.1](Documentation/API/CHANGELOG.md)
  - [#980](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/issues/980) Update SILL URL : use `code.gouv.fr/sill/` instead of `sill.etalab.gouv.fr`
  - [#974](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/974) use production URLs instead of development URLs for the following properties:
    - `api_documentation -> changelog`
    - `api_documentation -> documentation`


----------

## [2.13.0](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.13.0)  - 2022-12-26

### Added

- software: add external Id (CNLL)
- add "Open Data" page
- add [API changelog](Documentation/API/CHANGELOG.md)
- add [API documentation](Documentation/API)

### Changed

- update french "Accessibility" page
- [API 1.2.0](Documentation/API/CHANGELOG.md)
  - add `software -> external_resources -> cnll` (software ID and URL)
  - add `software -> license` (will replace  `software -> licence` in next major version)
  - add `data_date` (will replace  `date_of_export` in next major version)
  - add `data_statistics`
  - add `data_documentation -> license` #972
  - add `api_documentation`
      - `api_documentation -> version` #969
      - `api_documentation -> changelog` #970
      - `data_documentation -> deprecated` #970
  - deprecated:
    - `date_of_export` will be replaced by [ `data_date` ] in next major version
    - `number_of_software` will be replaced by [ `data_statistics -> software` ] in next major version
    - `software -> licence` will be replaced by [ `software -> license` ] in next major version


----------

## [2.12.0](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.12.0)  - 2022-09-10

### Added

- FEAT(software): add external Ids (SILL, WikiData, FramaLibre, Wikipedia)
- FEAT(software export): add external ressources (SILL, WikiData, FramaLibre, Wikipedia)

### Changed

- FIX(sill): allow to import SILL and Wikidata IDs


----------

## [2.11.0](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.11.0)  - 2022-06-09

### Added

- [#966](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/966) Admin - Ajout d'un export JSON et CSV des utilisateurs

### Fixed

Pour le dev :
- [!555](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/merge_requests/555) Vagrant - Correction des données ajoutées pour la cartographie


## [2.10.0](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.10.0)  - 2021-08-13

### Added

- [!544](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/merge_requests/544) Ajout d'un export JSON des logiciels

### Fixed

- [!543](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/merge_requests/543) Correction du script d'import du SILL


----------

## [2.9.3](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.9.3)  - 2021-04-06

### Fixed

- [#956](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/956) UX - Ajout d'un bouton "Modifier votre profil" sur la page de l'utilisateur
- [#955](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/955) UX - Empêcher l'utilisateur connecté d'afficher les formulaires de connexion et de création d'un compte
- [#954](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/954) SEO - Les URLs `/fr/tags/<id>/` doivent rediriger vers `/fr/tags/<id>/software`
- [#953](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/953) Compte utilisateur : vérifier le format de l'URL du site web (backend)
- [#952](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/952) Formulaire inscription : renforcer l'anti-spam (interdire la réutilisation de token dès la 1er detection)


## [2.9.2](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.9.2)  - 2021-03-23

### Fixed

- [#951](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/951) Formulaire inscription : ajout d'un jeton + temporisation pour diminuer le spam
- [#643](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/643) Tests fonctionnels: les tests de création de compte sont maintenant idempotent


## [2.9.1](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.9.1)  - 2021-03-18

### Fixed

- [#941](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/941) UX - Création d'un compte : supprimer l'avatar (nombreux bugs pour les utilisateurs)
- [#950](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/950) Accessibilité - Ajouter une balise `<header>` (RGAA 9.2.1)
- [#947](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/947) Vagrant - Forcer l'utilisation de composer 1.x


## [2.9.0](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.9.0)  - 2020-06-26

### Added
- [#937](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/937) Page d'un logiciel : rendre visible "Pré-sélection Adullact" dans la rubrique Cartographie

### Changed
- [#938](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/938) UX - Menu principal : déplacer "Cartographie" après "prestataire"
- [#930](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/930) UX - Amélioration du menu utilisateur ('profil', 'Paramètres', 'Se déconnecter')
- [#924](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/924) UX - Cartographie, formulaire : renommer le bouton "Envoyer"
- [#933](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/933) UX - Cartographie, formulaire : modification du titre et du sous-titre
- [#934](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/934) UX - Cartographie, formulaire : ajout d'un texte pour éviter de cocher trop de cases
- [#935](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/935) Page d'un logiciel : rendre plus visible les données de la cartographie
- [#940](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/940) Page d'un logiciel : ajout des boutons pour modidifer la cartographie

### Fixed
- [#929](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/929) Formulaire de login : la redirection doit être faite sur `/<lang>/`
- [#922](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/922) UX - Cartographie, page "Cas d'usage du Logiciel" : ajout du lien vers le logiciel


## [2.8.0](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.8.0)  - 2020-06-15

### Added
* [#903](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/903) Ajout du bouton modifier pour la cartographie
* [#917](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/917) Page d'un utilisateur : afficher les données de la cartographie
* [#918](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/918) Page d'un logiciel : afficher les données de la cartographie

### Fixed
* [#910](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/910) Traitement correcte des langues utilisateur non supportées par l'application
* [#914](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/914) Cartographie : corriger les chaines de textes non traduites
* [#915](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/915) Cartographie : modification des textes (titres, boutons, tableaux, ...)
* [#919](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/919) Cartographie : le formulaire s'affiche uniquement si on est déjà déclaré comme utilisateur du logiciel
* [#898](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/898) Correction du bug des boutons non utilisables après l'édition de son profil


## [2.7.0](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.7.0) - 2020-05-29

### Added
* [#905](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/905)  Cartographie - 1er version public

Pour le dev :
* [#469](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/469) Vagrant - Ajout de MailHog pour simuler l'envoi et la consultation des emails
* [#862](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/862) Vagrant - Permettre l'import automatique de données SQL supplémentaires au dataset
* [#860](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/860) Vagrant - Appliquer les migrations de base de données au démarrage de la machine virtuelle

### Changed
* [#852](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/852) Webperf - Supprimer l'image d'arrière-plan de la page
* [#863](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/863) Webperf - Images : utiliser le lazyloading natif des navigateurs
* [#908](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/908) SEO - Vérifier si l'URL inclus le prefixe de la langue (`/fr/` ou `/en/`)
* [#904](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/904) SEO - Les liens pour changer de langue renvois vers la page d'accueil
* [#881](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/881) Ajout d'un logiciel : proposer (si pertinent) d'être utilisateur ou prestataire
* [#880](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/880) Édition d'un logiciel : proposer (si pertinent) d'être utilisateur ou prestataire

Pour la prod :
* [#893](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/893) crudMailer : envoi d'un message de log lorsque l'utilisateur est modifié
* [#894](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/894) crudMailer : ajout de la cartographie dans le processus de log

### Fixed
* [#879](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/879) UX - Optimiser l'affichage des messages d'erreurs et de succès
* [#895](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/895) UX - Page prestataire : correction du titre de la page
* [#890](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/890) UX - Afficher des sauts de lignes dans les descriptions (logiciel, utilisateur, témoignage)
* [#889](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/889) UX - Ajout d'un témoignage : le champ TITRE n'est plus un textarea
* [#900](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/900) UX - Modifier l'image par défaut des logiciels
* [#901](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/901) UX - Corriger les retours à la ligne des messages "flash"
* [#867](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/867) Accessibilité - Correction du formulaire de filtre
* [#858](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/858) Accessibilité - Liens en pied de page : ajout d'un indicateur si c'est la page courante
* [#884](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/884) Accessibilité - Améliorer les contrastes (couleur du texte / couleur d'arrière plan)
* [#885](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/885) Accessibilité - Supprimer les `title` non pertinant sur les liens
* [#855](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/855) Correction de l'i18n sur le formulaire de mot de passe perdu
* [#868](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/868) Nettoyage du code HTML des aperçus de logiciel

Pour le dev :
* [#814](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/814) L'outil PHPCS de la CI ne teste plus inutilement les fichiers supprimés
* [#861](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/861) Vagrant - Forcer le mode debug à TRUE pour lancer les tests unitaires

### Security
* [#871](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/871) Mot de passe perdu : le token n'est plus actif après usage ou après 24h
* [#869](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/869) Nouveau compte : appliquer correctement les ACL après la création du compte


## [2.6.0](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.6.0)  - 2020-04-07

### Added
* [#788](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/788)  Cartographie - 1er version en accès restreint

Pour la prod :
* [#801](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/801) DOC - Ajout d'un prérequis optionnel : mode headers et deflate d'Apache

Pour le dev :
* [#807](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/807) Vagrant : ajout de scripts utiles (évolution db, ...)
* [#808](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/808) Cartographie : auto-generation des templates MVC via bin/cake bake

### Changed
* [#846](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/846)  UX - Le formulaire de contact n'est plus utilisable anonymement
* [#847](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/847)  UX - Le formulaire de contact n'est plus utilisable par les prestataires
* [#787](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/787) SEO - Imposer le préfixe /fr/ ou /en/ sur tous les liens du site web
* [#809](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/809) Webperf - Optimiser le chargement de Matomo (ex-Piwik)
* [#838](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/838) Vagrant - Le mode debug est désactivé pour les tests fonctionnels

### Fixed
* [#810](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/810) UX - Trie alphabétique de logiciels sur une page "Tag"
* [#826](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/826) UX - Trie alphabétique pour les pages "Utilisateurs" et "Prestataires" du logiciel
* [#830](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/830) UX - Témoignage : le titre n'est plus tronqué à l'affichage
* [#840](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/840) UX - Les espaces en trop dans une recherche n'ont pas d'impact sur le résultat affiché
* [#844](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/844) UX - Ajouter un message explicite au mail de contact d'un utilisateur
* [#851](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/851) UX - Le formulaire de recherche ne peut plus être envoyé si il est vide
* [#856](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/856) UX - Correction des liens du fil d’Ariane des pages "Mentions légales" et "Accessibilité"
* [#815](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/815) SEO - Ajout du fil d'ariane au format JsonLD pour les moteurs de recherche
* [#813](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/813) Webperf - Supprimer un fichier CSS inutilisé
* [#825](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/825) Webperf - Optimisation des logos (Comptoir, Adullact, Feder) + arrière-plan
* [#827](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/827) DOC - Correction de la syntaxe markdown
* [#850](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/850) Correction d'une erreur PHP si la recherche est vide
* [#857](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/857) Page d'erreur : entête HTML avec le code de la langue de l'utilisateur

Pour le dev :
* [#816](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/816) SILL - Adaptation du script d'import aux changements de format du fichier source
* [#831](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/831) DOC - Compléter les prérequis pour l'utilisation de hirak/prestissimo
* [#832](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/832) Git - Ajout des extensions .7z|.zip|.bz2 au fichier .gitattributes
* [#859](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/859) Cmposer - Mise à jour du fichier des dépendances .lock

### Deprecated
* [#828](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/828) Test - Suppression d'un test unitaire inutile

### Security
* [#833](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/833) L'utilisateur ne doit pas pouvoir modifier son rôle
* [#835](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/835) Un nouvel utilisateur ne doit pas pouvoir forcer son rôle
* [#836](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/836) API - L'ajout d'un utilisateur ne permet pas de modifier son rôle
* [#837](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/837) API - La modification d'utilisateur ne permet pas de modifier son rôle
* [#843](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/843) Log - Ajout d'un log pour l'utilisation du formulaire de contact d'un utilisateur
* [#853](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/853) L'édition ou la suppression d'un tag est autorisé uniquement pour les rôles > "user"


## [2.5.0](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.5.0) - 2020-02-06

### Added
* [#240](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/240) UX - Ajouter la fonction recherche sur les Tags
* [#574](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/574) UX - Les tags acceptent les caractères accentués
* [#789](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/789) UX - Champ mot de passe : utiliser l'attribut autocomplete="new-password"
* [#371](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/371) UX - Accès direct à une ss-rubrique d'un logiciel ou d'un utilisateur
* [#755](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/755) UX - Ajout d'un fil d'ariane
* [#798](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/798) UX - Logo par défault en fonction du type d'utilisateur (associaion, collectivité...)
* [#796](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/796) WebPerf - Ajout des en-têtes HTTP "Cache-Control : immuable" pour les fichiers CSS et JS

Pour le dev :
* [#694](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/694) Vagrant : afficher les erreurs PHP et augmenter le niveau d'erreur
* [#695](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/695) Vagrant : permettre la synchronisation du code de l'IDE avec la VM
* [#702](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/702) Vagrant : activer l'auto-correction de collision des ports
* [#696](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/696) Vagrant : permettre l'accès à PostgreSQL en dehors de la machine virtuelle
* [#764](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/764) Pre-commit : ajout grumphp pour gérer les hooks php via composer
* [#757](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/757) CI + pre-commit: vérifier la cohérence des fichiers composer.json .lock
* [#762](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/762) CI + pre-commit: vérifier la syntaxe PHP

### Changed
* [#637](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/637) UX - Monter la limite autorisé à 10 tags pour un logiciel
* [#759](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/759) SEO - Fiche utilisateur : passer le lien externe en nofollow

Pour le dev :
* [#753](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/753) Préciser de manière plus précise la version de PHP dans le composer.json

### Fixed
* [#597](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/597) UX - Logo par défaut pour les logiciels et les utilisateurs
* [#479](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/479) UX - Afficher la licence sur la fiche logiciel
* [#768](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/768) UX - Ajout d'une page dédiée à toutes les commenaires d'un utilisateur
* [#771](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/771) UX - Ajout d'une page dédiée aux alternatives à un logiciel
* [#514](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/514) UX - Ajout d'une page dédiée aux logiciels complémentaires à un logiciel
* [#540](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/540) UX - Après l'édition d'un logiciel, la page du logiciel est affiché
* [#718](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/718) UX - Après l'édition d'un utilisateur, la page de l'utilisateur est affiché
* [#554](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/554) UX - Afficher la date de création correcte d'un commentaire
* [#704](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/704) UX - Page Tags : filtres "plus | moins utilisés" inversés
* [#703](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/703) UX - Page Tags : affiche tous les tags
* [#650](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/650) UX - Impossible de cliquer sur l'entrée Etiquettes de la barre de navigation
* [#713](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/713) UX - Recherche avec 2 mots (ou plus)
* [#714](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/714) UX - Recherche avec des caractères spéciaux et des accents
* [#585](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/585) UX - Correction des filtres sur la page des utilisateurs
* [#790](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/790) Correction du lien financement "FEDER par région Occitanie"
* [#794](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/794) Correction de l'i18n pour le mail réinitialisation du mot de passe
* [#797](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/797) modifier l'email de contact pour limiter le spam


Pour le dev :
* [#648](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/648) Vagrant : les tests unitaires en cours ne doivent pas effacer le DB du comptoir

### Security
* [#705](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/705) - Added optional parameter to force the deny on GET requests
* [#758](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/758) - Ajouter les entêtes HTTP de sécurité
* [#766](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/766) - Correction d'une XSS sur formulaire de recherche
* [#767](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/767) - Le formulaire de contact ne doit pas autoriser les balises html


## [2.4.10](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.4.10) - 2019-04-08

### Added

Pour le dev :
* [#646](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/646) Vagrant: choix d'installer en mode debug ou production


## [2.4.10-rc.1](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.4.10-rc.1) - 2019-03-27

### Added

Pour le dev :
* [#575](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/575) Les images Docker peuvent envoyer des mails
* [#584](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/584) Les images Docker peuvent s'appuient l'image PHP (et non plus Ubuntu, trop volumineuse)
* [#580](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/580) Ajout de code sniffer dans les tests automatiques sur les fichiers modififés ajoutés dans le code source
* [#593](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/593) Ajout d'une base de données connue et fixe pour les tests fonctionnels
* [#603](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/603) Ajout de 4 utilisateurs documentés dans le jeu de données de tests
* [#534](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/534) Ajout de tests fonctionnels sur les créations de compte, les recherches, les tris
* [#589](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/589) Ajout de tests fonctionnels sur l'action utilisateur "connecter/deconnecter"
* [#609](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/609) Ajout de tests fonctionnels sur l'action utilisateur "mot de passe perdu"
* [#618](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/618) Ajout de tests fonctionnels sur l'action utilisateur "se déclarer utilisateur de"
* [#620](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/620) Sur une installation de dev, les mails sont envoyés par barman-dev
* [#638](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/638) Ajout machines Vagrant pour déploiement local du Comptoir

### Deprecated

* [#640](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/640) comptoir.default.php est déprécié au profit de son pendant dans le projet Puppet-Comptoir

### Fixed

* [#570](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/570) Rétablissement des tris et filtre des utilisateurs (suite à la création de la rubrique Prestataire dans la barre de navigation)
* [#609](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/609) Mot de passe perdu : Correction de l'envoi du mauvais formulaire qui demandait l'ancien mot de passe.
* [#587](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/587) Correction de l'erreur affichée lors de la déclaration d'un compte utilisateur comme utilisateur d'un logiciel


## [2.4.9](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.4.9) - 2018-08-16

### Fixed

* [#573](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/573) Correction du lien erroné dans le courriel de mot de passe perdu
* [#572](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/572) Mise à jour des paramètres STMP par défaut


## [2.4.8](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.4.8) - 2018-08-14

### Fixed

* [#542](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/542) Correction de l'affichage de tous les logiciels d'un tag


## [2.4.8-rc.2](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.4.8-rc.2) - 2018-08-14

### Added

* [#547](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/547) (Développeurs) Amélioration de la doc d'install sur Ubuntu 16.04
* [#562](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/562) (Développeurs) Création d'une image Docker utilisant le code source local (plus besoin de faire un git clone)
* [#563](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/563) (Développeurs) Ajout de la dernière version de DebugKit dans l'image Docker "locale"
* [#564](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/564) (Développeurs) Mise à jour de l'image Docker Comptoir base en v1.0.0-rc.2

### Fixed

* [#559](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/559) Page d'accueil : Corrigé le lien vers les derniers logiciels ajoutés
* [#557](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/557) Rétabli la mise en page du bouton "mot de passe perdu" (mauvaise grammaire HTML)
* [#560](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/560) Supprimé bouton en double ("liste de tags")

### Security

* [#467](https://gitlab.adullact.net/Comptoir/Comptoir-srv/issues/467) Correction XSS sur les formulaires


## [2.4.8-rc.1](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.4.8-rc.1) - 2018-08-07

### Added

* Nouveau système de gestion d'étiquettes (tags) pour les logiciels

### Changed

* Les prestataires sont maintenant séparés des utilisateurs et disposent d'une entrée propre dans la barre de navigation.
* Correction de bugs d'internationalisation (tout est censé être traduit de l'anglais vers le français)


## 2.4.7 - 2018-03-28

(not tagged; SHA: `86d815d1a1476683716130d81870725375ea2170`)

## 2.4.6 - 2018-03-01

(not tagged)

## 2.4.5 - 2018-02-02

(not tagged)

## 2.4.4 - 2018-01-03

(not tagged; SHA: `adf22a7f5aa17506666bddd633eee43cfa879315`)

## 2.4.3 - 2017-12-22

(not tagged; SHA: `a9bb10ae943be2daba3db99c1bc5ebce62b8e285`)

## 2.4.2 - 2017-11-14

(not tagged; SHA: `fcd9455df9e4e4965ae5fcff20e31237e3e2f259`)

## 2.4.1 - 2017-08-18

(not tagged; SHA: `860765ebecd10f1d337dc6f3c14334303ea29853`)

## 2.4.0 - 2017-08-04

(not tagged; SHA: `c59b2458a01cf41d100df9b387e8acb78dded8d1`)

## [2.3.2](https://gitlab.adullact.net/Comptoir/Comptoir-srv/tags/v2.3.2) - 2017-07-21

## Template

```markdown
## <major>.<minor>.patch_DEV     (unreleased)

### Added

### Changed

### Fixed

### Security


----------

```
