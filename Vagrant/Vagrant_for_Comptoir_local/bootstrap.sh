#!/usr/bin/env bash

# stop at first error
set -e

# If you set DEBUG on true, you will install all the environment to allow unit tests to play.
declare DEBUG=true
# If you set COMPOSER_UPDATE to true, a "composer update" will be done before the "composer install".
declare COMPOSER_UPDATE=false

# Variables
export DEBIAN_FRONTEND=noninteractive

# VAGRANT_HOME: Absolute directory, *without* trailing slash, eg "/home/vagrant"
declare VAGRANT_HOME="/home/vagrant"
# COMPTOIR_HOME: Absolute directory containing the Comptoir-srv, *without* trailing slash, eg "/home/comptoir"
declare COMPTOIR_HOME="/home/comptoir"
# COMPTOIR_DIR:  Absolute directory to Comptoir-srv, *without* trailing slash, eg "/home/comptoir/comptoir-srv"
declare COMPTOIR_DIR="${COMPTOIR_HOME}/Comptoir-srv"
# COMPTOIR_DIR_DATASET:  Absolute directory to copy the needed files to insert a dataset, *without* trailing slash, eg "/home/comptoir/Dataset"
declare COMPTOIR_DIR_DATASET="${COMPTOIR_HOME}/Dataset"
# COMPTOIR_REVISION:  The revision number of a particular commit or tag, eg "b444c25b232824c9edf94bfc733d39ddc7eaabb2" or "tags/<tag_name>"
declare COMPTOIR_REVISION=""
# DATASET_TIMESTAMP:  Timestamp of the files of data to import, eg "2016-06-28-15h06m42"
declare DATASET_TIMESTAMP="2018-09-12-16h13m32"
# SHARE_DIRECTORY:  Directory share between local user and the vagrant VM containing a copy of the current local Comptoir
declare SHARE_DIRECTORY="/vagrant/Share_directory"

# ETC_PG_DIRECTORY:  PostgreSQL server configuration folder
declare ETC_PG_DIRECTORY="/etc/postgresql/9.5/main"
# ETC_PHP_DIRECTORY:  PHP configuration folder
declare ETC_PHP_DIRECTORY="/etc/php/7.0"

# Start the timers
TOTAL_TIMER_START=$(date +%s)
APT_TIMER_START=$(date +%s)

# PREREQUISITES
apt-get update

## Ubuntu packages
echo "===== Ubuntu packages"
apt-get install -yqq \
    apache2 \
    git \
    libapache2-mod-php \
    libicu55 \
    php \
    php-curl \
    php-intl \
    php-mbstring \
    php-pgsql \
    php-xml \
    postgresql \
    unzip \
    zlib1g

## Ubuntu packages for devOps
apt-get install -yqq \
    ccze \
    tree \
    htop

# extra shell tools
# - bat: a cat clone with syntax highlighting and Git integration
echo "---------> INSTALL extra shell tools (bat)"
wget --no-verbose  https://github.com/sharkdp/bat/releases/download/v0.13.0/bat_0.13.0_amd64.deb
dpkg -i bat_0.13.0_amd64.deb
rm bat_0.13.0_amd64.deb

## Compute the execution time of the apt-get command
APT_TIMER_END=$(date +%s)
APT_TIMER_DIFF=$((${APT_TIMER_END} - ${APT_TIMER_START}))
APT_TIMER=$(awk -v t=${APT_TIMER_DIFF} 'BEGIN{t=int(t*1000); printf "%d:%02d:%02d\n", t/3600000, t/60000%60, t/1000%60}')
echo "---------> APT INSTALL duration: ${APT_TIMER}"

## PHP > timezone + max_upload
for i in apache2 cli; do
    echo "date.timezone = \"Europe/Paris\"" >>"${ETC_PHP_DIRECTORY}/${i}/conf.d/comptoir_du_libre.ini"
    echo "upload_max_filesize = 2M" >>"${ETC_PHP_DIRECTORY}/${i}/conf.d/comptoir_du_libre.ini"
    if [[ "${DEBUG}"  == true ]]; then
        echo "display_errors  = On" >>"${ETC_PHP_DIRECTORY}/${i}/conf.d/comptoir_du_libre.ini"
        echo "error_reporting = E_ALL" >>"${ETC_PHP_DIRECTORY}/${i}/conf.d/comptoir_du_libre.ini"
    fi
done

## Install Composer
echo "===== Install Composer"
cd /usr/local/bin
wget --no-verbose  https://getcomposer.org/composer-1.phar
mv composer-1.phar composer
chmod +x composer

	# OLD command line ---> but install composer 2.x, instead off composer 1.x
	# php -r "readfile('https://getcomposer.org/installer');" |
	#    sudo php -- --install-dir=/usr/local/bin --filename=composer

## Configure locale
sudo locale-gen fr_FR.UTF-8

## Configure TimeZone
sudo timedatectl set-timezone Europe/Paris

## Postgres authentication
echo "===== Postgres authentication"
sudo sed -i 's/local   all             all                                     peer/local   all             all                                     password/' "${ETC_PG_DIRECTORY}/pg_hba.conf"

sudo service postgresql restart

## Create system user
echo "===== Create system user"
useradd -d "${COMPTOIR_HOME}" -m -s /bin/bash comptoir

# Set Permissions to user Comptoir
echo "===== Set Permissions to user Comptoir"
cd /home
sudo chown -R comptoir:comptoir comptoir/
sudo usermod -aG www-data comptoir

# Set Permissions to user Vagrant
echo "===== Set Permissions to user Vagrant"
sudo usermod -aG comptoir vagrant
sudo usermod -aG www-data vagrant

## Isolate datasets from git repository
echo "===== Isolate datasets from git repository"
sudo -u comptoir cp -r "${COMPTOIR_DIR}/tests/Datasets/" "${COMPTOIR_DIR_DATASET}/"
sudo -u comptoir cp "${COMPTOIR_DIR}/bin/COMPTOIR_import_DB_data_AND_images.sh" "${COMPTOIR_DIR_DATASET}/"
sudo -u comptoir mkdir -p "${COMPTOIR_DIR_DATASET}/config/SQL/" && cp "${COMPTOIR_DIR}/config/SQL/COMPTOIR_DB_truncate_tables.sql" "${COMPTOIR_DIR_DATASET}/config/SQL/"

## OPTIONAL Installation Comptoir: set the COMPTOIR_VERSION to install an older version
echo "===== OPTIONAL Installation Comptoir"
if [[ -n "${COMPTOIR_REVISION}" ]]; then
    cd "${COMPTOIR_DIR}"
    sudo -u comptoir git checkout "${COMPTOIR_REVISION}"
fi

## POSTGRESQL Create user
echo "===== POSTGRESQL Create user"
sudo -u postgres psql -c "CREATE USER comptoir WITH PASSWORD 'comptoir'"

## POSTGRESQL - Allow access outside the virtual machine
sudo sed -i -e "s/listen_addresses = 'localhost'/listen_addresses = '*'/" "${ETC_PG_DIRECTORY}/postgresql.conf"
sudo sed -i -e "s/#listen_addresses = /listen_addresses = /" "${ETC_PG_DIRECTORY}/postgresql.conf"
sudo echo "host  all  all  10.0.2.2/32  md5" >>"${ETC_PG_DIRECTORY}/pg_hba.conf"
sudo service postgresql restart

## POSTGRESQL Set .pgpass file
echo "===== POSTGRESQL Set .pgpass file"
COMPTOIR_PGPASSFILE="${COMPTOIR_HOME}/.pgpass"
VAGRANT_PGPASSFILE="${VAGRANT_HOME}/.pgpass"
sudo -u comptoir touch "${COMPTOIR_PGPASSFILE}"
sudo -u comptoir chmod 0600 "${COMPTOIR_PGPASSFILE}"
sudo -u comptoir cat >"${COMPTOIR_PGPASSFILE}" <<EOF
# hostname:port:database:username:password
localhost:5432:comptoir:comptoir:comptoir
EOF
sudo cp "${COMPTOIR_PGPASSFILE}" "${VAGRANT_PGPASSFILE}"
sudo chown vagrant:vagrant "${VAGRANT_PGPASSFILE}"

## POSTGRESQL Create DB
echo "===== POSTGRESQL Create DB"
sudo "${COMPTOIR_DIR}/bin/COMPTOIR_create_DB_database_and_set_ownership.sh" -d "${COMPTOIR_DIR}"

## POSTGRESQL Create tables and procedures
echo "===== POSTGRESQL Create tables and procedures"
sudo -u comptoir "${COMPTOIR_DIR}/bin/COMPTOIR_create_DB_tables_and_procedures.sh" -d "${COMPTOIR_DIR}"

## PREREQUISITES for unitTest and debugKit
########################################################################################################################
if [[ "${DEBUG}"  == true ]]; then
    ### POSTGRESQL Create DB for UnitTests
    echo "===== POSTGRESQL Create DB for Unit tests"
    sudo -u postgres psql -c "CREATE DATABASE tests WITH TEMPLATE = template0;"
    sudo -u postgres psql -c "ALTER DATABASE tests OWNER TO comptoir;"
    sudo -u postgres psql -d tests -f "${COMPTOIR_DIR}/config/SQL/COMPTOIR_DB_For_Tests.sql"

    ### POSTGRESQL Set the DB name for phpUnit
    declare DB_TEST_DATABASE="tests"

    echo "===== Install xdebug"
    sudo apt-get install -yqq php-xdebug

    ### According to the config.vm.hostname setting in Vagrantfile
    declare DB_TEST_HOST="comptoir-vagrant"

    ### POSTGRESQL Create DB for DebugKit
    echo "===== POSTGRESQL Create DB for DebugKit"
    sudo -u postgres psql -c "CREATE DATABASE debug_kit WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8';"
    sudo -u postgres psql -c "ALTER DATABASE debug_kit OWNER TO comptoir;"

    ### POSTGRESQL Set the DB name for phpUnit
    declare DB_DEBUGKIT_DATABASE="debug_kit"
fi
########################################################################################################################

##### Composer
echo "===== composer plugin that downloads packages in parallel"
sudo -i -u comptoir /bin/bash -c \
    "cd \"${COMPTOIR_DIR}\" ; /usr/local/bin/composer global require hirak/prestissimo"

##  save the hash of the composer files before installation via composer
INFO_FILE=${VAGRANT_HOME}/91_composer.sha256
echo "--- Before installation via composer ----" >> ${INFO_FILE}
sudo sha256sum ${COMPTOIR_DIR}/composer.*        >> ${INFO_FILE} && echo ''

## Composer install Comptoir
COMPOSER_TIMER_START=$(date +%s)
echo "===== Composer install Comptoir [ COMPOSER_UPDATE  ${COMPOSER_UPDATE} ] "
if [[ "${COMPOSER_UPDATE}" == true ]]; then
    echo "---------------> composer UPDATE" >> ${INFO_FILE}
    echo "---------------> composer UPDATE"
    sudo -i -u comptoir /bin/bash -c \
        "cd \"${COMPTOIR_DIR}\" ; /usr/local/bin/composer --no-progress --no-suggest update"
fi
echo "---------------> composer INSTALL"
sudo -i -u comptoir /bin/bash -c "cd \"${COMPTOIR_DIR}\" ; /usr/local/bin/composer --no-progress --no-suggest install"

## Compute the execution time of the composer command (update and install)
COMPOSER_TIMER_END=$(date +%s)
COMPOSER_TIMER_DIFF=$((${COMPOSER_TIMER_END} - ${COMPOSER_TIMER_START}))
COMPOSER_TIMER=$(awk -v t=${COMPOSER_TIMER_DIFF} 'BEGIN{t=int(t*1000); printf "%d:%02d:%02d\n", t/3600000, t/60000%60, t/1000%60}')
echo "---------> COMPOSER duration: ${COMPOSER_TIMER}"

##  save the hash of the composer files after installation via composer
echo "--- After installation via composer ----" >> ${INFO_FILE}
sudo sha256sum ${COMPTOIR_DIR}/composer.*        >> ${INFO_FILE} && echo ''

## Optimise composer cache for comptoir user
echo "===== COMPOSER - Optimise composer cache for comptoir user"
mkdir -p "${COMPTOIR_HOME}/.composer"
cp -r  "${COMPTOIR_HOME}/.cache/composer" "${COMPTOIR_HOME}/.composer/cache"
chown -R comptoir  "${COMPTOIR_HOME}/.composer"
chgrp -R comptoir  "${COMPTOIR_HOME}/.composer"
    ###############################################################
    # For vagrant user, via 'sudo -i -u comptoir /bin/bash -c (...)'
    # composer cache directory is : ${COMPTOIR_HOME}/.cache/composer
    #
    # For comptoir user, via 'sudo su comptoir'
    # composer cache directory is : ${COMPTOIR_HOME}/.composer/cache
    ###############################################################


## APP.PHP Define Email Transport
### Something to do for smtp

## APP.PHP Configure database credentials
echo "===== APP.PHP Configure database credentials"
if [[ "${DEBUG}"  == true ]]; then
    sudo -u comptoir sed -i -e "s/\/\/COMPTOIR-DEBUG//" "${COMPTOIR_DIR}/config/app.php"
    sudo -u comptoir sed -i -e "s/env('DB_TEST_DATABASE', 'comptoir')/env('DB_TEST_DATABASE', '${DB_TEST_DATABASE}')/" "${COMPTOIR_DIR}/config/app.php"

    #  display all PHP errors
    sudo -u comptoir sed -i -e "s/'errorLevel' => E_ALL & ~E_DEPRECATED,/'errorLevel' => E_ALL,/" "${COMPTOIR_DIR}/config/app.php"
fi
sudo -u comptoir sed -i -e "s/__SALT__/somerandomsalt/" "${COMPTOIR_DIR}/config/app.php"
sudo -u comptoir sed -i -e "s/'php',/env('SESSION_DEFAULTS', 'php'),/" "${COMPTOIR_DIR}/config/app.php"
sudo -u comptoir sed -i -e "s/'className' => 'Smtp'/'className' => 'Debug'/" "${COMPTOIR_DIR}/config/app.php"
# sudo -u comptoir sed -i -e "s/'host' => 'localhost',/'host' => 'smtp',/" "${COMPTOIR_DIR}/config/app.php"
sudo -u comptoir sed -i -e "s/'from' => 'barman@comptoir-du-libre.org',/'from' => 'barman-DEV@comptoir-du-libre.org',/" "${COMPTOIR_DIR}/config/app.php"

## COMPTOIR.PHP Configure
echo "===== COMPTOIR.PHP Configure"
sudo -u comptoir cp "${COMPTOIR_DIR}/config/comptoir.default.php" "${COMPTOIR_DIR}/config/comptoir.php"

## Codeception : fix browser URL
sudo -u comptoir sed -i -e "s/localhost:8080/localhost:80/" "${COMPTOIR_DIR}/tests/Acceptance.suite.yml"

## PHP - Adding phpinfo() file
if [[ "${DEBUG}"  == true ]]; then
    echo "===== Adding phpinfo.php"
    echo "<?php phpinfo();" >"${COMPTOIR_DIR}/webroot/debug_phpinfo.php"
fi

## FILES - Changing file access rights
echo "===== FILES - Changing file access rights"
cd ${COMPTOIR_HOME}
chmod -R o-w Comptoir-srv/*
for i in webroot logs tmp; do
    chown -R www-data "Comptoir-srv/${i}"
    chgrp -R comptoir "Comptoir-srv/${i}"
    find "Comptoir-srv/$i" -type d -exec sudo chmod 775 {} \;
    find "Comptoir-srv/$i" -type f -exec sudo chmod 664 {} \;
done

# LOGS Files
echo "===== LOGS Files"
echo "" >"${COMPTOIR_DIR}/logs/debug.log"
echo "" >"${COMPTOIR_DIR}/logs/error.log"
chmod 664 -v "${COMPTOIR_DIR}/logs/debug.log"
chmod 664 -v "${COMPTOIR_DIR}/logs/error.log"
chown -v www-data "${COMPTOIR_DIR}/logs/debug.log"
chown -v www-data "${COMPTOIR_DIR}/logs/error.log"
chgrp -v comptoir "${COMPTOIR_DIR}/logs/debug.log"
chgrp -v comptoir "${COMPTOIR_DIR}/logs/error.log"

## APACHE Vhost preparation
echo "127.0.0.1	comptoir.example.com" >> /etc/hosts

## APACHE Vhost creation "comptoir.example.com"
echo "===== APACHE Vhost creation comptoir.example.com"
cat >/etc/apache2/sites-available/comptoir-srv.conf <<EOF
<VirtualHost *:80>
    ServerName comptoir.example.com
    ServerAdmin webmaster@localhost
    DocumentRoot ${COMPTOIR_DIR}/webroot/

EOF

{
    # shellcheck disable=SC2016
    echo '    ErrorLog ${APACHE_LOG_DIR}/comptoir.example.com.log'
    # shellcheck disable=SC2016
    echo '    CustomLog ${APACHE_LOG_DIR}/comptoir.example.com.log combined'
} >>/etc/apache2/sites-available/comptoir-srv.conf

cat >>/etc/apache2/sites-available/comptoir-srv.conf <<EOF

    <Directory "${COMPTOIR_DIR}/webroot/">
        AllowOverride All
    </Directory>

    <Location />
        Require all granted
    </Location>
</VirtualHost>
EOF

## APACHE enable mod_rewrite and new vhosts
echo "===== APACHE enable mod_rewrite and new vhosts"
sudo a2enmod rewrite
sudo a2enmod headers
sudo a2ensite comptoir-srv.conf
sudo service apache2 reload
sudo a2dissite 000-default.conf
sudo service apache2 reload

## Easy consultation of PHP and Apache logs
echo "===== Easy consultation of PHP and Apache logs"
sudo usermod -aG adm comptoir
sudo usermod -aG adm vagrant
sudo touch /var/log/apache2/comptoir.example.com.log
sudo chgrp adm /var/log/apache2/comptoir.example.com.log
sudo ln -s /var/log/apache2/comptoir.example.com.log "${VAGRANT_HOME}/80_LOG_Apache_comptoir.log"
sudo ln -s /var/log/apache2/comptoir.example.com.log "${COMPTOIR_HOME}/80_LOG_Apache_comptoir.log"
sudo ln -s "${COMPTOIR_DIR}/logs/" "${COMPTOIR_HOME}/70_LOG_php_comptoir"
sudo ln -s "${COMPTOIR_DIR}/logs/" "${VAGRANT_HOME}/70_LOG_php_comptoir"

## Puts the data files back in place
echo "===== DATA FILES back in place"
if [[ -n "${COMPTOIR_REVISION}" ]]; then
    sudo -u comptoir mkdir -p "${COMPTOIR_DIR_DATASET}/tests/Datasets" && cp -r "${COMPTOIR_DIR_DATASET}/Dataset02" "${COMPTOIR_DIR}/tests/Datasets/"
    sudo -u comptoir mkdir -p "${COMPTOIR_DIR}/bin/" && cp "${COMPTOIR_DIR_DATASET}/COMPTOIR_import_DB_data_AND_images.sh" "${COMPTOIR_DIR}/bin/"
    sudo -u comptoir mkdir -p "${COMPTOIR_DIR}/config/SQL/" && cp "${COMPTOIR_DIR_DATASET}/config/SQL/COMPTOIR_DB_truncate_tables.sql" "${COMPTOIR_DIR}/config/SQL/"
fi

## INSERT demo data
echo "===== COMPTOIR: Import data"

sudo -u comptoir "${COMPTOIR_DIR}/bin/COMPTOIR_import_DB_data_AND_images.sh" \
    -d "${COMPTOIR_DIR}" \
    -t "${DATASET_TIMESTAMP}" \
    -h localhost \
    -p "${COMPTOIR_DIR}/tests/Datasets/Dataset02" \
    --no-sudo

echo "===== IMG FILES: fix unix ACL (dev only)"
sudo find "${COMPTOIR_DIR}/webroot/img/files/" -type d -exec chmod 777 {} \;
sudo find "${COMPTOIR_DIR}/webroot/img/files/" -type f -exec chmod 666 {} \;

## Database migration
echo "===== COMPTOIR: database migration"
cd "${VAGRANT_HOME}"
sudo -u vagrant "00_bin/32_bin-cake_migration.migrate.sh"

## Database migration
echo "===== COMPTOIR: database ---> import last data"
cd "${VAGRANT_HOME}"
sudo -u vagrant "00_bin/12_psql_import_SQL.files_during.vagrantUp.sh"

##  Backup softwares versions
INFO_FILE=${VAGRANT_HOME}/90_softwares-version.txt
echo "--- OS -------------" >>${INFO_FILE} &&
    cat /etc/os-release >>${INFO_FILE} && echo '' &&
    echo "--- PostgreSQL ------" >>${INFO_FILE} &&
    psql --version >>${INFO_FILE} && echo '' &&
    echo "---  Apache -----------" >>${INFO_FILE} &&
    apache2 -v >>${INFO_FILE} &&
    echo "--- PHP ---------" >>${INFO_FILE} && echo '' &&
    php --version >>${INFO_FILE}

## Compute the execution time
TOTAL_TIMER_END=$(date +%s)
TOTAL_TIMER_DIFF=$((${TOTAL_TIMER_END} - ${TOTAL_TIMER_START}))
TOTAL_TIMER=$(awk -v t=${TOTAL_TIMER_DIFF} 'BEGIN{t=int(t*1000); printf "%d:%02d:%02d\n", t/3600000, t/60000%60, t/1000%60}')
echo "--------------------------------------------------"
echo "---------> APT INSTALL duration: ${APT_TIMER}"
echo "---------> COMPOSER duration: ${COMPOSER_TIMER}"
echo "---------> TOTAL TIME: ${TOTAL_TIMER}"
echo "--------------------------------------------------"
