# Vagrant: Vagrant_for_Comptoir_local

This virtual machine (VM) is used when developing Comptoir.
By default we set the host name to `comptoir-vagrant`

It based on the source code from the developer's host.
Run it to show modifications you worked on or to play tests.

## Prerequisite
- Vagrant + VirtualBox
- Don't install Vagrant and VirtualBox with apt-get.
  -  @@@TODO https://www.vagrantup.com/downloads.html

## Usage
```bash
# To be executed in the directory containing the vagrantfile

# Creates and configures guest machines according to the Vagrantfile
vagrant destroy -f  # stops the running machine Vagrant and destroys all resources
vagrant up

# Watches all local directories of any "rsync synced folders" (see Vagranfile)
# and automatically initiates an rsync transfer when changes are detected.
vagrant rsync-auto
    # This command does not exit until an interrupt is received.
    # start the same process in the background
    vagrant rsync-auto &

# SSH into the running Vagrant machine and give you access to a shell
vagrant ssh

# Stops the running machine Vagrant and destroys all resources
# that were created during the machine creation process.
vagrant destroy -f
```

## URLs
- Web site: http://localhost:8282
- MailHog Webmail: http://localhost:8025 `(1)`
- PHP info: http://localhost:8282/debug_phpinfo.php

`(1)` MailHog must be activated before by the following command line:
 ```bash
  vagrant ssh -c '00_bin/80_mailHog.sh'   # MailHog is running in the virtual machine
 ```

### Web site users

10 additional users (for testing purposes) are added.

All of these users have the same password: `comptoir`
* 5 users with `User` role:
    * `dev-user_administration@comptoir-du-libre.org`   ---> type: `Administration`
    * `dev-user2_administration@comptoir-du-libre.org` ---> type: `Administration`
    * `dev-user_person@comptoir-du-libre.org` ---> type: `Person`
    * `dev-user_company@comptoir-du-libre.org`  ---> type: `Company`
    * `dev-user_association@comptoir-du-libre.org` ---> type: `Association`
* 5 users with `adminr` role:
    * `dev-admin_administration@comptoir-du-libre.org` ---> type: `Administration`
    * `dev-admin2_administration@comptoir-du-libre.org` ---> type: `Administration`
    * `dev-admin_person@comptoir-du-libre.org` ---> type: `Person`
    * `dev-admin_company@comptoir-du-libre.org` ---> type: `Company`
    * `dev-admin_association@comptoir-du-libre.org` ---> type: `Association`

see : `./bin_vagrant-user/import_SQL.files_during.vagrantUp/03*.sql` files


## Vagrant user
```bash
vagrant ssh      # "vagrant"  Linux user of virtual machine
sudo su comptoir # "comptoir" Linux user of virtual machine
```

## Listening Ports

| Service               |    Port VM   |      Port Host |
| :-------------------- | :----------: | -------------: |
| Web server            |   80         |       **8282** |
| PostgreSQL            |   5432       |      **5439**  |
| MailHog Webmail `(1)` |   8025       |      **8025**  |

### MailHog `(1)`

MailHog is a email testing tool for developers (SMTP server + webmail).

You have to start it using one of the following command lines:
 ```bash
  # MailHog is running in the virtual machine
  vagrant ssh -c '00_bin/80_mailHog.sh'

  # MailHog is running in another host (docker, another VM, ...)
  vagrant ssh -c '00_bin/80_mailHog.sh -i' # interactive mode
  vagrant ssh -c '00_bin/80_mailHog.sh -s <host> -p <smtpPort> -g <webmailPort>'
        # For this, you can use the official MailHog docker image:
        docker run  -p 1026:1025 -p 8026:8025 -t mailhog/mailhog
 ```


## Usefull scripts, log files and directories

```bash
# /home/vagrant
####################################################################
vagrant ssh

# Log files
cat 90_softwares-version.txt           # Sofwares version (PHP, Apache, PostgreSQL, Ubuntu)
cat 91_composer.sha256                 # Checksum of the composer files before and after installation of the VM
tail -f 80_LOG_Apache_comptoir.log     # Apache log
tail -f 70_LOG_php_comptoir/*          # PHP log  (debug + error)
tail -f 70_LOG_php_comptoir/debug.log  # PHP debug log
tail -f 70_LOG_php_comptoir/error.log  # PHP error log

# Usefull scripts
00_bin/10_psql_access.to.the.database.sh      # PSQL command line
00_bin/20_database_reload.sh                  # Reload the database with the same dataset as when the VM was created
00_bin/32_bin-cake_migration.migrate.sh       # Apply database upgrade
00_bin/33_bin-cake_cache.clear.sh             # Clear all cache

# bin/cake
00_bin/30_bin-cake.sh                         # bin/cake help
00_bin/30_bin-cake.sh routes                  # show all routes
00_bin/30_bin-cake.sh cache clear_all         # clear all caches
00_bin/30_bin-cake.sh cache list_prefixes     # list all caches
00_bin/30_bin-cake.sh orm_cache clear         # clear ORM cache
00_bin/30_bin-cake.sh orm_cache build         # build ORM cache
00_bin/30_bin-cake.sh migrations              # help for migrations command line (db)
00_bin/30_bin-cake.sh migrations status       # show migration status
00_bin/30_bin-cake.sh migrations migrate      # launch the migration (db)
00_bin/30_bin-cake.sh plugin loaded           # show all loaded plugins
00_bin/30_bin-cake.sh i18n                    # i18n command line tools
00_bin/30_bin-cake.sh bake  --help            # help for bake command line
00_bin/30_bin-cake.sh bake controller         # generate controller skeleton code
00_bin/30_bin-cake.sh bake model              # generate table and entity classes  skeleton code
00_bin/30_bin-cake.sh benchmark --n 100  http://localhost/  # run 100 request

# Composer and 'vendor' directory
00_bin/40_vendor_erase(...).sh            # delete 'vendor/', delete 'composer.lock' file and launch 'composer install'
00_bin/41_vendor_erase.dir_and.install.sh # delete 'vendor/', keep   'composer.lock' file and launch 'composer install'
00_bin/42_vendor_update.sh                # launch 'composer update'
00_bin/43_vendor_update.and.install.sh    # launch 'composer update' and 'composer install'

# PhpUnit (unit tests)
00_bin/50_TU_launch.unit.tests.sh --help
00_bin/50_TU_launch.unit.tests.sh --debug                       # Display debugging information during test execution
00_bin/50_TU_launch.unit.tests.sh --no-configuration            # Ignore default configuration file (phpunit.xml).
00_bin/50_TU_launch.unit.tests.sh --coverage-text               # Show code coverage report in standard output
00_bin/50_TU_launch.unit.tests.sh --coverage-text==logs/...     # Generate code coverage report in text format. (1)
00_bin/50_TU_launch.unit.tests.sh --testdox-html=logs/doc.html  # Write documentation in HTML format to file    (1)
00_bin/50_TU_launch.unit.tests.sh --testdox-text=logs/doc.txt   # Write documentation in Text format to file    (1)
00_bin/50_TU_launch.unit.tests.sh --list-groups                 # List available test groups.
00_bin/50_TU_launch.unit.tests.sh --list-suites                 # List available test suites.
00_bin/50_TU_launch.unit.tests.sh --testsuite <name>            # Filter which testsuite to run.
00_bin/50_TU_launch.unit.tests.sh --filter=UserName             # Filter which tests to run.
00_bin/50_TU_launch.unit.tests.sh --group=default               # Only runs tests from the specified group(s). (2)
00_bin/50_TU_launch.unit.tests.sh --exclude-group=default       # Exclude tests from the specified group(s).   (2)

                                                    #  (1) logs/ directory = /home/vagrant/70_LOG_php_comptoir/
                                                    #  (2) see @group annotation
# Codeception (functional tests)
00_bin/60_TF_launch.functional.tests.sh                         # help for 'php vendor/bin/codecept'
00_bin/60_TF_launch.functional.tests.sh run     --help          # help for 'php vendor/bin/codecept run'
00_bin/60_TF_launch.functional.tests.sh dry-run --help          # help for 'php vendor/bin/codecept dry-run'
00_bin/60_TF_launch.functional.tests.sh run                     # run functional tests
00_bin/60_TF_launch.functional.tests.sh run --report            # run functional tests and show only report
00_bin/60_TF_launch.functional.tests.sh run --group=search      # run functional tests for 'search' group (2)
00_bin/60_TF_launch.functional.tests.sh run --skip-group=search # run functional tests except for 'search' group (2)
00_bin/60_TF_launch.functional.tests.sh dry-run Acceptance      #  Prints step-by-step scenario-driven test
00_bin/60_TF_launch.functional.tests.sh dry-run Acceptance CheckSearchCest # ... but only for 'CheckSearchCest' tests

# MailHog (email testing tool for developers)
00_bin/80_mailHog.sh     # run  MailHog in the virtual machine              + update webapp config
00_bin/80_mailHog.sh -q  # stop MailHog in the virtual machine              + update webapp config
00_bin/80_mailHog.sh -i  # interactive mode to use external MailHog server  + update webapp config
00_bin/80_mailHog.sh -h  # display documentation

# Backup VM directories: src, tests, config
90_bakcup_src.test-directories.sh  # see backup files in "<vagrantHostDirectory>/data"


# /home/comptoir
####################################################################
sudo su comptoir
cd ~
ls
    Comptoir-srv  # PHP application directory
    Dataset       # Dataset directory
```

## PHP config

Files : `/etc/php/7.0/*/conf.d/comptoir_du_libre.ini`

| setting       |    Value     |
| :------------ | :-------------: |
| display_errors    |    On     |
| error_reporting   |   E_ALL   |

The `error_reporting` parameter is overwritten by Cake in the `config/app.php` file.
Currently, it is the same value.

## PostgreSQL

### Tools

- [pgAdmin](https://www.pgadmin.org/)
- IDE: IntelliJ IDEA, ..
- CLI: `psql`
  - `sudo apt-get install postgresql-client`

### Database
- comptoir
- test
- ?

### From the virtual machine
after `vagrant ssh`

```bash
# Database super-user  (no password is required)
sudo -u postgres psql

# Database "comptoir" user (no password is required)
sudo -u comtpoir psql  # without /home/vagrant/.pgpass file
psql -U comptoir       # with    /home/vagrant/.pgpass file

    # Or more quickly (no password is required):
    00_bin/10_psql_access.to.the.database.sh
```

or
```bash
sudo su comptoir
cd ~
psql
    # no password is required
    # see: /home/comptoir/.pgpass
```

### From the host machine.

```bash
# server: 127.0.0.1
# port:   5439
# user:   comptoir
# pass:   comptoir
psql -h 127.0.0.1 -p 5439 -U comptoir
```

If you don't want to enter the password,
you can create your `/home/<user>/.pgpass` file.
```bash
# hostname:port:database:username:password
localhost:5439:comptoir:comptoir:comptoir
```

