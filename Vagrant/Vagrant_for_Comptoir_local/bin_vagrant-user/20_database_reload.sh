#!/usr/bin/env bash

# stop at first error
set -e

VAGRANT_HOME="/home/vagrant"

# delete tmp files
cd /home/comptoir/Comptoir-srv/
sudo rm -rfv "/home/comptoir/Comptoir-srv/webroot/img/files-tmp/"

# database reload
sudo -u comptoir bin/COMPTOIR_import_DB_data_AND_images.sh    \
-d /home/comptoir/Comptoir-srv/                               \
-p /home/comptoir/Comptoir-srv/tests/Datasets/Dataset02/      \
-t 2018-09-12-16h13m32                                        \
-h localhost                                                  \
--no-sudo

# Import some SQL files in database
bash "${VAGRANT_HOME}/00_bin/12_psql_import_SQL.files_during.vagrantUp.sh"

# add image files
sudo chown -R comptoir "/home/comptoir/Comptoir-srv/webroot/img"
sudo chgrp -R www-data "/home/comptoir/Comptoir-srv/webroot/img"
sudo find "/home/comptoir/Comptoir-srv/webroot/img/$i" -type d -exec sudo chmod 775 {} \;
sudo find "/home/comptoir/Comptoir-srv/webroot/img/$i" -type f -exec sudo chmod 664 {} \;

# delete tmp files
sudo rm -rfv "/home/comptoir/Comptoir-srv/webroot/img/files-tmp"
