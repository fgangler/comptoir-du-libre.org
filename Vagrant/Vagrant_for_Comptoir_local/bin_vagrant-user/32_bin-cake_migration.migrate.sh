#!/usr/bin/env bash

# stop at first error
set -e

#  use /home/comptoir/Comptoir-srv/bin/cake
cd /home/comptoir/Comptoir-srv/
sudo -u comptoir bin/cake migrations migrate;
sudo -u comptoir bin/cake migrations status;

