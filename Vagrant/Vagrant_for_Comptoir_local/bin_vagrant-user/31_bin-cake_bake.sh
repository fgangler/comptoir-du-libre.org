#!/usr/bin/env bash

# stop at first error
set -e

#  use /home/comptoir/Comptoir-srv/bin/cake
cd /home/comptoir/Comptoir-srv/
sudo -u comptoir bin/cake bake ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10};

# sudo -u comptoir bin/cake bake all
# sudo -u comptoir bin/cake bake behavior
# sudo -u comptoir bin/cake bake cell
# sudo -u comptoir bin/cake bake component
# sudo -u comptoir bin/cake bake controller
# sudo -u comptoir bin/cake bake fixture
# sudo -u comptoir bin/cake bake form
# sudo -u comptoir bin/cake bake helper
# sudo -u comptoir bin/cake bake mailer
# sudo -u comptoir bin/cake bake migration
# sudo -u comptoir bin/cake bake migration_snapshot
# sudo -u comptoir bin/cake bake model
# sudo -u comptoir bin/cake bake plugin
# sudo -u comptoir bin/cake bake shell
# sudo -u comptoir bin/cake bake shell-helper
# sudo -u comptoir bin/cake bake template
# sudo -u comptoir bin/cake bake test
