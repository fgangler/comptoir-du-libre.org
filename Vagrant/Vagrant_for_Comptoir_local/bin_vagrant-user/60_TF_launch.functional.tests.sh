#!/usr/bin/env bash

# stop at first error
set -e

VAGRANT_HOME="/home/vagrant"

# disable debug mode
bash "${VAGRANT_HOME}/00_bin/00_switch_debug_FALSE.sh"

# launch functional tests
cd /home/comptoir/Comptoir-srv/
sudo -u comptoir php vendor/bin/codecept ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10};

# enable debug mode
bash "${VAGRANT_HOME}/00_bin/01_switch_debug_TRUE.sh"
