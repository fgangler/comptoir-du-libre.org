#!/usr/bin/env bash

# stop at first error
set -e

# COMPTOIR_HOME: Absolute directory containing the Comptoir-srv, *without* trailing slash, eg "/home/comptoir"
declare COMPTOIR_HOME="/home/comptoir"
# COMPTOIR_DIR:  Absolute directory to Comptoir-srv, *without* trailing slash, eg "/home/comptoir/comptoir-srv"
declare COMPTOIR_DIR="${COMPTOIR_HOME}/Comptoir-srv"
# VENDOR_DIR:  Absolute directory to vendor, *without* trailing slash, eg "/home/comptoir/comptoir-srv/vendor"
declare VENDOR_DIR="${COMPTOIR_DIR}/vendor"
# COMPOSER_LOCK_FILE:  Absolute path to composer.lock file, *without* trailing slash, eg "/home/comptoir/comptoir-srv/composer.lock"
declare COMPOSER_LOCK_FILE="${COMPTOIR_DIR}/composer.lock"
# COMPOSER_JSON_FILE:  Absolute path to composer.json file, *without* trailing slash, eg "/home/comptoir/comptoir-srv/composer.json"
declare COMPOSER_JSON_FILE="${COMPTOIR_DIR}/composer.json"

#  Backup old composer files (.json + .lock)
#########################################################
NOW=$(date +%Y.%m.%d_%H.%M.%S)
BACKUP_DIR="/vagrant_data/composer.update_${NOW}"
mkdir -p "${BACKUP_DIR}"
cp "${COMPOSER_JSON_FILE}" "${BACKUP_DIR}/composer.json.old"
if [[ -f "${COMPOSER_LOCK_FILE}" ]]; then
    cp "${COMPOSER_LOCK_FILE}" "${BACKUP_DIR}/composer.lock.old"
fi

#  composer update + composer install
#########################################################

echo "----> composer update"
sudo -i -u comptoir /bin/bash -c \
    "cd \"${COMPTOIR_DIR}\" ; /usr/local/bin/composer --no-suggest update"

echo "----> composer install"
sudo -i -u comptoir /bin/bash -c \
    "cd \"${COMPTOIR_DIR}\" ; /usr/local/bin/composer --no-suggest install"


#  Backup new composer files (.json + .lock)
#########################################################
cp "${COMPOSER_JSON_FILE}" "${BACKUP_DIR}/composer.json"
if [[ -f "${COMPOSER_LOCK_FILE}" ]]; then
    cp "${COMPOSER_LOCK_FILE}" "${BACKUP_DIR}/composer.lock"
fi
cd "${BACKUP_DIR}"
sha256sum * > "${BACKUP_DIR}/checksum.sha256"
cat "${BACKUP_DIR}/checksum.sha256"



