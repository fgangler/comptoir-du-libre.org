#!/usr/bin/env bash

# stop at first error
set -e

#  use /home/comptoir/Comptoir-srv/bin/cake
cd /home/comptoir/Comptoir-srv/
sudo -u comptoir bin/cake ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10};

# sudo -u comptoir bin/cake
# sudo -u comptoir bin/cake migrations
# sudo -u comptoir bin/cake migrations status
# sudo -u comptoir bin/cake migrations migrate
# sudo -u comptoir bin/cake bake
# sudo -u comptoir bin/cake bake controller
# sudo -u comptoir bin/cake bake model
# ...
