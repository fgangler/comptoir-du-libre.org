
-- sofware: 7-Zip
UPDATE public.softwares SET sill = 1
                        WHERE id = 72;

-- sofware: LibreOffice
UPDATE public.softwares SET wikidata = 'Q10135'
                        WHERE id = 33;

-- sofware: Asqatasun
UPDATE public.softwares SET sill = 12,
                            wikidata = 'Q24026504'
                        WHERE id = 9;

-- sofware: Firefox
UPDATE public.softwares SET sill = 89,
                            cnll = 98,
                            wikidata = 'Q698',
                            framalibre = 'firefox',
                            wikipedia_en = 'Firefox',
                            wikipedia_fr = 'Mozilla_Firefox'
                        WHERE id = 82;



