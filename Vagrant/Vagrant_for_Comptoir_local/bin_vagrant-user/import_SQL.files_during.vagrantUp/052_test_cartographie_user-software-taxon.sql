
-- user: dev-admin_administration@comptoir-du-libre.org
-- ----------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------
INSERT INTO public.relationships_softwares_users (software_id, user_id, relationship_id, created, modified) VALUES (33, 90, 7,  (now() - interval '5 day'), (now() - interval '5 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (10, 33, 90, (now() - interval '5 day'), (now() - interval '5 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (12, 33, 90, now(), now());

INSERT INTO public.relationships_softwares_users (software_id, user_id, relationship_id, created, modified) VALUES (10, 90, 7,  (now() - interval '4 day'), (now() - interval '4 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (12, 10, 90, (now() - interval '4 day'), (now() - interval '4 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (13, 10, 90, (now() - interval '1 day'), (now() - interval '1 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (8, 10, 90, (now() - interval '2 day'), (now() - interval '2 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (10, 10, 90, (now() - interval '3 day'), (now() - interval '3 day'));

INSERT INTO public.relationships_softwares_users (software_id, user_id, relationship_id, created, modified) VALUES (3, 90, 7,  (now() - interval '4 day'), (now() - interval '4 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (7, 3, 90, (now() - interval '6 day'), (now() - interval '6 day'));


-- user: dev-admin2_administration@comptoir-du-libre.org
-- ----------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------
INSERT INTO public.relationships_softwares_users (software_id, user_id, relationship_id, created, modified) VALUES (33, 94, 7,  (now() - interval '7 day'), (now() - interval '7 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (12, 33, 94, (now() - interval '7 day'), (now() - interval '7 day'));

INSERT INTO public.relationships_softwares_users (software_id, user_id, relationship_id, created, modified) VALUES (10, 94, 7,  (now() - interval '8 day'), (now() - interval '8 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (13, 10, 94, (now() - interval '8 day'), (now() - interval '8 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (8, 10, 94, (now() - interval '9 day'), (now() - interval '9 day'));

INSERT INTO public.relationships_softwares_users (software_id, user_id, relationship_id, created, modified) VALUES (79, 94, 7,  (now() - interval '10 day'), (now() - interval '10 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (20, 79, 94, (now() - interval '10 day'), (now() - interval '10 day'));

INSERT INTO public.relationships_softwares_users (software_id, user_id, relationship_id, created, modified) VALUES (3, 94, 7, (now() - interval '11 day'), (now() - interval '11 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (7, 3, 94, (now() - interval '11 day'), (now() - interval '11 day'));

INSERT INTO public.relationships_softwares_users (software_id, user_id, relationship_id, created, modified) VALUES (49, 94, 7,  (now() - interval '12 day'), (now() - interval '12 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified)               VALUES (8, 49, 94,  (now() - interval '12 day'), (now() - interval '12 day'));


-- user: dev-user_administration@comptoir-du-libre.org  with comment
-- ----------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------
INSERT INTO public.relationships_softwares_users (software_id, user_id, relationship_id, created, modified) VALUES (79, 95, 7,  (now() - interval '45 day'), (now() - interval '45 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified, comment)      VALUES (20, 79, 95, (now() - interval '45 day'), (now() - interval '45 day'),
                                                                                                               'Avantages : évolutivité, robustesse
Inconvénients: nécessité de développer des surcouches Web car share est trop compliqué pour nos agents');
-- ----------------------------------------------------------------------------------------------------------------


-- user: dev-admin_administration@comptoir-du-libre.org  with comment
-- ----------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------------
INSERT INTO public.relationships_softwares_users (software_id, user_id, relationship_id, created, modified) VALUES (79, 90, 7,  (now() - interval '30 day'), (now() - interval '30 day'));
INSERT INTO public.taxonomys_softwares (taxonomy_id, software_id, user_id, created, modified, comment)      VALUES (20, 79, 90, (now() - interval '30 day'), (now() - interval '30 day'),
                                                                                                               'Il y a douze ans notre collectivité à décidé de remplacer l''ensemble des suites bureautiques du parc (environ 500 machines !). Passage oblige par openoffice puis migration vers Libre Office. Le logiciel est plus joli, les icônes sont sympas, la documentation est importante sur le site de libre office et la communauté fournie et efficace !

Nous avons d''ailleurs déjà rencontré certains "spécialistes" au Paris Open Source Summit plutôt sympas et plutôt doués ! Sinon que dire de la solution ? Évidemment  LibOff n''est pas au niveau de ... l''autre ...celui dont je ne me souviens plus du nom ;-) mais il fait largement le travail. Si vous migrez vos utilisateurs de la version proprio à la version libre la clef de la réussite est l''accompagnement (une dsi bien formée et un accompagnement des agents au changement). Dans tous les cas 95% des utilisateurs pourront basculer de l''un vers l''autre. La seule difficulté reste les 5% restant (bases access, macro ...) un peu plus de temps sera alors nécessaire !

Une chose est sûre si l''opération est réussie elle réjouira forcément la Direction des Affaires Financières');
-- ----------------------------------------------------------------------------------------------------------------
