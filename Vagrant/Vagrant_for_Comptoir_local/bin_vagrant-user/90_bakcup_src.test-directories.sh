#!/usr/bin/env bash

# stop at first error
set -e

read -p "did you stop rsync-auto? (y/n)" answer
case ${answer:0:1} in
     n|N)
        echo Backup process stopped
    ;;
esac

NOW=$(date +%Y.%m.%d_%H.%M.%S)
BACKUP_DIR="/vagrant_data/backup_${NOW}"
mkdir -p "${BACKUP_DIR}"

# backup change
cp -rf /home/comptoir/Comptoir-srv/config "${BACKUP_DIR}"
cp -rf /home/comptoir/Comptoir-srv/src    "${BACKUP_DIR}"
cp -rf /home/comptoir/Comptoir-srv/tests  "${BACKUP_DIR}"
tar -czvf "${BACKUP_DIR}/config.tar.gz"   "${BACKUP_DIR}/config"
tar -czvf "${BACKUP_DIR}/src.tar.gz"      "${BACKUP_DIR}/src"
tar -czvf "${BACKUP_DIR}/tests.tar.gz"    "${BACKUP_DIR}/tests"
rm -rf "${BACKUP_DIR}/config/"
rm -rf "${BACKUP_DIR}/src/"
rm -rf "${BACKUP_DIR}/tests/"
