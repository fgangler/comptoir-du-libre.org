#!/usr/bin/env bash

# Abort the script on errors and undbound variables
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

# Lood VM
# - stops the running machine Vagrant and destroys all resources
# - creates and configures guest machines according to the Vagrantfile
load_vm() {
    vagrant destroy -f
    vagrant up
}

# Compute Timers
compute_timers() {
    local TOTAL_TIMER_END=$(date +%s)
    local TOTAL_TIMER_DIFF=$((${TOTAL_TIMER_END} - ${TOTAL_TIMER_START}))
    local TOTAL_TIMER=$(awk -v t=${TOTAL_TIMER_DIFF} 'BEGIN{t=int(t*1000); printf "%d:%02d:%02d\n", t/3600000, t/60000%60, t/1000%60}')
    echo "--------------------------------------------------"
    echo "---------> TOTAL TIME: ${TOTAL_TIMER}"
    echo "--------------------------------------------------"
}

main() {
    load_vm
    compute_timers
}

# Start timer + start process
readonly TOTAL_TIMER_START=$(date +%s)
main
