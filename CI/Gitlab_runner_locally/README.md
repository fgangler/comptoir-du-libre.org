# Setting up a Gitlab runner locally

This vagrant file is here to create a vm on developer's machine dedicated to host a runner for gitlab-ci
(more doc on : https://fr.wikipedia.org/wiki/Vagrant)

## Install VirtualBox

On your physical machine

```
sudo apt-add-repository "deb http://download.virtualbox.org/virtualbox/debian xenial contrib"
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
sudo apt-get update
sudo apt-get install virtualbox-5.2 dkms 
```

## Install Vagrant

On your host, you need to download vagrant on: https://www.vagrantup.com/downloads.html
(We don't use the ubuntu Vagrant package repo/ppa because of some changes on key. A PPA is to be done one day, see https://github.com/hashicorp/vagrant-installers/issues/12)

```
wget https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.deb
sudo dpkg -i vagrant_2.0.3_x86_64.deb
```

Verify installation

```
vagrant
```

You should see:

```
 Usage: vagrant [options] <command> [<args>]

    -v, --version                    Print the version and exit.
    -h, --help                       Print this help.

 ...
```

## Launch the VM

From this directory, do:

```
cd Vagrant
vagrant up --provider=virtualbox
```

## Log into the VM

```
vagrant ssh
```

## Create and register the runner

This creates a runner dedicated to the project *Comptoir du Libre* (the link is made by the registration token). You **must** use `sudo`.

```
sudo gitlab-runner register \
    --non-interactive \
    --name my_local_runner \
    --url "https://gitlab.adullact.net/" \
    --registration-token LfKmMZU4vjRSdACxsZ17 \
    --tag-list "docker,ubuntu:16.04" \
    --executor docker \
    --docker-image "ubuntu:16.04" \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock"
```

## Check the runner is created

```
sudo gitlab-runner list
```

You should see:
```
Listing configured runners                         ConfigFile=/home/vagrant/.gitlab-runner/config.toml
my_local_runner                                    Executor=docker Token=123456789abcdefghijk123456789 URL=https://gitlab.adullact.net/
```

## Check the runner is registered

... on https://gitlab.adullact.net/Comptoir/Comptoir-srv/settings/ci_cd > Runner Settings

## Manage the runner

To remove a runner do:

```
sudo gitlab-runner unregister --name <runnerName>
```
