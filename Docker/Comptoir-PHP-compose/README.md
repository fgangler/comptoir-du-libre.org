# Docker image: Comptoir-PHP-compose

This image is meant to speed up Comptoir testing, having `composer install` done (this taken a few minutes).

/!\ CAUTION:

This image must be re-built and re-uploaded to Gitlab Registry each time `composer.json` or `composer.lock` is modified.

## How to build + upload image to Gitlab Docker Registry

```
GITLAB_REGISTRY="gitlab.adullact.net:4567"
PROJECT="/comptoir/comptoir-srv"
IMAGE="comptoir-php-compose"
TAG="v1.0.0-rc.1"

docker login "${GITLAB_REGISTRY}"
docker build -t "${GITLAB_REGISTRY}${PROJECT}/${IMAGE}:${TAG}" .
docker images               # check that the image has been built.
docker push "${GITLAB_REGISTRY}${PROJECT}/${IMAGE}:${TAG}"
```
