# Docker image: Comptoir-PHP

This image is used to save time when building the actual image for Comptoir app.

## How to build + upload image to Gitlab Docker Registry

```
docker login gitlab.adullact.net:4567
docker build -t gitlab.adullact.net:4567/comptoir/comptoir-srv/comptoir-php:v1.0.0-rc.10 .
docker images
```

check that the image has been built.

```
docker push gitlab.adullact.net:4567/comptoir/comptoir-srv/comptoir-php:v1.0.0-rc.10
```

Be careful to the tag of the version. Set up the right version you want to use.
If no tag specified, by default it will be tag `latest`.
One time the build of the image will be done directly on the gitlab.
