#!/bin/bash
# wait-for-postgres.sh

set -e

host="$1"
shift
cmd="$@"

#until PGPASSWORD=$POSTGRES_PASSWORD psql -h "$host" -U "comptoir" -c '\q'; do
until psql -h "${host}" -U "comptoir" -c '\q' 1>/dev/null 2>/dev/null ; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"
exec ${cmd}

