# -*- coding: utf-8 -*-
import time
import unittest

import settings
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


class TestUsersCreateAccount(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.display = Display(visible=0, size=(1920, 1080))
        cls.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages", "fr")
        cls.driver = webdriver.Firefox(profile)
        cls.driver.set_window_size(1920, 1080)
        cls.driver.implicitly_wait(20)
        cls.base_url = settings.COMPTOIR_URL
        cls.accept_next_alert = True

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def setUp(self):
        self.verificationErrors = []

    def tearDown(self):
        self.assertEqual([], self.verificationErrors, "Results: " + str(self.verificationErrors))

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            self.driver.save_screenshot("administration.error.message" + '.png')
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        self.driver.save_screenshot('image.png')
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def test_users_create_account_person(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        time.sleep(1)
        driver.find_element_by_link_text("S'inscrire").click()
        time.sleep(1)
        driver.find_element_by_id("user_type_id-2").click()
        time.sleep(1)
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("TEST-individu")
        time.sleep(1)
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-individu")
        time.sleep(1)
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-individu@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("TEST-individu")
        # driver.find_element_by_id("photo").clear()
        # driver.find_element_by_id("photo").send_keys("/builds/Comptoir/Comptoir-srv/tests/TestFiles/Users/correctUserAvatar.jpg")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        # self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        self.assertTrue(self.driver.find_element_by_css_selector("div.message.success"))

    def test_users_create_account_administration(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("S'inscrire").click()
        time.sleep(1)
        driver.find_element_by_id("user_type_id-1").click()
        time.sleep(1)
        driver.find_element_by_id("username").send_keys("TEST-Administration")
        time.sleep(1)
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        time.sleep(1)
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_id("confirm_password").send_keys("TEST-Administration")
        time.sleep(1)
        # driver.find_element_by_id("photo").clear()
        # driver.find_element_by_id("photo").send_keys("/builds/Comptoir/Comptoir-srv/tests/TestFiles/Users/correctUserAvatar.jpg")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))

    def test_users_create_account_company(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("S'inscrire").click()
        time.sleep(1)
        driver.find_element_by_id("user_type_id-3").click()
        time.sleep(1)
        driver.find_element_by_id("username").send_keys("TEST-MyCompany")
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.ID, "email"))
        driver.find_element_by_id("email").send_keys("mp-presta@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_id("password").clear()
        self.assertTrue(self.is_element_present(By.ID, "password"))
        driver.find_element_by_id("password").send_keys("TEST-MyCompany")
        time.sleep(1)
        driver.find_element_by_id("confirm_password").send_keys("TEST-MyCompany")
        # driver.find_element_by_id("photo").clear()
        # driver.find_element_by_id("photo").send_keys("/builds/Comptoir/Comptoir-srv/tests/TestFiles/Users/correctUserAvatar.jpg")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))

    def test_users_create_account_non_profit_user(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        time.sleep(1)
        driver.find_element_by_link_text("S'inscrire").click()
        time.sleep(1)
        driver.find_element_by_id("user_type_id-4").click()
        time.sleep(1)
        driver.find_element_by_id("username").send_keys("TEST-nonProfit")
        time.sleep(1)
        driver.find_element_by_id("email").send_keys("mp-asso@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_id("password").send_keys("TEST-nonProfit")
        time.sleep(1)
        driver.find_element_by_id("confirm_password").send_keys("TEST-nonProfit")
        # driver.find_element_by_id("photo").clear()
        # driver.find_element_by_id("photo").send_keys("/builds/Comptoir/Comptoir-srv/tests/TestFiles/Users/correctUserAvatar.jpg")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))

    def test_users_create_account_existing_user(self):
        driver = self.driver
        driver.get(self.base_url + "/users/add")
        driver.find_element_by_link_text("S'inscrire").click()
        time.sleep(1)
        driver.find_element_by_id("user_type_id-2").click()
        time.sleep(1)
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("Adullact")
        time.sleep(1)
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("Adullact")
        time.sleep(1)
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-individu@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("Adullact")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.error"))


if __name__ == "__main__":
    unittest.main()
