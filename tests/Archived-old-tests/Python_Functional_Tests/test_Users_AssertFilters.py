# -*- coding: utf-8 -*-
import unittest

import settings
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


class TestUsersAssertFilters(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.display = Display(visible=0, size=(1920, 1080))
        cls.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages", "fr")
        cls.driver = webdriver.Firefox(profile)
        cls.driver.set_window_size(1920, 1080)
        cls.driver.implicitly_wait(20)
        cls.base_url = settings.COMPTOIR_URL
        cls.accept_next_alert = True

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def setUp(self):
        self.verificationErrors = []

    def tearDown(self):
        self.assertEqual([], self.verificationErrors, "Results: " + str(self.verificationErrors))

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def test_users_assert_filters_is_using_software(self):
        driver = self.driver
        driver.get(self.base_url + "/pages")
        driver.find_element_by_link_text("Utilisateurs").click()
        Select(driver.find_element_by_name("isUserOf")).select_by_visible_text("Oui")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertEqual(35, len(driver.find_elements_by_css_selector("div.stamp.badge")))

    def test_users_assert_filters_declared_at_least_one_service_on_a_software_on_comptoir_du_libre(self):
        driver = self.driver
        driver.get(self.base_url + "/")

        driver.find_element_by_xpath("//div[@id='navbar']/nav/ul/li[2]/a").click()
        Select(driver.find_element_by_name("IsSerivicesProvider")).select_by_visible_text("Oui")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()

        self.assertEqual(11, len(driver.find_elements_by_css_selector("div.stamp.badge")))

        self.assertEqual(1, len(driver.find_elements_by_css_selector("div.stamp.badge.badgeAsso")))

        self.assertEqual(9, len(driver.find_elements_by_css_selector("div.stamp.badge.badgeCompany")))

        self.assertEqual(1, len(driver.find_elements_by_css_selector("div.stamp.badge.badgeAdministration")))

    def test_users_assert_filters_user_type_administration(self):
        driver = self.driver
        driver.get(self.base_url + "/pages")
        driver.find_element_by_link_text("Utilisateurs").click()
        Select(driver.find_element_by_name("userType")).select_by_visible_text("Administration")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.XPATH,
                                                u"//*[contains(text(),\"Communauté d'Agglomération Sud...\")]/ancestor::li"))
        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgePerson"))
        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgeCompany"))
        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgeAsso"))

    def test_users_assert_filters_user_type_association(self):
        driver = self.driver
        driver.get(self.base_url + "/pages")
        driver.find_element_by_link_text("Utilisateurs").click()
        Select(driver.find_element_by_name("userType")).select_by_visible_text("Association")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgeCompany"))
        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgeAdministration"))
        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgePerson"))
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgeAsso"))

    def test_users_assert_filters_user_type_company(self):
        driver = self.driver
        driver.get(self.base_url + "/pages")
        driver.find_element_by_link_text("Utilisateurs").click()
        Select(driver.find_element_by_name("userType")).select_by_visible_text(u"Société")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgeCompany"))
        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgeAdministration"))
        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgePerson"))
        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, "div.stamp.badge.badgeAsso"))

    def test_users_assert_filtered_had_posted_a_review(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Utilisateurs").click()
        Select(driver.find_element_by_name("hadReview")).select_by_visible_text("Oui")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        # ERROR: Caught exception [ERROR: Unsupported command [isOrdered | //*[contains(text(),"Centre Régional Réunion (974)")]/ancestor::li | //*[contains(text(),"Ville de Paris (75)")]/ancestor::li]]
        self.assertTrue(
            self.is_element_present(By.XPATH, u"//*[contains(text(),\"Centre Régional Réunion (974)\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"COGITIS\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH,
                                                u"//*[contains(text(),\"Communauté d'Agglomération Sud...\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH,
                                                u"//*[contains(text(),\"Communauté d'Agglomération Sud...\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH,
                                                "//*[contains(text(),\"Ville de Saint Martin D'Uriage...\")]/ancestor::li"))


if __name__ == "__main__":
    unittest.main()
