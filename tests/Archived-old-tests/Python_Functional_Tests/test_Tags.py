# -*- coding: utf-8 -*-
import time
import unittest

import settings
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


class TestTags(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.display = Display(visible=0, size=(1920, 1080))
        cls.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages", "fr")
        cls.driver = webdriver.Firefox(profile)
        cls.driver.set_window_size(1920, 1080)
        cls.driver.implicitly_wait(20)
        cls.base_url = settings.COMPTOIR_URL
        cls.accept_next_alert = True

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def setUp(self):
        self.verificationErrors = []

    def tearDown(self):
        self.assertEqual([], self.verificationErrors, "Results: " + str(self.verificationErrors))

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def test_tags_sort_by_name_asc(self):
        driver = self.driver
        driver.get(self.base_url + "/tags")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li/a/div").text,
                         "new")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[2]/a/div").text,
                         "tag")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[3]/a/div").text,
                         "test")

    def test_tags_sort_by_name_desc(self):
        driver = self.driver
        driver.get(self.base_url + "/tags")
        Select(driver.find_element_by_name("order")).select_by_value('name.desc')
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li/a/div").text,
                         "test")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[2]/a/div").text,
                         "tag")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[3]/a/div").text,
                         "new")

    def test_tags_sort_by_occurences_asc(self):
        driver = self.driver
        driver.get(self.base_url + "/tags")
        Select(driver.find_element_by_name("order")).select_by_value('used_tag_number.asc')
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(5)
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li/a/div").text,
                         "test")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[2]/a/div").text,
                         "new")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[3]/a/div").text,
                         "tag")

    def test_tags_sort_by_occurences_desc(self):
        driver = self.driver
        driver.get(self.base_url + "/tags")
        Select(driver.find_element_by_name("order")).select_by_value('used_tag_number.desc')
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li/a/div").text,
                         "tag")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[2]/a/div").text,
                         "new")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[3]/a/div").text,
                         "test")

    def test_view_with_two_softwares(self):
        driver = self.driver
        driver.get(self.base_url + "/tags/1")
        self.assertTrue(
            self.is_element_present(By.XPATH, "//h2[contains(text(),\"Liste de logiciels pour l'étiquette new (2)\")]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//a[contains(@href,'/tags/')]"))


if __name__ == "__main__":
    unittest.main()
