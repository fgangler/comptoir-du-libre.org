<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TaxonomysFixture
 *
 */
class TaxonomysFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'parent_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'title_i18n_en' => ['type' => 'string', 'length' => 250, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'title_i18n_fr' => ['type' => 'string', 'length' => 250, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'description_i18n_en' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'description_i18n_fr' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'taxonomys_parent_id' => ['type' => 'foreign', 'columns' => ['parent_id'], 'references' => ['taxonomys', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
     //       'id' => 1,
            'parent_id' => null,
            'title_i18n_en' => 'Business',
            'title_i18n_fr' => 'Métiers',
            'description_i18n_en' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'description_i18n_fr' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'created' => 1581686273,
            'modified' => 1581686273
        ],
        [
     //       'id' => 2,
            'parent_id' => null,
            'title_i18n_en' => 'Generics',
            'title_i18n_fr' => 'Génériques',
            'description_i18n_en' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'description_i18n_fr' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'created' => 1581686273,
            'modified' => 1581686273
        ],
        [
    //        'id' => 3,
            'parent_id' => 1,
            'title_i18n_en' => 'Town planning / Public space / Environment',
            'title_i18n_fr' => 'Urbanisme / Espace public / Environnement',
            'description_i18n_en' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'description_i18n_fr' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'created' => 1581686273,
            'modified' => 1581686273
        ],
        [
    //         'id' => 4,
            'parent_id' => 2,
            'title_i18n_en' => 'Office automation',
            'title_i18n_fr' => 'Bureautique',
            'description_i18n_en' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'description_i18n_fr' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'created' => 1581686273,
            'modified' => 1581686273
        ],
        [
    //        'id' => 5,
            'parent_id' => 2,
            'title_i18n_en' => 'Infrastructure',
            'title_i18n_fr' => 'Infrastructure',
            'description_i18n_en' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'description_i18n_fr' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'created' => 1581686273,
            'modified' => 1581686273
        ],
        [
   //        'id' => 6,
            'parent_id' => 2,
            'title_i18n_en' => 'Design',
            'title_i18n_fr' => 'Design',
            'description_i18n_en' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'description_i18n_fr' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida',
            'created' => 1581686273,
            'modified' => 1581686273
        ],
    ];
}
