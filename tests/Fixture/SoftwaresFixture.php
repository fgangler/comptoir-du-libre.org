<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SoftwaresFixture
 *
 */
class SoftwaresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => [
            'type' => 'integer',
            'length' => 20,
            'autoIncrement' => true,
            'default' => null,
            'null' => false,
            'comment' => null,
            'precision' => null,
            'unsigned' => null
        ],
        'softwarename' => [
            'type' => 'string',
            'length' => 255,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null
        ],
        'url_repository' => [
            'type' => 'string',
            'length' => 512,
            'default' => null,
            'null' => false,
            'collate' => null,
            'comment' => null,
            'precision' => null
        ],
        'description' => [
            'type' => 'text',
            'length' => null,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null
        ],
        'licence_id' => [
            'type' => 'biginteger',
            'length' => 20,
            'default' => null,
            'null' => true,
            'comment' => null,
            'precision' => null,
            'unsigned' => null,
            'autoIncrement' => null
        ],
        'created' => [
            'type' => 'timestamp',
            'length' => null,
            'default' => null,
            'null' => true,
            'comment' => null,
            'precision' => null
        ],
        'modified' => [
            'type' => 'timestamp',
            'length' => null,
            'default' => null,
            'null' => true,
            'comment' => null,
            'precision' => null
        ],
        'logo_directory' => [
            'type' => 'text',
            'length' => 255,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null
        ],
        'photo' => [
            'type' => 'text',
            'length' => 255,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null
        ],
        'url_website' => [
            'type' => 'string',
            'length' => 255,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null,
            'fixed' => null
        ],
        'tag_string' => [
            'type' => 'string',
            'length' => 255,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null,
        ],
        'web_site_url' => [
            'type' => 'string',
            'length' => 255,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null,
            'fixed' => null
        ],
        'sill' => [
            'type' => 'biginteger',
            'length' => 20,
            'default' => null,
            'null' => true,
            'comment' => null,
            'precision' => null,
            'unsigned' => null,
            'autoIncrement' => null
        ],
        'cnll' => [
            'type' => 'biginteger',
            'length' => 20,
            'default' => null,
            'null' => true,
            'comment' => null,
            'precision' => null,
            'unsigned' => null,
            'autoIncrement' => null
        ],
        'wikidata' => [
            'type' => 'string',
            'length' => 255,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null,
            'fixed' => null
        ],
        'framalibre' => [
            'type' => 'string',
            'length' => 255,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null,
            'fixed' => null
        ],
        'wikipedia_en' => [
            'type' => 'string',
            'length' => 255,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null,
            'fixed' => null
        ],
        'wikipedia_fr' => [
            'type' => 'string',
            'length' => 255,
            'default' => null,
            'null' => true,
            'collate' => null,
            'comment' => null,
            'precision' => null,
            'fixed' => null
        ],
        '_indexes' => [
            'idx_26540_fk_software_licence_idx' => [
                'type' => 'index',
                'columns' => ['licence_id'],
                'length' => []
            ],
        ],
        '_constraints' => [
            'primary' => [
                'type' => 'primary',
                'columns' => ['id'],
                'length' => []
            ],
            'idx_26540_softwarename_unique' => [
                'type' => 'unique',
                'columns' => ['softwarename'],
                'length' => [200]
            ],
            'idx_26540_software_url_repository_unique' => [
                'type' => 'unique',
                'columns' => ['url_repository'],
                'length' => []
            ],
            'unique_web_site_url' => [
                'type' => 'unique',
                'columns' => ['web_site_url'],
                'length' => []
            ],
            'fk_software_licence' => [
                'type' => 'foreign',
                'columns' => ['licence_id'],
                'references' => ['licenses', 'id'],
                'update' => 'noAction',
                'delete' => 'noAction',
                'length' => []
            ],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */

    public function init()
    {
        $this->records = [
            [
//            'id' => 1,
                'softwarename' => 'Asalae',
                'url_repository' => 'http://www.fake-repo-asalae.git',
                'description' => 'Asalae description',
                'licence_id' => 1,
                'created' => 1487084568,
                'modified' => 1487084568,
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-asalae.com',
                'web_site_url' => null,
                'tag_string' => 'myNewTagCreated myTagDeleted',
                'sill' => null,
                'cnll' => null,
                'wikidata' => null,
                'framalibre' => null,
                'wikipedia_en' => null,
                'wikipedia_fr' => null,
            ],
            [
//            'id' => 2,
                'softwarename' => 'Lutèce',
                'url_repository' => 'http://www.fake-repo-lutece.git',
                'description' => 'Lutèce description',
                'licence_id' => 1,
                'created' => 1487084568,
                'modified' => 1487084568,
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-lutece.com',
                'web_site_url' => null,
                'tag_string' => 'myNewTagCreated myTagAdded1',
                'sill' => null,
                'cnll' => null,
                'wikidata' => null,
                'framalibre' => null,
                'wikipedia_en' => null,
                'wikipedia_fr' => null,
            ],
            [
//            'id' => 3,
                'softwarename' => 'Soft 3',
                'url_repository' => 'http://www.fake-repo-soft3.git',
                'description' => 'Description Lorem Ipsum',
                'licence_id' => 1,
                'created' => 1487084568,
                'modified' => 1487084568,
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-soft3.com',
                'web_site_url' => null,
                'tag_string' => 'myNewTagCreated myTagAdded1 myTagAdded2',
                'sill' => null,
                'cnll' => null,
                'wikidata' => null,
                'framalibre' => null,
                'wikipedia_en' => null,
                'wikipedia_fr' => null,
            ],
            [
//            'id' => 4,
                'softwarename' => 'Soft 4',
                'url_repository' => 'http://www.fake-repo-soft4.git',
                'description' => 'Description Lorem Ipsum',
                'licence_id' => 1,
                'created' => 1487084568,
                'modified' => 1487084568,
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-soft4.com',
                'web_site_url' => null,
                'tag_string' => '',
                'sill' => null,
                'cnll' => null,
                'wikidata' => null,
                'framalibre' => null,
                'wikipedia_en' => null,
                'wikipedia_fr' => null,
            ],
            [
//            'id' => 5,
                'softwarename' => 'Soft 5',
                'url_repository' => 'http://www.fake-repo-soft555.git',
                'description' => 'Description 5555 Lorem Ipsum',
                'licence_id' => 1,
                'created' => 1487084568,
                'modified' => 1487084568,
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-soft5.com',
                'web_site_url' => null,
                'tag_string' => '',
                'sill' => null,
                'cnll' => null,
                'wikidata' => null,
                'framalibre' => null,
                'wikipedia_en' => null,
                'wikipedia_fr' => null,
            ],
            [
//            'id' => 6,
                'softwarename' => 'Soft 6',
                'url_repository' => 'http://www.fake-repo-soft666.git',
                'description' => 'Description 6666 Lorem Ipsum',
                'licence_id' => 1,
                'created' => 1487084568,
                'modified' => 1487084568,
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-soft6.com',
                'web_site_url' => null,
                'tag_string' => '',
                'sill' => null,
                'cnll' => null,
                'wikidata' => null,
                'framalibre' => null,
                'wikipedia_en' => null,
                'wikipedia_fr' => null,
            ],
            [
//          'id' => 7,
                'softwarename' => 'Soft 7',
                'url_repository' => 'http://www.fake-repo-soft777.git',
                'description' => 'Description 777 Lorem Ipsum',
                'licence_id' => 1,
                'created' => 1487084568,
                'modified' => 1487084568,
                'logo_directory' => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                'photo' => "correctSoftwareLogo.jpg",
                'url_website' => 'http://www.fake-sof77.com',
                'web_site_url' => null,
                'tag_string' => '',
                'sill' => '89',
                'cnll' => '284',
                'wikidata' => 'Q698',
                'framalibre' => 'firefox',
                'wikipedia_en' => 'Firefox',
                'wikipedia_fr' => 'Mozilla_Firefox',
            ]
        ];
        parent::init();
    }
}
