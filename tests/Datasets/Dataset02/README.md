# Dataset 02

Used for acceptance testing.

* Second dataset for Comptoir du Libre
* Created on: 2018-09-11 by MFaure

## Notable data

* Removed old debug entries (table `panels`)
* All softwares from SILL-2018 are created
* All SILL softwares are tagged `SILL-2018`
* Users emails are anonymized

## Pre-defined users (for testing purpose)

All of these users have the same password: `comptoir`
* `dev-individu@comptoir-du-libre.org`   ---> role: `User`, type: `Person`
* `dev-presta@comptoir-du-libre.org`   ---> role: `User`, type: `Company`
* `dev-asso@comptoir-du-libre.org`   ---> role: `User`, type: `Association`
* `dev-collectivite@comptoir-du-libre.org`   ---> role: `User`, type: `Administration`

### Vagrant

In Vagrant, 10 additional users (for testing purposes) are added.

All of these users have the same password: `comptoir`
* 5 users with `User` role:
    * `dev-user_administration@comptoir-du-libre.org`   ---> type: `Administration`
    * `dev-user2_administration@comptoir-du-libre.org` ---> type: `Administration`
    * `dev-user_person@comptoir-du-libre.org` ---> type: `Person`
    * `dev-user_company@comptoir-du-libre.org`  ---> type: `Company`
    * `dev-user_association@comptoir-du-libre.org` ---> type: `Association`
* 5 users with `adminr` role:
    * `dev-admin_administration@comptoir-du-libre.org` ---> type: `Administration`
    * `dev-admin2_administration@comptoir-du-libre.org` ---> type: `Administration`
    * `dev-admin_person@comptoir-du-libre.org` ---> type: `Person`
    * `dev-admin_company@comptoir-du-libre.org` ---> type: `Company`
    * `dev-admin_association@comptoir-du-libre.org` ---> type: `Association`

see : `./bin_vagrant-user/import_SQL.files_during.vagrantUp/03*.sql` files


##  Process used to anonymize and clean the data:

* unpacked the SQL file
* removed content of table `panels`
* on the "users" section, searched and replaced `[a-zA-Z.]*?@.*?\t` with `fake-email@comptoir-du-libre.org`
* repacked the SQL file
