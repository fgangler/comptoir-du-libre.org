<?php
/**
 * This file is to test actions that a service provider account can do:
 * Add and edit a software,
 * declare and undeclare as user of a soft,
 * edit its own user account,
 * by login a user, testing an action and logout.
 * The account used is dev-asso from the Dataset02.
 *
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

/**
 * Inherited Methods from App\src\TestSuite.Codeception\AcceptanceTester
 *
 * @method  void loginMe
 * @method  void logoutMe
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
class UserAssociationCheckActionsCest
{
    private $lang = 'en';
    private $userId = 287;

    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->amOnPage('/');
        $I->loginMe(
            'dev-asso@comptoir-du-libre.org',
            'comptoir',
            'dev-asso',
            $this->userId
        );
        $I->seeElement('div.message.success');
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->logoutMe('dev-asso');
        $I->canSeeElement('#signinform');
    }

    /**
     * Function to test the user dev-asso declaring himself as user of the ATOM software
     *
     * @group user_declareUserOfSoftware
     * @group user_association
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function declareAsUserOfSoftware(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->click('//*[@id="softwaresPage"]');                      // button 'Logiciels'
        $I->click("//a[@href=\"/$lang/softwares/163\"]");           // software Atom
        $I->seeInCurrentUrl("/$lang/softwares/163");
        $I->dontSee('dev-asso', ['css' => 'ol li']);
        $I->seeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->click('//*[@id="btn_Softwares-usersSoftware-163"]');           // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->seeElement('button.btn.btn-default.removeOne');
        $I->see('dev-asso', ['css' => 'ol li']);
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->seeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
    }

    /**
     * Function to test if dev-asso is not declared as a user of Atom software anymore
     *
     * @group user_declareUserOfSoftware
     * @group user_association
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function removeUserOfSoftware(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->click('a[id="usersPage"]');                             // button Users
        $I->canSeeInCurrentUrl("/$lang/users");
        $I->click("a[href=\"/$lang/users/287\"]");                          // User dev-presta
        $I->seeInCurrentUrl("/$lang/users/287");
        $I->seeElement("//ol/li/div/a[@href=\"/$lang/softwares/163\"]"); // Atom software declared
        $I->click("//ol/li/div/a[@href=\"/$lang/softwares/163\"]");
        $I->seeInCurrentUrl("/$lang/softwares/163");
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
        $I->click('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');  // button : 'Ne plus être utilisateur'
        $I->dontSeeElement("//ol/li/div/a[@href=\"/$lang/softwares/163\"]");
        $I->seeElement('//*[@id="btn_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-usersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_Softwares-deleteUsersSoftware-163"]');
        $I->dontSeeElement('//*[@id="btnMapping_TaxonomysSoftwares-mappingForm-163"]');
    }

    /**
     * Check a correct declaration of userOf by the user dev-asso
     *
     * @group user_declareUserOfSoftware
     * @group user_association
     *
     * @param AcceptanceTester $I codeception constant
     * @return void
     */
    public function declareUserOfSoftwareWithMoreThan3Users(AcceptanceTester $I)
    {
        $I->amOnPage('/users/287');                                    // dev-asso's page
        $I->dontSeeElement('7-zip');
        $I->click('//*[@id="softwaresPage"]');                          // button 'Logiciels'
        $I->click('7-zip');
        $I->seeInCurrentUrl('softwares/72');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-72"]');
        $I->click('//*[@id="btn_Softwares-usersSoftware-72"]');           // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-72"]');
        $I->seeElement('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');
        $I->click('//section[2]/section[1]/ol/li[4]/div/p/a');           // button : see all users of 7-Zip
        $I->seeInCurrentUrl('softwares/usersSoftware/72');
        $I->see('dev-asso', ['css' => 'ol li']);                        // dev-asso on the list of users
    }

    /**
     * Remove a declared user dev-asso from the userList of a software 7-zip with more than 3 users.
     *
     * @group user_declareUserOfSoftware
     * @group user_association
     *
     * @param AcceptanceTester $I codeception constant
     * @return void
     */
    public function removeUserOfSoftwareWithMoreThan3Users(AcceptanceTester $I)
    {
        $I->amOnPage('/users/287');                                    // dev-asso's page
        $I->see('7-zip');                                               // declared as userOf 7-zip
        $I->click('7-zip');
        $I->seeInCurrentUrl('softwares/72');
        $I->dontSeeElement('//*[@id="btn_Softwares-usersSoftware-72"]');
        $I->dontSeeElement('//*[@id="btn_TaxonomysSoftwares-mappingForm-72"]');
        $I->click('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');  // button : 'Ne plus être utilisateur'
        $I->seeElement('div.message.success');
        $I->seeElement('//*[@id="btn_Softwares-usersSoftware-72"]');
        $I->dontSeeElement('//*[@id="btn_Softwares-deleteUsersSoftware-72"]');
        $I->click('//section[2]/section[1]/ol/li[4]/div/p/a');           // button : see all users of 7-Zip
        $I->dontsee('dev-asso', ['css' => 'ol li']);                    // dev-asso is not in the list of users
    }

    /**
     * Edit a connected user account to change the password 'comptoir' by a new password 'comptoir'.
     * A new password can be the same as an older one.
     * We check that the user can reconnect himself with the new password
     *
     * @group userAccount
     * @group userAccount_changePassword
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function changePasswordOfConnectedUser(AcceptanceTester $I)
    {
        $newPassword = 'comptoir';

        $userId = 287;
        $lang = $this->lang;
        $I->click("a[href=\"/$lang/users/edit/$userId\"]");             // Edit user's button
        $I->canSeeInCurrentUrl("/$lang/users/edit/$userId");
        $I->click("a[href=\"/api/v1/users/change-password/$userId?language=$lang\"]");       // Change password link
        $I->canSeeInCurrentUrl("users/change-password/$userId?language=$lang");
        $I->submitForm(
            '#editAccountPasswordForm',
            [
                'old_password' => 'comptoir',
                'new_password' => $newPassword,
                'confirm_password' => $newPassword,
            ]
        );
        $I->seeInCurrentUrl("/users/edit/$userId");
        $I->seeElement('div.message.success');
        $I->logoutMe('dev-asso');
        $I->loginMe('dev-asso@comptoir-du-libre.org', $newPassword, 'dev-asso');
        $I->seeElement('div.message.success');
    }

    /**
     * Edit a connected user account to modify information such as name, url, description, email and avatar.
     * Here we add an url and a word as description.
     *
     * @group userAccount
     * @group userAccount_edit
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function editeUserAccount(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $I->click("a[href=\"/$lang/users/edit/287\"]");    // Edit user's button
        $I->canSeeInCurrentUrl('/users/edit/287');
        $I->submitForm(
            '#editInformationAccountForm',
            [
                'url' => 'http://example.com:8080/users/287',
                'description' => 'association',
            ]
        );
        $I->canSeeInCurrentUrl('/users/287');
        $I->seeElement('div.message.success');
    }


    /**
     * When trying to modify another user's data (by changing the id in the url),
     * verify that we can't even see the form
     *
     * @group  security
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function tryToEditAnotherUserAccount(AcceptanceTester $I)
    {
        $I->amOnPage('/users/edit/3'); // "Adullact" acount (id:3)

        $I->dontSeeElement('form#editInformationAccountForm');
        $I->dontSeeInTitle('Edit your account');
        $I->dontSee('Edit your account');

        $I->seeInTitle('Error-400');
        $I->seeElement('div.message.error');
        $I->see('You are not allowed to do that');
    }


    /**
     * When I display a software,
     * I don't see the external resources (wikidata, sill)
     * (only user with "admin" role can do it)
     *
     * @group  security
     * @group  admin
     * @group  sofware
     * @group  sofware_external-ressources
     * @group  user
     * @group  user_association
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function displaySoftwareNotSeeExternalRessources(AcceptanceTester $I)
    {
        $lang = 'fr';
        $sofwareId = 82;  // Firefox software
        $I->amOnPage("/$lang/softwares/$sofwareId");

        $I->dontSeeElement("//div[@id='only-for-admin_software-external-ressources']");
        $I->dontSeeElement("//a[@id='external-link_sill_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_cnll_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikidata_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_framalibre_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-en_$sofwareId']");
        $I->dontSeeElement("//a[@id='external-link_wikipedia-fr_$sofwareId']");

        $I->dontSee('Ressources externes visible uniquement par les admins');
        $I->dontSee('Consulter la fiche CNLL');
        $I->dontSee('Consulter la fiche wikidata.org');
        $I->dontSee('Consulter la fiche FramaLibre');
        $I->dontSee('Consulter la page Wikipédia en français');
        $I->dontSee('Consulter la page Wikipédia en anglais');
        $I->dontSee('Q698');
    }

    /**
     * When I edit a software,
     * I don't see form inputs for external resources (wikidata, sill)
     * (only user with "admin" role can do it)
     *
     * @group  security
     * @group  admin
     * @group  sofware
     * @group  sofware_external-ressources
     * @group  user
     * @group  user_association
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function editSoftwareNotSeeFormInputsForExternalRessources(AcceptanceTester $I)
    {
        $lang = 'fr';
        $sofwareId = 82;  // Firefox software
        $I->amOnPage("/$lang/softwares/edit/$sofwareId");

        $I->dontSeeElement("//div[@id='only-for-admin_software-external-ressources_edit']");
        $I->dontSee('Ressources externes visible uniquement par les admins');
        $I->dontSee('Identifiant SILL');
        $I->dontSee('Identifiant CNLL');
        $I->dontSee('Identifiant Wikidata.org');
        $I->dontSee('Identifiant FramaLibre');
        $I->dontSee('Identifiant Wikipedia en anglais');
        $I->dontSee('Identifiant Wikipedia en français');
        $I->dontSeeElement("//input[@id='sill']");
        $I->dontSeeElement("//input[@id='cnll']");
        $I->dontSeeElement("//input[@id='wikidata']");
        $I->dontSeeElement("//input[@id='framalibre']");
        $I->dontSeeElement("//input[@id='wikipedia-en']");
        $I->dontSeeElement("//input[@id='wikipedia-fr']");
    }
}
