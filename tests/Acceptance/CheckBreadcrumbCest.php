<?php

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

/**
 * Class CheckBreadcrumbCest
 *
 * @todo check for edit user and edit password for connected user
 * @todo check for reset password link (send by mail)
 *
 * @package App\Test\Acceptance
 */
class CheckBreadcrumbCest
{
    const SOFTWARE_CSS_ID_PREFIX = 'softwarePreviewCard';
    const USER_CSS_ID_PREFIX = 'userPreviewCard';

    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->amOnPage('/');
        $I->seeInTitle('Comptoir du libre');
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
    }


    /**
     * Shared checks for Breadcrumb
     *
     * @param AcceptanceTester $I
     * @param string $search
     * @param array $expectedSoftware
     * @param array $expectedUsers
     */
    private function sharedChecks(AcceptanceTester $I, array $breadcrumbs)
    {
        $nb = count($breadcrumbs);
        $selectorCurrentLink = 'ol.breadcrumb li:last-child a[aria-current="page"]';
        $selectorFirstLink = 'ol.breadcrumb li:first-child a';

        $I->seeElement('ol.breadcrumb');
        $I->seeNumberOfElements('ol.breadcrumb > li', $nb);
        $I->seeNumberOfElements('ol.breadcrumb a[aria-current="page"]', 1);
        $I->seeNumberOfElements($selectorCurrentLink, 1);
        $I->seeElement($selectorCurrentLink, ['href' => \array_key_last($breadcrumbs)]);// check last link in the html
        $I->seeElement($selectorFirstLink, ['href' => \array_key_first($breadcrumbs)]); // check 1st link in the html

        $i = 0;
        foreach ($breadcrumbs as $url => $txt) {
            $i++;

            // check if the link is unique
            $I->seeNumberOfElements('ol.breadcrumb a[href="'.$url.'"]', 1);

            // check if the link is at the right level in the html
            $selectorLink = "ol.breadcrumb li:nth-child(0n+$i) a";
            $I->seeElement($selectorLink, ['href' => $url]); // + check the URL
            if (strlen($txt) > 34) { // Check title attribut if link text is too long
                $I->seeElement($selectorLink, ['title' => $txt]); // + check the title link
            } else {
                $I->see($txt, $selectorLink);  // + check the text link
            }
        }
    }


    // MAPPING
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Check Breadcrumb on mapping pages
     * Tested URL:  /fr/cartographie/outillage/courrier/maarch-courrier/8.49
     *              /fr/cartographie/outillage/courrier/8
     *              /fr/cartographie/outillage/sae-archivage/asalae/7.3
     *              /fr/cartographie/outillage/sae-archivage/7
     *              /fr/cartographie/outillage/
     *              /fr/cartographie/
     *
     * @group nav
     * @group breadcrumb
     * @group taxonomy
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnMappingPages(AcceptanceTester $I)
    {

        // URL = /fr/cartographie/
        $BaseBreadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/cartographie/' => 'Cartographie',
        ];
        $I->amOnPage(\array_key_last($BaseBreadcrumbs));
        $this->sharedChecks($I, $BaseBreadcrumbs);

        // URL = /fr/cartographie/outillage/sae-archivage/asalae/7.3
        $Breadcrumbs = [
            '/fr/cartographie/outillage/' => 'Outillage',
            '/fr/cartographie/outillage/sae-archivage/7' => 'SAE (archivage)',
            '/fr/cartographie/outillage/sae-archivage/asalae/7.3' => 'Logiciel As@lae',
         ];
        $Breadcrumbs = array_merge($BaseBreadcrumbs, $Breadcrumbs);
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs); // URL = /fr/cartographie/outillage/sae-archivage/asalae/7.3

        array_pop($Breadcrumbs); // delete last element
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs); // URL = /fr/cartographie/outillage/sae-archivage/7

        array_pop($Breadcrumbs); // delete last element
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs); // URL = /fr/cartographie/outillage/

        // URL = /fr/cartographie/outillage/courrier/maarch-courrier/8.49
        $Breadcrumbs = [
            '/fr/cartographie/outillage/' => 'Outillage',
            '/fr/cartographie/outillage/courrier/8' => 'Courrier',
            '/fr/cartographie/outillage/courrier/maarch-courrier/8.49' => 'Logiciel Maarch Courrier',
        ];
        $Breadcrumbs = array_merge($BaseBreadcrumbs, $Breadcrumbs);
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs); // URL = /fr/cartographie/outillage/courrier/maarch-courrier/8.49

        array_pop($Breadcrumbs); // delete last element
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs); // URL = /fr/cartographie/outillage/courrier/8
    }


    /**
     * Check Breadcrumb on mapping form used by connected user (type = "Administration")
     * after registering as user of a sofware.
     *
     * Tested URL:  /fr/mappingForm/10
     *              /en/mappingForm/10
     *
     * @group nav
     * @group breadcrumb
     * @group form
     * @group taxonomy
     * @group taxonomy-software
     * @group user_administration
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnMappingForm(AcceptanceTester $I)
    {
        // Admin user --> to be replaced by a connected user (type = "Administration") when development is complete.
        $I->amOnPage('/');
        $I->loginMe('dev-user_administration@comptoir-du-libre.org', 'comptoir', 'dev-user_administration');
        $I->seeElement('div.message.success');

        // declare as user (french version)
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/softwares' => 'Logiciels',
            '/fr/softwares/10' => 'LUTECE',
            '/fr/mappingForm/10' => 'Se déclarer utilisateur',
        ];
        $I->amOnPage('/fr/softwares/10'); // Logiciel "LUTECE"
        $I->click('(//button[@type=\'submit\'])[2]');  // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
        $I->seeInCurrentUrl(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);

        // no longer being a user
        $I->amOnPage('/fr/softwares/10'); // Logiciel "LUTECE"
        $I->click('(//button[@type=\'submit\'])[2]');  // button : 'Se déclarer utilisateur'
        $I->seeElement('div.message.success');
    }


    // SOFTWARE
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Check Breadcrumb on software list page
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSofwaresListPage(AcceptanceTester $I)
    {
        $I->click('a[id=softwaresPage]');                       // Login link
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/softwares' => 'Logiciels',
        ];
        $this->sharedChecks($I, $Breadcrumbs);
    }



    // SOFTWARE  ID-35 = GLPI
    /////////////////////////////////////////////////////////////////////////////////////
    /**
     * Shared breadcrumb on software ID-35 page
     * @return array
     */
    private function sharedBreadcrumbSoftware35()
    {
        return  [
            '/fr/' => 'Accueil',
            '/fr/softwares' => 'Logiciels',
            '/fr/softwares/35' => 'GLPI',
        ];
    }

    /**
     * Check Breadcrumb on software ID-35 page
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSoftware35Page(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbSoftware35();
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }


    /**
     * Check Breadcrumb on "users" page  of software ID-35
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSoftware35UsersPage(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbSoftware35();
        $Breadcrumbs['/fr/softwares/usersSoftware/35'] = 'Utilisateurs du logiciel';
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    /**
     * Check Breadcrumb on "providers"  page  of software ID-35
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSoftware35ProvidersPage(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbSoftware35();
        $Breadcrumbs['/fr/softwares/servicesProviders/35'] = 'Prestataires';
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    /**
     * Check Breadcrumb on "screenshots"  page  of software ID-35
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSoftware35ScreenshotsPage(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbSoftware35();
        $Breadcrumbs['/fr/softwares/35/screenshots'] = 'Copies d\'écran';
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    /**
     * Check Breadcrumb on "worksWell"  page  of software ID-35
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSoftware35WorksWellPage(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbSoftware35();
        $Breadcrumbs['/fr/softwares/worksWellSoftwares/35'] = 'Logiciels complémentaires';
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }


    // SOFTWARE  ID-29 = Alfresco
    /////////////////////////////////////////////////////////////////////////////////////
    /**
     * Shared breadcrumb on software ID-29 page
     * @return array
     */
    private function sharedBreadcrumbSoftware29()
    {
        return  [
            '/fr/' => 'Accueil',
            '/fr/softwares' => 'Logiciels',
            '/fr/softwares/29' => 'Alfresco',
        ];
    }

    /**
     * Check Breadcrumb on software ID-29 page
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSoftware29Page(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbSoftware29();
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }


    /**
     * Check Breadcrumb on "reviews" page  of software ID-29
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSoftware29ReviewsPage(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbSoftware29();
        $Breadcrumbs['/fr/softwares/29/reviews'] = 'Témoignages';
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    // USERS
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Check Breadcrumb on user list page     *
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnUsersListPage(AcceptanceTester $I)
    {
        $I->click('a[id=usersPage]');                       // Login link
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/users' => 'Utilisateurs',
        ];
        $this->sharedChecks($I, $Breadcrumbs);
    }


    // USER id=74  - Territoires Numériques Bourgogne Franche-Comté
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Shared breadcrumb on user ID-74 page
     * @return array
     */
    private function sharedBreadcrumbUser74()
    {
        return  [
            '/fr/' => 'Accueil',
            '/fr/users' => 'Utilisateurs',
            '/fr/users/74' => 'Territoires Numériques Bourgogne Franche-Comté',
        ];
    }

    /**
     * Check Breadcrumb on User ID-74 page
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnUser74Page(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbUser74();
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }


    /**
     * Check Breadcrumb on usedSoftwares  page  of user ID-74
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnUser74UsedSoftwaresPage(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbUser74();
        $Breadcrumbs['/fr/users/usedSoftwares/74'] = 'Logiciels utilisés';
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    /**
     * Check Breadcrumb on reviews  page  of user ID-74
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnUser74ReviewsPage(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbUser74();
        $Breadcrumbs['/fr/users/74/reviews'] = 'Témoignages';
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }



    // PROVIDERS
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Check Breadcrumb on Providers list page     *
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnProvidersListPage(AcceptanceTester $I)
    {
        $I->click('a[id=ServicesProvidersPage]');                       // Login link
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/users/providers' => 'Prestataires',
        ];
        $this->sharedChecks($I, $Breadcrumbs);
    }


    // PROVIDER id=85  - Maarch
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Shared breadcrumb on provider ID-85 page
     * @return array
     */
    private function sharedBreadcrumbProvider85()
    {
        return  [
            '/fr/' => 'Accueil',
            '/fr/users/providers' => 'Prestataires',
            '/fr/users/85' => 'Maarch',
        ];
    }

    /**
     * Check Breadcrumb on User ID-85 page
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnProvider85Page(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbProvider85();
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }


    /**
     * Check Breadcrumb on "provider for Softwaress"  page  of user ID-85
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnProvider85providerforSoftwaresPage(AcceptanceTester $I)
    {
        $Breadcrumbs = $this->sharedBreadcrumbProvider85();
        $Breadcrumbs['/fr/users/providerforSoftwares/85'] = 'Prestations proposées';
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }


    // TAGS
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Check Breadcrumb on tags list page
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnTagsListPage(AcceptanceTester $I)
    {
        $I->click('a[id=tagsPage]');                       // Login link
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/tags' => 'Étiquettes',
        ];
        $this->sharedChecks($I, $Breadcrumbs);
    }



    /**
     * Check Breadcrumb on SILL-2017 tag page
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSill2017TagPage(AcceptanceTester $I)
    {
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/tags' => 'Étiquettes',
            '/fr/tags/29/software' => 'SILL-2017',
        ];
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    // SIGN UP / SIGN IN / ....
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Check Breadcrumb on Sign up page
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSignUpPage(AcceptanceTester $I)
    {
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/users/add'   => 'Créer un compte',
        ];
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    /**
     * Check Breadcrumb on Sign in page
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnSignInPage(AcceptanceTester $I)
    {
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/users/login'   => 'Se connecter',
        ];
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    /**
     * Check Breadcrumb on forgot-password page
     * @group nav
     * @group breadcrumb
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnForgotPasswordPage(AcceptanceTester $I)
    {
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/users/login'   => 'Se connecter',
            '/fr/users/forgot-password'   => 'Réinitialiser votre mot de passe',
        ];
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    // PAGES
    /////////////////////////////////////////////////////////////////////////////////////

    /**
     * Check Breadcrumb on "Contact" page
     * @group nav
     * @group breadcrumb
     * @group page
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnContactPage(AcceptanceTester $I)
    {
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/pages/contact'   => 'Contact',
        ];
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    /**
     * Check Breadcrumb on "Legal" page
     * @group nav
     * @group breadcrumb
     * @group page
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnLegalPage(AcceptanceTester $I)
    {
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/pages/legal'   => 'Mentions légales',
        ];
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    /**
     * Check Breadcrumb on "Accessibility" page
     * @group nav
     * @group breadcrumb
     * @group page
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnAccessibilityPage(AcceptanceTester $I)
    {
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/pages/accessibility'   => "Déclaration d'accessibilité",
        ];
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }

    /**
     * Check Breadcrumb on "OpenData" page
     * @group nav
     * @group breadcrumb
     * @group page
     * @param AcceptanceTester $I
     */
    public function checkBreadcrumbOnOpenDataPage(AcceptanceTester $I)
    {
        $Breadcrumbs = [
            '/fr/' => 'Accueil',
            '/fr/pages/opendata'   => "Open data",
        ];
        $I->amOnPage(\array_key_last($Breadcrumbs));
        $this->sharedChecks($I, $Breadcrumbs);
    }
}
