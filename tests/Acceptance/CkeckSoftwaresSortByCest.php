<?php

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

class CkeckSoftwaresSortByCest
{
    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
        $I->amOnPage('/softwares');
        $I->seeInTitle('Liste des logiciels sur le comptoir du Libre');
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
    }

    /*********************************************************************************************
     ********************************* Software page *********************************************
     ********************************************************************************************/

// Note: MFaure 2019-07-15
// All following tests are commented out as we prefer having a working subset of F-tests
// than a larger non working set. Those F-tests will be re-factored, hopefully in a near future
// Nevertheless, we don't want to loose this code, hence we keep it commented.
// If not refactored by 2020-09-01, the commented code can be deleted

/*
    // Tests Sort by functionality on Softwares page
    public function checkSortBySoftwaresAsc(AcceptanceTester $I)
    {
        $firstSoftwareSortByAsc = '//ol/li[1]/div/a[@title="Logiciel :7-zip"]'; // 7-zip
        $secondSoftwareSortByAsc = '//ol/li[2]/div/a[@title="Logiciel :Alfresco"]'; // Alfresco
        $Software95SortByAsc = '//ol/li[95]/div/a[@title="Logiciel :web-CIL"]'; // WebCiL

        $I->selectOption('//form/div[6]/select', 'softwarename.asc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[6]/select', 'Ordre alphabétique : A-Z');
        $I->seeElement($firstSoftwareSortByAsc);
        $I->seeElement($secondSoftwareSortByAsc);
        $I->seeElement($Software95SortByAsc);
    }

    public function checkSortBySoftwaresDesc(AcceptanceTester $I)
    {

        $firstSoftwareSortByDesc = '//ol/li[1]/div/a[@title="Logiciel :Zimbra"]'; // Zimbra
        $secondSoftwareSortByDesc = '//ol/li[2]/div/a[@title="Logiciel :Zabbix"]'; // Zabbix
        $Software95SortByDesc = '//ol/li[95]/div/a[@title="Logiciel :BlueGriffon"]'; // BlueGriffon

        $I->selectOption('//form/div[6]/select', 'softwarename.desc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[6]/select', 'Ordre alphabétique : Z-A');
        $I->seeElement($firstSoftwareSortByDesc);
        $I->seeElement($secondSoftwareSortByDesc);
        $I->seeElement($Software95SortByDesc);
    }

    public function checkSortBySoftwaresOlderToRecent(AcceptanceTester $I)
    {

        $firstSoftwareSortByOlderToRecent = '//ol/li[1]/div/a[@title="Logiciel :Pastell"]'; // Pastell
        $secondSoftwareSortByOlderToRecent = '//ol/li[2]/div/a[@title="Logiciel :As@lae"]'; // As@lae
        $Software95SortByOlderToRecent =
            '//ol/li[95]/div/a[@title="Logiciel :LDAP Tool Box Self Service Password"]'; // LDAP Tool Box..

        $I->selectOption('//form/div[6]/select', 'created.asc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[6]/select', 'Date de création : plus ancienne d\'abord');
        $I->seeElement($firstSoftwareSortByOlderToRecent);
        $I->seeElement($secondSoftwareSortByOlderToRecent);
        $I->seeElement($Software95SortByOlderToRecent);
    }

    public function checkSortBySoftwaresRecentToOlder(AcceptanceTester $I)
    {

        $firstSoftwareSortByOlderToRecent = '//ol/li[1]/div/a[@title="Logiciel :testSoftware1"]'; // testSoftware1
        $secondSoftwareSortByOlderToRecent = '//ol/li[2]/div/a[@title="Logiciel :Zimbra"]'; // Zimbra
        $Software95SortByOlderToRecent = '//ol/li[95]/div/a[@title="Logiciel :OpenRecensement"]'; // Open Rencensement

        $I->selectOption('//form/div[6]/select', 'created.desc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[6]/select', 'Date de création : plus récente d\'abord');
        $I->seeElement($firstSoftwareSortByOlderToRecent);
        $I->seeElement($secondSoftwareSortByOlderToRecent);
        $I->seeElement($Software95SortByOlderToRecent);
    }

    public function checkSortBySoftwaresModifiedOlderToRecent(AcceptanceTester $I)
    {

        $firstSoftwareSortByModifiedOlderFirst = '//ol/li[1]/div/a[@title="Logiciel :Xwiki"]'; // Xwiki
        $secondSoftwareSortByModifiedOlderFirst = '//ol/li[2]/div/a[@title="Logiciel :Publik"]'; // Publik
        $Software95SortByModifiedOlderFirst = '//ol/li[95]/div/a[@title="Logiciel :Apache JMeter™"]'; // Apache JMeter

        $I->selectOption('//form/div[6]/select', 'modified.asc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[6]/select', 'Date de mise à jour : plus ancienne d\'abord');
        $I->seeElement($firstSoftwareSortByModifiedOlderFirst);
        $I->seeElement($secondSoftwareSortByModifiedOlderFirst);
        $I->seeElement($Software95SortByModifiedOlderFirst);
    }

    public function checkSortBySoftwaresModifiedRecentToOlder(AcceptanceTester $I)
    {
        $firstSoftwareSortByModifiedRecentFirst = '//ol/li[1]/div/a[@title="Logiciel :Alfresco"]'; // Alfresco
        $secondSoftwareSortByModifiedRecentFirst =
            '//ol/li[2]/div/a[@title="Logiciel :testSoftware1"]'; // testSoftware1
        $Software95SortByModifiedRecentFirst = '//ol/li[95]/div/a[@title="Logiciel :openCimetière"]'; // open cimetiere

        $I->selectOption('//form/div[6]/select', 'modified.desc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[6]/select', 'Date de mise à jour : plus récente d\'abord');
        $I->seeElement($firstSoftwareSortByModifiedRecentFirst);
        $I->seeElement($secondSoftwareSortByModifiedRecentFirst);
        $I->seeElement($Software95SortByModifiedRecentFirst);
    }

    public function checkSortBySoftwaresLowestToHighestScore(AcceptanceTester $I)
    {

        $firstSoftwareSortByLowToHightScore = '//ol/li[1]/div/a[@title="Logiciel :S²LOW"]'; // s2low
        $secondSoftwareSortByLowToHightScore =
            '//ol/li[2]/div/a[@title="Logiciel :GRR - Gestion Réservation de Ressources"]'; // GRR
        $Software95SortByLowToHightScore = '//ol/li[95]/div/a[@title="Logiciel :Ultradefrag"]'; //

        $I->selectOption('//form/div[6]/select', 'average_review.asc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[6]/select', 'Score : croissant');
        $I->seeElement($firstSoftwareSortByLowToHightScore);
        $I->seeElement($secondSoftwareSortByLowToHightScore);
        $I->seeElement($Software95SortByLowToHightScore);
    }

    public function checkSortBySoftwaresHighestToLowestScore(AcceptanceTester $I)
    {

        $firstSoftwareSortByHightToLowScore = '//ol/li[1]/div/a[@title="Logiciel :Publik"]';
        $secondSoftwareSortByHightToLowScore = '//ol/li[2]/div/a[@title="Logiciel :Lutèce"]';
        $Software95SortByHightToLowScore = '//ol/li[95]/div/a[@title="Logiciel :OpenReglement"]';

        $I->selectOption('//form/div[6]/select', 'average_review.desc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[6]/select', 'Score : décroissant');
        $I->seeElement($firstSoftwareSortByHightToLowScore);
        $I->seeElement($secondSoftwareSortByHightToLowScore);
        $I->seeElement($Software95SortByHightToLowScore);
    }

    // Test Sort by A-Z with specification
    public function checkSortBySoftwaresWithReview(AcceptanceTester $I)
    {

        $firstSoftwareWithReview = '//ol/li[1]/div/a[@title="Logiciel :Alfresco"]'; // Alfresco
        $secondSoftwareWithReview = '//ol/li[2]/div/a[@title="Logiciel :i-Parapheur"]'; // i-parapheur
        $fifthSoftwareWithReview = '//ol/li[5]/div/a[@title="Logiciel :Publik"]'; // publik

        $I->selectOption('//form/div[2]/select', 'true');
        $I->selectOption('//form/div[6]/select', 'softwarename.asc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[2]/select', 'Oui');
        $I->seeOptionIsSelected('//form/div[6]/select', 'Ordre alphabétique : A-Z');
        $I->seeElement($firstSoftwareWithReview);
        $I->seeElement($secondSoftwareWithReview);
        $I->seeElement($fifthSoftwareWithReview);
    }

    public function checkSortBySoftwaresWithScreenshots(AcceptanceTester $I)
    {

        $firstSoftwareWithReview = '//ol/li[1]/div/a[@title="Logiciel :As@lae"]'; // As@lae
        $secondSoftwareWithReview = '//ol/li[2]/div/a[@title="Logiciel :Asqatasun"]'; // Asqatasun
        $fifthSoftwareWithReview = '//ol/li[5]/div/a[@title="Logiciel :Exoplatform"]'; // Exoplatform
        $fourteenthSoftwareWithReview = '//ol/li[14]/div/a[@title="Logiciel :Pastell"]'; // Pastell

        $I->selectOption('//form/div[3]/select', 'true');
        $I->selectOption('//form/div[6]/select', 'softwarename.asc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[3]/select', 'Oui');
        $I->seeOptionIsSelected('//form/div[6]/select', 'Ordre alphabétique : A-Z');
        $I->seeElement($firstSoftwareWithReview);
        $I->seeElement($secondSoftwareWithReview);
        $I->seeElement($fifthSoftwareWithReview);
        $I->seeElement($fourteenthSoftwareWithReview);
    }

    public function checkSortBySoftwaresWithUser(AcceptanceTester $I)
    {

        $firstSoftwareWithUser = '//ol/li[1]/div/a[@title="Logiciel :Alfresco"]'; // Alfresco
        $secondSoftwareWithUser = '//ol/li[2]/div/a[@title="Logiciel :As@lae"]'; // As@lae
        $fifthSoftwareWithUser = '//ol/li[5]/div/a[@title="Logiciel :eGroupware"]'; // eGroupware
        $fourteenthSoftwareWithUser = '//ol/li[14]/div/a[@title="Logiciel :Nuxeo"]'; // nuxeo

        $I->selectOption('//form/div[4]/select', 'true');
        $I->selectOption('//form/div[6]/select', 'softwarename.asc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[4]/select', 'Oui');
        $I->seeOptionIsSelected('//form/div[6]/select', 'Ordre alphabétique : A-Z');
        $I->seeElement($firstSoftwareWithUser);
        $I->seeElement($secondSoftwareWithUser);
        $I->seeElement($fifthSoftwareWithUser);
        $I->seeElement($fourteenthSoftwareWithUser);
    }

    public function checkSortBySoftwaresWithServicePrivider(AcceptanceTester $I)
    {

        $firstSoftwareWithServicePrivider = '//ol/li[1]/div/a[@title="Logiciel :Alfresco"]'; // Alfresco
        $secondSoftwareWithServicePrivider = '//ol/li[2]/div/a[@title="Logiciel :Apache JMeter™"]'; // ApacheJmeter
        $fifthSoftwareWithServicePrivider = '//ol/li[5]/div/a[@title="Logiciel :Asqatasun"]'; // Asqatasun
        $fourteenthSoftwareWithServicePrivider =
            '//ol/li[14]/div/a[@title="Logiciel :LDAP Synchronisation connector"]'; // LDAP Synchronisation connector

        $I->selectOption('//form/div[5]/select', 'true');
        $I->selectOption('//form/div[6]/select', 'softwarename.asc');
        $I->click('Filtrer');
        // Second paramater is only available on optionText, which make us dependent of I18N
        $I->seeOptionIsSelected('//form/div[5]/select', 'Oui');
        $I->seeOptionIsSelected('//form/div[6]/select', 'Ordre alphabétique : A-Z');
        $I->seeElement($firstSoftwareWithServicePrivider);
        $I->seeElement($secondSoftwareWithServicePrivider);
        $I->seeElement($fifthSoftwareWithServicePrivider);
        $I->seeElement($fourteenthSoftwareWithServicePrivider);
    }
*/
}
