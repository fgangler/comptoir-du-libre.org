<?php
/**
 * This file is to test basics login/logout action for the 4 types of user account: Create an account, login, logout.
 * And the failure of creating an account without respecting integrity rules of username, email and password.
 *
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */

namespace App\Test\Acceptance;

use App\TestSuite\Codeception\AcceptanceTester;

/**
 * Inherited Methods from App\src\TestSuite.Codeception\AcceptanceTester
 *
 * @method  void createAccountMinimal
 * @method  void loginMe
 * @method  void logoutMe
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
class BasicTestsAccountsCest
{
    private $lang = 'en';

    // @codingStandardsIgnoreStart
    public function _before(AcceptanceTester $I)// @codingStandardsIgnoreEnd
    {
    }

    // @codingStandardsIgnoreStart
    public function _after(AcceptanceTester $I, $userName)// @codingStandardsIgnoreEnd
    {
    }

    /********************************************************************
     * ***************   ASSOCIATION   **********************************
     *******************************************************************/

    /**
     * Tests the creation functionality as an Association user named Association1
     *
     * @group createUser
     * @group createUserAssociation
     * @group userAccountAssociation
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function createAccountAsAssociation(AcceptanceTester $I)
    {
        $ramdom = $I->getCurrentRandom();
        $I->createAccountMinimal('6', 'Association1', 'asso@comptoir-du-libre.org', 'mdptest', 'mdptest');
        $I->seeElement('div.message.success');
        $I->dontSeeInCurrentUrl('/users/add');
        $I->canSeeInCurrentUrl('/users/'); // url: /users/<idUser> --> how to really test it?
        $I->see("Association1_$ramdom");
    }

    /**
     * Tests the login functionality as an Association named Association1
     *
     * @group userLogin
     * @group userLoginAssociation
     * @group userAccountAssociation
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function tryToLoginAsAssociation(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $ramdom = $I->getCurrentRandom();
        $userName = "Association1_$ramdom";
        $I->loginMe("asso$ramdom@comptoir-du-libre.org", 'mdptest', $userName);
        $I->seeElement('div.message.success');
        $I->canSee("Association1_$ramdom");
        $I->cantSeeElement("//a[@href=\"/$lang/users/login\"]");
    }

    /**
     * Tests the logout functionality as an Association account named Association1
     *
     * @group userLogout
     * @group userLogoutAssociation
     * @group userAccountAssociation
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function checkLougoutAssociation(AcceptanceTester $I)
    {
        $lang = $this->lang;
        $ramdom = $I->getCurrentRandom();
        $I->loginMe("asso$ramdom@comptoir-du-libre.org", 'mdptest', "Association1_$ramdom");
        $I->logoutMe("Association1_$ramdom");
        $I->cantSee("Association1_$ramdom");
        $I->seeElement("//a[@href=\"/$lang/users/login\"]");
    }

    /******************************************************************
     *  ***************   PERSON    ***********************************
     ******************************************************************/

    /**
     * Tests the account creation functionality as a Person named Person1
     *
     * @group createUser
     * @group createUserPerson
     * @group userAccountPerson
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function createAccountAsPerson(AcceptanceTester $I)
    {
        $ramdom = $I->getCurrentRandom();
        $I->createAccountMinimal(
            '4',
            'Person1',
            'person@comptoir-du-libre.org',
            'mdptest',
            'mdptest'
        );
        $I->seeElement('div.message.success');
        $I->dontSeeInCurrentUrl('/users/add');
        $I->canSeeInCurrentUrl('/users/'); // url: /users/<idUser> --> how to really test it?
        $I->see("Person1_$ramdom");
    }

    /**
     * Tests the login functionality as a Person account named Person1
     *
     * @group userLogin
     * @group userLoginPerson
     * @group userAccountPerson
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function tryToLoginAsPerson(AcceptanceTester $I)
    {
        $ramdom = $I->getCurrentRandom();
        $lang = $this->lang;
        $I->loginMe("person$ramdom@comptoir-du-libre.org", 'mdptest', "Person1_$ramdom");
        $I->seeElement('div.message.success');
        $I->canSee("Person1_$ramdom");
        $I->cantSeeElement("//a[@href=\"/$lang/users/login\"]");
    }

    /**
     * Tests the login functionality as a Person account named Person1
     *
     * @group userLogout
     * @group userLogoutPerson
     * @group userAccountPerson
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function checkLougoutPerson(AcceptanceTester $I)
    {
        $ramdom = $I->getCurrentRandom();
        $lang = $this->lang;
        $I->loginMe("person$ramdom@comptoir-du-libre.org", 'mdptest', "Person1_$ramdom");
        $I->logoutMe("Person1_$ramdom");
        $I->cantSee("Person1_$ramdom");
        $I->seeElement("//a[@href=\"/$lang/users/login\"]");
    }

    /***************************************************************
     * *****************  ADMINISTRATION    ************************
     ***************************************************************/

    /**
     * Tests the account creation functionality as an Administration named Administration1
     *
     * @group createUser
     * @group createUserAdministration
     * @group userAccountAdministration
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function createAccountAsAdministration(AcceptanceTester $I)
    {
        $ramdom = $I->getCurrentRandom();
        $I->createAccountMinimal('2', 'Administration1', 'administration@comptoir-du-libre.org', 'mdptest', 'mdptest');
        $I->seeElement('div.message.success');
        $I->dontSeeInCurrentUrl('/users/add');
        $I->canSeeInCurrentUrl('/users/'); // url: /users/<idUser> --> how to really test it?
        $I->see("Administration1_$ramdom");
    }

    /**
     * Tests the login functionality as an Administration account named Administration1
     *
     * @group userLogin
     * @group userLoginAdministration
     * @group userAccountAdministration
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function tryToLoginAsAdministration(AcceptanceTester $I)
    {
        $ramdom = $I->getCurrentRandom();
        $lang = $this->lang;
        $I->loginMe("administration$ramdom@comptoir-du-libre.org", 'mdptest', "Administration1_$ramdom");
        $I->seeElement('div.message.success');
        $I->canSee("Administration1_$ramdom");
        $I->cantSeeElement("//a[@href=\"/$lang/users/login\"]");
    }

    /**
     * Tests the logout functionality as an Administration account named Administration1
     *
     * @group userLogout
     * @group userLogoutAdministration
     * @group userAccountAdministration
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function checkLougoutAdministration(AcceptanceTester $I)
    {
        $ramdom = $I->getCurrentRandom();
        $lang = $this->lang;
        $I->loginMe("administration$ramdom@comptoir-du-libre.org", 'mdptest', "Administration1_$ramdom");
        $I->logoutMe("Administration1_$ramdom");
        $I->cantSee("Administration1_$ramdom");
        $I->seeElement("//a[@href=\"/$lang/users/login\"]");
    }

    /***************************************************************
     *  ***************   COMPANY    *******************************
     ***************************************************************/

    /**
     * Tests the account creation functionality as a Company named Company1
     *
     * @group createUser
     * @group createUserCompany
     * @group userAccountCompany
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function createAccountAsServiceProvider(AcceptanceTester $I)
    {
        $ramdom = $I->getCurrentRandom();
        $I->createAccountMinimal('2', 'Company1', 'company@comptoir-du-libre.org', 'mdptest', 'mdptest');
        $I->seeElement('div.message.success');
        $I->dontSeeInCurrentUrl('/users/add');
        $I->canSeeInCurrentUrl('/users/'); // url: /users/<idUser> --> how to really test it?
        $I->see("Company1_$ramdom");
    }

    /**
     * Tests the login functionality as a Company account named Company1
     *
     * @group userLogin
     * @group userLoginCompany
     * @group userAccountCompany
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function tryToLoginAsServiceProvider(AcceptanceTester $I)
    {
        $ramdom = $I->getCurrentRandom();
        $lang = $this->lang;
        $I->loginMe("company$ramdom@comptoir-du-libre.org", 'mdptest', "Company1_$ramdom");
        $I->seeElement('div.message.success');
        $I->canSee("Company1_$ramdom");
        $I->cantSeeElement("//a[@href=\"/$lang/users/login\"]");
    }

    /**
     * Tests the logout functionality as a Company account named Company1
     *
     * @group userLogout
     * @group userLogoutCompany
     * @group userAccountCompany
     * @group userAccount
     * @group user
     *
     * @param AcceptanceTester $I codeception variable
     * @return void
     */
    public function checkLougoutServiceProvider(AcceptanceTester $I)
    {
        $ramdom = $I->getCurrentRandom();
        $lang = $this->lang;
        $I->loginMe("company$ramdom@comptoir-du-libre.org", 'mdptest', "Company1_$ramdom");
        $I->logoutMe("Company1_$ramdom");
        $I->cantSee("Company1_$ramdom");
        $I->seeElement("//a[@href=\"/$lang/users/login\"]");
    }

    /**************************************************************
     *  ***************   CREATE ACCOUNT WITH ERROR  **************
     **************************************************************/

    //
    //  These functions are not coded because they are managed by html5 (required argument).
    //  But it would be interesting to code them based on curl requests
    //

    //    public function createAccountWithoutUserType(AcceptanceTester $I)
    //    {
    //    }
    //
    //    public function createAccountWithoutName(AcceptanceTester $I)
    //    {
    //    }

    /**
     * Test the failure of creation of an account with a userName already existing.
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function createAccountUsernameAlreadyExisting(AcceptanceTester $I)
    {
        $I->createAccountMinimal('2', 'Company1', 'company2@comptoir-du-libre.org', 'mdptest', 'mdptest');
        $I->canSeeInCurrentUrl('/users/add');
        $I->seeElement('div.message.error');
    }

    /**
     * Test the failure of creation of an account with an email already existing.
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function createAccountEmailAlreadyExisting(AcceptanceTester $I)
    {
        $I->createAccountMinimal('2', 'Company2', 'company@comptoir-du-libre.org', 'mdptest', 'mdptest');
        $I->canSeeInCurrentUrl('/users/add');
        $I->seeElement('div.message.error');
    }

    /**
     * Test the failure of creation of an account with different combination of password/confirmPwd.
     *
     * @param AcceptanceTester $I codeception variable
     *
     * @return void
     */
    public function createAccountWithWrongConfirmPassword(AcceptanceTester $I)
    {
        $I->createAccountMinimal('2', 'Company3', 'company3@comptoir-du-libre.org', 'mdptest', 'mdpuserError');
        $I->canSeeInCurrentUrl('/users/add');
        $I->seeElement('div.message.error');
    }
}
