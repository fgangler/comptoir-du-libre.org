<?php

namespace App\Test\TestCase\View\Cell;

use App\View\Cell\SoftwaresStatisticsCell;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\TestSuite\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * App\View\Cell\SoftwaresStatisticsCell Test Case
 */
class SoftwaresStatisticsCellTest extends TestCase
{

    /**
     * Request mock
     *
     * @var Request|PHPUnit_Framework_MockObject_MockObject
     */
    public $request;

    /**
     * Response mock
     *
     * @var Response|PHPUnit_Framework_MockObject_MockObject
     */
    public $response;

    /**
     * Test subject
     *
     * @var SoftwaresStatisticsCell
     */
    public $SoftwaresStatistics;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->request = $this->getMock('Cake\Network\Request');
        $this->response = $this->getMock('Cake\Network\Response');
        $this->SoftwaresStatistics = new SoftwaresStatisticsCell($this->request, $this->response);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SoftwaresStatistics);

        parent::tearDown();
    }

    /**
     * Test display method
     *
     * @return void
     */
    public function testDisplay()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
