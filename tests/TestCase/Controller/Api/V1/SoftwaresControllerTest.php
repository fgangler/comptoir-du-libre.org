<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\SoftwaresController;
use App\Model\Table\SoftwaresTable;
use Cake\Event\EventList;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\SoftwaresController Test Case
 * @property SoftwaresTable $Softwares
 */
class SoftwaresControllerTest extends ApiIntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */

    public $fixtures = [
        'app.taxonomys_softwares',
        'app.taxonomys',
        'app.softwares',
        'app.users',
        'app.user_types',
        'app.relationships_softwares_users',
        'app.licenses',
        'app.licence_types',
        'app.reviews',
        'app.screenshots',
        'app.relationships',
        'app.relationship_types',
        'app.tags',
        'app.softwares_tags'
    ];

    public $rules;

    public $correctFile;
    public $wrongFile;
    public $exceedFile;
    private $licenses;

    public function setUp()
    {

        parent::setUp();

        $this->correctFile = [
            'name' => 'correctAvatarLogo.jpg',
            'tmp_name' => TESTS . "TestFiles" . DS . "Common" . DS . "correctAvatarLogo.jpg",
            'type' => 'image/jpeg',
            'size' => 8407,
            'error' => 0
        ];
        $this->exceedFile = [
            'name' => 'exceedPhpSizeLimitUserAvatar.jpg',
            'tmp_name' => TESTS . "TestFiles" . DS . "Common" . DS . 'exceedPhpSizeLimitUserAvatar.jpg',
            'type' => 'image/jpeg',
            'size' => 0,
            'error' => 1
        ];
        $this->wrongFile = [
            'name' => 'wrongAvatarLogo.jpg',
            'tmp_name' => TESTS . "TestFiles" . DS . "Common" . DS . 'wrongAvatarLogo.jpg',
            'type' => 'image/jpeg',
            'size' => 1132550,
            'error' => 0
        ];

        $this->licenses = [
            "1" => "CeCill V2",
            "2" => "MIT",
            "3" => "BSD",
            "4" => "LGPLv3",
            "5" => "GNU GPLv2"
        ];

        $this->rules = [
            "id" => [
                "presenceRequired" => false,
                "allowEmpty" => "create"
            ],
            "softwarename" => [
                "presenceRequired" => true,
                "allowEmpty" => false
            ],
            "url_repository" => [
                "presenceRequired" => "create",
                "allowEmpty" => false,
                "url" => [
                    "rule" => "url",
                    "message" => null,
                    "limit" => false
                ]
            ],
            "description" => [
                "presenceRequired" => false,
                "allowEmpty" => true
            ],
            "logo_directory" => [
                "presenceRequired" => false,
                "allowEmpty" => true
            ],
            "photo" => [
                "presenceRequired" => false,
                "allowEmpty" => true,
                "file" => [
                    "rule" => "mimeType",
                    "message" => "This format is not allowed. Please use one in this list: JPEG, PNG, GIF, SVG.",
                    "limit" => [
                        "image/jpeg",
                        "image/png",
                        "image/gif",
                        "image/svg+xml"
                    ]
                ],
                "fileBelowMaxSize" => [
                    "rule" => "isBelowMaxSize",
                    "message" => "The maximum weight allowed is 1MB, please use a file less than 1MB.",
                    "limit" => 1048576
                ],
                "fileBelowMaxHeight" => [
                    "rule" => "isBelowMaxHeight",
                    "message" => "The size of your image is too big, please use an image fitting a 350x350px size.",
                    "limit" => 350
                ],
                "fileBelowMaxWidth" => [
                    "rule" => "isBelowMaxWidth",
                    "message" => "The size of your image is too big, please use an image fitting a 350x350px size.",
                    "limit" => 350
                ],
            ],
            "sill" => [
                "presenceRequired" => false,
                "allowEmpty" => true
            ],
            "cnll" => [
                "presenceRequired" => false,
                "allowEmpty" => true
            ],
            "wikidata" => [
                "presenceRequired" => false,
                "allowEmpty" => true
            ],
            "framalibre" => [
                "presenceRequired" => false,
                "allowEmpty" => true
            ],
            "wikipedia_en" => [
                "presenceRequired" => false,
                "allowEmpty" => true
            ],
            "wikipedia_fr" => [
                "presenceRequired" => false,
                "allowEmpty" => true
            ],
        ];

        $config = TableRegistry::exists('Softwares')
            ? []
            : ['className' => 'App\Model\Table\SoftwaresTable'];
        $this->Softwares = TableRegistry::get('Softwares', $config);
        $this->Softwares->eventManager()->setEventList(new EventList());
        $this->Softwares->Reviews->eventManager()->setEventList(new EventList());
        $this->Softwares->Screenshots->eventManager()->setEventList(new EventList());
    }

//    TODO : uncomment as soon as issue #592 is resolved
//
//    /**
//     * Test add a software with a correct parameters
//     *
//     * Note: no tag is specified
//     */
//    public function testAddSoftware()
//    {
//        $this->session([
//            'Auth' => [
//                'User' => [
//                    'id' => 3,
//                    "user_type_id" => 3,
//                ]
//            ]
//        ]);
//
//        $data =
//            [
//                'softwarename' => 'My new software',
//                'url_repository' => "http://www.mynewsoftware.com/",
//                'description' => 'This is my new software and...',
//                'licence_id' => 1,
//                "url_website" => null,
//                'photo' => $this->correctFile,
//                "tag_string" => "",
//            ];
//
//        $this->post('api/v1/softwares/add.json', $data);
//        $this->assertEventFired('Model.Software.created', $this->Softwares->eventManager());
//
//        // Vérifie que le code de réponse est 200
//        $this->assertResponseOk();
//        $this->assertContains("Success", $this->_response->body());
//        define(
//            "SOFTWARE_AVATAR",
//            WWW_ROOT . "img" . DS . "files/Softwares/My new software/avatar/" . $this->correctFile["name"]
//        );
//        $this->assertFileExists(SOFTWARE_AVATAR);
//
//        // Delete the file after testing
//        unlink(SOFTWARE_AVATAR);
//        $this->assertFileNotExists(SOFTWARE_AVATAR);
//    }

    /**
     * Test add a software with a correct parameters + 3 tag string
     */
    public function testAddSoftwareWithTags()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data =
            [
                'softwarename' => 'My new software',
                'url_repository' => "http://www.mynewsoftware.com/",
                'description' => 'This is my new software and...',
                'licence_id' => 1,
                "url_website" => null,
                'photo' => $this->correctFile,
                "tag_string" => "test aze great",
            ];

        $this->post('api/v1/softwares/add.json', $data);
        $this->assertEventFired('Model.Software.created', $this->Softwares->eventManager());

        // Vérifie que le code de réponse est 200
        $this->assertResponseOk();
        $this->assertContains("Success", $this->_response->body());

        define(
            "SOFTWARE_AVATAR",
            WWW_ROOT . 'img' . DS . "files/Softwares/My new software/avatar/" . $this->correctFile["name"]
        );

        $this->assertFileExists(SOFTWARE_AVATAR);
        //Delete the file after testing
        unlink(SOFTWARE_AVATAR);
        $this->assertFileNotExists(SOFTWARE_AVATAR);
    }

    /**
     * Test add a software with a correct parameters + 11 tags string
     *
     * Note: A software is limited to 10 tags at most
     */
    public function testAddSoftwareWithMoreThan10Tags()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data =
            [
                'softwarename' => 'My new software',
                'url_repository' => "http://www.mynewsoftware.com/",
                'description' => 'This is my new software and...',
                'licence_id' => 1,
                "url_website" => null,
                'photo' => $this->correctFile,
                "tag_string" => "un deux trois quatre cinq six sept huit neuf dix onze",
            ];

        $this->post('api/v1/softwares/add.json', $data);

        // Vérifie que le code de réponse est 200
        $this->assertResponseOk();
        $this->assertContains("Error", $this->_response->body());
    }


    /**
     * Test edit a software as an Administration User,
     * with correct parameters but without any tag and add it an existing tag
     *
     */
    public function testEditSoftwareAddNewTag()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Soft 4",
            "tag_string" => "myTagAdded1"
        ];

        $this->put('api/v1/softwares/4.json', $data);
        $this->assertResponseOk();
        $this->assertEventFired('Model.Software.modified', $this->Softwares->eventManager());

        //Check in the database
        $query = $this->Softwares->find()->where(["id" => 4, "tag_string" => "myTagAdded1"]);
        $this->assertEquals(1, $query->count());
    }

    /**
     * Test edit a software as an Administration User, with a correct parameters and alrady related to 2 tags.
     * Modify a tag "myTagAdded1" by "myTagAdded2"
     *
     */

    public function testEditSoftwareChangeARelatedTagByAnotherExistingTag()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Lutèce",
            "tag_string" => "myNewTagCreated myTagAdded2"
        ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();
        $this->assertEventFired('Model.Software.modified', $this->Softwares->eventManager());

        //Check in the database
        $query = $this->Softwares->find()->where(["id" => 2, "tag_string" => "myNewTagCreated myTagAdded2"]);
        $this->assertEquals(1, $query->count());
    }

    /**
     * Test edit a software as as an Administration User, with a correct parameters and related to several tags.
     *
     * Note: Delete every relations between tag existing of a software
     */
    // TODO : uncomment as soon as issue #592 is resolved
    public function testEditSoftwareRemoveAllRelatedTags()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Asalae",
            "tag_string" => ""
        ];

        $this->put('api/v1/softwares/1.json', $data);
        $this->assertResponseOk();
        $this->assertEventFired('Model.Software.modified', $this->Softwares->eventManager());

        //Check in the database
        $query = $this->Softwares->find()->where(["id" => 1, "tag_string" => ""]);
        $this->assertEquals(1, $query->count());
    }

    /**
     * Test edit a software as as an Administration User, with a correct parameters and related to several tags.
     *
     * Note: Delete every tag existing of a software except one of them.
     */
    public function testEditSoftwareRemoveRelatedTagsButOne()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Soft 3",
            "tag_string" => "myTagAdded1"
        ];

        $this->put('api/v1/softwares/3.json', $data);
        $this->assertResponseOk();
        $this->assertEventFired('Model.Software.modified', $this->Softwares->eventManager());

        //Check in the database
        $query = $this->Softwares->find()->where(["id" => 3, "tag_string" => "myTagAdded1"]);
        $this->assertEquals(1, $query->count());
    }

    /**
     * Test edit a software as as an Administration User, with a correct parameters and no tag related.
     *
     * Note: Add a relationship to a corresponding tag by creating it.
     */
    public function testEditSoftwareCreatNewTag()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Soft 4",
            "tag_string" => "veryNewTag"
        ];

        $this->put('api/v1/softwares/4.json', $data);
        $this->assertResponseOk();
        $this->assertEventFired('Model.Software.modified', $this->Softwares->eventManager());

        //Check in the database
        $query = $this->Softwares->find()->where(["id" => 4, "tag_string" => "veryNewTag"]);
        $this->assertEquals(1, $query->count());
    }
    //---------------------------------------------------------

    /**
     * Test add a software with a correct image.
     */
    public function testAddWithWrongLogo()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->post('api/v1/softwares.json');

        $data =
            [
                'softwarename' => 'My new software',
                'url_repository' => "http://www.mynewsoftware.com/",
                'description' => 'This is my new software and...',
                'licence_id' => 1,
                'created' => "2017-02-14T15:02:48+00:00",
                'modified' => "2017-02-14T15:02:48+00:00",
                "url_website" => null,
                'photo' => $this->wrongFile,
            ];

        $this->post('api/v1/softwares/add.json', $data);

        // Vérifie que le code de réponse est 200
        $this->assertResponseOk();

        $expected = [
            "software" => [
                "photo" => [
                    "fileBelowMaxSize"
                    => "The maximum weight allowed is 1MB, please use a file less than 1MB.",
                    "fileBelowMaxHeight"
                    => "The size of your image is too big, please use an image fitting a 350x350px size."
                ]
            ],
            "licenses" => $this->licenses,
            "rules" => $this->rules,
            'message' => "Error",
        ];

        $this->assertContains("Error", $this->_response->body());
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test if an user can be declared as user of a software
     * Test usersSoftware method
     */
    public function testDeclareAsUserOf()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->post('api/v1/softwares/usersSoftware/2.json');

        $expected =
            [
                "software" => [
                    "id" => 2,
                    "softwarename" => "Lutèce",
                    "url_repository" => "http://www.fake-repo-lutece.git",
                    "description" => "Lutèce description",
                    "licence_id" => 1,
                    "created" => "2017-02-14T15:02:48+00:00",
                    "modified" => "2017-02-14T15:02:48+00:00",
                    "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                    "photo" => "correctSoftwareLogo.jpg",
                    "url_website" => "http://www.fake-lutece.com",
                    "tag_string" => "myNewTagCreated myTagAdded1",
                    "sill" => null,
                    "cnll" => null,
                    "wikidata" => null,
                    "framalibre" => null,
                    "wikipedia_en" => null,
                    "wikipedia_fr" => null,
                    "userssoftwares" => [],
                    "average_review" => 0,
                ],
                "message" => "Success",
            ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $RSU_Table = TableRegistry::get('RelationshipsSoftwaresUsers');
        $RSU_Table->exists(["software_id" => 1, "user_id" => 3]);

        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test if an user (type = person) can undo his declared as a user of a software
     * Test deleteServicesProviders method
     */
    public function testFallBackAsUserOfForPersonUser()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 4,
                    "user_type_id" => 4,
                ]
            ]
        ]);

        $this->delete('api/v1/softwares/deleteUsersSoftware/1.json');

        $expected =
            [
                "message" => "Success",
            ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $RSU_Table = TableRegistry::get('RelationshipsSoftwaresUsers');
        $this->assertFalse($RSU_Table->exists(["software_id" => 1, "user_id" => 4]));
        $this->assertResponseOk();
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test if an user (type = administration) can undo
     * his declared as a user of a software (without an associated taxonomy)
     *
     * @group taxonomy
     * @group taxonomy-software
     */
    public function testFallBackAsUserOfForAdministrationUserWithoutAssociatedTaxonomy()
    {
        $userId = 3;
        $softwareId = 1; // 0 record in TaxonomysSoftwares table for this user
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => $userId,
                    "user_type_id" => 3,
                ]
            ]
        ]);
        // no record in TaxonomysSoftwares table for this user and this sofware
        $RSU_Table = TableRegistry::get('TaxonomysSoftwares');
        $this->assertFalse($RSU_Table->exists(["software_id" => $softwareId, "user_id" => $userId]));

        $expected = ["message" => "Success",];
        $expectedJson = json_encode($expected, JSON_PRETTY_PRINT);
        $this->delete("api/v1/softwares/deleteUsersSoftware/$softwareId.json");
        $this->assertResponseOk();
        $this->assertEquals($expectedJson, $this->_response->body());

        $RSU_Table = TableRegistry::get('RelationshipsSoftwaresUsers');
        $this->assertFalse($RSU_Table->exists(["software_id" => $softwareId, "user_id" => $userId]));
    }

    /**
     * Test if an user (type = administration) can undo
     * his declared as a user of a software (with an associated taxonomy).
     *
     * Checks:
     * - remove record in "RelationshipsSoftwaresUsers" table
     * - remove records in "TaxonomysSoftwares" table for this user and this software
     * - don't remove others records in "TaxonomysSoftwares" table for this user and another software
     *
     * @group taxonomy
     * @group taxonomy-software
     */
    public function testFallBackAsUserOfForAdministrationUserWithAssociatedTaxonomy()
    {
        $userId = 3;
        $softwareId = 4;        // 2 records in TaxonomysSoftwares table for this user
        $anotherSoftwareId = 2; // 1 record  in TaxonomysSoftwares table for this user
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => $userId,
                    "user_type_id" => 3,
                ]
            ]
        ]);
        // 2 records in TaxonomysSoftwares table
        // for this user and this software
        $RSU_Table = TableRegistry::get('TaxonomysSoftwares');
        $this->assertTrue($RSU_Table->exists(["software_id" => $softwareId, "user_id" => $userId]));
        $this->assertEquals(
            2,
            $RSU_Table->find()
                      ->where(["software_id" => $softwareId, "user_id" => $userId])
                      ->count()
        );
        // 1 record in TaxonomysSoftwares table
        // for this user and a another software
        $this->assertEquals(
            1,
            $RSU_Table->find()
                ->where(["software_id" => $anotherSoftwareId, "user_id" => $userId])
                ->count()
        );

        $expected = ["message" => "Success",];
        $expectedJson = json_encode($expected, JSON_PRETTY_PRINT);
        $this->delete("api/v1/softwares/deleteUsersSoftware/$softwareId.json");
        $this->assertResponseOk();
        $this->assertEquals($expectedJson, $this->_response->body());

        $RSU_Table = TableRegistry::get('RelationshipsSoftwaresUsers');
        $this->assertFalse($RSU_Table->exists(["software_id" => $softwareId, "user_id" => $userId]));

        // no record in TaxonomysSoftwares table
        // for this user and this software
        // ---> The 2 records have been deleted.
        $RSU_Table = TableRegistry::get('TaxonomysSoftwares');
        $this->assertFalse($RSU_Table->exists(["software_id" => $softwareId, "user_id" => $userId]));

        // 1 record in TaxonomysSoftwares table
        // for this user and a another software
        $this->assertEquals(
            1,
            $RSU_Table->find()
                ->where(["software_id" => $anotherSoftwareId, "user_id" => $userId])
                ->count()
        );
    }



    /**
     */
    public function testAddreview()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data = [
            "comment" => "My comment",
            "title" => "My title",
            "evaluation" => 4,
            "software_id" => 3,
            "user_id" => 1
        ];


        $this->post('api/v1/softwares/3/reviews.json', $data);

        $expected =
            [
                "message" => "Success",
                "review" => [
                    "comment" => "My comment",
                    "title" => "My title",
                    "evaluation" => 4,
                    "software_id" => 3,
                    "user_id" => 1,
                    "created" => "2016-10-11T09:50:52+00:00",
                    "modified" => "2016-10-11T09:50:52+00:00",
                    "id" => 2
                ]

            ];

        $this->assertResponseOk();
        $this->assertContains($expected["message"], $this->_response->body());
        $this->assertContains($expected['review']['comment'], $this->_response->body());
        $this->assertContains($expected['review']['title'], $this->_response->body());
        $this->assertContains((string)$expected['review']['software_id'], $this->_response->body());
        $this->assertEventFired('Model.Review.created', $this->Softwares->Reviews->eventManager());
    }

    public function testAddreviewNotAuthorizedUser()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "comment" => "My comment",
            "title" => "My title",
            "evaluation" => 4,
            "software_id" => 3,
            "user_id" => 1
        ];

        $this->post('api/v1/softwares/3/reviews.json', $data);
        $this->assertResponseCode(403);
    }


    /**
     */
    public function testAddreviewTwice()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);


        $data = [
            "comment" => "My comment",
            "title" => "My title",
            "evaluation" => 4,
            "software_id" => 3,
            "user_id" => 1
        ];

        $this->post('api/v1/softwares/2/reviews.json', $data);
        $this->post('api/v1/softwares/2/reviews.json', $data);

        $expected =
            [
                "message" => "Error",
                "review" => [
                    "software_id" => [
                        "_isUnique" => "You can not post more than one testimony for a software."
                    ]
                ]
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $this->assertResponseOk();
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     *
     */
    public function testDeclareAsUserOfTwice()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->post('api/v1/softwares/usersSoftware/1.json');
        $expected =
            [
                "software" => [
                    "software_id" => [
                        "_isUnique" => "You can not be declare twice for the same relationships."
                    ]
                ],
                "message" => "Error",
            ];
        $this->post('api/v1/softwares/usersSoftware/1.json');

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertResponseOk();
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     */
    public function testDeclareAsProviderFor()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $this->post('api/v1/softwares/servicesProviders/1.json');

        $expected =
            [
                "software" => [
                    "id" => 1,
                    "softwarename" => "Asalae",
                    "url_repository" => "http://www.fake-repo-asalae.git",
                    "description" => "Asalae description",
                    "licence_id" => 1,
                    "created" => "2017-02-14T15:02:48+00:00",
                    "modified" => "2017-02-14T15:02:48+00:00",
                    "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                    "photo" => "correctSoftwareLogo.jpg",
                    "url_website" => "http://www.fake-asalae.com",
                    "tag_string" => "myNewTagCreated myTagDeleted",
                    "sill" => null,
                    "cnll" => null,
                    "wikidata" => null,
                    "framalibre" => null,
                    "wikipedia_en" => null,
                    "wikipedia_fr" => null,
                    "providerssoftwares" => [],
                    "average_review" => 0,
                ],
                "message" => "Success",

            ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $this->assertResponseOk();
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     */
    public function testDeclareAsProviderForTwice()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $this->post('api/v1/softwares/servicesProviders/1.json');
        $this->assertResponseOk();

        $expected =
            [
                "software" => [
                    "software_id" => [
                        "_isUnique" => "You can not be declare twice for the same relationships."
                    ]
                ],
                "message" => "Error",
            ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $this->post('api/v1/softwares/servicesProviders/1.json');
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test add method
     *Don't test the license inserrt. Problem in fixture of License of lisense_types
     * @return void
     */
    public function testAddAuthenticatedTrue()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->post('api/v1/softwares.json');

        $data =
            [
                'softwarename' => 'My new software',
                'description' => 'This is my new software and...',
                'licence_id' => 1,
            ];
        $this->post('api/v1/softwares.json', $data);

        // Vérifie que le code de réponse est 200
        $this->assertResponseOk();
        $this->assertContains("Error", $this->_response->body());
        $this->assertContains("This field is required", $this->_response->body());
    }

    /**
     * Test add method
     *Don't test the license inserrt. Problem in fixture of License of lisense_types
     * @return void
     */
    public function testAddAuthenticatedFalse()
    {
        $this->post('api/v1/softwares.json');

        $data =
            [
                'softwarename' => 'My new software',
                'url_repository' => "url",
                'description' => 'This is my new software and...',
                'licence_id' => 1,
                'created' => "2017-02-14T15:02:48+00:00",
                'modified' => "2017-02-14T15:02:48+00:00",
            ];

        $this->post('api/v1/softwares/add.json', $data);
        $this->assertResponseCode(302);
    }

    /**
     * Test add method
     *  Test when bad url was given for url_repository
     * @return void
     */
    public function testAddBadUrlOnUrlRepository()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data =
            [
                'softwarename' => 'My new software',
                'url_repository' => "url",
                'description' => 'This is my new software and...',
                'licence_id' => 1,
                'created' => "2017-02-14T15:02:48+00:00",
                'modified' => "2017-02-14T15:02:48+00:00",
            ];

        $this->post('api/v1/softwares.json', $data);
        $this->assertContains('"url": "The provided value is invalid"', $this->_response->body());
    }

    /**
     * Test add Relationships Softwares users
     *
     * @return void
     */
    public function testUsersSoftware()
    {
        $this->post('api/v1/softwares/usersSoftware/3.json', []);
        $this->assertResponseCode('302');

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data =
            [
                "software_id" => 3,
                "user_id" => 1,
                "relationship_id" => 2,
                "created" => null,
                "modified" => null,

            ];

        $this->post('api/v1/softwares/usersSoftware/3.json', $data);

        // Vérifie que le code de réponse est 200
        $this->assertResponseOk();

        $expected = [
            "software" => [
                "id" => 3,
                "softwarename" => "Soft 3",
                "url_repository" => "http://www.fake-repo-soft3.git",
                "description" => "Description Lorem Ipsum",
                "licence_id" => 1,
                "created" => "2017-02-14T15:02:48+00:00",
                "modified" => "2017-02-14T15:02:48+00:00",
                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                "photo" => "correctSoftwareLogo.jpg",
                "url_website" => "http://www.fake-soft3.com",
                "tag_string" => "myNewTagCreated myTagAdded1 myTagAdded2",
                "sill" => null,
                "cnll" => null,
                "wikidata" => null,
                "framalibre" => null,
                "wikipedia_en" => null,
                "wikipedia_fr" => null,
                "userssoftwares" => [],
                "average_review" => 0,

            ],
            "message" => "Success"
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test add Relationships Softwares users
     *
     * @return void
     */
    public function testusersSoftwareWithBadDatas()
    {
        $this->post('api/v1/softwares/usersSoftware/1.json', []);
        $this->assertResponseCode('302');
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->post('api/v1/softwares/usersSoftware/10.json', []);
        $this->assertResponseCode('404');
    }

    public function testDeleteUsersSoftwareUserNotMatch()
    {

        $this->post('api/v1/softwares/deleteUsersSoftware/1.json', []);
        $this->assertResponseCode('302');

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->delete('api/v1/softwares/deleteUsersSoftware/2.json', []);
        $this->assertResponseOk();

        $expected = [
            "message" => "No relationship in our data base."
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    public function testDeleteUsersSoftwareUser()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data =
            [
                "software_id" => 1,
                "user_id" => 3,
                "relationship_id" => 2,
                "created" => null,
                "modified" => null,

            ];

        $this->post('api/v1/softwares/usersSoftware/1.json', $data);
        $this->delete('api/v1/softwares/deleteUsersSoftware/1.json', []);
        $this->assertResponseOk();

        $expected = [
            "message" => "Success"
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test the deleteServicesProvider method
     * A user can roll back himself of the list of service providers for a software
     * But if he does no in the list an error message will be sent.
     */
    public function testDeleteServicesProvidersNotMatch()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $this->delete('api/v1/softwares/deleteServicesProviders/1.json', []);

        $expected = [
            "message" => "No relationship in our data base."
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test the deleteServicesProvider method
     * A user can roll back himself of the list of service providers for a software
     */
    public function testDeleteServicesProviders()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->delete('api/v1/softwares/deleteServicesProviders/2.json', []);
        $this->assertResponseOk();

        $expected = [
            "message" => "Success"
        ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test the edit method
     * Only a connected user can edit a software
     * All connected users can modify a software as he wants.
     */
    public function testEditSoftware()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data =
            [
                "softwarename" => "toto",
            ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();
        $this->Softwares->exists(["id" => 2, "softwarename" => "toto"]);
        $this->assertEventFired('Model.Software.modified', $this->Softwares->eventManager());
    }

    /**
     * Test the edit method
     * Only a connected user can edit a software
     * All connected users can modify a software as he wants.
     * If the user is not connected then return error code => 401 = Unauthorized
     */
    public function testEditSoftwareNotConnectedUser()
    {
        $data =
            [
                "softwarename" => "toto",
            ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseCode(302);
    }

    /**
     * Test the edit method
     * Only a connected user can edit a software => EDIT THE LOGO => fields 'logo_directory' and 'photo'
     * All connected users can modify a software as he wants.
     */
    public function testEditSoftwareLogo()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Lutèce",
            "photo" => $this->correctFile
        ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();

        $this->assertEventFired('Model.Software.modified', $this->Softwares->eventManager());

        //Check in the database
        $query = $this->Softwares->find()->where([
            "id" => 2,
            "logo_directory" => "files/Softwares/Lutèce/avatar",
            "photo" => $this->correctFile["name"]
        ]);

        $this->assertEquals(1, $query->count());

        define(
            "LUTECE_AVATAR",
            WWW_ROOT . 'img' . DS . "files/Softwares/Lutèce/avatar/" . $this->correctFile["name"]
        );

        $this->assertFileExists(LUTECE_AVATAR);

        //Delete the file after testing
        unlink(LUTECE_AVATAR);
        rmdir(WWW_ROOT . 'img' . DS . "files/Softwares/Lutèce/avatar");
        rmdir(WWW_ROOT . 'img' . DS . "files/Softwares/Lutèce");
        $this->assertFileNotExists(LUTECE_AVATAR);
    }


    /**
     * Test the edit method
     * Only a connected user can edit a software => EDIT THE LOGO WRONG FILE => fields 'logo_directory' and 'photo'
     * All connected users can modify a software as he wants.
     */
    public function testEditSoftwareLogoDoesNotRespectTheRules()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Lutèce",
            "photo" => $this->wrongFile
        ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();
        $this->assertContains("errors", $this->_response->body());

        //Value of message
        $this->assertContains("error", $this->_response->body());

        //Check in the database
        $query = $this->Softwares->find()->where([
            "id" => 2,
            "logo_directory" => "files/Softwares/2/photo/avatar",
            "photo" => $this->wrongFile["name"]
        ]);
        $this->assertEquals(0, $query->count());
        $this->assertFileNotExists(
            WWW_ROOT . 'img' . DS . "files/Softwares/2/photo/avatar/" . $this->wrongFile["name"]
        );
    }

    /**
     * Test the edit method
     * Only a connected user can edit a software => ADD A SCREENSHOT
     * All connected users can modify a software as he wants.
     */
    public function testEditSoftwareAddAScreenshot()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Lutèce",
            "screenshots" => [
                "0" => [
                    "photo" => $this->correctFile
                ]
            ]
        ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();

        //Check in the database
        $query = $this->Softwares->Screenshots->find()->where([
            "software_id" => 2,
            "url_directory" => "files/Screenshots",
            "photo" => time() . "_" . $this->correctFile["name"]
        ]);
        $this->assertEventFired('Model.Screenshot.created', $this->Softwares->Screenshots->eventManager());

        $this->assertEquals(1, $query->count());
        define(
            "SOFTWARE_SCREENSHOT",
            WWW_ROOT . 'img' . DS . "files/Screenshots/" . time() . "_" . $this->correctFile["name"]
        );
        $this->assertFileExists(SOFTWARE_SCREENSHOT);

        //Delete the file after testing
        unlink(SOFTWARE_SCREENSHOT);
        $this->assertFileNotExists(SOFTWARE_SCREENSHOT);
    }


    /**
     * Test the edit method
     * Only a connected user can edit a software => ADD A WRONG SCREENSHOT
     * All connected users can modify a software as he wants.
     */
    public function testEditSoftwareAddAWrongScreenshot()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Lutèce",
            "screenshots" => [
                "0" => [
                    "photo" => $this->wrongFile
                ]
            ]
        ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();
        $this->assertContains("errors", $this->_response->body());

        //Check in the database
        $query = $this->Softwares->Screenshots->find()->where([
            "software_id" => 2,
            "url_directory" => "files/Screenshots",
            "photo" => time() . "_" . $this->wrongFile["name"]
        ]);

        $this->assertEquals(0, $query->count());
        $this->assertFileNotExists(
            WWW_ROOT . 'img' . DS . "files/Screenshots/" . time() . "_" . $this->wrongFile["name"]
        );
    }




//////  Checks for the ADD software form with or without "userOf" or "ProviderFor" parameters (or equal to "no")
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Common checks for the Add Software Form
     * - Add new software by sending an HTTP Post request (the UI of the form is not tested)
     * - Check that the software now exists in the database.
     * - Check that the main data is correctly recorded in the database.
     * - Check if current HTTP response is a redirection to the software URL.
     *
     * @param array $extraData  additional data for the form (addition or overloading)
     * @param string $redirectType   'softwares' or 'mappingForm'
     * @return int ID of added software
     */
    private function commonCheckForHtmlFormAddSoftware(array $extraData = [], string $redirectType = 'softwares')
    {
        $softwareName = 'My new software '. mt_rand();
        $commonData = [
            'softwarename' => $softwareName,
            'url_repository' => 'http://www'. mt_rand() .'.mynewsoftware.com/',
            'description' => 'This is my new software and...',
            'licence_id' => 1,
            "url_website" => null,
            "tag_string" => "test aze great",
        ];

        // Cheking that the software does not exist in the database before creating it.
        $this->assertEquals(0, $this->Softwares->find()->where(['softwarename' => $softwareName])->count());

        // Add software
        $dataForm = $commonData;
        foreach ($extraData as $key => $value) {
            $dataForm[$key] = $value;
        }
        $this->post('/fr/softwares/add', $dataForm);
        $this->assertEventFired('Model.Software.created', $this->Softwares->eventManager());

        // Cheking that the software now exists in the database and grab it's ID.
        $this->assertEquals(1, $this->Softwares->find()->where(['softwarename' => $softwareName])->count());
        $software = $this->Softwares->find()->where(['softwarename' => $softwareName])->limit(1)->toList()[0];
        $softwareId = $software->id;

        // Cheking if current HTTP response is a redirection to the software URL
        $redirectTo = "/fr/$redirectType/$softwareId";
        $this->currentResponseIsRedirectionToAnotherUrl($redirectTo);

        // Check that the main data is correctly recorded in the database.
        $dataInDb = [
            'softwarename' => $software->get('softwarename'),
            'url_repository' => $software->get('url_repository'),
            'description' => $software->get('description'),
            'licence_id' => $software->get('licence_id'),
            "url_website" => $software->get('url_website'),
            "tag_string" => $software->get('tag_string'),
        ];
        $this->assertTrue($this->arraysAreSimilar($commonData, $dataInDb));

        // return ID of added software
        return $softwareId;
    }

    /**
     * Common checks for the Add Software Form
     * without "userOf" or "ProviderFor" parameters (or equal to "no")
     * - (1) Add new software by sending an HTTP Post request (the UI of the form is not tested)
     * - (1) Check that the software now exists in the database.
     * - (1) Check that the main data is correctly recorded in the database.
     * - (1) Check if current HTTP response is a redirection to the software URL.
     * - For current connected user and this new software:
     *   - Check if there are 0 records in 'TaxonomysSoftwares' table
     *   - Check if there are 0 records in 'RelationshipsSoftwaresUsers' table
     *
     * (1) via commonCheckForHtmlFormAddSoftware()
     *
     * @param array $extraData  additional data for the form (addition or overloading)
     */
    private function commonCheckForHtmlFormAddSoftwareWithoutExtraFormData($extraData = [])
    {
        $softwareId = $this->commonCheckForHtmlFormAddSoftware($extraData);
        $userId = $this->getConnectedUserId();

        // Check if there are 0 records in 'TaxonomysSoftwares' table
        // for this user and this software
        $registry = TableRegistry::get('TaxonomysSoftwares');
        $nb = $registry->find()->where(["software_id" => $softwareId, "user_id" => $userId])->count();
        $this->assertEquals(0, $nb);

        // Check if there are 0 records in 'RelationshipsSoftwaresUsers' table
        // for this user and this software
        $registry = TableRegistry::get('RelationshipsSoftwaresUsers');
        $nb = $registry->find()->where(["software_id" => $softwareId, "user_id" => $userId])->count();
        $this->assertEquals(0, $nb);
    }


    /**
     * Common checks for the Add Software Form
     * with "userOf" or "ProviderFor" parameters equal to "yes".
     * - (1) Add new software by sending an HTTP Post request (the UI of the form is not tested)
     * - (1) Check that the software now exists in the database.
     * - (1) Check that the main data is correctly recorded in the database.
     * - (1) Check if current HTTP response is a redirection to the software URL.
     * - For this user and this new software:
     *   - Check if there are 0 records in 'TaxonomysSoftwares' table
     *   - Check if there are 1 records in 'RelationshipsSoftwaresUsers' table
     *   - Check if there are 1 records in 'RelationshipsSoftwaresUsers' table, with correct relationship ID
     *
     * (1) via commonCheckForHtmlFormAddSoftware()
     *
     * @param string $redirectType   'softwares' or 'mappingForm'
     * @param array $extraData  additional data for the form (addition or overloading)
     */
    private function commonCheckForHtmlFormAddSoftwareWithExtraFormData(
        string $relationshipName,
        string $redirectType = 'softwares'
    ) {
        // Launch common tests
        $extraData  = [$relationshipName => 'yes'];
        $softwareId = $this->commonCheckForHtmlFormAddSoftware($extraData, $redirectType);

        // Get connected user ID
        $userId = $this->getConnectedUserId();
        $conditions = [
            "software_id" => $softwareId,
            "user_id" => $userId
        ];

        // Check if there are 0 records in 'TaxonomysSoftwares' table
        // for this user and this software
        $registry = TableRegistry::get('TaxonomysSoftwares');
        $nb = $registry->find()->where($conditions)->count();
        $this->assertEquals(0, $nb);

        // Check if there are 1 records in 'RelationshipsSoftwaresUsers' table
        // for this user and this software
        $registry = TableRegistry::get('RelationshipsSoftwaresUsers');
        $nb = $registry->find()->where($conditions)->count();
        $this->assertEquals(1, $nb);

        // Get relationship ID for current relationship
        $registry = TableRegistry::get('Relationships');
        $relationship = $registry->find("all")->where(["cd" => ucfirst($relationshipName)])->first();
        $conditions['relationship_id'] = $relationship->id;

        // Check if there are 1 records in 'RelationshipsSoftwaresUsers' table
        // for this user, this software and current relationship
        $registry = TableRegistry::get('RelationshipsSoftwaresUsers');
        $nb = $registry->find()->where($conditions)->count();
        $this->assertEquals(1, $nb);
    }



    /**
     * Test HTML form:
     * for the different user types ("Person", "Association", "Administration" and "Company"),
     * add new software by sending an HTTP Post request (the UI of the form is not tested)
     * without "userOf" or "ProviderFor" parameters (or equal to "no").
     *
     * - (1) Add new software by sending an HTTP Post request (the UI of the form is not tested)
     * - (1) Check that the software now exists in the database.
     * - (1) Check that the main data is correctly recorded in the database.
     * - (1) Check if current HTTP response is a redirection to the software URL.
     * - For this user and this new software:
     *   - (2) Check if there are 0 records in 'TaxonomysSoftwares' table
     *   - (2) Check if there are 0 records in 'RelationshipsSoftwaresUsers' table
     *
     *      (1) via commonCheckForHtmlFormAddSoftware()
     *      (2) via commonCheckForHtmlFormAddSoftwareWithoutExtraFormData()
     *
     * @group user
     * @group user_person
     * @group software
     * @group software_add
     * @group software_methode_add
     */
    public function testHtmlFormAddSoftwareWithoutExtraFormData()
    {

        // "Person" user
        $this->setConnectedPersonSession();
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData([]); // without "userOf" or "ProviderFor" parameter
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData(['userOf' => 'no']);
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData(['userOf' => 'hackerInForm']);
//      $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData(['providerFor' => 'yes']); // ----> must FAIL

        // "Association" user
        $this->setConnectedAssociationSession();
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData([]); // without "userOf" or "ProviderFor" parameter
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData(['userOf' => 'no']);
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData(['userOf' => 'hackerInForm']);

        // "Administration" user
        $this->setConnectedAdministrationSession();
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData([]); // without "userOf" or "ProviderFor" parameter
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData(['userOf' => 'no']);
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData(['userOf' => 'hackerInForm']);

            // "Administration" user, with "admin" role  @@@TODO
            $this->setAdminSessionOfAdministrationType();
            $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData(['userOf' => 'no']);

        // "Company" user
        $this->setConnectedCompanySession();
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData([]); // without "userOf" or "ProviderFor" parameter
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData(['providerFor' => 'no']);
        $this->commonCheckForHtmlFormAddSoftwareWithoutExtraFormData(['providerFor' => 'hackerInForm']);
    }


    /**
     * Test HTML form:
     * for the different user types ("Person", "Association", "Administration" and "Company"),
     * add new software by sending an HTTP Post request (the UI of the form is not tested)
     * with "userOf" or "ProviderFor" parameters equal to "yes".
     *
     * - (1) Add new software by sending an HTTP Post request (the UI of the form is not tested)
     * - (1) Check that the software now exists in the database.
     * - (1) Check that the main data is correctly recorded in the database.
     * - (1) Check if current HTTP response is a redirection to the software URL.
     * - For this user and this new software:
     *   - (2) Check if there are 0 records in 'TaxonomysSoftwares' table
     *   - (2) Check if there are 1 records in 'RelationshipsSoftwaresUsers' table
     *   - (2) Check if there are 1 records in 'RelationshipsSoftwaresUsers' table, with correct relationship ID
     *
     *      (1) via commonCheckForHtmlFormAddSoftware()
     *      (2) via commonCheckForHtmlFormAddSoftwareWithExtraFormData()
     *
     * @group user
     * @group user_person
     * @group software
     * @group software_add
     * @group software_methode_add
     */
    public function testHtmlFormAddSoftwareWithExtraFormData()
    {

        // "Person" user
        $this->setConnectedPersonSession();
        $this->commonCheckForHtmlFormAddSoftwareWithExtraFormData('userOf');

        // "Association" user
        $this->setConnectedAssociationSession();
        $this->commonCheckForHtmlFormAddSoftwareWithExtraFormData('userOf');

        // "Administration" user
        $this->setConnectedAdministrationSession();
        $this->commonCheckForHtmlFormAddSoftwareWithExtraFormData('userOf', 'mappingForm');

        // "Company" user
        $this->setConnectedCompanySession();
        $this->commonCheckForHtmlFormAddSoftwareWithExtraFormData('providerFor');
    }


//////  Checks for the EDIT software form with or without "userOf" or "ProviderFor" parameters (or equal to "no")
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Common checks for the Edit Software Form
     * - Edit a software by sending an HTTP Post request (the UI of the form is not tested)
     * - Check that the main data is correctly recorded in the database.
     * - Check if current HTTP response is a redirection to the software URL.
     *
     * @param array $extraData  additional data for the form (addition or overloading)
     * @param string $redirectType   'softwares' or 'mappingForm'
     * @return int ID of edited software
     */
    private function commonCheckForHtmlFormEditSoftware(
        int $softwareId,
        array $extraData = [],
        string $redirectType = 'softwares'
    ) {
        // Grab a software in the database before updating it.
        $software = $this->Softwares->find()->where(['id' => $softwareId])->toList()[0];
        $alea = mt_rand();
        $newSoftwareName = $software->get('softwarename'). " $alea";
        $commonData = [
            'softwarename' => $newSoftwareName,
            'url_repository' => $software->get('url_repository') . "/$alea",
            'description' => $software->get('description')       . " $alea",
            'licence_id' => $software->get('licence_id'),
            "url_website" => $software->get('url_website')       . "/$alea",
            "tag_string" => $software->get('tag_string'),
        ];

        // Edit software
        $dataForm = $commonData;
        foreach ($extraData as $key => $value) {
            $dataForm[$key] = $value;
        }
        $this->put("/fr/softwares/edit/$softwareId", $dataForm);
        $this->assertEventFired('Model.Software.modified', $this->Softwares->eventManager());

        // Cheking that the software now exists with its new name
        // in the database and grab it's ID.
        $this->assertEquals(
            1,
            $this->Softwares
                ->find()
                ->where(['softwarename' => $newSoftwareName ])
                ->count()
        );
        $softwareUpdated = $this->Softwares->find()->where(['id' => $softwareId ])->limit(1)->toList()[0];
        $this->assertEquals($softwareId, $softwareUpdated->id);

        // Cheking if current HTTP response is a redirection to the software URL
        $redirectTo = "/fr/$redirectType/$softwareId";
        $this->currentResponseIsRedirectionToAnotherUrl($redirectTo);

        // Check that the main data is correctly recorded in the database.
        $dataInDb = [
            'softwarename' => $softwareUpdated->get('softwarename'),
            'url_repository' => $softwareUpdated->get('url_repository'),
            'description' => $softwareUpdated->get('description'),
            'licence_id' => $softwareUpdated->get('licence_id'),
            "url_website" => $softwareUpdated->get('url_website'),
            "tag_string" => $softwareUpdated->get('tag_string'),
        ];
        $this->assertTrue($this->arraysAreSimilar($commonData, $dataInDb));

        // return ID of added software
        return $softwareId;
    }

    /**
     * Common checks for the Edit Software Form
     * without "userOf" or "ProviderFor" parameters (or equal to "no")
     * - (1) Edit a software by sending an HTTP Post request (the UI of the form is not tested)
     * - (1) Check that the main data is correctly recorded in the database.
     * - (1) Check if current HTTP response is a redirection to the software URL.
     * - For current connected user and this new software:
     *   - Check if there are 0 records in 'TaxonomysSoftwares' table
     *   - Check if there are 0 records in 'RelationshipsSoftwaresUsers' table
     *
     *  (1) via commonCheckForHtmlFormEditSoftware()
     *
     * @param array $extraData  additional data for the form (addition or overloading)
     */
    private function commonCheckForHtmlFormEditSoftwareWithoutExtraFormData($extraData = [])
    {
        $softwareId = 2;
        $softwareId = $this->commonCheckForHtmlFormEditSoftware($softwareId, $extraData);
        $userId = $this->getConnectedUserId();

        // Check if there are 0 records in 'RelationshipsSoftwaresUsers' table
        // for this user and this software
        $registry = TableRegistry::get('RelationshipsSoftwaresUsers');
        $nb = $registry->find()->where(["software_id" => $softwareId, "user_id" => $userId])->count();
        $this->assertEquals(0, $nb);
    }

    /**
     * Common checks for the Edit Software Form
     * with "userOf" or "ProviderFor" parameters equal to "yes".
     * - (1) Add new software by sending an HTTP Post request (the UI of the form is not tested)
     * - (1) Check that the main data is correctly recorded in the database.
     * - (1) Check if current HTTP response is a redirection to the software URL.
     * - For this user and this new software:
     *   - Check if there are 1 records in 'RelationshipsSoftwaresUsers' table
     *   - Check if there are 1 records in 'RelationshipsSoftwaresUsers' table, with correct relationship ID
     *
     *  (1) via commonCheckForHtmlFormEditSoftware()
     *
     * @param string $redirectType   'softwares' or 'mappingForm'
     * @param array $extraData  additional data for the form (addition or overloading)
     */
    private function commonCheckForHtmlFormEditSoftwareWithExtraFormData(
        string $relationshipName,
        string $redirectType = 'softwares'
    ) {

        $softwareId = 2;

        // Get connected user ID
        $userId = $this->getConnectedUserId();
        $conditions = [
            "software_id" => $softwareId,
            "user_id" => $userId
        ];

        // Before editing software, for this user and this software,
        // check if there are 0 records in 'RelationshipsSoftwaresUsers' table
        $registry = TableRegistry::get('RelationshipsSoftwaresUsers');
        $nb = $registry->find()->where($conditions)->count();
        $this->assertEquals(0, $nb);

        // Launch common tests
        $extraData  = [$relationshipName => 'yes'];
        $this->commonCheckForHtmlFormEditSoftware($softwareId, $extraData, $redirectType);

        // Check if there are 1 records in 'RelationshipsSoftwaresUsers' table
        // for this user and this software
        $registry = TableRegistry::get('RelationshipsSoftwaresUsers');
        $nb = $registry->find()->where($conditions)->count();
        $this->assertEquals(1, $nb);

        // Get relationship ID for current relationship
        $registry = TableRegistry::get('Relationships');
        $relationship = $registry->find("all")->where(["cd" => ucfirst($relationshipName)])->first();
        $conditions['relationship_id'] = $relationship->id;

        // Check if there are 1 records in 'RelationshipsSoftwaresUsers' table
        // for this user, this software and current relationship
        $registry = TableRegistry::get('RelationshipsSoftwaresUsers');
        $nb = $registry->find()->where($conditions)->count();
        $this->assertEquals(1, $nb);
    }


    /**
     * Test HTML form:
     * for the different user types ("Person", "Association", "Administration" and "Company"),
     * edit a software by sending an HTTP Post request (the UI of the form is not tested)
     * without "userOf" or "ProviderFor" parameters (or equal to "no").
     *
     * - (1) Edit new software by sending an HTTP Post request (the UI of the form is not tested)
     * - (1) Check that the main data is correctly recorded in the database.
     * - (1) Check if current HTTP response is a redirection to the software URL.
     * - For this user and this software:
     *   - (2) Check if there are 0 records in 'RelationshipsSoftwaresUsers' table
     *
     *      (1) via commonCheckForHtmlFormEditSoftware()
     *      (2) via commonCheckForHtmlFormEditSoftwareWithoutExtraFormData()
     *
     * @group user
     * @group user_person
     * @group software
     * @group software_edit
     * @group software_methode_edit
     */
    public function testHtmlFormEditSoftwareWithoutExtraFormData()
    {
        // "Person" user
        $this->setConnectedPersonSession();
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData([]); // without userOf or ProviderFor parameter
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData(['userOf' => 'no']);
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData(['userOf' => 'hackerInForm']);
//      $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData(['providerFor' => 'yes']); // ----> must FAIL

        // "Association" user
        $this->setConnectedAssociationSession();
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData([]); // without userOf or ProviderFor parameter
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData(['userOf' => 'no']);
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData(['userOf' => 'hackerInForm']);

        // "Administration" user
        $this->setConnectedAdministrationSession();
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData([]); // without userOf or ProviderFor parameter
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData(['userOf' => 'no']);
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData(['userOf' => 'hackerInForm']);

        // "Administration" user, with "admin" role  @@@TODO
        $this->setAdminSessionOfAdministrationType();
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData(['userOf' => 'no']);

        // "Company" user
        $this->setConnectedCompanySession();
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData([]); // without userOf or ProviderFor parameter
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData(['providerFor' => 'no']);
        $this->commonCheckForHtmlFormEditSoftwareWithoutExtraFormData(['providerFor' => 'hackerInForm']);
    }

    /**
     * Test HTML form:
     * for the different user types ("Person", "Association", "Administration" and "Company"),
     * edit a software by sending an HTTP Post request (the UI of the form is not tested)
     * with "userOf" or "ProviderFor" parameters equal to "yes".
     *
     * - (1) Edit a software by sending an HTTP Post request (the UI of the form is not tested)
     * - (1) Check that the main data is correctly recorded in the database.
     * - (1) Check if current HTTP response is a redirection to the software URL.
     * - For this user and this new software:
     *   - (2) Check if there are 1 records in 'RelationshipsSoftwaresUsers' table
     *   - (2) Check if there are 1 records in 'RelationshipsSoftwaresUsers' table, with correct relationship ID
     *
     *      (1) via commonCheckForHtmlFormEditSoftware()
     *      (2) via commonCheckForHtmlFormEditSoftwareWithExtraFormData()
     *
     * @group user
     * @group user_person
     * @group software
     * @group software_edit
     * @group software_methode_edit
     */
    public function testHtmlFormEditSoftwareWithExtraFormData()
    {

        // "Person" user
        $this->setConnectedPersonSession();
        $this->commonCheckForHtmlFormEditSoftwareWithExtraFormData('userOf');

        // "Association" user
        $this->setConnectedAssociationSession();
        $this->commonCheckForHtmlFormEditSoftwareWithExtraFormData('userOf');

        // "Administration" user
        $this->setConnectedAdministrationSession();
        $this->commonCheckForHtmlFormEditSoftwareWithExtraFormData('userOf', 'mappingForm');

        // "Company" user
        $this->setConnectedCompanySession();
        $this->commonCheckForHtmlFormEditSoftwareWithExtraFormData('providerFor');
    }
}
