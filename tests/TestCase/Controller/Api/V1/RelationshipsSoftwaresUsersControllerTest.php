<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\RelationshipsSoftwaresUsersController;

/**
 * App\Controller\RelationshipsSoftwaresUsersController Test Case
 */
class RelationshipsSoftwaresUsersControllerTest extends ApiIntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.relationships_softwares_users',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.users',
        'app.user_types',
        'app.relationships',
        'app.relationship_types',
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
