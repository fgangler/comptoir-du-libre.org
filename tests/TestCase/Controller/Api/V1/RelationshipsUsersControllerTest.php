<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\RelationshipsUsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RelationshipsUsersController Test Case
 */
class RelationshipsUsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [

        'app.users',
        'app.user_types',
        'app.reviews',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.screenshots',
        'app.relationships',
        'app.relationships_users',
        'app.relationship_types',
        'app.relationships_softwares',
//        'app.relationships_softwares_users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
