<?php

namespace App\Test\TestCase\Controller\Api\V1;

/**
 * App\Controller\LicenceTypesController Test Case
 */
class LicenceTypesControllerTest extends ApiIntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
        'app.licence_types',
        'app.user_types',
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('api/v1/licenceTypes.json');
        $this->assertResponseOk();

        $expected = [
            'licenceTypes' => [
                [
                    'id' => 1,
                    'name' => 'Libre',
                    'created' => '2017-02-14T15:02:35+00:00',
                    'modified' => '2017-02-14T15:02:35+00:00',
                    'cd' => 'Free'
                ],
                [
                    'id' => 2,
                    'name' => 'Propriétaire',
                    'created' => '2017-02-14T15:02:35+00:00',
                    'modified' => '2017-02-14T15:02:35+00:00',
                    'cd' => 'NotFree'
                ]
            ]
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->get('api/v1/licenceTypes/view/1.json');
        $this->assertResponseOk();

        $expected = [
            'licenceType' => [
                'id' => 1,
                'name' => 'Libre',
                'created' => '2017-02-14T15:02:35+00:00',
                'modified' => '2017-02-14T15:02:35+00:00',
                'cd' => 'Free'
            ]
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }
    /**
     * Test view method when licenceTypes does not exists
     *
     * @return void
     */
    public function testViewWhenDoesNotExists()
    {

        $this->get('api/v1/licenceTypes/333.json');
        $this->assertEquals($this->_exception->getCode(), 404);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddWhenNotConnected()
    {

        $data = [
                'name' => 'LibreOuPas',
                'cd' => 'FreeOrNot'
        ];
        $this->post('api/v1/licenceTypes/add.json', $data);

        $this->assertResponseCode(302);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddWhenConnectedAsAdminUser()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    "user_type_id" => 1,
                    'role'=>"admin",
                ]
            ]
        ]);

        $data = [
                'name' => 'LibreOuPas',
                'cd' => 'FreeOrNot'
        ];
        $this->post('api/v1/licenceTypes/add.json', $data);

        $this->assertResponseCode(302);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddWhenConnectedAsLambdaUser()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                    'role'=>"User",
                ]
            ]
        ]);

        $data = [
            'name' => 'LibreOuPas',
            'cd' => 'FreeOrNot'
        ];
        $this->post('api/v1/licenceTypes/add.json', $data);
        $this->assertResponseCode(403);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditWhenKnownAsAdminUser()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    "user_type_id" => 1,
                    'role'=>"admin",
                ]
            ]
        ]);

        $data = [
            'name' => 'LibreOuPas',
            'cd' => 'FreeOrNot'
        ];
        $this->put('api/v1/licenceTypes/edit/1.json', $data);

        $this->assertResponseCode(302); // redirect when success
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditWhenNotKnownAsAdminUser()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                    'role'=>"User",
                ]
            ]
        ]);

        $data = [
            'name' => 'LibreOuPas',
            'cd' => 'FreeOrNot'
        ];
        $this->put('api/v1/licenceTypes/edit/1.json', $data);

        $this->assertResponseCode(403); // redirect when success
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDeleteAsAdminUser()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    "user_type_id" => 1,
                    'role'=>"admin",
                ]
            ]
        ]);

        $this->delete('api/v1/licenceTypes/delete/1.json');

        $this->assertResponseCode(302); // redirect when success
    }


    /**
     * Test delete method
     *
     * @return void
     */
    public function testDeleteWhenNotKnownAsAdminUser()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                    'role'=>"User",
                ]
            ]
        ]);

        $this->delete('api/v1/licenceTypes/delete/1.json');

        $this->assertResponseCode(403); // redirect when success
    }
}
