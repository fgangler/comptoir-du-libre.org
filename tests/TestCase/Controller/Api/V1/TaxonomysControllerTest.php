<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\TaxonomysController;

/**
 * App\Controller\TaxonomysController Test Case
 */
class TaxonomysControllerTest extends ApiIntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.taxonomys',
        'app.taxonomys_softwares',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.reviews',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_users',
   //     'app.usedsoftwares',
   //     'app.backedsoftwares',
   //     'app.createdsoftwares',
   //     'app.contributionssoftwares',
   //     'app.providerforsoftwares',
        'app.screenshots',
        'app.softwares_statistics',
        'app.tags',
        'app.softwares_tags',
    //    'app.userssoftwares',
    //    'app.backerssoftwares',
    //    'app.creatorssoftwares',
    //    'app.contributorssoftwares',
    //    'app.providerssoftwares',
    //    'app.workswellsoftwares',
    //    'app.alternativeto'
    ];


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Public actions /////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Mapping method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Mapping method : main unit tests
     *
     *      URL: /fr/cartographie
     *           /en/mapping/
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mapping
     * @return void
     */
    public function testMapping()
    {
        $url = '/fr/cartographie/';
        $title = "<title>Cartographie des logiciels métiers pour les collectivités</title>";

        // Anonymous user
        $this->setAnonymousUserSession();
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', $html);
        $this->assertContains("$title", $html);

        // Connected user
        $this->setConnectedUserSession();
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', $html);
        $this->assertContains("$title", $html);
    }

    /**
     * Mapping method
     * Check that redirects work when the URL is slightly different
     * - slash missing at the end of the URL
     * - the language prefix is missing at the beginning of the URL
     * - the user has changed the language prefix in the URL
     *
     *      URL: /fr/cartographie
     *           /en/mapping/
     *
     * @group seo
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mapping
     * @return void
     */
    public function testMappingCheckRedirects()
    {
        // Anonymous user
        $this->setAnonymousUserSession();

        // French user:
        $expectedUrl = '/fr/cartographie/';
        $testeddUrl = '/fr/cartographie'; // slash missing at the end of the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
        $testeddUrl = '/fr/mapping/'; // the user has changed the language prefix in the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        // English user
        $expectedUrl = '/en/mapping/';
        $testeddUrl = '/en/mapping'; // slash missing at the end of the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
        $testeddUrl = '/en/cartographie/'; // the user has changed the language prefix in the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        //  English user: language prefix is missing at the beginning of URL
        $expectedUrl = '/en/mapping/';
        $this->get("/en/"); // force user language to english
        $testeddUrl = '/mapping/'; //  the language prefix is missing at the beginning of URLs
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
        $testeddUrl = '/cartographie/';
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        //  French user: language prefix is missing at the beginning of URL
        $expectedUrl = '/fr/cartographie/';
        $this->get("/fr/"); // force user language to french
        $testeddUrl = '/mapping'; //  the language prefix is missing at the beginning of URLs
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
        $testeddUrl = '/cartographie'; //  the language prefix is missing at the beginning of URLs
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
    }

    /**
     * Mapping method with URL prefix /api/v1/
     * - HTML format redirects to the official URL
     * - JSON format returns a 404 "not found" error.
     *
     *      URL: /api/v1/taxonomys/mapping
     *      URL: /api/v1/taxonomys/mapping.json
     *
     * @group seo
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mapping
     * @return void
     */
    public function testMappingCheckApiPrefixInUrl()
    {
        $url = '/api/v1/taxonomys/mapping';
        $expectedUrl = '/fr/cartographie/';

        // Connected user
        $this->setConnectedUserSession();
        $this->get("/fr/"); // force user language to french
        $this->checkUrlNotFound($url, ['json']); // JSON format
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 301); // HTML format

        // Anonymous user
        $this->setAnonymousUserSession();
        $this->get("/fr/"); // force user language to french
        $this->checkUrlNotFound($url, ['json']); // JSON format
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 301); // HTML format
    }


// MappingPrimaryLevel method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * MappingPrimaryLevel method : main unit tests
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>
     *           /en/mapping/<slugPrimaryLevel>
     *
     *      ex:  /en/mapping/business     /fr/cartographie/metiers
     *           /en/mapping/activities   /fr/cartographie/activites
     *           /en/mapping/generics     /fr/cartographie/generiques
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mappingPrimaryLevel
     * @return void
     */
    public function testPrimaryLevel()
    {
        $url = '/fr/cartographie/metiers/';
        $title = "<title>Métiers";

        // Anonymous user
        $this->setAnonymousUserSession();
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', $html);
        $this->assertContains("$title", $html);

        // Connected user
        $this->setConnectedUserSession();
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', $html);
        $this->assertContains("$title", $html);
    }

    /**
     * MappingPrimaryLevel method
     * Check that redirects work when the URL is slightly different
     * - slash missing at the end of the URL
     * - the language prefix is missing at the beginning of the URL
     * - the user has changed the language prefix in the URL ----> redirect to '/en/mapping/' or '/fr/cartographie/'
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>
     *           /en/mapping/<slugPrimaryLevel>
     *
     *      ex:  /en/mapping/business     /fr/cartographie/metiers
     *           /en/mapping/activities   /fr/cartographie/activites
     *           /en/mapping/generics     /fr/cartographie/generiques
     *
     * @group seo
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mappingPrimaryLevel
     * @return void
     */
    public function testPrimaryLevelCheckRedirects()
    {
        // Anonymous user
        $this->setAnonymousUserSession();

        // French user:
        $expectedUrl = '/fr/cartographie/metiers/';
        $testeddUrl = '/fr/cartographie/metiers'; // slash missing at the end of the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
        $testeddUrl = '/en/cartographie/metiers/'; // the user has changed the language prefix in the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, '/en/mapping/', ['html'], 301);

        //  French user: language prefix is missing at the beginning of URL
        $this->get("/fr/"); // force user language to french
        $testeddUrl = '/cartographie/metiers/'; //  the language prefix is missing at the beginning of URLs
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        // English user
        $expectedUrl = '/en/mapping/business/';
        $testeddUrl = '/en/mapping/business'; // slash missing at the end of the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
        $testeddUrl = '/fr/mapping/business/'; // the user has changed the language prefix in the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, '/fr/cartographie/', ['html'], 301);

        //  English user: language prefix is missing at the beginning of URL
        $this->get("/en/"); // force user language to english
        $testeddUrl = '/mapping/business/'; //  the language prefix is missing at the beginning of URLs
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
    }



    /**
     * MappingPrimaryLevel method
     *
     * Check that if the first level requested does not exist,
     * a redirection is made to '/en/mapping/' or '/fr/cartographie/'
     *
     *      URL: /fr/cartographie/<slugPrimaryLevelNotFound>
     *           /en/mapping/<slugPrimaryLevelNotFound>
     *
     *      ex:  /en/mapping/business     /fr/cartographie/metiers
     *           /en/mapping/activities   /fr/cartographie/activites
     *           /en/mapping/generics     /fr/cartographie/generiques
     *
     * @group seo
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mappingPrimaryLevel
     * @return void
     */
    public function testPrimaryLevelNotFound()
    {
        // Anonymous user
        $this->setAnonymousUserSession();

        // French user:
        $expectedUrl = '/fr/cartographie/';
        $testeddUrl = '/fr/cartographie/PremierNiveauInconnu';
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        // English user
        $expectedUrl = '/en/mapping/';
        $testeddUrl = '/en/mapping/notFoundPrimaryLevel';
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
    }

    /**
     * MappingPrimaryLevel method with URL prefix /api/v1/
     * - HTML format redirects to the official URL
     * - JSON format returns a 404 "not found" error.
     *
     *      URL: /api/v1/taxonomys/mappingPrimaryLevel/<id>       ---> redirect to /en/mapping/ or /fr/cartographie/
     *      URL: /api/v1/taxonomys/mappingPrimaryLevel/<id>.json  ---> disable: 404 not found
     *
     * @group taxonomy
     * @group taxonomy_notAllowed
     * @group taxonomy_method_mappingPrimaryLevel
     * @return void
     */
    public function testPrimaryLevelCheckApiPrefixInUrl()
    {
        $url = '/api/v1/taxonomys/mappingPrimaryLevel/1';
        $expectedUrl = '/fr/cartographie/';

        // Connected user
        $this->setConnectedUserSession();
        $this->get("/fr/"); // force user language to french
        $this->checkUrlNotFound($url, ['json']); // JSON format
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 301); // HTML format

        // Anonymous user
        $this->setAnonymousUserSession();
        $this->get("/fr/"); // force user language to french
        $this->checkUrlNotFound($url, ['json']); // JSON format
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 301); // HTML format
    }


// MappingTaxon method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Test mappingTaxon method : a taxon has a software with users (one or more)
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *           /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *
     *      ex:  /fr/cartographie/metiers/sante-social/14      /en/mapping/business/healthcare-social/14
     *           /fr/cartographie/generiques/bureautique/20    /en/mapping/generics/office-automation/20
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mappingTaxon
     *
     * @return void
     */
    public function testMappingTaxonShouldHaveSoftwareWithUsers()
    {
        $idTaxon = 3;
        $idSoftware = 1;
        $idUserA = 5;
        $idUserB = 10;
        $url = "/fr/cartographie/metiers/urbanisme-espace-public-environnement/$idTaxon";
        $softwareUrl = "/fr/softwares/$idSoftware";
        $htmlTitle = "<title>Urbanisme / Espace public / Environnement";

        // Connected user
        $this->setConnectedUserSession();
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', $html);
        $this->assertContains($htmlTitle, $html);
        $this->assertContains("id=\"sofwareListForTaxon$idTaxon\"", $html);
        $this->assertContains("class=\"item-softwareForTaxon$idTaxon\"", $html);
        $this->assertContains("id=\"item-software$idSoftware-forTaxon$idTaxon\"", $html);
        $this->assertContains("id=\"link-software$idSoftware-forTaxon$idTaxon\"", $html);

        // Anonymous user
        $this->setAnonymousUserSession();
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', $html);
        $this->assertContains($htmlTitle, $html);
        $this->assertContains("id=\"sofwareListForTaxon$idTaxon\"", $html);
        $this->assertContains("class=\"item-softwareForTaxon$idTaxon\"", $html);
        $this->assertContains("id=\"item-software$idSoftware-forTaxon$idTaxon\"", $html);
        $this->assertContains("id=\"link-software$idSoftware-forTaxon$idTaxon\"", $html);

        // on software page: found link for taxon ID 6
        $r = $this->checkUrlOk($softwareUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"mappingForSoftware-$idSoftware\"", $html);
        $this->assertContains("class=\"linkMapping-taxonSoftware\"", $html);
        $this->assertContains("id=\"linkMapping-taxon-$idTaxon-Software-$idSoftware\"", $html);

        // on user (ID 5) page: found link for taxon ID 6
        $userUrl = "/fr/users/$idUserA";
        $r = $this->checkUrlOk($userUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"softwarePreviewCard-$idSoftware\"", $html);
        $this->assertContains("id=\"mappingForUser-$idUserA\"", $html);
        $this->assertContains("class=\"linkMappingTaxon\"", $html);
        $this->assertContains("id=\"linkMappingTaxon-$idTaxon\"", $html);

        // on user (ID 10)  page: found link for taxon ID 6
        $userUrl = "/fr/users/$idUserB";
        $r = $this->checkUrlOk($userUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"softwarePreviewCard-$idSoftware\"", $html);
        $this->assertContains("id=\"mappingForUser-$idUserB\"", $html);
        $this->assertContains("class=\"linkMappingTaxon\"", $html);
        $this->assertContains("id=\"linkMappingTaxon-$idTaxon\"", $html);
    }


    /**
     * Test mappingTaxon method :
     * a taxon has a software without user (preselected software)
     * and this software is only associated with taxons without users (pre-selected software).
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *           /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *
     *      ex:  /fr/cartographie/metiers/sante-social/14      /en/mapping/business/healthcare-social/14
     *           /fr/cartographie/generiques/bureautique/20    /en/mapping/generics/office-automation/20
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mappingTaxon
     * @return void
     */
    public function testMappingTaxonShouldHaveSoftwareWithoutUsersAssociatedForAnyTaxon()
    {
        $idTaxon = 3;
        $idSoftwareA = 6;
        $idSoftwareB = 7;
        $url = "/fr/cartographie/metiers/urbanisme-espace-public-environnement/$idTaxon";
        $softwareUrlA = "/fr/softwares/$idSoftwareA";
        $softwareUrlB = "/fr/softwares/$idSoftwareB";
        $htmlTitle = "<title>Urbanisme / Espace public / Environnement";

        // Anonymous user
        $this->setAnonymousUserSession();
        $r = $this->checkUrlOk($url, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', $html);
        $this->assertContains($htmlTitle, $html);
        $this->assertContains("id=\"sofwareListForTaxon$idTaxon\"", $html);
        $this->assertContains("class=\"item-softwareForTaxon$idTaxon\"", $html);
        $this->assertContains("id=\"item-software$idSoftwareA-forTaxon$idTaxon\"", $html);
        $this->assertContains("id=\"item-software$idSoftwareB-forTaxon$idTaxon\"", $html);
        $this->assertNotContains("id=\"link-software$idSoftwareA-forTaxon$idTaxon\"", $html);
        $this->assertNotContains("id=\"link-software$idSoftwareB-forTaxon$idTaxon\"", $html);

        // on software page (ID 6): no link for taxon ID 3
        $r = $this->checkUrlOk($softwareUrlA, ['html'], false);
        $html = $r['html']['data'];
        $s = ''; // this software is only associated with one taxonomy in preselection.
        $this->assertNotContains("id=\"mappingForSoftware-$idSoftwareA\"", $html);
        $this->assertNotContains("class=\"linkMapping-taxonSoftware\"", $html);
        $this->assertContains("id=\"mappingForSoftware-noRecord\"", $html);
        $this->assertContains("Pré-sélection <em>Adullact</em> pour le$s cas d'usage$s suivant$s : ", $html);
        $this->assertContains("class=\"linkMapping--PreSelectionTaxon\"", $html);
        $this->assertContains("id=\"linkMapping-taxon-$idTaxon\"", $html);
        $this->assertNotContains("id=\"linkMapping-taxon-$idTaxon-Software-$idSoftwareA\"", $html);

        // on software page (ID 7): no link for taxon ID 3
        $r = $this->checkUrlOk($softwareUrlB, ['html'], false);
        $html = $r['html']['data'];
        $s = 's'; // this software is associated with two taxonomies in preselection.
        $this->assertNotContains("id=\"mappingForSoftware-$idSoftwareB\"", $html);
        $this->assertNotContains("class=\"linkMapping-taxonSoftware\"", $html);
        $this->assertContains("id=\"mappingForSoftware-noRecord\"", $html);
        $this->assertContains("Pré-sélection <em>Adullact</em> pour le$s cas d'usage$s suivant$s : ", $html);
        $this->assertContains("class=\"linkMapping--PreSelectionTaxon\"", $html);
        $this->assertContains("id=\"linkMapping-taxon-$idTaxon\"", $html);
        $this->assertNotContains("id=\"linkMapping-taxon-$idTaxon-Software-$idSoftwareB\"", $html);
    }

    /**
     * Test mappingTaxon method :
     * a taxon has a software without user (preselected software),
     * but this software is also associated with other taxons with associated users.
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *           /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *
     *      ex:  /fr/cartographie/metiers/sante-social/14      /en/mapping/business/healthcare-social/14
     *           /fr/cartographie/generiques/bureautique/20    /en/mapping/generics/office-automation/20
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mappingTaxon
     * @return void
     */
    public function testMappingTaxonShouldHaveSoftwareWithoutUsers()
    {
        $idTaxon = 6;
        $idSoftware = 4;
        $url = "/fr/cartographie/generiques/design/$idTaxon";
        $softwareUrl = "/fr/softwares/$idSoftware";
        $htmlTitle = "<title>Design";

        // Anonymous user
        $this->setAnonymousUserSession();
        $r = $this->checkUrlOk($url, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', $html);
        $this->assertContains($htmlTitle, $html);
        $this->assertContains("id=\"sofwareListForTaxon$idTaxon\"", $html);
        $this->assertContains("class=\"item-softwareForTaxon$idTaxon\"", $html);
        $this->assertContains("id=\"item-software$idSoftware-forTaxon$idTaxon\"", $html);
        $this->assertNotContains("id=\"link-software$idSoftware-forTaxon$idTaxon\"", $html);

        // on software page: no link for taxon ID 6
        $r = $this->checkUrlOk($softwareUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"mappingForSoftware-$idSoftware\"", $html);
        $this->assertContains("class=\"linkMapping-taxonSoftware\"", $html);
        $this->assertNotContains("id=\"mappingForSoftware-noRecord\"", $html);
        $this->assertNotContains("class=\"linkMapping--PreSelectionTaxon\"", $html);
        $this->assertNotContains("id=\"linkMapping-taxon-$idTaxon\"", $html);
        $this->assertNotContains("id=\"linkMapping-taxon-$idTaxon-Software-$idSoftware\"", $html);
    }




    /**
     * Test mappingTaxon method : a taxon has no software
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *           /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *
     *      ex:  /fr/cartographie/metiers/sante-social/14      /en/mapping/business/healthcare-social/14
     *           /fr/cartographie/generiques/bureautique/20    /en/mapping/generics/office-automation/20
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mappingTaxon
     * @return void
     */
    public function testMappingTaxonShouldNotHaveSoftware()
    {
        $idTaxon = 4;
        $url = "/fr/cartographie/generiques/bureautique/$idTaxon";
        $htmlTitle = "<title>Bureautique";

        // Anonymous user
        $this->setAnonymousUserSession();
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', $html);
        $this->assertContains($htmlTitle, $html);
        $this->assertNotContains("id=\"sofwareListForTaxon$idTaxon\"", $html);
        $this->assertNotContains("class=\"item-softwareForTaxon$idTaxon\"", $html);
    }

    /**
     * MappingTaxon method
     *
     * Check that redirects work when the URL is slightly different
     * - extra slash at the end of the URL
     * - the language prefix is missing at the beginning of the URL
     * - the user has changed the language prefix in the URL
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *           /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *
     *      ex:  /fr/cartographie/metiers/sante-social/14      /en/mapping/business/healthcare-social/14
     *           /fr/cartographie/generiques/bureautique/20    /en/mapping/generics/office-automation/20
     *
     * @group seo
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mappingTaxon
     * @return void
     */
    public function testMappingTaxonCheckRedirects()
    {
        // Anonymous user
        $this->setAnonymousUserSession();

        // French user:
        $expectedUrl = '/fr/cartographie/generiques/bureautique/4';
        $testeddUrl = '/fr/cartographie/generiques/bureautique/4/'; // extra slash at the end of the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
        $testeddUrl = '/fr/mapping/generics/office-automation/4'; // the user has changed the language prefix in the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        //  French user: language prefix is missing at the beginning of URL
        $this->get("/fr/"); // force user language to french
        $testeddUrl = '/cartographie/generiques/bureautique/4'; // language prefix is missing at the beginning of URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        // English user
        $expectedUrl = '/en/mapping/generics/office-automation/4';
        $testeddUrl = '/en/mapping/generics/office-automation/4/'; // extra slash at the end of the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
        $testeddUrl = '/en/cartographie/generiques/bureautique/4'; // user has changed the language prefix in the URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        //  English user: language prefix is missing at the beginning of URL
        $this->get("/en/"); // force user language to english
        $testeddUrl = '/mapping/generics/office-automation/4'; //  language prefix is missing at the beginning of URL
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
    }



    /**
     * MappingTaxon method
     *
     * Check that if the requested taxon does not exist,
     * a redirection is made to '/en/mapping/' or '/fr/cartographie/'
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<taxonId_notFound>
     *           /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<taxonId_notFound>
     *
     *      ex:  /fr/cartographie/metiers/sante-social/14      /en/mapping/business/healthcare-social/14
     *           /fr/cartographie/generiques/bureautique/20    /en/mapping/generics/office-automation/20
     *
     * @group seo
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_method_mappingTaxon
     * @return void
     */
    public function testMappingTaxonNotFound()
    {
        // Anonymous user
        $this->setAnonymousUserSession();

        // French user:
        $expectedUrl = '/fr/cartographie/';
        $testeddUrl = '/fr/cartographie/metiers/notFoundTaxon/825';
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        // English user
        $expectedUrl = '/en/mapping/';
        $testeddUrl = '/en/mapping/metiers/notFoundTaxon/825';
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
    }

    /**
     * MappingTaxon method with URL prefix /api/v1/
     * - HTML format redirects to the official URL
     * - JSON format returns a 404 "not found" error.
     *
     *      URL: /api/v1/taxonomys/mappingTaxon/<id>       ---> redirect to /en/mapping/ or /fr/cartographie/
     *      URL: /api/v1/taxonomys/mappingTaxon/<id>.json  ---> disable: 404 not found
     *
     * @group taxonomy
     * @group taxonomy_notAllowed
     * @group taxonomy_method_mappingTaxon
     * @return void
     */
    public function testMappingTaxonCheckApiPrefixInUrl()
    {
        $url = '/api/v1/taxonomys/mappingTaxon/4';
        $expectedUrl = '/fr/cartographie/';

        // Connected user
        $this->setConnectedUserSession();
        $this->get("/fr/"); // force user language to french
        $this->checkUrlNotFound($url, ['json']); // JSON format
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 301); // HTML format

        // Anonymous user
        $this->setAnonymousUserSession();
        $this->get("/fr/"); // force user language to french
        $this->checkUrlNotFound($url, ['json']); // JSON format
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 301); // HTML format
    }


// mappingTaxonUsersOf method
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * mappingTaxonUsersOf method:
     * a taxon has a software with users (one or more), without comment
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId>
     *           /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId>
     *
     *      ex:  /fr/cartographie/generiques/bureautique/libreoffice/20.33
     *           /en/mapping/generics/office-automation/libreoffice/20.33
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_mappingTaxonUsersOf
     * @return void
     */
    public function testMappingTaxonUsersOfShouldHaveSoftwareWithUsersWithoutComments()
    {
        $idTaxon = 3;
        $idSoftware = 1;
        $idUserA = 5;
        $idUserB = 10;
        $url = "/fr/cartographie/metiers/urbanisme-espace-public-environnement/asalae/$idTaxon.$idSoftware";
        $title = "<title>Logiciel Asalae, cas d'usages Urbanisme / Espace public / Environnement";

        // Connected user (type != "Administration")   --> to be deleted when development is complete.
        $this->setConnectedPersonSession();
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', "$html");
        $this->assertContains("$title", "$html");
        $this->assertContains("id=\"mappingTaxon$idTaxon-UsersOf$idSoftware\"", $html); // <section id="">
        $this->assertContains("class=\"mappingTaxonUsersOfPage", $html);                   // <section class="">
        $this->assertContains("id=\"userPreviewCard-$idUserA\"", $html);
        $this->assertContains("id=\"userPreviewCard-$idUserB\"", $html);
        $this->assertNotContains("class=\"taxonomyComment\"", $html);

        // Anonymous user
        $this->setAnonymousUserSession();
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', "$html");
        $this->assertContains("$title", "$html");
        $this->assertContains("id=\"mappingTaxon$idTaxon-UsersOf$idSoftware\"", $html); // <section id="">
        $this->assertContains("class=\"mappingTaxonUsersOfPage", $html);                   // <section class="">
        $this->assertContains("id=\"userPreviewCard-$idUserA\"", $html);
        $this->assertContains("id=\"userPreviewCard-$idUserB\"", $html);
        $this->assertNotContains("class=\"taxonomyComment\"", $html);
    }


    /**
     * mappingTaxonUsersOf method:
     * a taxon has a software with users (one or more), with comment (one or more)
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId>
     *           /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId>
     *
     *      ex:  /fr/cartographie/generiques/bureautique/libreoffice/20.33
     *           /en/mapping/generics/office-automation/libreoffice/20.33
     *
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_mappingTaxonUsersOf
     * @return void
     */
    public function testMappingTaxonUsersOfShouldHaveSoftwareWithUsersWithComments()
    {
        $idTaxon = 5;
        $idSoftware = 2;
        $idUser = 3;
        $url = "/fr/cartographie/generiques/infrastructure/lutece/$idTaxon.$idSoftware";
        $title = "<title>Utilisateurs du logiciel Lutèce - Infrastructure";

        // Anonymous user
        $this->setAnonymousUserSession();
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
//        $this->assertContains('<html lang="fr">', "$html");
//        $this->assertContains("$title", "$html");
//        $this->assertContains("id=\"mappingTaxon$idTaxon-UsersOf$idSoftware\"", $html); // <section id="">
//        $this->assertContains("class=\"mappingTaxonUsersOfPage", $html);                   // <section class="">
//        $this->assertContains("id=\"userPreviewCard-$idUser\"", $html);
//        $this->assertContains("class=\"taxonomyComment\"", $html);
//        $this->assertContains("id=\"mappingTaxon$idTaxon-UsersOf$idSoftware-comment-user$idUser\"", $html);
//        $this->assertContains("blabla bla du comentaire", $html);
        $this->markTestIncomplete('Not implemented yet.');
    }


    /**
     * mappingTaxonUsersOf method: a taxon has a software without user (preselected software)
     *
     *      URL: /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId_withoutUser>
     *           ----> redirect to /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *
     *      ex:  /fr/cartographie/generiques/bureautique/libreoffice/20.33
     *           /en/mapping/generics/office-automation/libreoffice/20.33
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_mappingTaxonUsersOf
     * @return void
     */
    public function testMappingTaxonUsersOfShouldHaveSoftwareWithoutUsers()
    {
        $idTaxon = 6;
        $idSoftware = 4;
        $testeddUrl = "/fr/cartographie/generiques/design/soft-4/$idTaxon.$idSoftware";

        // Anonymous user
        $this->setAnonymousUserSession();
        $expectedUrl = "/fr/cartographie/generiques/design/$idTaxon";
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
    }


    /**
     * MappingTaxonUsersOf method: a taxon is not associated with this software
     *
     *      URL: /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId_notAssociated>
     *           ----> redirect to /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *
     *      ex:  /fr/cartographie/generiques/bureautique/libreoffice/20.33
     *           /en/mapping/generics/office-automation/libreoffice/20.33
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_mappingTaxonUsersOf
     * @return void
     */
    public function testMappingTaxonUsersOfShouldNotBeAssociatedWithThisSoftware()
    {
        $idTaxon = 4;
        $idSoftware = 4;
        $testeddUrl = "/fr/cartographie/generiques/bureautique/soft-4/$idTaxon.$idSoftware";

        // Anonymous user
        $this->setAnonymousUserSession();
        $expectedUrl = "/fr/cartographie/generiques/bureautique/$idTaxon";
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
    }


    /**
     * MappingTaxonUsersOf method: taxon not found in database
     *
     * Check that if the requested taxon does not exist,
     * a redirection is made to '/en/mapping/' or '/fr/cartographie/'
     *
     *      URL: /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId_notFound>.<softwareId>
     *           /fr/mapping/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId_notFound>.<softwareId>
     *
     *      ex:  /fr/cartographie/generiques/bureautique/libreoffice/20.33
     *           /en/mapping/generics/office-automation/libreoffice/20.33
     *
     * @group seo
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_mappingTaxonUsersOf
     * @return void
     */
    public function testMappingTaxonUsersOfTaxonNotFound()
    {
        $idTaxon = 999; // not found taxon
        $idSoftware = 4;

        // Anonymous user
        $this->setAnonymousUserSession();

        // French user:
        $expectedUrl = '/fr/cartographie/';
        $testeddUrl = "/fr/cartographie/metiers/notFoundTaxon/soft4Name/$idTaxon.$idSoftware";
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        // English user
        $expectedUrl = '/en/mapping/';
        $testeddUrl = "/en/mapping/business/notFoundTaxon/soft4Name/$idTaxon.$idSoftware";
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
    }


    /**
     * MappingTaxonUsersOf method:
     *
     * Check that redirects work when the URL is slightly different
     * - extra slash at the end of the URL
     * - the language prefix is missing at the beginning of the URL
     * - the user has changed the language prefix in the URL
     *
     *      URL: /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId>
     *           /fr/mapping/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId>
     *
     *      ex:  /fr/cartographie/generiques/bureautique/libreoffice/20.33
     *           /en/mapping/generics/office-automation/libreoffice/20.33
     *
     * @group seo
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_mappingTaxonUsersOf
     * @return void
     */
    public function testMMappingTaxonUsersOfCheckRedirects()
    {
        $idTaxon = 5;
        $idSoftware = 4;

        // Anonymous user
        $this->setAnonymousUserSession();

        // French user
        //////////////////////////////////////////
        $expectedUrl = "/fr/cartographie/generiques/infrastructure/soft-4/$idTaxon.$idSoftware";

        // extra slash at the end of the URL
        $testeddUrl = "$expectedUrl/";
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        // the user has changed the language prefix in the URL
        $testeddUrl = "/fr/mapping/generics/infrastructure/soft-4/$idTaxon.$idSoftware";
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        //  French user: language prefix is missing at the beginning of URL
        $this->get("/fr/"); // force user language to french
        $testeddUrl = "/cartographie/generiques/infrastructure/soft-4/$idTaxon.$idSoftware";
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        // English user
        //////////////////////////////////////////
        $expectedUrl = "/en/mapping/generics/infrastructure/soft-4/$idTaxon.$idSoftware";

        // extra slash at the end of the URL
        $testeddUrl = "$expectedUrl/";
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        // user has changed the language prefix in the URL
        $testeddUrl = "/en/cartographie/generiques/infrastructure/soft-4/$idTaxon.$idSoftware";
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);

        //  English user: language prefix is missing at the beginning of URL
        $this->get("/en/"); // force user language to english
        $testeddUrl = "/mapping/generics/infrastructure/soft-4/$idTaxon.$idSoftware";
        $this->checkUrlRedirectToAnotherUrl($testeddUrl, $expectedUrl, ['html'], 301);
    }




    /**
     * MappingTaxonUsersOf method with URL prefix /api/v1/
     * - HTML format redirects to the official URL
     * - JSON format returns a 404 "not found" error.
     *
     *    URL: /api/v1/taxonomys/mappingTaxonUsersOf           ---> redirect to '/en/mapping/'
     *         /api/v1/taxonomys/mappingTaxonUsersOf/<id>      ---> redirect to '/en/mapping/'
     *         /api/v1/taxonomys/mappingTaxonUsersOf.json      ---> disable via parent::beforeFilter()
     *         /api/v1/taxonomys/mappingTaxonUsersOf/<id>.json ---> disable via parent::beforeFilter()
     *
     * @group seo
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy_mappingTaxonUsersOf
     * @return void
     */
    public function testMappingTaxonUsersOfCheckApiPrefixInUrl()
    {
        $urlBase = '/api/v1/taxonomys/mappingTaxonUsersOf'; // missing software ID
        $url = "$urlBase/2"; // software ID 2
        $expectedUrl = '/fr/cartographie/';

        // Connected user
        $this->setConnectedUserSession();
        $this->get("/fr/"); // force user language to french
        $this->checkUrlNotFound($urlBase, ['json']); // JSON format
        $this->checkUrlNotFound($url, ['json']);     // JSON format
        $this->checkUrlRedirectToAnotherUrl($urlBase, $expectedUrl, ['html'], 301); // HTML format
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 301);     // HTML format

        // Anonymous user
        $this->setAnonymousUserSession();
        $this->get("/fr/"); // force user language to french
        $this->checkUrlNotFound($urlBase, ['json']); // JSON format
        $this->checkUrlNotFound($url, ['json']);     // JSON format
        $this->checkUrlRedirectToAnotherUrl($urlBase, $expectedUrl, ['html'], 301); // HTML format
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 301);     // HTML format
    }



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Admin actions ---> self-generated by CakePHP  //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // Admin user
    /////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Test index method for admin user ---> allowed
     *
     *      URL: /api/v1/taxonomys
     *           ----> allowed only for user with "admin" role
     *
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_index
     *
     * @return void
     */
    public function testIndexForAdminUser()
    {
        $this->setAdminSessionOfPersonType();
        $this->checkUrlOk('/api/v1/taxonomys');
    }

    /**
     * Test view method
     *
     *      URL: /api/v1/taxonomys/view/1
     *           ----> allowed only for user with "admin" role
     *
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_view
     * @return void
     */
    public function testViewForAdminUser()
    {
        $this->setAdminSessionOfPersonType();
        $this->checkUrlOk('/api/v1/taxonomys/view/1');
    }

    /**
     * Test add method
     *
     *      URL: /api/v1/taxonomys/add
     *           ----> allowed only for user with "admin" role
     *
     * @todo Not implemented yet.
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_add
     * @return void
     */
    public function testAddForAdminUser()
    {
        $this->setAdminSessionOfPersonType();
        $this->checkUrlOk('/api/v1/taxonomys/add');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     *      URL: /api/v1/taxonomys
     *           ----> allowed only for user with "admin" role
     *
     * @todo  not implemented yet.
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_edit
     * @return void
     */
    public function testEditForAdminUser()
    {
        $this->setAdminSessionOfPersonType();
        $this->checkUrlOk('/api/v1/taxonomys/edit/1');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     *      URL: /api/v1/taxonomys
     *           ----> allowed only for user with "admin" role
     *
     * @todo  not implemented yet.
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_delete
     * @return void
     */
    public function testDeleteForAdminUser()
    {
        $this->setAdminSessionOfPersonType();

        $this->markTestIncomplete('Not implemented yet.');
    }


    // Connected user
    /////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Test index method for connected user ---> not authorized
     *
     *      URL: /api/v1/taxonomys
     *           ----> allowed only for user with "admin" role
     *
     * @group security
     * @group user
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_index
     * @return void
     */
    public function testFailIndexForConnectedUser()
    {
        $this->setConnectedUserSession();
        $this->checkUrlNotAuthorized('/api/v1/taxonomys');
    }

    /**
     * Test view method for connected user ---> not authorized
     *
     *      URL: /api/v1/taxonomys/view/1
     *           ----> allowed only for user with "admin" role
     *
     * @group security
     * @group user
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_view
     * @return void
     */
    public function testFailViewForConnectedUser()
    {
        $this->setConnectedUserSession();
        $this->checkUrlNotAuthorized('/api/v1/taxonomys/view/1');
    }

    /**
     * Test add method for connected user
     * GET ---> not authorized
     * POST ---> @todo  not implemented yet
     *
     *      URL: /api/v1/taxonomys/add
     *           ----> allowed only for user with "admin" role
     *
     * @group security
     * @group user
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_add
     * @return void
     */
    public function testFailAddForConnectedUser()
    {
        $this->setConnectedUserSession();
        $this->checkUrlNotAuthorized('/api/v1/taxonomys/add');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method for connected user
     * GET ---> not authorized
     * POST ---> @todo  not implemented yet
     *
     * @group security
     * @group user
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_edit
     * @return void
     */
    public function testFailEditForConnectedUser()
    {
        $this->setConnectedUserSession();
        $this->checkUrlNotAuthorized('/api/v1/taxonomys/edit/1');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method for connected user
     *
     * @todo    not implemented yet
     *
     * @group security
     * @group user
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_delete
     * @return void
     */
    public function testFailDeleteForConnectedUser()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }


    // Anonymous user
    /////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Test index method for anonymous user ---> redirect to login form
     *
     *      URL: /api/v1/taxonomys
     *           ----> allowed only for user with "admin" role
     *
     * @group security
     * @group anonymous
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_index
     * @return void
     */
    public function testFailIndexForAnonymousUser()
    {
        $this->setAnonymousUserSession();
        $this->checkUrlRedirectToLogin('/api/v1/taxonomys');
    }

    /**
     * Test view method for anonymous user ---> redirect to login form
     *
     *      URL: /api/v1/taxonomys/view/1
     *           ----> allowed only for user with "admin" role
     *
     * @group security
     * @group anonymous
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_view
     * @return void
     */
    public function testFailViewForAnonymousUser()
    {
        $this->setAnonymousUserSession();
        $this->checkUrlRedirectToLogin('/api/v1/taxonomys/view/1');
    }

    /**
     * Test add method for anonymous user
     * GET ---> redirect to login form
     * POST ---> @todo  not implemented yet
     *
     *      URL: /api/v1/taxonomys/add
     *           ----> allowed only for user with "admin" role
     *
     * @group security
     * @group anonymous
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_add
     * @return void
     */
    public function testFailAddForAnonymousUser()
    {
        $this->setAnonymousUserSession();
        $this->checkUrlRedirectToLogin('/api/v1/taxonomys/add');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method for anonymous user
     * GET ---> redirect to login form
     * POST ---> @todo  not implemented yet
     *
     * @group security
     * @group anonymous
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_edit
     * @return void
     */
    public function testFailEditForAnonymousUser()
    {
        $this->setAnonymousUserSession();
        $this->checkUrlRedirectToLogin('/api/v1/taxonomys/edit/1');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method for anonymous user
     *
     * @todo  not implemented yet
     *
     * @group security
     * @group anonymous
     * @group admin
     * @group taxonomy
     * @group taxonomy_admin
     * @group taxonomy_method_delete
     * @return void
     */
    public function testFailDeleteForAnonymousUser()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
