<?php

namespace App\Test\TestCase\Controller\Api\V1;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestCase;

abstract class ApiIntegrationTestCase extends IntegrationTestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->configRequest(["headers" => ["Accept-Language" => "en"]]);

        // Update all users with the password "goo-password"
        $UsersTable = TableRegistry::get('Users');
        $password = (new DefaultPasswordHasher)->hash("good-password");
        $UsersTable->updateAll(['password' => $password], []);
    }



    /**
     * Return the connected user ID or null
     *
     * @return null|int  connected user ID
     */
    final public function getConnectedUserId()
    {
        if (isset($this->_session['Auth']['User']['id'])) {
            return $this->_session['Auth']['User']['id'];
        }
        return null;
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Set session for a anonymous, connected user and admin      /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    final public function login($email, $password)
    {
        $this->post("users/login", ["email" => $email, "password" => $password]);

        $this->session([
            'Auth' => [
                'User' => $this->_controller->Auth->identify()
            ]
        ]);
    }

    /**
     * Set session for a anonymous user
     */
    final protected function setAnonymousUserSession()
    {
        $this->_session = [];
        $this->session([]);
    }

    /**
     * alias of setConnectedAdministrationSession()
     *
     * Set session for a connected user of "Administration" type
     * ---> user_type_id = 2 in dataset2
     * ---> user_type_id = 3 in fixture
     */
    final protected function setConnectedUserSession()
    {
        $this->setConnectedAdministrationSession();
    }

    /**
     * Set session for a connected user of "Administration" type
     * ---> user_type_id = 2 in dataset2
     * ---> user_type_id = 3 in fixture
     */
    final protected function setConnectedAdministrationSession()
    {
        $this->_session = [];
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 6,
                    "user_type_id" => 3, // Administration
                    "role" => 'User',
                ]
            ]
        ]);
    }

    /**
     * Set session for a connected user of "Person" type
     * ---> user_type_id = 4 in dataset2
     * ---> user_type_id = 4 in fixture
     */
    final protected function setConnectedPersonSession()
    {
        $this->_session = [];
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 4,
                    "user_type_id" => 4, // Person
                    "role" => 'User',
                ]
            ]
        ]);
    }

    /**
     * Set session for a connected user of "Company" type
     * ---> user_type_id = 5 in dataset2
     * ---> user_type_id = 2 in fixture
     */
    final protected function setConnectedCompanySession()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2, // Company
                    "role" => 'User',
                ]
            ]
        ]);
    }

    /**
     * Set session for a connected user of "Association" type
     * ---> user_type_id = 6 in dataset2
     * ---> user_type_id = 1 in fixture
     */
    final protected function setConnectedAssociationSession()
    {
        $this->_session = [];
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 7,
                    "user_type_id" => 1, // Association
                    "role" => 'User',
                ]
            ]
        ]);
    }

    /**
     * Set session for a admin user with user type = 'Person'
     * ---> user_type_id = 4 in dataset2
     * ---> user_type_id = 4 in fixture
     */
    final protected function setAdminSessionOfPersonType()
    {
        $this->_session = [];
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 9,
                    "user_type_id" => 4, // Person
                    "role" => 'admin',
                ]
            ]
        ]);
    }

    /**
     * Set session for a admin user with user type = 'Administration'
     * ---> user_type_id = 2 in dataset2
     * ---> user_type_id = 3 in fixture
     */
    final protected function setAdminSessionOfAdministrationType()
    {
        $this->_session = [];
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 5,
                    "user_type_id" => 3, // Administration
                    "role" => 'admin',
                ]
            ]
        ]);
    }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Common URL tests:  codes HTTP 200, 302, 301, 404 and 403   /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Checking if a URL retun a HTTP code 200 "Found"
     *
     * For each requested format, loads the HTTP GET request
     * and expects a "200 Found" response consistent:
     *   - requested format: HTML ----> load "$url"
     *   - requested format: JSON ----> load "$url.json"
     *
     * This method return a array like this:
     *                  [  'html' => [     'headers' => $htmlHeaders,
     *                                   'data'    =>  $html       ],
     *                    'json' => [     'headers' => $jsonHeaders,
     *                                   'data'    => $json,
     *                                   'obj'     => $objJson    ]]
     *
     * @param string $url
     * @param array $requestFormats       (optional) requested format, by default it's Array('html', 'json')
     * @param bool $checkSecurityHeaders  (optional) checks security headers, by default it's TRUE
     * @return array  example: Array('html' => [], 'json' => [])
     */
    final protected function checkUrlOk(
        string $url,
        array $requestFormats = ['html', 'json'],
        bool $checkSecurityHeaders = true
    ): Array {
        $result = [];
        if (in_array('html', $requestFormats)) { // HTML request
            $this->get($url);
            $this->assertResponseCode(200);
            $html = $this->_response->body();
            $htmlHeaders = $this->_response->header();
            $this->assertEquals('text/html; charset=UTF-8', $htmlHeaders['Content-Type']);
            if ($checkSecurityHeaders === true) {
                $this->assertEquals('DENY', $htmlHeaders['X-Frame-Options']);
                $this->assertEquals('nosniff', $htmlHeaders['X-Content-Type-Options']);
                $this->assertEquals('1; mode=block', $htmlHeaders['X-XSS-Protection']);
                $this->assertEquals('strict-origin-when-cross-origin', $htmlHeaders['Referrer-Policy']);
            }
            $result['html'] = [ 'headers' => $htmlHeaders, 'data' => $html ];
        }

        if (in_array('json', $requestFormats)) { // JSON request
            $this->get("$url .json");
            $this->assertResponseCode(200);
            $jsonHeaders = $this->_response->header();
            $json    = $this->_response->body();
            $objJson = json_decode($json);
            $this->assertEquals('application/json; charset=UTF-8', $jsonHeaders['Content-Type']);
            $result['json'] = [
                'headers' => $jsonHeaders,
                'data' => $json,
                'obj' => $objJson,
            ];
        }
        return $result;
    }

    /**
     * Checking if a URL retun a HTTP code 403 "Not authorized"
     *
     * For each requested format, loads the HTTP GET request
     * and expects a "403 Forbidden" response consistent:
     *   - requested format: HTML ----> load "$url"
     *   - requested format: JSON ----> load "$url.json"
     *
     * This method return a array like this:
     *                  [  'html' => [     'headers' => $htmlHeaders,
     *                                   'data'    =>  $html       ],
     *                    'json' => [     'headers' => $jsonHeaders,
     *                                   'data'    => $json,
     *                                   'obj'     => $objJson    ]]
     *
     * @param string $url
     * @param array $requestFormats  (optional) requested format, by default it's Array('html', 'json')
     * @return array  example: Array('html' => [], 'json' => [])
     */
    final protected function checkUrlNotAuthorized(string $url, $requestFormats = ['html', 'json']): Array
    {
        $result = [];
        if (in_array('html', $requestFormats)) { // HTML request
            $this->get($url);
            $html = $this->_response->body();
            $htmlHeaders = $this->_response->header();
            $this->assertResponseCode(403);
            $this->assertContains("<title>Error-400</title>", $html);
            $result['html'] = [ 'headers' => $htmlHeaders, 'data' => $html ];
        }

        if (in_array('json', $requestFormats)) { // JSON request
            $this->get("$url .json");
            $jsonHeaders = $this->_response->header();
            $json    = $this->_response->body();
            $objJson = json_decode($json);
            $this->assertResponseCode(403);
            $this->assertEquals("You are not authorized to access that location.", $objJson->message);
            $this->assertEquals(403, $objJson->code);
            $result['json'] = [
                'headers' => $jsonHeaders,
                'data' => $json,
                'obj' => $objJson,
            ];
        }
        return $result;
    }

    /**
     * Checking if a URL retun a HTTP code 404 "Not Found"
     *
     * For each requested format, loads the HTTP GET request
     * and expects a "404 Not Found" response consistent:
     *   - requested format: HTML ----> load "$url"
     *   - requested format: JSON ----> load "$url.json"
     *
     * This method return a array like this:
     *                  [  'html' => [     'headers' => $htmlHeaders,
     *                                   'data'    =>  $html       ],
     *                    'json' => [     'headers' => $jsonHeaders,
     *                                   'data'    => $json,
     *                                   'obj'     => $objJson    ]]
     *
     * @param string $url
     * @param array $requestFormats  (optional) requested format, by default it's Array('html', 'json')
     * @return array  example: Array('html' => [], 'json' => [])
     */
    final protected function checkUrlNotFound(string $url, $requestFormats = ['html', 'json']): Array
    {
        $result = [];
        if (in_array('html', $requestFormats)) { // HTML request
            $this->get($url);
            $html = $this->_response->body();
            $htmlHeaders = $this->_response->header();
            $this->assertResponseCode(404);
            $this->assertContains("<title>Error-400</title>", $html);
            $result['html'] = [ 'headers' => $htmlHeaders, 'data' => $html ];
        }

        if (in_array('json', $requestFormats)) { // JSON request
            $this->get("$url .json");
            $jsonHeaders = $this->_response->header();
            $json    = $this->_response->body();
            $objJson = json_decode($json);
            $this->assertResponseCode(404);
            $this->assertEquals("Not Found", $objJson->message);
            $this->assertEquals(404, $objJson->code);
            $result['json'] = [
                'headers' => $jsonHeaders,
                'data' => $json,
                'obj' => $objJson,
            ];
        }
        return $result;
    }

    /**
     * Checking if a URL redirects to another URL
     *
     * For each requested format, loads the HTTP GET request
     * and expects a redirect to the new URL:
     *   - requested format: HTML ----> load "$url"
     *   - requested format: JSON ----> load "$url.json"
     *
     * @param string $url
     * @param string $redirectTo
     * @param array $requestFormats  (optional) requested format, by default it's Array('html', 'json')
     * @param int $httpCode   optional, by default it's 302
     * @return void
     */
    final protected function checkUrlRedirectToAnotherUrl(
        string $url,
        string $redirectTo,
        $requestFormats = ['html', 'json'],
        int $httpCode = 302
    ) {
        // HTML request
        if (in_array('html', $requestFormats)) {
            $this->get("$url");
            $this->assertResponseCode($httpCode);
            $headers = $this->_response->header();
            $this->assertEquals($redirectTo, $headers['Location']);
        }

        // JSON request
        if (in_array('json', $requestFormats)) {
            $this->get("$url.json");
            $this->assertResponseCode($httpCode);
            $headers = $this->_response->header();
            $this->assertEquals($redirectTo, $headers['Location']);
        }
    }


    /**
     * Checking if current HTTP response
     * is a redirection to another URL
     *
     * @param string $redirectTo
     * @param int $httpCode   optional, by default it's 302
     * @return void
     */
    final protected function currentResponseIsRedirectionToAnotherUrl(
        string $redirectTo,
        int $httpCode = 302
    ) {
            $this->assertResponseCode($httpCode);
            $headers = $this->_response->header();
            $this->assertEquals($redirectTo, $headers['Location']);
    }

    /**
     * Checking if a URL redirects to the login form
     *
     * For each requested format, loads the HTTP GET request
     * and expects a redirect to login form:
     *   - requested format: HTML ----> load "$url"
     *   - requested format: JSON ----> load "$url.json"
     *
     * @param string $url
     * @param array $requestFormats  (optional) requested format, by default it's Array('html', 'json')
     * @param string $lang  (optional) lang prefix in url ('fr' or 'en'), by default it's empty
     * @return void
     */
    final protected function checkUrlRedirectToLogin(string $url, $requestFormats = ['html', 'json'], $lang = '')
    {
        $allowedLanguage = ['fr', 'en'];
        $prefixUrl = '';
        if (in_array($lang, $allowedLanguage)) {
            $prefixUrl = "/$lang";
        }
        $redirectTo = $prefixUrl . '/users/login';
        $this->checkUrlRedirectToAnotherUrl($url, $redirectTo, $requestFormats);
    }

    /**
     * Determine if two associative arrays are similar
     *
     * Both arrays must have the same indexes with identical values
     * without respect to key ordering
     *
     * @param array $a
     * @param array $b
     * @return bool
     */
    final protected function arraysAreSimilar($a, $b)
    {
        // if the indexes don't match, return immediately
        if (count(array_diff_assoc($a, $b))) {
            return false;
        }
        // we know that the indexes, but maybe not values, match.
        // compare the values between the two arrays
        foreach ($a as $k => $v) {
            if ($v !== $b[$k]) {
                return false;
            }
        }
        // we have identical indexes, and no unequal values
        return true;
    }
}
