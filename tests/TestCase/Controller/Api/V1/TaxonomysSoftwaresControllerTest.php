<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\TaxonomysSoftwaresController;

/**
 * App\Controller\TaxonomysSoftwaresController Test Case
 */
class TaxonomysSoftwaresControllerTest extends ApiIntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.taxonomys_softwares',
        'app.taxonomys',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.reviews',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_users',
      //  'app.usedsoftwares',
     //   'app.backedsoftwares',
     //   'app.createdsoftwares',
    //    'app.contributionssoftwares',
    //    'app.providerforsoftwares',
        'app.screenshots',
        'app.softwares_statistics',
        'app.tags',
        'app.softwares_tags',
     //   'app.userssoftwares',
     //   'app.backerssoftwares',
     //   'app.creatorssoftwares',
     //   'app.contributorssoftwares',
     //   'app.providerssoftwares',
     //   'app.workswellsoftwares',
    //    'app.alternativeto'
    ];


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Public actions /////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Try to display mapping form by connected user (type = "Administration")
     * without being declared user of the software
     *
     *      URL: /en/mappingForm/<softwareId>  ---> redirect 302 to /en/softwares/<softwareId>
     *           /fr/mappingForm/<softwareId>  ---> redirect 302 to /fr/softwares/<softwareId>
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy-software
     * @group taxonomy-software_public
     * @group taxonomy-software_mappingForm
     *
     * @return void
     */
    public function testMappingFormDisplayWithoutDeclaredUserOf()
    {

        $softwareId = 2; // software ID 2 "Lutèce"
        $url =  "/fr/mappingForm/$softwareId";
        $expectedUrl = "/fr/softwares/$softwareId";

        // connected user (type = "Administration")
        // is not a "user of"
        $this->setConnectedAdministrationSession();
        $userUrl = "/fr/users/6";
        $r = $this->checkUrlOk($userUrl, ['html'], false);
        $this->assertNotContains("id=\"softwarePreviewCard-$softwareId\"", $r['html']['data']);

        // Mapping form redirect to software page
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 302);
    }


    /**
     * MappingForm method used by connected user (type = "Administration")
     * after registering as user of a sofware.
     *
     *      URL: /en/mappingForm/<softwareId>
     *           /fr/mappingForm/<softwareId>
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy-software
     * @group taxonomy-software_public
     * @group taxonomy-software_mappingForm
     * @return void
     */
    public function testMappingFormDisplay()
    {
        $softwareId = 2; // software ID 2 "Lutèce"
        $url =  "/fr/mappingForm/$softwareId";
        $title = "<title>Logiciel Lutèce : déclarer vos usages</title>";

        // Anonymous user
        $this->setAnonymousUserSession();
        $this->checkUrlRedirectToLogin($url, ['html'], 'fr');

        // Connected user (type != "Administration")
        $this->setConnectedPersonSession();
        $this->checkUrlNotAuthorized($url, ['html']);
        $this->setConnectedCompanySession();
        $this->checkUrlNotAuthorized($url, ['html']);
        $this->setConnectedAssociationSession();
        $this->checkUrlNotAuthorized($url, ['html']);

        // connected user (type = "Administration")
        $this->setConnectedAdministrationSession(); // user ID = 6

        // Beforehand, the user declares himself/herself to be a user of the software.
        $userUrl = "/fr/users/6";
        $r = $this->checkUrlOk($userUrl, ['html'], false);
        $this->assertNotContains("id=\"softwarePreviewCard-$softwareId\"", $r['html']['data']);
        $this->post("api/v1/softwares/usersSoftware/$softwareId.json");
        $r = $this->checkUrlOk($userUrl, ['html'], false);
        $this->assertContains("id=\"softwarePreviewCard-$softwareId\"", $r['html']['data']);

        // Check form
        $r = $this->checkUrlOk($url, ['html']);
        $html = $r['html']['data'];
        $this->assertContains('<html lang="fr">', $html);
        $this->assertContains("$title", $html);
        $this->assertContains('class="taxonomyForm_step1_content"', $html);
        $this->assertNotContains('id="checkbox_1"', $html); // Primary level --> no checkbox
        $this->assertNotContains('id="checkbox_2"', $html); // Primary level --> no checkbox
        $this->assertContains('id="checkbox_3"', $html);
        $this->assertContains('id="checkbox_4"', $html);
        $this->assertContains('id="checkbox_5"', $html);
        $this->assertContains('id="checkbox_6"', $html);
        $this->assertNotContains('id="checkbox_7"', $html); // no found taxon --> no checkbox
        $htmlCheckBox  = '<label for="checkbox_3">';
        $htmlCheckBox .= '<input type="checkbox" name="taxonCheckboxes[]" value="3" id="checkbox_3">';
        $htmlCheckBox .= 'Urbanisme / Espace public / Environnement</label>';
        $this->assertContains($htmlCheckBox, $html); // no found taxon --> no checkbox
    }


    /**
     * MappingForm method used by connected user (type = "Administration")
     * after registering as user of a sofware.
     *
     *      URL: /en/mappingForm/<softwareId>
     *           /fr/mappingForm/<softwareId>
     *
     * User sends form several times (add/edit):
     * - Check BEFORE send form data ---> no associed entry (user-software-taxon)
     * - Send form data with 2 associed entries
     * - Check AFTER ---> 2 entries found
     * - Resend form data: user unchecks an entry
     * - Check AFTER ---> only last entry found
     * - Resend form data: user unchecks last entry
     * - Check AFTER ---> no associed entry (user-software-taxon)
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy-software
     * @group taxonomy-software_public
     * @group taxonomy-software_mappingForm
     *
     * @return void
     */
    public function testMappingFormSend()
    {
        $taxonIdA = 3;
        $taxonSlugA = 'urbanisme-espace-public-environnement';
        $taxonIdB = 4;
        $taxonSlugB = 'bureautique';
        $softwareId = 5;
        $softwareSlug = 'soft-5';
        $idUserA = 6;
        $urlForm =  "/fr/mappingForm/$softwareId";
        $softwareUrl =  "/fr/softwares/$softwareId";
        $userUrl =  "/fr/users/$idUserA";
        $testeddUrlTaxonA = "/fr/cartographie/metiers/$taxonSlugA/$softwareSlug/$taxonIdA.$softwareId";
        $redirectUrlTaxonA = "/fr/cartographie/metiers/$taxonSlugA/$taxonIdA";
        $testeddUrlTaxonB = "/fr/cartographie/generiques/$taxonSlugB/$softwareSlug/$taxonIdB.$softwareId";
        $redirectUrlTaxonB = "/fr/cartographie/generiques/$taxonSlugB/$taxonIdB";

        // connected user (type = "Administration")
        $this->setConnectedAdministrationSession();

        // Beforehand, the user declares himself/herself to be a user of the software.
        $r = $this->checkUrlOk($userUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertNotContains("id=\"softwarePreviewCard-$softwareId\"", $html);
        $this->post('api/v1/softwares/usersSoftware/5.json');


        // Check BEFORE send form data
        ///////////////////////////////////

        // (1) no entry for current user (ID-6), software ID-5 and taxon ID 3 | 4

        // on mapping page (1)
        $this->checkUrlRedirectToAnotherUrl($testeddUrlTaxonA, $redirectUrlTaxonA, ['html'], 301);
        $this->checkUrlRedirectToAnotherUrl($testeddUrlTaxonB, $redirectUrlTaxonB, ['html'], 301);

        // on software page (1)
         $r = $this->checkUrlOk($softwareUrl, ['html'], false);
         $html = $r['html']['data'];
         $this->assertcontains("id=\"mappingForSoftware-noRecord\"", $html);
         $this->assertNotContains("id=\"mappingForSoftware-$softwareId\"", $html);
         $this->assertNotContains("class=\"linkMapping-taxonSoftware\"", $html);
         $this->assertNotContains("id=\"linkMapping-taxon-$taxonIdA-Software-$softwareId\"", $html);
         $this->assertNotContains("id=\"linkMapping-taxon-$taxonIdB-Software-$softwareId\"", $html);

        // on user page (1)
        $r = $this->checkUrlOk($userUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"softwarePreviewCard-$softwareId\"", $html);
        $this->assertNotContains("id=\"mappingForUser-$idUserA\"", $html);
        $this->assertNotContains("class=\"linkMappingTaxon\"", $html);
        $this->assertNotContains("id=\"linkMappingTaxon-$taxonIdA\"", $html);
        $this->assertNotContains("id=\"linkMappingTaxon-$taxonIdB\"", $html);

        // on form page, checkbox (of taxon ID 3|ID 4) must not be checked
        $r = $this->checkUrlOk($urlForm, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains('id="checkbox_'.$taxonIdA.'"', $html);
        $this->assertContains('id="checkbox_'.$taxonIdB.'"', $html);
        $this->assertNotContains('id="checkbox_'.$taxonIdA.'" checked="checked"', $html);
        $this->assertNotContains('id="checkbox_'.$taxonIdB.'" checked="checked"', $html);

        // Send form data
        ///////////////////////////////////
        $data = [ 'id' => "$softwareId", 'taxonCheckboxes' => [$taxonIdA, $taxonIdB]];
        $this->post($urlForm, $data);
        $headers = $this->_response->header();
        $this->assertResponseCode(302);
        $this->assertEquals($softwareUrl, $headers['Location']);

        // Check AFTER send form data
        ///////////////////////////////////

        // (2) entries found for current user (ID-6), software ID-5 and taxon ID 3 | 4

        // on software page (2)
        $r = $this->checkUrlOk($softwareUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertNotContains("id=\"mappingForSoftware-noRecord\"", $html);
        $this->assertContains("id=\"mappingForSoftware-$softwareId\"", $html);
        $this->assertContains("class=\"linkMapping-taxonSoftware\"", $html);
        $this->assertContains("id=\"linkMapping-taxon-$taxonIdA-Software-$softwareId\"", $html);
        $this->assertContains("id=\"linkMapping-taxon-$taxonIdB-Software-$softwareId\"", $html);

        // on user page (2)
        $r = $this->checkUrlOk($userUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"softwarePreviewCard-$softwareId\"", $html);
        $this->assertContains("id=\"mappingForUser-$idUserA\"", $html);
        $this->assertContains("class=\"linkMappingTaxon\"", $html);
        $this->assertContains("id=\"linkMappingTaxon-$taxonIdA\"", $html);
        $this->assertContains("id=\"linkMappingTaxon-$taxonIdB\"", $html);

        // on mapping page: entry found for current user (ID-6), software ID-5 and taxon ID 3
        $r = $this->checkUrlOk($testeddUrlTaxonA, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"mappingTaxon$taxonIdA-UsersOf$softwareId\"", $html); // <section id="">
        $this->assertContains("id=\"userPreviewCard-$idUserA\"", $html);

        // on mapping page: entry found for current user (ID-6), software ID-5 and taxon ID 4
        $r = $this->checkUrlOk($testeddUrlTaxonB, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"mappingTaxon$taxonIdB-UsersOf$softwareId\"", $html); // <section id="">
        $this->assertContains("id=\"userPreviewCard-$idUserA\"", $html);

        // on form page, checkbox (of taxon ID3|ID4) must be checked
        $r = $this->checkUrlOk($urlForm, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains('id="checkbox_'.$taxonIdA.'" checked="checked"', $html);
        $this->assertContains('id="checkbox_'.$taxonIdB.'" checked="checked"', $html);

        // Resend form data: user unchecks an entry $taxonIdA
        /////////////////////////////////////////////////////
        $data = [ 'id' => "$softwareId", 'taxonCheckboxes' => [$taxonIdB]];
        $this->post($urlForm, $data);
        $headers = $this->_response->header();
        $this->assertResponseCode(302);
        $this->assertEquals($softwareUrl, $headers['Location']);

        // Check AFTER resend send form data (user unchecks an entry $taxonIdA)
        ///////////////////////////////////////////////////////////////////////

        // (3) entry found for current user (ID-6), software ID-5 and taxon ID 4, but not for taxon ID 3

        // on software page (3)
        $r = $this->checkUrlOk($softwareUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertNotContains("id=\"mappingForSoftware-noRecord\"", $html);
        $this->assertContains("id=\"mappingForSoftware-$softwareId\"", $html);
        $this->assertContains("class=\"linkMapping-taxonSoftware\"", $html);
        $this->assertNotContains("id=\"linkMapping-taxon-$taxonIdA-Software-$softwareId\"", $html);
        $this->assertContains("id=\"linkMapping-taxon-$taxonIdB-Software-$softwareId\"", $html);

        // on user page (3)
        $r = $this->checkUrlOk($userUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"softwarePreviewCard-$softwareId\"", $html);
        $this->assertContains("id=\"mappingForUser-$idUserA\"", $html);
        $this->assertContains("class=\"linkMappingTaxon\"", $html);
        $this->assertNotContains("id=\"linkMappingTaxon-$taxonIdA\"", $html);
        $this->assertContains("id=\"linkMappingTaxon-$taxonIdB\"", $html);

        // on mapping page: no entry for current user (ID-6), software ID-5 and taxon ID 3
        $this->checkUrlRedirectToAnotherUrl($testeddUrlTaxonA, $redirectUrlTaxonA, ['html'], 301);

        //  on mapping page: entry found for current user (ID-6), software ID-5 and taxon ID 4
        $r = $this->checkUrlOk($testeddUrlTaxonB, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"mappingTaxon$taxonIdB-UsersOf$softwareId\"", $html); // <section id="">
        $this->assertContains("id=\"userPreviewCard-$idUserA\"", $html);

        // on form page, checkbox (of taxon ID4) must be checked, but not for taxon ID3
        $r = $this->checkUrlOk($urlForm, ['html'], false);
        $html = $r['html']['data'];
        $this->assertNotContains('id="checkbox_'.$taxonIdA.'" checked="checked"', $html);
        $this->assertContains('id="checkbox_'.$taxonIdB.'" checked="checked"', $html);


        // Resend form data: user unchecks the last entry $taxonIdB
        /////////////////////////////////////////////////////////////////////////////
        $data = [ 'id' => "$softwareId"];
        $this->post($urlForm, $data);
        $headers = $this->_response->header();
        $this->assertResponseCode(302);
        $this->assertEquals($softwareUrl, $headers['Location']);

        // Check AFTER resend send form data (user unchecks the last entry $taxonIdB)
        /////////////////////////////////////////////////////////////////////////////

        // (4) no entry for current user (ID-6), software ID-5 and taxon ID 3 | 4

        // on software page (4)
        $r = $this->checkUrlOk($softwareUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"mappingForSoftware-noRecord\"", $html);
        $this->assertNotContains("id=\"mappingForSoftware-$softwareId\"", $html);
        $this->assertNotContains("class=\"linkMapping-taxonSoftware\"", $html);
        $this->assertNotContains("id=\"linkMapping-taxon-$taxonIdA-Software-$softwareId\"", $html);
        $this->assertNotContains("id=\"linkMapping-taxon-$taxonIdB-Software-$softwareId\"", $html);

        // on user page (4)
        $r = $this->checkUrlOk($userUrl, ['html'], false);
        $html = $r['html']['data'];
        $this->assertContains("id=\"softwarePreviewCard-$softwareId\"", $html);
        $this->assertNotContains("id=\"mappingForUser-$idUserA\"", $html);
        $this->assertNotContains("class=\"linkMappingTaxon\"", $html);
        $this->assertNotContains("id=\"linkMappingTaxon-$taxonIdA\"", $html);
        $this->assertNotContains("id=\"linkMappingTaxon-$taxonIdB\"", $html);

        //  on mapping page (4)
        $this->checkUrlRedirectToAnotherUrl($testeddUrlTaxonA, $redirectUrlTaxonA, ['html'], 301);
        $this->checkUrlRedirectToAnotherUrl($testeddUrlTaxonB, $redirectUrlTaxonB, ['html'], 301);

        // on form page, checkbox (of taxon ID 3|ID 4) must not be checked
        $r = $this->checkUrlOk($urlForm, ['html'], false);
        $html = $r['html']['data'];
        $this->assertNotContains('id="checkbox_'.$taxonIdA.'" checked="checked"', $html);
        $this->assertNotContains('id="checkbox_'.$taxonIdB.'" checked="checked"', $html);
    }

    /**
     * MappingForm method for a not found software
     *
     *      URL: /en/mappingForm/<softwareIdNotFound>   ----> redirect to /en/mapping/
     *           /fr/mappingForm/<softwareIdNotFound>   ----> redirect to /fr/cartographie/
     *
     * @group public
     * @group taxonomy
     * @group taxonomy_public
     * @group taxonomy-software
     * @group taxonomy-software_public
     * @group taxonomy-software_mappingForm
     * @return void
     */
    public function testMappingFormSofwareIdNotFound()
    {
        $url =  '/fr/mappingForm/888'; // software ID 888 ---> not in database

        // connected user (type = "Administration")
        $this->setConnectedAdministrationSession();
        $this->get("/fr/"); // force user language to french
        $expectedUrl = '/fr/cartographie/';
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 301);
    }


    /**
     * MappingForm method with URL prefix /api/v1/
     * - HTML format redirects to the official URL
     * - JSON format returns a 404 "not found" error.
     *
     *    URL: /api/v1/taxonomys-softwares/mappingForm                   ---> redirect to '/en/mapping/'
     *         /api/v1/taxonomys-softwares/mappingForm/<softwareId>      ---> redirect to '/en/mappingForm/<softwareId>'
     *         /api/v1/taxonomys-softwares/mappingForm.json              ---> disable via parent::beforeFilter()
     *         /api/v1/taxonomys-softwares/mappingForm/<softwareId>.json ---> disable via parent::beforeFilter()
     *
     * @group seo
     * @group public
     * @group taxonomy
     * @group taxonomy-software
     * @group taxonomy-software_public
     * @group taxonomy-software_mappingForm
     * @return void
     */
    public function testMappingFormCheckApiPrefixInUrl()
    {
        $urlBase = '/api/v1/taxonomys-softwares/mappingForm'; // missing software ID
        $url = "$urlBase/2"; // software ID 2

        // connected user (type = "Administration")
        $this->setConnectedAdministrationSession();

        // JSON format
        $this->checkUrlNotFound($urlBase, ['json']);
        $this->checkUrlNotFound($url, ['json']);

        // HTML format
        $this->get("/fr/"); // force user language to french
        $expectedUrl = '/fr/cartographie/'; // software ID 2
        $this->checkUrlRedirectToAnotherUrl($urlBase, $expectedUrl, ['html'], 301);
        $expectedUrl = '/fr/mappingForm/2'; // software ID 2
        $this->checkUrlRedirectToAnotherUrl($url, $expectedUrl, ['html'], 301);

        // Connected user (type != "Administration")
        $this->setConnectedPersonSession();
        $this->checkUrlNotFound($url, ['json']);
        $this->checkUrlNotAuthorized($url, ['html']);

        // Anonymous user
        $this->setAnonymousUserSession();
        $this->checkUrlNotFound($url, ['json']);
        $this->checkUrlRedirectToLogin($url, ['html']);
    }



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Admin actions ---> self-generated by CakePHP  //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // Admin user
    /////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Test index method for admin user ---> allowed
     *
     *      URL: /api/v1/taxonomys-softwares
     *           ----> allowed only for user with "admin" role
     *
     * @group admin
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testIndexForAdminUser()
    {
        $this->setAdminSessionOfPersonType();
        $result = $this->checkUrlOk('/api/v1/taxonomys-softwares');
        $json = $result['json'];
        $html = $result['html'];
    }

    /**
     * Test view method
     *
     *      URL: /api/v1/taxonomys-softwares/view/1
     *           ----> allowed only for user with "admin" role
     *
     * @group admin
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testViewForAdminUser()
    {
        $this->setAdminSessionOfPersonType();
        $result = $this->checkUrlOk('/api/v1/taxonomys-softwares/view/1');
    }

    /**
     * Test add method
     *
     *      URL: /api/v1/taxonomys-softwares/add
     *           ----> allowed only for user with "admin" role
     *
     * @todo Not implemented yet.
     * @group admin
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testAddForAdminUser()
    {
        $this->setAdminSessionOfPersonType();
        $result = $this->checkUrlOk('/api/v1/taxonomys-softwares/add');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     *      URL: /api/v1/taxonomys-softwares
     *           ----> allowed only for user with "admin" role
     *
     * @todo  not implemented yet.
     * @group admin
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testEditForAdminUser()
    {
        $this->setAdminSessionOfPersonType();
        $result = $this->checkUrlOk('/api/v1/taxonomys-softwares/edit/1');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     *      URL: /api/v1/taxonomys-softwares
     *           ----> allowed only for user with "admin" role
     *
     * @todo  not implemented yet.
     * @group admin
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testDeleteForAdminUser()
    {
        $this->setAdminSessionOfPersonType();

        $this->markTestIncomplete('Not implemented yet.');
    }


    // Connected user
    /////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Test index method for connected user ---> not authorized
     *
     *      URL: /api/v1/taxonomys-softwares
     *           ----> allowed only for user with "admin" role
     *
     * @group user
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testFailIndexForConnectedUser()
    {
        $this->setConnectedUserSession();
        $this->checkUrlNotAuthorized('/api/v1/taxonomys-softwares');
    }

    /**
     * Test view method for connected user ---> not authorized
     *
     *      URL: /api/v1/taxonomys-softwares/view/1
     *           ----> allowed only for user with "admin" role
     *
     * @group user
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testFailViewForConnectedUser()
    {
        $this->setConnectedUserSession();
        $this->checkUrlNotAuthorized('/api/v1/taxonomys-softwares/view/1');
    }

    /**
     * Test add method for connected user
     * GET ---> not authorized
     * POST ---> @todo  not implemented yet
     *
     *      URL: /api/v1/taxonomys-softwares/add
     *           ----> allowed only for user with "admin" role
     *
     * @group user
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testFailAddForConnectedUser()
    {
        $this->setConnectedUserSession();
        $this->checkUrlNotAuthorized('/api/v1/taxonomys-softwares/add');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method for connected user
     * GET ---> not authorized
     * POST ---> @todo  not implemented yet
     *
     * @group user
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testFailEditForConnectedUser()
    {
        $this->setConnectedUserSession();
        $this->checkUrlNotAuthorized('/api/v1/taxonomys-softwares/edit/1');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method for connected user
     *
     * @todo    not implemented yet
     * @group user
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testFailDeleteForConnectedUser()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }


    // Anonymous user
    /////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Test index method for anonymous user ---> redirect to login form
     *
     *      URL: /api/v1/taxonomys-softwares
     *           ----> allowed only for user with "admin" role
     *
     * @group anonymous
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testFailIndexForAnonymousUser()
    {
        $this->setAnonymousUserSession();
        $this->checkUrlRedirectToLogin('/api/v1/taxonomys-softwares');
    }

    /**
     * Test view method for anonymous user ---> redirect to login form
     *
     *      URL: /api/v1/taxonomys-softwares/view/1
     *           ----> allowed only for user with "admin" role
     *
     * @group anonymous
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testFailViewForAnonymousUser()
    {
        $this->setAnonymousUserSession();
        $this->checkUrlRedirectToLogin('/api/v1/taxonomys-softwares/view/1');
    }

    /**
     * Test add method for anonymous user
     * GET ---> redirect to login form
     * POST ---> @todo  not implemented yet
     *
     *      URL: /api/v1/taxonomys-softwares/add
     *           ----> allowed only for user with "admin" role
     *
     * @group anonymous
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testFailAddForAnonymousUser()
    {
        $this->setAnonymousUserSession();
        $this->checkUrlRedirectToLogin('/api/v1/taxonomys-softwares/add');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method for anonymous user
     * GET ---> redirect to login form
     * POST ---> @todo  not implemented yet
     *
     * @group anonymous
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testFailEditForAnonymousUser()
    {
        $this->setAnonymousUserSession();
        $this->checkUrlRedirectToLogin('/api/v1/taxonomys-softwares/edit/1');

        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method for anonymous user
     *
     * @todo  not implemented yet
     *
     * @group anonymous
     * @group taxonomy
     * @group taxonomy-software
     * @return void
     */
    public function testFailDeleteForAnonymousUser()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
