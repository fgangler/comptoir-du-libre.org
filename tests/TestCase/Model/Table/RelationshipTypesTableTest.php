<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RelationshipTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RelationshipTypesTable Test Case
 */
class RelationshipTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var RelationshipTypesTable
     */
    public $RelationshipTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.relationship_types',
        'app.relationships'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RelationshipTypes')
            ? []
            : ['className' => 'App\Model\Table\RelationshipTypesTable'];
        $this->RelationshipTypes = TableRegistry::get('RelationshipTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RelationshipTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
