<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TaxonomysSoftwaresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TaxonomysSoftwaresTable Test Case
 */
class TaxonomysSoftwaresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TaxonomysSoftwaresTable
     */
    public $TaxonomysSoftwares;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.taxonomys_softwares',
        'app.taxonomys',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.reviews',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_users',
        'app.usedsoftwares',
        'app.backedsoftwares',
        'app.createdsoftwares',
        'app.contributionssoftwares',
        'app.providerforsoftwares',
        'app.screenshots',
        'app.softwares_statistics',
        'app.tags',
        'app.softwares_tags',
        'app.userssoftwares',
        'app.backerssoftwares',
        'app.creatorssoftwares',
        'app.contributorssoftwares',
        'app.providerssoftwares',
        'app.workswellsoftwares',
        'app.alternativeto'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TaxonomysSoftwares') ? [] : [
            'className' => 'App\Model\Table\TaxonomysSoftwaresTable'
        ];
        $this->TaxonomysSoftwares = TableRegistry::get('TaxonomysSoftwares', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TaxonomysSoftwares);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
