<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LicenceTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LicenceTypesTable Test Case
 */
class LicenceTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var LicenceTypesTable
     */
    public $LicenceTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.licence_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LicenceTypes') ? [] : ['className' => 'App\Model\Table\LicenceTypesTable'];
        $this->LicenceTypes = TableRegistry::get('LicenceTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LicenceTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
