<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TaxonomysTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TaxonomysTable Test Case
 */
class TaxonomysTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TaxonomysTable
     */
    public $Taxonomys;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.taxonomys',
        'app.taxonomys_softwares'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Taxonomys') ? [] : ['className' => 'App\Model\Table\TaxonomysTable'];
        $this->Taxonomys = TableRegistry::get('Taxonomys', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Taxonomys);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
