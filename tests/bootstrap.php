<?php
/**
 * Test runner bootstrap.
 *
 * Add additional configuration/setup your application needs when running
 * unit tests in this file.
 */

use Cake\Core\Configure;

////////////////////////////////////////////////////////////////////
// Bypass min time to send sign up form (limit spam by bots)
// - it's works for unit tests
// - it's not works for acceptance tests
////////////////////////////////////////////////////////////////////
// define("TEST_BYPASS_DEFAULT_MIN_TIME_TO_SEND_SIGNUP_FORM", 1);
////////////////////////////////////////////////////////////////////

// Application bootstrap
require dirname(__DIR__) . '/config/bootstrap.php';

// Min time to send sign up form (limit spam by bots)
define("TEST_MIN_TIME_TO_SEND_SIGNUP_FORM", Configure::read('MIN_TIME_TO_SEND_SIGNUP_FORM'));
echo "\nMIN_TIME_TO_SEND_SIGNUP_FORM ". TEST_MIN_TIME_TO_SEND_SIGNUP_FORM ." seconde(s)\n";


// Polyfill for array_key_first() function added in PHP 7.3.
if (!function_exists('array_key_first')) {
    function array_key_first(array $array)
    {
        if ($array === []) {
            return null;
        }
        foreach ($array as $key => $_) {
            return $key;
        }
    }
}

// Polyfill for array_key_last() function added in PHP 7.3.
if (!function_exists('array_key_last')) {
    function array_key_last(array $array)
    {
        if ($array === []) {
            return null;
        }
        return array_key_first(array_slice($array, -1));
    }
}
