<?php
/**
 * Controller Of Users.
 *
 * @package App\Controller\Api\V1
 * @author  mickael pastor <mickael.pastor@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Controller\Component\RulesComponent;
use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Mailer\MailerAwareTrait;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;
use Cake\ORM\Query;
use Cake\Utility\Text;
use Cake\Filesystem\Folder;

/**
 * Users Controller
 *
 * @property UsersTable $Users
 * @package  App\Controller\Api\V1
 * @author   mickael pastor <mickael.pastor@adullact.org>
 * @license  https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
class UsersController extends AppController
{
    use MailerAwareTrait;

    /**
     * How long the token is valid
     * to change the password (in number of hours)
     */
    const TOKEN_VALIDITY_TIME_TO_CHANGE_PASSWORD = '24'; // 24 hours

    /**
     * Manage all rights for the controllers' actions.
     * Returns true if the user can use the current action, FALSE otherwise.
     * Returns true for add a project if the user is connected
     * Returns true for edit and delete action if the user is owner.
     *
     *
     * notice: $forceDeny parameter is mandatory to be compatible with parent::isAuthorized()
     *                     and it's forced to TRUE
     *
     * @param array $user A user.
     * @param  boolean $forceDeny by default FALSE, set TRUE to force the deny on parent::isAuthorized()
     * @return boolean
     */
    public function isAuthorized($user, $forceDeny = false)
    {

        // Access to edit method only for the current user
        $forceDeny = false;
        if ($this->request->param('action') === 'edit') {
            if ($this->Auth->user('id') == $this->request->params["pass"][0]) {
                return true;
            }
            // GET requests is not allowed, if the user tries to edit another user's record
            // ----> force the deny on parent::isAuthorized()
            $forceDeny = true;
        }

        // Access to contact method
        if ($this->request->param('action') === 'contact') {
            // Allow access to all users except providers
            $notAllowedUserTypeId = $this->getUserTypeIdByName('Company');
            if ($this->Auth->user('user_type_id') !== $notAllowedUserTypeId) {
                return true;
            } else { // if a provider (user_type = 'Company') tries to contact another user
                $forceDeny = true; // force the deny on parent::isAuthorized() for GET request
                                   // by default POST request are deny on parent::isAuthorized()
            }
        }

        // Access to "export" method
        if ($this->request->param('action') === 'export') {
            // only user with "admin" role can use "export" action --> see: parent::isAuthorized()
            $forceDeny = true;  // force the deny on parent::isAuthorized() for GET request
                                // by default POST request are deny on parent::isAuthorized()
        }

        if ($this->request->param('action') === 'changePassword') {
            $tokenOrId = $this->request->params["pass"][0];

            $user = $this->Users->findById($tokenOrId, ["contain" => []])->first();

            if ($this->Auth->user('id') == $user->id || $this->Users->findByToken(
                $tokenOrId,
                ["contain" => []]
            )->first()) {
                return true;
            }
        }

        //If user is auth
        if ($this->request->param('action') === 'register') {
            if ($user) {
                return true;
            }
        }
        return parent::isAuthorized($user, $forceDeny);
    }

    /**
     * Initialisation of a user
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->paginate = [
            'limit' => Configure::read('LIMIT'),
            'maxLimit' => Configure::read('LIMIT'),
            'order' => [
                'Users.username' => Configure::read('ORDER'),
            ],
            'fields' => [
                'id',
                'username',
                'email',
                'user_type_id',
                'url',
                'description',
                'photo',
                'logo_directory',
                'UserTypes.name'
            ],
            'contain' => ['UserTypes']
        ];
    }


    /**
     * $links = [ 0 => [ 'name' => '…', 'url' => '/dir/file', 'title' => '…'],
     *            1 => [ 'name' => '…', 'url' => '/dir/file', 'title' => '…'], ]
     *
     * @param array $links
     */
    protected function setBreadcrumbs(array $links = [])
    {
        $firstLink = [
            'name' => __d('Breadcrumbs', 'User.ListOfUsers'),
            'url' => 'users'
        ];
        array_unshift($links, $firstLink);
        parent::setBreadcrumbs($links);
    }

    /**
     * $links = [ 0 => [ 'name' => '…', 'url' => '/dir/file', 'title' => '…'],
     *            1 => [ 'name' => '…', 'url' => '/dir/file', 'title' => '…'], ]
     *
     * @param array $links
     */
    protected function setBreadcrumbsUser(array $links = [], $user = null)
    {

        if (is_object($user) and $user->user_type->name === 'Company') {
            $firstLink = [
                'name' => __d('Breadcrumbs', 'Users.Providers'),
                'url' => "users/providers"
            ];
        } else {
            $firstLink = [
                'name' => __d('Breadcrumbs', 'User.ListOfUsers'),
                'url' => 'users'
            ];
        }
        array_unshift($links, $firstLink);
        parent::setBreadcrumbs($links);
    }


    /**
     * Allow = actions allow without being connected
     * Before filter manage right access of users
     *
     * @param Event $event Events
     *
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(
            [
                'add',
                'providers',
                'administrations',
                'nonProfitUsers',
                'providerforSoftwares',
                'reviewsforSoftware',
                'usedSoftwares',
                'forgotPassword',
                'changePassword',
                'resetPassword',
            ]
        );
        $this->Auth->deny(['delete', 'edit', 'register', 'contact']);

        parent::beforeFilter($event);
    }

    /**
     * Returns a list of users.
     *
     * Note: whereas service providers are part of the User entity in the database, they are not considered as a user
     *   from the application point of view. They have their dedicated "identity". Maybe one day, they'll be extracted
     *   as a separated entity in the DB schema.
     *
     * @return void
     */
    public function index()
    {
        if (!empty($this->request->query)
            && isset($this->request->query["order"])
        ) {
            $sort = explode(".", $this->request->query["order"]);
            // Note JGauthier 2019-07-16: just discovered this horror. This is not the correct way to do this.
            // TODO This should be fixed
            $this->request->query["sort"] = $sort[0];
            $this->request->query["direction"] = $sort[1];
        }

        $users = $this->Users
            ->find(
                'search',
                [
                    'sortWhitelist' => [
                        'username',
                        'average_review',
                        'created',
                        'modified',
                    ],
                    'search' => $this->request->query,
                    'contain' => ['UserTypes'],
                    'order' =>
                        isset($this->request->query["sort"])
                        && isset($this->request->query["direction"])
                            ?
                            ['Users.' . $this->request->query["sort"] => $this->request->query["direction"]]
                            :
                            ['Users.username' => Configure::read('ORDER')]
                    ,
                ]
            )
            ->where(["UserTypes.cd <> " => "Company"]);

        //For filters
        $hadReview = [
            "false" => __d("Forms", "Search.Filter.Users.NoFilterOnReview"),
            "true" => __d("Forms", "Search.Filter.Users.WithReview"),
        ];

        $userType = [
            "all" => __d("Forms", "Search.Filter.Users.NoFilterOnUserType"),
            "Administration" => __d("Forms", "Search.Filter.Users.userTpye.Administration"),
            "Person" => __d("Forms", "Search.Filter.Users.userTpye.Person"),
            "Association" => __d("Forms", "Search.Filter.Users.userTpye.Association"),
        ];

        $isUserOf = [
            "false" => __d("Forms", "Search.Filter.Users.NoFilterOnIsUserOf"),
            "true" => __d("Forms", "Search.Filter.Users.IsUserOf"),
        ];

        $isServiceProvider = [
            "false" => __d("Forms", "Search.Filter.Users.NoFilterOnIsServicesProvider"),
            "true" => __d("Forms", "Search.Filter.Users.IsServicesProvider"),
        ];

        $order = [
            "username.asc" => __d("Forms", "Search.Sort.usernameAsc"),
            "username.desc" => __d("Forms", "Search.Sort.usernameDesc"),
            "created.asc" => __d("Forms", "Search.Sort.createdAsc"),
            "created.desc" => __d("Forms", "Search.Sort.createdDesc"),
            "modified.asc" => __d("Forms", "Search.Sort.modifiedAsc"),
            "modified.desc" => __d("Forms", "Search.Sort.modifiedDesc"),
        ];
        //End For filters

        $this->set('hadReview', $hadReview);
        $this->set('userType', $userType);
        $this->set('isUserOf', $isUserOf);
        $this->set('order', $order);
        $this->set('isServiceProvider', $isServiceProvider);

        $this->set(
            [
                'users' => $this->paginate($users),
                '_serialize' => ['users']
            ]
        );
        $this->setBreadcrumbs();
    }

    /**
     * Export method
     *
     * @return void
     */
    public function export()
    {
        $this->request->allowMethod(['get']);

        ////////////////////////////////////////////////////////////
        // Retrieve data from database
        ////////////////////////////////////////////////////////////
        $this->paginate = [
            "Users" => [
                'sortWhitelist' => [
                    'username'
                ],

                'limit' => Configure::read('LIMIT'),
                'maxLimit' => Configure::read('LIMIT'),
                'order' => ['username' => Configure::read('ORDER')],
            ]
        ];

        $users = $this->Users
            ->find(
                'search',
                [
                    'sortWhitelist' => [
                        'username',
                        'average_review',
                        'created',
                        'modified',
                    ],
                    'search' => $this->request->query,
                    'contain' => [
                        'UserTypes',
                        'Reviews' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Reviews.created' => 'ASC']);
                            },
                            'Softwares' => [
                                'fields' => [
                                    'id',
                                    'softwarename',
                                    'logo_directory',
                                    'photo',
                                ]
                            ]
                        ],
                        'Usedsoftwares' => [
                            'strategy' => 'select',
                            'queryBuilder' => function (Query $q) {
                                return $q->order(['Softwares.softwarename' => 'ASC']);
                            },
                            'Softwares' => [
                                'Reviews' => [
                                ],
                                'fields' => [
                                    'id',
                                    'softwarename',
                                    'logo_directory',
                                    'photo',
                                    'description',
                                ]
                            ]
                        ],
                        "Providerforsoftwares" => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Softwares.softwarename' => 'ASC']);
                            },
                            'Softwares' => [
                                'Reviews' => [
                                ],
                                'fields' => [
                                    'id',
                                    'softwarename',
                                    'logo_directory',
                                    'photo',
                                    'description'
                                ]
                            ],
                        ],
                    ],
                    'order' =>   ['UserTypes.cd'=> 'ASC', 'Users.username' => 'ASC'],
                ]
            );

        ////////////////////////////////////////////////////////////
        // Generate CSV ans JSON exports
        ////////////////////////////////////////////////////////////
        $data = [
            'date_of_export' => date(DATE_ATOM),
            'number_of_users' => $users->count(),
            'users' => []
        ];
        $csvData  = '';
        foreach ($this->paginate($users) as $user) {
            $lastUserUpdate = $user->modified; // Cake\I18n\Time object

            // Provider
            $providedForSoftwareUrl = '';
            $lastProvidedForSoftware = '';
            $providedForSoftwares = [];
            $providedForSoftwareNumber = count($user->providerforsoftwares);
            if ($providedForSoftwareNumber > 0) {
                $providedForSoftwareUrl =  'https://comptoir-du-libre.org/fr/users/providerforSoftwares/'. $user->id ;
            }
            foreach ($user->providerforsoftwares as $providedSoftware) {
                if ($lastUserUpdate < $providedSoftware->created) {
                    $lastUserUpdate = $providedSoftware->created;          // Cake\I18n\Time object
                }
                if (empty($lastProvidedForSoftware) || $lastProvidedForSoftware < $providedSoftware->created) {
                    $lastProvidedForSoftware = $providedSoftware->created; // Cake\I18n\Time object
                }
                $providedForSoftwares[] = [
                    'created' => $providedSoftware->created,
                    'software' => $providedSoftware->software->softwarename,
                    'software_id' => $providedSoftware->software->id,
                    'software_url' => 'https://comptoir-du-libre.org/fr/softwares/'.$providedSoftware->software->id,
                ];
            }

            // Review
            ////////////////////////////////////////////////////////////////////
            $reviewUrl = '';
            $lastReview = '';
            $reviews = [];
            $reviewNumber = count($user->reviews);
            if ($reviewNumber > 0) {
                $reviewUrl =  'https://comptoir-du-libre.org/fr/users/'. $user->id .'/reviews';
            }
            foreach ($user->reviews as $review) {
                if ($lastUserUpdate < $review->created) {
                    $lastUserUpdate = $review->created; // Cake\I18n\Time object
                }
                if (empty($lastReview) || $lastReview < $review->created) {
                    $lastReview = $review->created;    // Cake\I18n\Time object
                }
                $review_url  = '';
                $review_url .= 'https://comptoir-du-libre.org/fr/softwares/';
                $review_url .= $review->software->id.'/reviews/' . $review->id;
                $reviews[] = [
                    'review_id' => $review->id,
                    'review_url' => $review_url,
                    'review_created' => $review->created, // Cake\I18n\Time object
                    'software' => $review->software->softwarename,
                    'software_id' => $review->software->id,
                    'software_url' => 'https://comptoir-du-libre.org/fr/softwares/'.$review->software->id,
                ];
            }

            // User of
            ////////////////////////////////////////////////////////////////////
            $usedSoftwareUrl = '';
            $lastUsedSoftware = '';
            $usedSoftwares = [];
            $usedSoftwareNumber = count($user->usedsoftwares);
            if ($usedSoftwareNumber > 0) {
                $usedSoftwareUrl =  'https://comptoir-du-libre.org/fr/users/usedSoftwares/'. $user->id ;
            }
            foreach ($user->usedsoftwares as $usedsoftware) {
                if ($lastUserUpdate < $usedsoftware->created) {
                    $lastUserUpdate = $usedsoftware->created;   // Cake\I18n\Time object
                }
                if (empty($lastUsedSoftware) || $lastUsedSoftware < $usedsoftware->created) {
                    $lastUsedSoftware = $usedsoftware->created; // Cake\I18n\Time object
                }
                $usedSoftwares[] = [
                    'created' => $usedsoftware->created,        // Cake\I18n\Time object
                    'software' => $usedsoftware->software->softwarename,
                    'software_id' => $usedsoftware->software->id,
                    'software_url' => 'https://comptoir-du-libre.org/fr/softwares/'.$usedsoftware->software->id,
                ];
            }

            // Taxonomies
            $softwareDeclaredInTaxonomy = $this->getSofwareMappingForUser($user);
            $taxonomiesSoftware = [];
            $lastTaxonomiesSoftware = '';
            $taxonomiesSoftwareUrl = '';
            $taxonomiesSoftwareNumber = count($softwareDeclaredInTaxonomy);
            if ($taxonomiesSoftwareNumber > 0) {
                foreach ($softwareDeclaredInTaxonomy as $softwareName => $item) {
                    $updateDate = $item['modified'];
                    $softwareId = $item['softwareId'];
                    $taxonomiesSoftware[$softwareId] = $item;
                    if ($lastUserUpdate < $updateDate) {
                        $lastUserUpdate = $updateDate;         // Cake\I18n\Time object
                    }
                    if (empty($lastTaxonomiesSoftware) || $lastTaxonomiesSoftware < $updateDate) {
                        $lastTaxonomiesSoftware = $updateDate; // Cake\I18n\Time object
                    }
                }
            }

            // JSON data
            $item = [
                'id' => $user->id,
                'created' => $user->created,    // Cake\I18n\Time object
                'modified' => $lastUserUpdate,  // Cake\I18n\Time object
                'url' => 'https://comptoir-du-libre.org/fr/users/'. $user->id,
                'name' => $user->username,
                'user_type' => $user->user_type->cd,
                'external_resources' => [
                    'website' => $user->url,
                ],
                'reviews' => [
                    'number'   => $reviewNumber,
                    'last'     => $lastReview,                  // Cake\I18n\Time object
                    'url'     => $reviewUrl,
                    'software'  => $reviews,
                ],
                'usingSoftware' => [
                    'number'   => $usedSoftwareNumber,
                    'last'     => $lastUsedSoftware,            // Cake\I18n\Time object
                    'url'     => $usedSoftwareUrl,
                    'software'  => $usedSoftwares,
                ],
                'taxonomies' => [
                    'number'   => $taxonomiesSoftwareNumber,
                    'last'     => $lastTaxonomiesSoftware,      // Cake\I18n\Time object
                    'url'     => $taxonomiesSoftwareUrl,
                    'software'  => $taxonomiesSoftware,
                ],
                'providingSoftware' => [
                    'number'   => $providedForSoftwareNumber,
                    'last'     => $lastProvidedForSoftware,     // Cake\I18n\Time object
                    'url'     => $providedForSoftwareUrl,
                    'software'  => $providedForSoftwares,
                ],
            ];
            $data['users'][] = $item;

            // CSV data
            if ($reviewNumber === 0) {
                $reviewNumber = '';
            }
            if ($usedSoftwareNumber === 0) {
                $usedSoftwareNumber = '';
            }
            if ($providedForSoftwareNumber === 0) {
                $providedForSoftwareNumber = '';
            }
            if ($taxonomiesSoftwareNumber === 0) {
                $taxonomiesSoftwareNumber = '';
            }
            $csvData .= ''.  $user->id                  .';';
            $csvData .= '"'. $user->user_type->cd       .'";';
            $csvData .= '"'. $user->username            .'";';
            $csvData .= '' . $user->created             .';'; // Cake\I18n\Time->__toString()
            $csvData .= '' . $lastUserUpdate            .';'; // Cake\I18n\Time->__toString()
            $csvData .= ''. $reviewNumber               .';';
            $csvData .= ''. $usedSoftwareNumber         .';';
            $csvData .= ''. $taxonomiesSoftwareNumber   .';';
            $csvData .= ''. $providedForSoftwareNumber  .';';
            $csvData .= '' . $lastReview                .';'; // Cake\I18n\Time->__toString()
            $csvData .= '' . $lastUsedSoftware          .';'; // Cake\I18n\Time->__toString()
            $csvData .= '' . $lastTaxonomiesSoftware    .';'; // Cake\I18n\Time->__toString()
            $csvData .= '' . $lastProvidedForSoftware   .';'; // Cake\I18n\Time->__toString()
            $csvData .= "\n";
        }
        $csvHead = '';
        $csvHead .= "\"id\";";
        $csvHead .= "\"type\";";
        $csvHead .= "\"name\";";
        $csvHead .= "\"created\";";
        $csvHead .= "\"modified\";";
        $csvHead .= "\"reviews\";";
        $csvHead .= "\"using software\";";
        $csvHead .= "\"classified software\";";
        $csvHead .= "\"providing software\";";
        $csvHead .= "\"last review\";";
        $csvHead .= "\"last using software\";";
        $csvHead .= "\"last classified software\";";
        $csvHead .= "\"last providing software\";";
        $csvHead .= "\n";
        $csv = $csvHead . $csvData;

        // Clean export directory
        $folder = new Folder('public/export/users/temporary');
        $folder->delete();

        // Create a new temporary directory
        $aleaBytes = \openssl_random_pseudo_bytes(40);
        $aleaHex   = bin2hex($aleaBytes);
        $exportPath = "public/export/users/temporary/tmp_$aleaHex/" ;
        \mkdir("$exportPath", 0744, true);

        // Save export files
        $exportDate = date('Y.m.d_H\hi.s');
        $json = \json_encode($data, JSON_PRETTY_PRINT);
        $exportFileName = "comptoir-du-libre_users_$exportDate";
        $exportJsonFileName = "$exportFileName.json";
        $exportCsvFileName = "$exportFileName.csv";
        $exportJsonFilePath = $exportPath . $exportJsonFileName;
        $exportCsvFilePath = $exportPath  . $exportCsvFileName;
        if (is_dir(WWW_ROOT . $exportPath)) {
            file_put_contents(WWW_ROOT . $exportJsonFilePath, "$json");
            file_put_contents(WWW_ROOT . $exportCsvFilePath, "$csv");
            $this->set('exportJsonFileName', "$exportJsonFileName");
            $this->set('exportCsvFileName', "$exportCsvFileName");
            $this->set('exportJsonFilePath', "$exportJsonFilePath");
            $this->set('exportCsvFilePath', "$exportCsvFilePath");
            $this->set('exportJson', file_get_contents(WWW_ROOT . $exportJsonFilePath));
            $this->set('exportCsv', file_get_contents(WWW_ROOT . $exportCsvFilePath));
        }
    }




    /**
     * View method
     *
     * @param string|null $id User id.
     *
     * @return void
     * @throws NotFoundException When record not found.
     *
     */
    public function view($id = null)
    {
        $user = $this->Users->get(
            $id,
            [
                'contain' => [
                    'UserTypes',
                    'Reviews' => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Reviews.created' => 'DESC']);
                        },
                        'Softwares' => [
                            'fields' => [
                                'id',
                                'softwarename',
                                'logo_directory',
                                'photo',
                            ]
                        ]
                    ],
                    'Usedsoftwares' => [
                        'strategy' => 'select',
                        'queryBuilder' => function (Query $q) {
                            return $q->order(['Softwares.softwarename' => 'ASC']);
                        },
                        'Softwares' => [
                            'Reviews' => [
                            ],
                            'fields' => [
                                'id',
                                'softwarename',
                                'logo_directory',
                                'photo',
                                'description',
                            ]
                        ]
                    ],
                    "Providerforsoftwares" => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Softwares.softwarename' => 'ASC']);
                        },
                        'Softwares' => [
                            'Reviews' => [
                            ],
                            'fields' => [
                                'id',
                                'softwarename',
                                'logo_directory',
                                'photo',
                                'description'
                            ]
                        ],
                    ],
                ]
            ]
        );

        // Check that the current URL is correct
        $lang = $this->selectedLanguage;
        $allowedUrl = "/$lang/users/". (int) $id;
        if ($allowedUrl !== $this->request->here(false) && !$this->request->is('json')) {
            return $this->redirect("$allowedUrl", 301);
        }

        // Load mapping data if available for current user and populate view data
        $this->commonMappingForUser($user);

        //For Social MEDIAS => OPENGRAPH
        $openGraph = [
            "title" => $user->username,
            "description" => $user->description,
            "image" => Configure::read('App.fullBaseUrl') . DS . $user->logo_directory . DS . $user->photo,
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $card = [
            "title" => $user->username,
            "description" => $user->description,
            "image" => Configure::read('App.fullBaseUrl') . DS . $user->logo_directory . DS . $user->photo,
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $this->set('openGraph', $openGraph);
        $this->set('card', $card);

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $user->username,
            'url' => "users/".$user->id
        ];
        $this->setBreadcrumbsUser($links, $user);
    }

    /**
     * Load mapping data if available for given user ID and populate view data
     * @param User $user
     */
    protected function commonMappingForUser(User $user)
    {
        $data = $this->getMappingForUser($user);
        $taxonomiesSoftware = $data['taxonomiesSoftware'];
        $usedSoftwareKeys = $data['usedSoftwareKeys'];
        $mappingFirstLevels = $data['mappingFirstLevels'];
        $mappingTaxons = $data['mappingTaxons'];

        // Populate view data
        $this->set(compact(['mappingFirstLevels']));
        $this->set(compact(['mappingTaxons']));
        $this->set(compact(['usedSoftwareKeys']));
        $this->set(compact(['taxonomiesSoftware']));
        $this->set('_serialize', [
            'taxonomiesSoftware',
            'usedSoftwareKeys',
            'mappingTaxons',
            'mappingFirstLevels'
        ]);
    }


    /**
     * Load mapping data if available for given user ID
     * and return a array of all software declared by user for one or multiple taxon
     *
     * @param User $user
     * @return array   example: [
     *                   'LibreOffice' => ['softwareName' => 'LibreOffice',
     *                                     'softwareId'   => 33,
     *                                     'modified'     => '2022-06-01T11:05:53+00:00',
     *                                     'mappingTaxons' => [['slug' => 'bureautique', 'title' => 'Bureautique],]]
     */
    protected function getSofwareMappingForUser(User $user)
    {
        $mappingData = $this->getMappingForUser($user);
        $softwareDeclaredInTaxonomy = [];
        if (count($mappingData['taxonomiesSoftware']) > 0) {
            foreach ($mappingData['taxonomiesSoftware'] as $taxon) {
                foreach ($taxon as $softwareName => $taxonData) {
                    $taxonomyId = $taxonData->taxonomy_id;
                    $mappingTaxons = $mappingData['mappingTaxons'][$taxonomyId];
                    $softwareDeclaredInTaxonomy[$softwareName]['softwareName'] = $softwareName;
                    $softwareDeclaredInTaxonomy[$softwareName]['softwareId'] = $taxonData->software_id;
                    $softwareDeclaredInTaxonomy[$softwareName]['mappingTaxons'][$taxonomyId] =$mappingTaxons;
                    $modified = $taxonData['modified']; // Cake\I18n\Time
                    if (!isset($softwareDeclaredInTaxonomy[$softwareName]['modified'])) {
                        $softwareDeclaredInTaxonomy[$softwareName]['modified'] = $modified; // Cake\I18n\Time
                    } elseif ($softwareDeclaredInTaxonomy[$softwareName]['modified']  < $modified) {
                        $softwareDeclaredInTaxonomy[$softwareName]['modified'] = $modified; // Cake\I18n\Time
                    }
                }
            }
        }
        return $softwareDeclaredInTaxonomy;
    }


    /**
     * Load mapping data if available for given user ID
     *
     * @param User $user
     * @return array
     */
    protected function getMappingForUser(User $user)
    {
        $taxonomiesSoftware = [];
        $usedSoftwareKeys = [];
        $mappingFirstLevels = [];
        $mappingTaxons = [];

        $userId = $user->id;
        if ($user->isAdministrationType() && count($user->usedsoftwares) > 0) {
            // Get taxonomy records grouped by taxon ID for this user
            $this->loadModel("TaxonomysSoftwares");
            $taxonomiesSoftware = $this->TaxonomysSoftwares->getListByUserId($userId);

            // Load mapping data if necessary
            $mappingFirstLevels = [];
            $mappingTaxons = [];
            if (count($taxonomiesSoftware) > 0) {
                // Load mapping data
                $taxonIds = array_keys($taxonomiesSoftware);
                $mappingFirstLevels = $this->getMappingFirstLevels($this->selectedLanguage);
                $mappingTaxons = $this->getMappingTaxonsWithTaxonIdsFilter($taxonIds, $this->selectedLanguage);

                // Extract used software keys and software names
                $softwareNameList = [];
                foreach ($user->usedsoftwares as $usedSoftwareKey => $usedSoftware) {
                    $softwareId = $usedSoftware->software->id;
                    $softwareNameList[$softwareId] = $usedSoftware->software->softwarename;
                    $usedSoftwareKeys[$softwareId] = $usedSoftwareKey;
                }

                // Sort records by software name
                foreach ($taxonomiesSoftware as $taxonId => $taxonData) {
                    $taxonDataSorted = [];
                    foreach ($taxonData as $item) {
                        $softwareId = $item->software_id;
                        $softwareName = $softwareNameList[$softwareId];
                        $taxonDataSorted[$softwareName] = $item;
                    }
                    ksort($taxonDataSorted);
                    $taxonomiesSoftware[$taxonId] = $taxonDataSorted;
                }
            }
        }
        return [
            'taxonomiesSoftware' => $taxonomiesSoftware,
            'usedSoftwareKeys' => $usedSoftwareKeys,
            'mappingFirstLevels' => $mappingFirstLevels,
            'mappingTaxons' => $mappingTaxons,
        ];
    }



    /**
     * Tooling for "add" method: create anti-spam tokens
     * - create anti-spam tokens (get, post, form)
     * - save tokens in user session
     * - return token for HTTP GET method
     *
     * @param false $cleanBeforeCreateTokens
     * @return string token for HTTP GET method
     */
    private function addUserCreateTokens($cleanBeforeCreateTokens = false)
    {
        $prefixToken = bin2hex(openssl_random_pseudo_bytes(12));
        $getSuffixToken = bin2hex(openssl_random_pseudo_bytes(12));
        $postSuffixToken = bin2hex(openssl_random_pseudo_bytes(12));
        $formSuffixToken = bin2hex(openssl_random_pseudo_bytes(12));

        $dataToken = $this->request->session()->read('CreateUserAntiSpam');
        if (is_null($dataToken) | $cleanBeforeCreateTokens === true) {
            $dataToken = [];
        }
        $dataToken["$prefixToken"] = [
            "date"     => time(),
            "getToken" => "$prefixToken-$getSuffixToken",
            "postToken" => "$prefixToken-$postSuffixToken",
            "formToken" => "$prefixToken-$formSuffixToken",
        ];
        $this->request->session()->write('CreateUserAntiSpam', $dataToken);
        return "$prefixToken-$getSuffixToken";
    }

    /**
     * Tooling for "add" method: get current anti-spam token based on current HTTP method
     * @return string
     */
    private function addUserGetCurrentToken()
    {
        $method = strtolower($this->request->method());
        if ($method === 'get') {
            return strip_tags($this->request->query('t1'));
        } elseif ($method === 'post') {
            return strip_tags($this->request->query('t2'));
        }
        return ''; // wrong HTTP method
    }

    /**
     * Tooling for "add" method: check if current anti-spam token in URL is valid
     * @return bool
     */
    private function addUserIsValidTokenInUrl()
    {
        $token = $this->addUserGetCurrentToken();
        if (empty($token)) {
            return false; // wrong HTTP method or token parameter in the URL does not exist
        } else {
            $method = strtolower($this->request->method());
            $prefixToken = $this->addUserGetPrefixToken($token);
            $availableTokens = $this->request->session()->read('CreateUserAntiSpam');
            if (!isset($availableTokens[$prefixToken])) {
                return false;
            } elseif ($availableTokens[$prefixToken][$method .'Token'] !== $token) {
                return false;
            }
        }
        return true;
    }

    /**
     * Tooling for "add" method: get anti-spam prefix token
     *
     * @param string $token
     * @return mixed|string
     */
    private function addUserGetPrefixToken($token)
    {
        $tokenData = explode('-', $token);
        return $tokenData[0];
    }

    /**
     * Tooling for "add" method: get current token by type ('get', 'post' or 'form')
     *
     * @param string $type  'get', 'post' or 'form'
     * @return string
     */
    private function addUserGetTokenByType(string $type)
    {
        $prefixToken = $this->addUserGetPrefixToken($this->addUserGetCurrentToken());
        $availableTokens = $this->request->session()->read('CreateUserAntiSpam');
        $token = '';
        if (isset($availableTokens["$prefixToken"][$type.'Token'])) {
            $token = $availableTokens["$prefixToken"][$type.'Token'];
        }
        return $token;
    }

    /**
     * Tooling for "add" method: check if form is sent too quicky
     * @return bool
     */
    private function addUserIsFormSentTooQuickly()
    {
        $prefixToken = $this->addUserGetPrefixToken($this->addUserGetCurrentToken());
        $availableTokens = $this->request->session()->read('CreateUserAntiSpam');
        if (isset($availableTokens["$prefixToken"])) {
            $tokenTimestamp = $availableTokens["$prefixToken"]['date'];
            $diffTimestamp = time() - $tokenTimestamp;
            if ($diffTimestamp >= Configure::read('MIN_TIME_TO_SEND_SIGNUP_FORM')) {
                return false;
            }
        }
        return true;
    }


   /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $message = "";
        $lang = $this->selectedLanguage;

        // If user is already logged in, redirects them to their profile page
        if (!is_null($this->Auth->user('id'))) {
            return $this->redirect("/$lang/users/". $this->Auth->user('id'));
        }

        if (!empty($this->request->data)) {
            $user = $this->Users->newEntity($this->request->data);
        } else {
            $user = $this->Users->newEntity();
        }

        // Force token-based URL (HTTP GET and POST methods), expect for json API
        if (!$this->request->is('json')) {
            if ($this->request->method() === 'GET' && count($this->request->query) === 0) { // default page: /users/add
                return $this->redirect("/$lang/users/add?t1=". $this->addUserCreateTokens(false));
            } elseif ($this->addUserIsValidTokenInUrl() === false) {
                return $this->redirect("/$lang/users/add?t1=". $this->addUserCreateTokens(true));
            }
        }

        // Process form data
        if ($this->request->is('post')) {
            // Check that it's not a robot (expected token and speed submit), expect for json API
            if (!$this->request->is('json')) {
                $expectedFormToken = $this->addUserGetTokenByType('form');
                $formToken = $this->request->data("formToken");
                if ($expectedFormToken !== $formToken | $this->addUserIsFormSentTooQuickly()) {
                    return $this->redirect("/$lang/users/add?t1=". $this->addUserCreateTokens(true));
                }
            }

            // do not allow to upload an avatar when creating an account
            $this->request->data['photo'] = null;

           // the "role" field needs a different treatment in production
           // to avoid that a user can be added with an "admin" role
           // via the form or the json API.
            if (Configure::read('debug') !== true) {
                // in production:
                // the role of a new user must always
                // be "User" (via the form or the json API)
                $this->request->data['role'] = 'User';
            } else {
                // If debug mode is enabled,
                // we must allow the addition of a user via the form
                // by adding the missing field "role" with the value "user".
                if (isset($this->request->data['submit_signUpForm'])) {
                    $this->request->data['role'] = 'User';
                }
                // The "else" part of this "if"
                // concerns the JSON interrogation (used only by unit test), and thus is not relevant
            }

            $user = $this->Users->patchEntity($user, $this->request->data);

            if ($this->Users->save($user)) {
                $message = "Success";
                $this->Flash->success(__d("Forms", "Your are registred on the Comptoir du Libre, welcome !"));
                $this->request->session()->write('CreateUserAntiSpam', []); // Unset tokens
                if (!$this->request->is('json')) {
                    $currentUser = $this->Auth->identify();
                    $currentUser["user_type"] = $this->Users
                        ->UserTypes
                        ->get($currentUser["user_type_id"])->get("name");
                    $this->Auth->setUser($currentUser);

                    // REDIRECTS TO ---> /<lang>/users/<idUser>
                    return $this->redirect("/$lang/users/" . $user->get('id'));
                    // REDIRECTS TO ---> /api/v1/users/view/<idUser>
                    // return $this->redirect(['action' => 'view', $user->get('id')]);
                }
            } else {
                $message = "Error";
                $this->Flash->error(__d("Forms", "Your registration failed, please follow rules in red."));
            }
            $message == "Error" ? $this->set('errors', $user->errors()) : null;
        }
        $this->ValidationRules->config('tableRegistry', "Users");
        $rules = $this->ValidationRules->get();



        $tokenForForm = $this->addUserGetTokenByType('form');
        $formUrl = "/$lang/users/add?t2=" . $this->addUserGetTokenByType('post');
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        if (!$this->request->is('json')) {
            $this->set(compact(['formUrl']));
            $this->set(compact(['tokenForForm']));
        }
        $this->set(compact('user', 'userTypes', 'rules', 'message'));
        $this->set('_serialize', ['user', 'userTypes', 'rules', 'message', 'errors']);

        // Breadcrumbs
        $links[] = [
            'name' => __d('Breadcrumbs', 'Users.SignUp'),
            'url' => "users/add"
        ];
        parent::setBreadcrumbs($links);
    }

    /**
     * Allow a user to request a password reset. Sends an email to the user containing a link to change his password.
     *
     * @param string $token Token generated.
     *
     * @return void Redirects on login page or , plus successful message.
     */
    public function forgotPassword($token = null)
    {
        $user = $this->Users->newEntity();
        $message = "";
        if (!$token) {
            if (!empty($this->request->data)) {
                $user = $this->Users->find('byEmail', ['user' => $this->request->data["email"]])->first();

                if (!$user) {
                    $this->Flash->error(__d("Forms", 'Sorry, the email entered was not found.'));
                    $message = __d("Forms", 'Sorry, the email entered was not found.');
                } else {
                    $user = $this->generatePasswordToken($user);

                    if ($this->Users->save($user)
                        && $this->getMailer("User")->send("resetPassword", ["user" => $user])
                    ) {
                        $this->Flash->success(__d("Forms", 'password.reset.instructions.have.been.sent'));
                        $this->redirect(["prefix" => false, 'controller' => "users", "action" => "login"]);
                    }
                }
            }
        }
        $this->set(compact('message', 'user'));
        $this->set('_serialize', ['message', 'user']);
        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => __d('Breadcrumbs', 'Users.Login'),
            'url' => "users/login"
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Users.ForgotPassword'),
            'url' => "users/forgot-password"
        ];
        parent::setBreadcrumbs($links);
    }

    /**
     * Generate a unique hash / token.
     *
     * @param Object $user User
     *
     * @return Object User
     */
    private function generatePasswordToken($user)
    {
        // Generate an 'token'
        $user->token_plain
            = Text::uuid();
        $user->token = $user->token_plain;
        return $user;
    }

    /**
     * Unregistered user can change their password if they forgot it thanks to their email.
     *
     * @param null $token Token value.
     *
     * @return void Redirects on successful, renders view otherwise.
     */
    public function resetPassword($token = null)
    {
        $lang = $this->selectedLanguage;
        $message = "";
        $result = $this->changePasswordByToken($token);
        $result ? $message = $result["message"] : $message = "Error";
        $user = $result["user"];

        if ($message === 'Error_notValidToken') {
            $this->Flash->error(__d("Forms", "forgotPassord.tokenNotValid"));
            return $this->redirect("/$lang/users/forgot-password", 302);
        } elseif ($message === 'Success') {
            $this->Flash->success(__d("Forms", "Your password has been changed."));
            return $this->redirect("/$lang/users/login", 302);
        } else {
            $this->ValidationRules->config('tableRegistry', "Users");
            $rules = $this->ValidationRules->get();
            $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
            $this->set(compact('user', 'userTypes', 'rules', 'message'));
            $this->set('_serialize', ['user', 'userTypes', 'rules', 'message']);
        }
    }

    /**
     * Allow a user to change his password using a token
     *
     * @param string $token A unique token given via email.
     *
     * @return array
     */
    private function changePasswordByToken($token)
    {
        $message = '';
        $user = $this->Users->newEntity();
        $userByToken = $this->Users->findByToken($token, ["contain" => []])->first();
        $tokenValidityInHours = self::TOKEN_VALIDITY_TIME_TO_CHANGE_PASSWORD;
        if (is_null($userByToken)) {
            // This token does not exist in the database.
            $message = "Error_notValidToken";
        } elseif (false === $userByToken->modified->wasWithinLast("$tokenValidityInHours hours")) {
            // This token has expired (based on the user's modification date in the database)
            $user = $this->Users->patchEntity($userByToken, ['token' => null]);
            $this->Users->save($user);
            $message = "Error_notValidToken";
        } else {
            $this->viewBuilder()->template("reset_password");
            if ($this->request->is('POST') && $userByToken) {
                $user = $this->Users->patchEntity(
                    $userByToken,
                    [
                        'password' => $this->request->data['new_password'],
                        'confirm_password' => $this->request->data['confirm_password'],
                        'token' => null,
                    ]
                );
                if ($this->Users->save($user)) {
                    $message = "Success";
                } else {
                    $message = "Error";
                    $this->Flash->error(__d("Forms", "Your password can not be changed, please follow rules in red."));
                }
            }
        }
        return ["message" => $message, "user" => $user];
    }

    /**
     * Change the password for a current user logged.
     *
     * @param null $id Id of current user.
     *
     * @return void Redirects on successful edit, renders view otherwise.
     */
    public function changePassword($id = null)
    {
        $message = "";
        $user = $this->Users->get($id);
        $result = $this->changePasswordById($user);
        $user = $result["user"];
        $result ? $message = $result["message"] : $message = "Error";

        $message == "Success" ? $this->redirect(
            [
                "prefix" => false,
                "controller" => "Users",
                "action" => "edit",
                "language" => $this->request->param("language"),
                $user->id
            ]
        ) : null;


        $this->ValidationRules->config('tableRegistry', "Users");
        $rules = $this->ValidationRules->get();
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userTypes', 'rules', 'message'));
        $this->set('_serialize', ['user', 'userTypes', 'rules', 'message']);
    }

    /**
     * Change the password of a current user logged.
     *
     * @param array $user Current user.
     *
     * @return array Returns an user entity or errors regarding the user entity, plus a message (Success or Error )
     */
    private function changePasswordById($user)
    {
        $message = "";
        if ($this->request->is(['put', 'post'])) {
            if (!empty($this->request->data) && $user) {
                $user = $this->Users->patchEntity(
                    $user,
                    [
                        'old_password' => $this->request->data['old_password'],
                        'password' => $this->request->data['new_password'],
                        'confirm_password' => $this->request->data['confirm_password']
                    ],
                    ['validate' => 'password']
                );
                if ($this->Users->save($user)) {
                    $message = "Success";
                    $this->Flash->success(__d("Forms", "Your password has been changed."));
                } else {
                    $message = "Error";
                    $this->Flash->error(__d("Forms", "Your password can not be changed, please follow rules in red."));
                }
            }
        }
        return ["message" => $message, "user" => $user];
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     *
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     *
     */
    public function edit($id = null)
    {
        $user = $this->Users->get(
            $id,
            [
                'contain' => ['UserTypes']
            ]
        );

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            if (isset($data['role'])) {
                unset($data['role']);
            }
            $user = $this->Users->patchEntity($user, $data);
            if ($this->request->is('json')) {
                if ($this->Users->save($user)) {
                    $message = "Success";
                } else {
                    $message = "Error";
                    $this->set('errors', $user->errors());
                }
            } else {
                if ($this->Users->save($user)) {
                    // update username in session
                    $session = $this->request->session();
                    $session->write('Auth.User.username', $user->username);

                    $this->Flash->success(__d("Forms", "Edit.User.profile.success"));

                    // We would have written:
                    //   return $this->redirect(['action' => 'view', $user->get('id')]);
                    // but this generates the following URL
                    //   /api/v1/users/view/<idUser>
                    // and we want to have the following one:
                    //   /users/<idUser>
                    // so to write thie following statement:
                    return $this->redirect("users/" . $user->get('id'));
                } else {
                    $message = "Error";

                    $this->Flash->error(__d("Forms", "Edit.User.profile.error"));
                }
            }
        }

        $this->ValidationRules->config('tableRegistry', "Users");
        $rules = $this->ValidationRules->get();
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userTypes', 'rules', 'message'));
        $this->set('_serialize', ['user', 'userTypes', 'rules', 'message', 'errors']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $user->username,
            'url' => "users/".$user->id
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'User.edit'),
            'url' => "users/edit/".$user->id
        ];
        $this->setBreadcrumbsUser($links, $user);
    }

    /**
     * Display a list of user who are only service providers.
     *
     * @return void Redirects on successful edit, renders view otherwise.
     */
    public function providers()
    {
        $this->viewBuilder()->template("users_list");
        $this->set("title", "List of service providers");

        try {
            $users = $this->Users->find("all")->contain(["UserTypes"])
                ->where(["UserTypes.cd = " => "Company"]);
            $this->set(
                [
                    'message' => "Success",
                    'users' => $this->paginate($users),
                    '_serialize' => ['message', 'users']
                ]
            );
        } catch (Exception $e) {
        }

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => __d('Breadcrumbs', 'Users.Providers'),
            'url' => "users/providers"
        ];
        parent::setBreadcrumbs($links);
    }

    /**
     * Return an user with all used softwares
     *
     * @param string|null $id User id.
     *
     * @return void
     */
    public function usedSoftwares($id = null)
    {
        $user = $this->Users->get(
            $id,
            [
                "contain" =>
                    [
                        'Usedsoftwares' =>
                            [
                                'strategy' => 'select',
                                'queryBuilder' => function ($q) {
                                    return $q->order(['Softwares.softwarename' => 'ASC']);
                                },
                                'Softwares' =>
                                    [
                                        'Reviews' => [
                                        ],
                                        'fields' => [
                                            'id',
                                            'softwarename',
                                            'logo_directory',
                                            'photo',
                                            'description'
                                        ]
                                    ]
                            ],
                        'UserTypes',
                    ]
            ]
        );

        // Check that the current URL is correct
        $lang = $this->selectedLanguage;
        $allowedUrl = "/$lang/users/usedSoftwares/". (int) $id;
        if ($allowedUrl !== $this->request->here(false)) {
            return $this->redirect("$allowedUrl", 301);
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $user->username,
            'url' => "users/".$user->id
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Users.UsedSoftwares'),
            'url' => "users/usedSoftwares/".$user->id
        ];
        $this->setBreadcrumbs($links);
    }


    /**
     * Returns a user with a list of software as know as services provider
     *
     * @param string|null $id User id.
     *
     * @return void
     */
    public function providerforSoftwares($id = null)
    {
        $user = $this->Users->get(
            $id,
            [
                "contain" =>
                    [
                        'UserTypes',
                        'Providerforsoftwares' =>
                            [
                                'strategy' => 'select',
                                'queryBuilder' => function ($q) {
                                    return $q->order(['Softwares.softwarename' => 'ASC']);
                                },
                                'Softwares' => [
                                    'Reviews' => [
                                    ],
                                    'fields' => [
                                        'id',
                                        'softwarename',
                                        'logo_directory',
                                        'photo',
                                        'description'
                                    ]
                                ]
                            ]
                    ]
            ]
        );

        // Check that the current URL is correct
        $lang = $this->selectedLanguage;
        $allowedUrl = "/$lang/users/providerforSoftwares/". (int) $id;
        if ($allowedUrl !== $this->request->here(false)) {
            return $this->redirect("$allowedUrl", 301);
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $user->username,
            'url' => "users/".$user->id
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Users.providerforSoftwares'),
            'url' => "users/providerforSoftwares/".$user->id
        ];
        $this->setBreadcrumbsUser($links, $user);
    }



    /**
     * load a user with a list of his/her reviews
     *
     * @param string|null $id User id.     *
     * @return void
     */
    public function reviewsforSoftware($id = null)
    {
        $user = $this->Users->get(
            $id,
            [
                "contain" =>
                    [
                        'UserTypes',
                        'Reviews' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Reviews.created' => 'DESC']);
                            },
                            'Softwares' => [
                                'fields' => [
                                    'id',
                                    'softwarename',
                                    'logo_directory',
                                    'photo',
                                ]
                            ]
                        ],
                    ]
            ]
        );
        $id   = $user->id;

        // Check that the current URL is correct
        $lang = $this->selectedLanguage;
        $allowedUrl = "/$lang/users/$id/reviews";
        if ($allowedUrl !== $this->request->here(false)) {
            return $this->redirect("$allowedUrl", 301);
        }

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $user->username,
            'url' => "users/".$user->id
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Users.Reviews'),
            'url' => "users/$id/reviews"
        ];
        $this->setBreadcrumbs($links);


        $reviewsNumber = count($user->reviews);
        if ($reviewsNumber === 0) {
            $lang = $this->selectedLanguage;
            $this->redirect("$lang/users/$id", 301);
        } else {
            $this->set(compact('user'));
            $this->set('_serialize', ['user']);
        }
    }


    /**
     * Delete method
     *
     * @param string|null $id User id
     *
     * @return Response|null Redirects to index.
     * @throws NotFoundException When record not found.
     *
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * To contact another user of le Comptoir by sending an email.
     *
     * @param string|null $id Id of user to contact.
     *
     * @return void Flash message success|error
     */
    public function contact($id)
    {
        // provide the current user to the wiew (to use his email address in the form)
        if (is_null($this->Auth->user('id'))) {
            $currentUser = $this->Users->newEntity();
        } else {
            $currentUser = $this->Users->get($this->Auth->user('id'));
        }
        $user = $this->Users->get($id, ["fields" => ["id", "email", "username"]]);
        if (!$this->request->is('json') && $this->request->is('post') && !empty($this->request->data)) {
            $mailData = array();
            foreach ($this->request->data as $key => $value) {
                $mailData [$key] = strip_tags($value); // HTML tags are not allowed in mail
                $this->request->data[$key] = htmlspecialchars(strip_tags($value)); // prevent XSS injection into form
            }

            $event = new Event('Model.User.contact', $this->Users->newEntity(), $mailData);
            $event->data["recipient"] = $user->email;
            if ($this->eventManager()->dispatch($event)) {
                $logFrom = "[ ". $this->Auth->user('id') .' ] ' . $this->Auth->user('username');
                if (is_null($this->Auth->user('id'))) {
                    $logFrom = '(anonymous user) ' . $event->data['email'] ;
                }
                $logMsg = "MAIL From: $logFrom -->  To: [ " . $user->id .' ] ' . $user->username ;
                $this->log($logMsg, 'info');
                $this->Flash->success(__d("Forms", "Users.contact.success"));
            } else {
                $this->Flash->error(__d("Forms", "Users.contact.error"));
            }
        }

        $this->set(compact(['currentUser', 'user']));
        $this->set('_serialize', ['user', 'currentUser']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $user->username,
            'url' => "users/".$id
        ];
        $links[] = [
            'name' => __d('Breadcrumbs', 'Users.ContactUser'),
            'url' => "users/contact/".$id
        ];
        $this->setBreadcrumbs($links);
    }

    /**
     * To log out a registered user.
     *
     * @return void
     */
    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    /**
     * To log in a user thanks.
     *
     * @return Response|null
     */
    public function login()
    {
        $lang = $this->selectedLanguage;

        // If user is already logged in, redirects them to their profile page
        if (!is_null($this->Auth->user('id'))) {
            return $this->redirect("/$lang/users/". $this->Auth->user('id'));
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $user["user_type"] = $this->Users->UserTypes->get($user["user_type_id"])->get("name");
                $this->Auth->setUser($user);
                $this->Flash->success(__d("Forms", "You are logged"));
                return $this->redirect($this->Auth->redirectUrl("/$lang/"));
            }
            $this->Flash->error(__d("Forms", "You are not logged"));
        }

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => __d('Breadcrumbs', 'Users.Login'),
            'url' => "users/login"
        ];
        parent::setBreadcrumbs($links);
    }
}
