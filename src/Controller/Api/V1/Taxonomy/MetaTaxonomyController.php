<?php
namespace App\Controller\Api\V1\Taxonomy;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * Parent class for CommonTaxonomy*Controller classes
 *
 * Implement some magical CakePHP methods, that should not be overwritten:
 * - beforeFilter() to deny access to an anonymous user
 * - isAuthorized() to manage all rights for the controllers' actions,
 *                  with the exception of anonymous users.
 *
 * Implement following method:
 * setBreadcrumbs()
 * getBaseUrl()     returns the base URL of the mapping based on the user's language
 * isEditor()       business rules that define if current user is editor
 *
 */
class MetaTaxonomyController extends AppController
{
    /**
     * Mapping first levels
     * @var array
     */
    private $mappingFirstLevels = [];

    /**
     * Mapping data
     * @var array
     */
    private $mappingTaxons = [];

    /**
     * Actions allowed only to admin
     * @var array
     */
    protected $actionsAllowedOnlyToAdmin = [];

    /**
     * Actions allowed only to editor (connected user with business rules + admin)
     * @var array
     */
    protected $actionsAllowedOnlyToEditor = [];


    /**
     * Actions allowed only for connected user (+ admin + editor)
     * @var array
     */
    protected $actionsAllowedOnlyToConnectedUser = [];

    /**
     * Actions allowed to everyone
     * @var array
     */
    protected $actionsAllowedToEveryone = [];



    /**
     * Before filter (a magical CakePHP method)
     *
     * Called before the controller action.
     * - used to deny access to an anonymous user
     * - used to stop the JSON requests (except for actions allowed to admin)
     *
     *      notice:
     *      $this->Auth->user('id'|'role') are null at this step
     *
     * @uses parent::beforeFilter()
     * @param Event $event
     * @return Response|null|void
     * @throws NotFoundException  if it's JSON request (except for actions allowed to admin)
     */
    final public function beforeFilter(Event $event)
    {
        // not provide JSON requests (except for actions allowed to admin)
        $action = $this->request->param('action');
        if ($this->request->is('json') && !in_array($action, $this->actionsAllowedOnlyToAdmin)) {
            $event->stopPropagation();
            throw new NotFoundException();
        }

        // Call the parent method
        parent::beforeFilter($event); // required to call $this->setLocale() and $this->setOpenGraph();

        // Allow access to an anonymous user
        // for actions allowed to everyone
        $this->Auth->allow($this->actionsAllowedToEveryone); // add all items from the allowed list

        // Deny access to an anonymous user
        // for actions allowed only to admin, editor and connected user
        $this->Auth->deny(array_merge(
            $this->actionsAllowedOnlyToAdmin,
            $this->actionsAllowedOnlyToEditor,
            $this->actionsAllowedOnlyToConnectedUser
        )); // remove all items from the allowed list
    }

    /**
     * Is authorized (a magical CakePHP method)
     *
     * Manage all rights for the controllers' actions,
     * with the exception of anonymous users (managed by beforeFilter() method)
     *
     * Return true if the user can use the current action, false otherwise.
     * - Connected user with "admin" role can use all actions
     * - Connected user can use his dedicated actions and allowed actions to anonymous user
     * - Editor is allowed to use its dedicated actions in addition
     *   to the actions allowed to connected user and everyone.
     *   --> see isEditor() method to understand which connected users are editors.
     *
     *      notice: $forceDeny parameter is mandatory
     *              to be compatible with parent::isAuthorized()
     *              but not used in this class
     *
     * @param array $user A user.
     * @param  boolean $forceDeny by default FALSE, set TRUE to force the deny on parent::isAuthorized()
     * @return boolean
     */
    final public function isAuthorized($user, $forceDeny = false)
    {
        $action = $this->request->param('action');
        if (isset($user['role']) && $user['role'] === 'admin') {
            // The administrator role can use every action,
            // except for actions dedicated to editors.
            // In this case, it must also be an editor.
            if (in_array($action, $this->actionsAllowedOnlyToEditor)) {
                if ($this->isEditor()) {
                    return true;
                }
            } else {
                return true;
            }
        } elseif (isset($user['role']) && $user['role'] === 'User') {
            $userActions = array_merge(
                $this->actionsAllowedOnlyToConnectedUser,
                $this->actionsAllowedToEveryone
            );
            if (in_array($action, $userActions)) {
                // Connected user is allowed to use :
                // - his dedicated actions
                // - actions allowed to everyone
                return true;
            } elseif ($this->isEditor() && in_array($action, $this->actionsAllowedOnlyToEditor)) {
                // Editor is allowed to use its dedicated actions
                // in addition to the actions allowed to connected user and everyone.
                return true;
            }
        }
        // Other cases are not allowed
        $this->Flash->error(__('You are not allowed to do that.'));
        return false;
    }

    /**
     * Business rules that define if current user is editor
     *
     * @return bool  true if current user is editor, otherwise false
     */
    final protected function isEditor()
    {
        $action = $this->request->param('action');
        $user_type_id = $this->Auth->user('user_type_id');
                        // null -----> anonymous user
                        // 2 - Administration
                        // 4 - Person
                        // 5 - Company
                        // 6 - Association

        $allowedUserTypeId = $this->getUserTypeIdByName('Administration');
        if ($user_type_id === $allowedUserTypeId) { // Administration
            return true;
        }
        return false;
    }
}
