<?php
namespace App\Controller\Api\V1;

use App\Controller\Api\V1\Taxonomy\CommonTaxonomyController;
use App\Model\Table\TaxonomysTable;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
//use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;
use Cake\Utility\Text;

/**
 * Taxonomys Controller
 * - define actions allowed to editor
 * - define actions allowed to connected user
 * - define actions allowed to anonymous user
 * - implemente actions used by editor, connected user and anonymous user
 *
 * notice:
 * - actions dedicated to admin must be implemented in the parent class.
 * - beforeFilter() is defined in the parent class and should not be overwritten.
 * - isAuthorized() is defined in the parent class and should not be overwritten.
 *
 * @property TaxonomysTable $Taxonomys
 */
class TaxonomysController extends CommonTaxonomyController
{

    /**
     * Actions allowed only to editor (connected user + business rules)
     * @var array
     */
    protected $actionsAllowedOnlyToEditor = [];

    /**
     * Actions allowed only for connected user
     * @var array
     */
    protected $actionsAllowedOnlyToConnectedUser = [];

    /**
     * Actions allowed to everyone
     * @var array
     */
    protected $actionsAllowedToEveryone = [
        'mapping',
        'mappingPrimaryLevel',
        'mappingTaxon',
        'mappingTaxonUsersOf',
    ];


    /**
     * Root page of mapping
     *
     *      URL: /api/v1/taxonomys/mapping       ---> redirect to "/en/mapping/"
     *           /api/v1/taxonomys/mapping.json  ---> disable via parent::beforeFilter()
     *           /fr/cartographie
     *           /en/mapping/
     *
     * @return Response|null
     */
    public function mapping()
    {
          // Fabrice documentation ---> do not remove now!
//        $this->paginate = [
//            'limit' => Configure::read('LIMIT'),
//            'maxLimit' => Configure::read('LIMIT'),
//            'contain' => ['Softwares'],
//            'order' => ['name' => "ASC"]
//        ];

        // Check that the current URL is correct
        $currentUrl = $this->request->here(false);
        $allowedUrl = $this->getBaseUrl('mapping');
        if ($allowedUrl !== $currentUrl) {
            return $this->redirect("$allowedUrl", 301);
        }

        // Get data
        $mappingTaxons = $this->getMappingTaxons($this->selectedLanguage);
        $mappingHead = $this->getMappingFirstLevels($this->selectedLanguage);

        // Parameters for the view
        $this->set(compact('mappingTaxons'));
        $this->set(compact('mappingHead'));
        $this->set('_serialize', ['mappingTaxons']);
        $this->set('_serialize', ['mappingHead']);
        $this->setBreadcrumbs();
    }


    /**
     * Primary level page
     * - Get primaryId from the URL slug
     * - checks :
     *   - If the requested primary level does not exist, redirect to the root mapping.
     *   - If the current URL is not correct, redirect to the official URL.
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>
     *           /en/mapping/<slugPrimaryLevel>
     *           /api/v1/taxonomys/mappingPrimaryLevel/<id>       ---> disable ---> redirect to /en/mapping/
     *           /api/v1/taxonomys/mappingPrimaryLevel/<id>.json  ---> disable via parent::beforeFilter()
     *
     *      ex:  /en/mapping/business     /fr/cartographie/metiers
     *           /en/mapping/activities   /fr/cartographie/activites
     *           /en/mapping/generics     /fr/cartographie/generiques
     *
     * @param string|null $primaryId Taxonomy id.
     * @return Response|null
     */
    public function mappingPrimaryLevel($primaryId = null)
    {
        // Get data
        $mappingTaxons = $this->getMappingTaxons($this->selectedLanguage);
        $mappingHead = $this->getMappingFirstLevels($this->selectedLanguage);

        // Get the ID from the URL slug
        $primaryId = null;
        if (isset($this->request->params['primary_slug'])) {
            $requestPrimarySlug = $this->request->params['primary_slug'];
            foreach ($mappingHead as $id => $name) {
                $chekSlug = strtolower(Text::slug($name));
                if ($chekSlug === $requestPrimarySlug) {
                    $primaryId = $id;
                    $primaryName = $name;
                    $primarySlug = $requestPrimarySlug;
                    break;
                }
            }
        }

        // Some checks before display content
        if (is_null($primaryId)) {
            // if the requested primary level  does not exist, redirect to the root mapping.
            return $this->redirect($this->getBaseUrl('mapping'), 301);
        } else {
            // Check that the current URL is correct
            $currentUrl = $this->request->here(false);
            $allowedUrl = $this->getBaseUrl('mapping') ."$primarySlug/";
            if ($allowedUrl !== $currentUrl) {
                return $this->redirect("$allowedUrl", 301);
            }
        }

        // Parameters for the view
        $this->set('title', $primaryName);
        $this->set(compact('primaryId'));
        $this->set(compact('primarySlug'));
        $this->set(compact('mappingTaxons'));
        $this->set(compact('mappingHead'));
        $this->set('_serialize', ['mappingTaxons']);
        $this->set('_serialize', ['mappingHead']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $primaryName,
            'url'  => $this->getBaseUrl('mapping', false) ."$primarySlug/",
        ];
        $this->setBreadcrumbs($links);
    }


    /**
     * Taxpon page
     * - Get taxonId from the URL
     * - checks :
     *   - If the requested taxon does not exist, redirect to the root mapping.
     *   - If the requested taxon is a primary level, redirect it to the official URL.
     *   - If the current URL is not correct, redirect to the official URL.
     *
     *      URL: /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *           /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<taxonId>
     *           /api/v1/taxonomys/mappingTaxon/<id>       ---> disable ---> redirect to /en/mapping/
     *           /api/v1/taxonomys/mappingTaxon/<id>.json  ---> disable via parent::beforeFilter()
     *
     *      ex:  /fr/cartographie/metiers/sante-social/14      /en/mapping/business/healthcare-social/14
     *           /fr/cartographie/generiques/bureautique/20    /en/mapping/generics/office-automation/20
     *
     * @param string|null $taxonId Taxonomy id.
     * @return Response|null
     */
    public function mappingTaxon($taxonId = null)
    {
        // Get data
        $mappingTaxons = $this->getMappingTaxons($this->selectedLanguage);
        $mappingHead = $this->getMappingFirstLevels($this->selectedLanguage);

        // Get the ID from the URL
        $taxonId = null;
        $parent_id = null;
        if (isset($this->request->params['taxon_id'])) {
            $taxonId = $this->request->params['taxon_id'];
            if (isset($mappingTaxons[$taxonId])) {
                $taxonSlug  = $mappingTaxons[$taxonId]['slug'];
                if (isset($mappingTaxons[$taxonId]['id_parent'])) {
                    $parent_id  = $mappingTaxons[$taxonId]['id_parent'];
                    $primaryName = $mappingTaxons[$parent_id]['title'];
                    $primarySlug = $mappingTaxons[$parent_id]['slug'];
                }
            } else {
                $taxonId = null;
            }
        }

        // Some checks before display content
        if (is_null($taxonId)) {
            // if the requested taxon does not exist, redirect to the root mapping.
            return $this->redirect($this->getBaseUrl('mapping'), 301);
        } elseif (is_null($parent_id)) {
            // if the requested taxon is a primary level, redirect it to the official URL.
            return $this->redirect($this->getBaseUrl('mapping') ."$taxonSlug/", 301);
        } else {
            // Check that the current URL is correct
            $currentUrl = $this->request->here(false);
            $allowedUrl = $this->getBaseUrl('mapping') ."$primarySlug/$taxonSlug/$taxonId";
            if ($allowedUrl !== $currentUrl) {
                return $this->redirect("$allowedUrl", 301);
            }
        }

        // Get data
        $this->loadModel("TaxonomysSoftwares");
        $data = $this->TaxonomysSoftwares->getSoftwareListWithUsersFromTaxonomyId($taxonId);

        // Parameters for the view
        $baseTaxonUrl = "$primarySlug/$taxonSlug";
        $taxonName = $mappingTaxons[$taxonId]['title'];
        $this->set('taxonId', "$taxonId");
        $this->set('baseUrl', $this->getBaseUrl('mapping') ."$baseTaxonUrl");
        $this->set('title', "$taxonName");
        $this->set('data', $data);
        $this->set('_serialize', ['data']);

        // Breadcrumb
        $links = array();
        $links[] = [
            'name' => $primaryName,
            'url'  => $this->getBaseUrl('mapping', false) ."$primarySlug/",
        ];
        $links[] = [
            'name' => "$taxonName",
            'url'  =>  $this->getBaseUrl('mapping', false) ."$baseTaxonUrl/$taxonId",
        ];
        $this->setBreadcrumbs($links);
    }



    /**
     * Display the users of a software for a taxon
     * - Get IDs from the URL (taxon ID and software ID)
     * - Some checks before load data (the requested taxon does not exist, ...)
     * - Load data form database
     * - Check that the current URL is correct
     * - Set parameters for the view
     *
     *      URL: /api/v1/taxonomys/mappingTaxonUsersOf/<id>  ---> redirect to '/en/mapping/'
     *      URL: /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId>
     *           /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId>
     *
     *      ex:  /fr/cartographie/generiques/bureautique/libreoffice/20.33
     *           /en/mapping/generics/office-automation/libreoffice/20.33
     */
    public function mappingTaxonUsersOf()
    {
        // Get data
        $mappingTaxons = $this->getMappingTaxons($this->selectedLanguage);
        $mappingHead = $this->getMappingFirstLevels();

        // Get the IDs from the URL
        $taxonId = null;
        $softwareId = null;
        $parent_id = null;
        if (isset($this->request->params['taxon_id']) && isset($this->request->params['software_id'])) {
            $taxonId = (int) $this->request->params['taxon_id'];
            $softwareId = (int) $this->request->params['software_id'];
            if (isset($mappingTaxons[$taxonId])) {
                $taxonName  = $mappingTaxons[$taxonId]['title'];
                $taxonSlug  = $mappingTaxons[$taxonId]['slug'];
                if (isset($mappingTaxons[$taxonId]['id_parent'])) {
                    $parent_id  = $mappingTaxons[$taxonId]['id_parent'];
                    $primaryName = $mappingTaxons[$parent_id]['title'];
                    $primarySlug = $mappingTaxons[$parent_id]['slug'];
                }
            } else {
                $taxonId = null;
            }
        }

        // Some checks before load data
        if (is_null($taxonId)) {
            // the requested taxon does not exist, redirect to the root mapping.
            return $this->redirect($this->getBaseUrl('mapping'), 301);
        } elseif (is_null($parent_id)) {
            // the requested taxon is a primary level, redirect it to the official URL.
            return $this->redirect($this->getBaseUrl('mapping') ."$taxonSlug/", 301);
        } else {
            $parentTaxonUrl = "$primarySlug/";
            $taxonUrl = $parentTaxonUrl ."$taxonSlug/$taxonId";
        }


        // Load data
        $this->loadModel("TaxonomysSoftwares");
        $data = $this->TaxonomysSoftwares->getSoftwareTaxonomyWithUsers($taxonId, $softwareId);
        if (!isset($data) || !is_array($data) || count($data) === 0) {
            // the requested IDs does not exist in database, redirect to the taxon page.
            return $this->redirect($this->getBaseUrl('mapping') .$taxonUrl, 301);
        } else {
            $software = $data[$softwareId]['software'];
            $softwareSlug = $software->slug_name;
            $currentUrl = $parentTaxonUrl ."$taxonSlug/$softwareSlug/$taxonId.$softwareId";
        }

        // Check that the current URL is correct
        $realCurrentUrl = $this->request->here(false);
        $allowedUrl = $this->getBaseUrl('mapping') . $currentUrl;
        if ($allowedUrl !== $realCurrentUrl) {
            return $this->redirect("$allowedUrl", 301);
        }

        // Parameters for the view
        $this->set('softwareId', "$softwareId");
        $this->set('taxonId', "$taxonId");
        $this->set('taxonName', "$taxonName");
        $this->set(compact('data'));
        $this->set(compact('mappingTaxons'));
        $this->set(compact('mappingHead'));
        $this->set('_serialize', ['data']);
        $this->set('_serialize', ['mappingTaxons']);
        $this->set('_serialize', ['mappingHead']);

        // Breadcrumb
        $links = array();
        $links[] = [
            'name' => $primaryName,
            'url'  => $this->getBaseUrl('mapping', false) . $parentTaxonUrl,
        ];
        $links[] = [
            'name' => "$taxonName",
            'url'  =>  $this->getBaseUrl('mapping', false) . $taxonUrl,
        ];
        $links[] = [
            'name' => "Utilisateurs du logiciel ". $software->softwarename,
            'name' => "Logiciel ". $software->softwarename,
            'url'  =>  $this->getBaseUrl('mapping', false) . $currentUrl,
        ];
        $this->setBreadcrumbs($links);
    }
}
