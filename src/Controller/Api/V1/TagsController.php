<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Model\Table\TagsTable;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;

/**
 * Tags Controller
 *
 * @property TagsTable $Tags
 */
class TagsController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->paginate = [
            'sortWhitelist' => [
                'used_tag_number',
                'name'
            ],
            'limit' => Configure::read('LIMIT'),
            'maxLimit' => Configure::read('LIMIT'),
            'contain' => ['Softwares'],
            'order' => ['name' => "ASC"]
        ];
    }

    /**
     * Index method
     *
     * @return Response|null
     */
    public function index()
    {

        if (!empty($this->request->data) && isset($this->request->data["order"])) {
            $sort = explode(".", $this->request->data["order"]);
            $this->request->query["sort"] = $sort[0];
            $this->request->query["direction"] = $sort[1];
        }


        $order = [
            "name.asc" => __d("Forms", "Search.Sort.tagNameAsc"),
            "name.desc" => __d("Forms", "Search.Sort.tagNameDesc"),
            "used_tag_number.desc" => __d("Forms", "Search.Sort.usedTagNumberDesc"),
            "used_tag_number.asc" => __d("Forms", "Search.Sort.usedTagNumberAsc"),
        ];


        $tags = $this->Tags->find("all", ['Softwares'])
            ->select(['used_tag_number' => $this->Tags->query()->func()->count('SoftwaresTags.tag_id')])
            ->innerJoinWith('Softwares')
            ->group(['Tags.id', 'SoftwaresTags.tag_id'])
            ->autoFields(true);

        $this->paginate($tags);
        $this->set(compact('tags'));
        $this->set('order', $order);
        $this->set('_serialize', ['tags']);
        $this->setBreadcrumbs();
        return null;
    }

    /**
     * $links = [ 0 => [ 'name' => '…', 'url' => '/dir/file', 'title' => '…'],
     *            1 => [ 'name' => '…', 'url' => '/dir/file', 'title' => '…'], ]
     *
     * @param array $links
     */
    protected function setBreadcrumbs(array $links = [])
    {
        $firstLink = [
            'name' => __d('Breadcrumbs', 'Tag.ListOfTags'),
            'url' => 'tags'
        ];
        array_unshift($links, $firstLink);
        parent::setBreadcrumbs($links);
    }


    /**
     * Before filter (a magical CakePHP method)
     *
     * Called before the controller action.
     * - used to deny access to an anonymous user (add, edit, delete)
     *
     *      notice:
     *      $this->Auth->user('id'|'role') are null at this step
     *
     * @uses parent::beforeFilter()
     * @param Event $event
     * @return Response|null|void
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(
            [
                'index',
                'view',
                'listAllSoftwareForAGivenTag'
            ]
        );
        parent::beforeFilter($event);
    }


    /**
     * Is authorized (a magical CakePHP method)
     *
     * Manage all rights for the controllers' actions,
     * with the exception of anonymous users (managed by beforeFilter() method)
     *
     * Return true if the user can use the current action, false otherwise.
     * - edit and delete action are allowed only to connected user with "admin" role
     * - Connected user with "admin" role can use all actions
     * - Connected user with "user" role can use his/her dedicated actions (add a tag)
     *   and allowed actions to anonymous user
     *
     * notice: $forceDeny parameter is used here (forced to true),
     *                     and mandatory to be compatible with parent::isAuthorized()
     *
     * @param array $user A user.
     * @param  boolean $forceDeny by default FALSE, set TRUE to force the deny on parent::isAuthorized()
     * @return boolean
     */
    public function isAuthorized($user, $forceDeny = false)
    {
        //If the user is connected
        if ($this->Auth->user('id')) {
            if (in_array($this->request->action, [
                "index",
                "view",
                "add",
                "listAllSoftwareForAGivenTag"
            ])) {
                return true;
            }
        }
        $forceDeny = true;
        return parent::isAuthorized($user, $forceDeny);
    }

    /**
     * API view method of a given tag
     *
     *      URL :   API     /$lang/tags/$idTag.json
     *              Webapp  /$lang/tags/$idTag   ---> redirect 301 to /$lang/tags/$idTag/software
     *                      /$lang/tags/$idTag/  ---> redirect 301 to /$lang/tags/$idTag/software
     *
     * @param string|null $id Tag id.
     * @return Response|null
     * @throws RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if ($this->request->is('json')) {
            $tag = $this->Tags->get(
                $id,
                ['contain' => ['Softwares']]
            );

            $this->set('tag', $tag);
            $this->set('_serialize', ['tag']);
            return null;
        } else {
            $idTag = (int) $id;
            $lang = $this->selectedLanguage;
            return $this->redirect("/$lang/tags/$idTag/software", 301);
        }
    }


    /**
     * Returns a list of software associated to a given tag
     *
     * @param int $id The id of a given tag
     */
    public function listAllSoftwareForAGivenTag($id)
    {
        //  Sort software list by softwarename
        $tag = $this->Tags->get(
            $id,
            ['contain' =>
                [
                    'Softwares' => [
                        'sort' => ['Softwares.softwarename' => 'ASC']
                    ]
                ]
            ]
        );

        $this->set('tag', $tag);
        $this->set('_serialize', ['tag']);

        // Breadcrumbs
        $links = array();
        $links[] = [
            'name' => $tag->name,
            'url'  =>  'tags/'. $tag->id .'/software',
        ];
        $this->setBreadcrumbs($links);
    }

    /**
     * Add method
     *
     * @return Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tag = $this->Tags->newEntity();
        if ($this->request->is('post')) {
            $tag = $this->Tags->patchEntity($tag, $this->request->data);
            $this->log(var_export($tag, true));
            if ($this->Tags->save($tag)) {
                $message = "Success";
            } else {
                $this->Flash->error(__d('Tags', "error"));
                $message = "Error";
                $tag = $tag->errors();
            }
        }
        $softwares = $this->Tags->Softwares->find('list', ['limit' => 125]);
        $this->set(compact('tag', 'softwares', 'message'));
        $this->set('_serialize', ['tag', 'message']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tag id.
     * @return Response|void Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tag = $this->Tags->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $tag = $this->Tags->patchEntity($tag, $this->request->data);
            if ($this->Tags->save($tag)) {
                $this->Flash->success(__d('Tags', 'The tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__d('Tags', 'The tag could not be saved. Please, try again.'));
            }
        }
        $softwares = $this->Tags->Softwares->find('list', ['limit' => 160]);
        $this->set(compact('tag', 'softwares'));
        $this->set('_serialize', ['tag', 'message']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tag id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tag = $this->Tags->get($id);
        if ($this->Tags->delete($tag)) {
            $this->Flash->success(__d('Tags', 'The tag has been deleted.'));
        } else {
            $this->Flash->error(__d('Tags', 'The tag could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
