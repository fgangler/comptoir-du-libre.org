<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Model\Table\RelationshipTypesTable;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;

/**
 * RelationshipTypes Controller
 *
 * @property RelationshipTypesTable $RelationshipTypes
 */
class RelationshipTypesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('relationshipTypes', $this->paginate($this->RelationshipTypes));
        $this->set('_serialize', ['relationshipTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Relationship Type id.
     * @return void
     * @throws NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $relationshipType = $this->RelationshipTypes->get(
            $id,
            [
                'contain' => ['Relationships']
            ]
        );
        $this->set('relationshipType', $relationshipType);
        $this->set('_serialize', ['relationshipType']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $relationshipType = $this->RelationshipTypes->newEntity();
        if ($this->request->is('post')) {
            $relationshipType = $this->RelationshipTypes->patchEntity($relationshipType, $this->request->data);
            if ($this->RelationshipTypes->save($relationshipType)) {
                $this->Flash->success(__('The relationship type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The relationship type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('relationshipType'));
        $this->set('_serialize', ['relationshipType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Relationship Type id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $relationshipType = $this->RelationshipTypes->get(
            $id,
            [
                'contain' => []
            ]
        );
        if ($this->request->is(['patch', 'post', 'put'])) {
            $relationshipType = $this->RelationshipTypes->patchEntity($relationshipType, $this->request->data);
            if ($this->RelationshipTypes->save($relationshipType)) {
                $this->Flash->success(__('The relationship type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The relationship type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('relationshipType'));
        $this->set('_serialize', ['relationshipType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Relationship Type id.
     * @return Response|null Redirects to index.
     * @throws NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $relationshipType = $this->RelationshipTypes->get($id);
        if ($this->RelationshipTypes->delete($relationshipType)) {
            $this->Flash->success(__('The relationship type has been deleted.'));
        } else {
            $this->Flash->error(__('The relationship type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
