<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Model\Table\RelationshipsSoftwaresUsersTable;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;

/**
 * RelationshipsSoftwaresUsers Controller
 *
 * @property RelationshipsSoftwaresUsersTable $RelationshipsSoftwaresUsers
 */
class RelationshipsSoftwaresUsersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Softwares', 'Users', 'Relationships']
        ];
        $this->set('relationshipsSoftwaresUsers', $this->paginate($this->RelationshipsSoftwaresUsers));
        $this->set('_serialize', ['relationshipsSoftwaresUsers']);
    }

    /**
     * Returns all services providers in Comptoir du libre.
     */
    public function getServicesProviders()
    {

        if ($this->request->is('get') && $this->response->type('json')) {
            $Relationships = $this->RelationshipsSoftwaresUsers->find(
                'all',
                [
                    'contain' => ["Users", "Relationships"],
                    'conditions' => ["Relationships.cd = " => $this->request->query['Relationships']],
                ]
            )
                ->distinct("user_id");
            $message = "Success";

            $this->set('relationshipsSoftwaresUsers', ['message' => $message, 'Relationships' => $Relationships]);
            $this->set('_serialize', ['message', 'relationshipsSoftwaresUsers']);
        } else {
            $message = "Error";
            $this->set(
                [
                    'message' => $message,
                    'relationshipsSoftwaresUser' => $relationshipsSoftwaresUser,
                    '_serialize' => ['message', 'relationshipsSoftwaresUser']
                ]
            );
        }
    }

    /**
     * View method
     *
     * @param string|null $id Relationships Softwares User id.
     * @return void
     * @throws NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $relationshipsSoftwaresUser = $this->RelationshipsSoftwaresUsers->get(
            $id,
            [
                'contain' => ['Softwares', 'Users', 'Relationships']
            ]
        );
        $this->set('relationshipsSoftwaresUser', $relationshipsSoftwaresUser);
        $this->set('_serialize', ['relationshipsSoftwaresUser']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $relationshipsSoftwaresUser = $this->RelationshipsSoftwaresUsers->newEntity();
        if ($this->request->is('post') && !$this->request->is('json')) {
            $relationshipsSoftwaresUser = $this->RelationshipsSoftwaresUsers->patchEntity(
                $relationshipsSoftwaresUser,
                $this->request->data
            );
            if ($this->RelationshipsSoftwaresUsers->save($relationshipsSoftwaresUser)) {
                $this->Flash->success(__('The relationships softwares user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The relationships softwares user could not be saved. Please, try again.'));
            }
            $softwares = $this->RelationshipsSoftwaresUsers->Softwares->find('list', ['limit' => 200]);
            $users = $this->RelationshipsSoftwaresUsers->Users->find('list', ['limit' => 200]);
            $relationships = $this->RelationshipsSoftwaresUsers->Relationships->find('list', ['limit' => 200]);
            $this->set(compact('relationshipsSoftwaresUser', 'softwares', 'users', 'relationships'));
            $this->set('_serialize', ['relationshipsSoftwaresUser']);
        } elseif ($this->request->is('post') && $this->request->is('json')) {
            $relationshipsSoftwaresUser = $this->RelationshipsSoftwaresUsers->patchEntity(
                $relationshipsSoftwaresUser,
                $this->request->data
            );
            if ($this->RelationshipsSoftwaresUsers->save($relationshipsSoftwaresUser)) {
                $message = "Success";
            } else {
                $message = 'Error';
            }
            $this->set(
                [
                    'message' => $message,
                    'relationshipsSoftwaresUser' => $relationshipsSoftwaresUser,
                    '_serialize' => ['message', 'relationshipsSoftwaresUser']
                ]
            );
        } else {//Pour le template avant d'avoir poste les données.
            $softwares = $this->RelationshipsSoftwaresUsers->Softwares->find(
                'list',
                ['limit' => 200]
            )->orderAsc('softwarename');
            $users = $this->RelationshipsSoftwaresUsers->Users->find('list', ['limit' => 200])->orderAsc('username');
            $relationships = $this->RelationshipsSoftwaresUsers->Relationships->find(
                'list',
                ['limit' => 200]
            )->orderAsc('name');
            $this->set(compact('relationshipsSoftwaresUser', 'softwares', 'users', 'relationships'));
            $this->set('_serialize', ['relationshipsSoftwaresUser']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Relationships Softwares User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $relationshipsSoftwaresUser = $this->RelationshipsSoftwaresUsers->get(
            $id,
            [
                'contain' => []
            ]
        );
        if ($this->request->is(['patch', 'post', 'put']) && !$this->request->is('json')) {
            $relationshipsSoftwaresUser = $this->RelationshipsSoftwaresUsers->patchEntity(
                $relationshipsSoftwaresUser,
                $this->request->data
            );
            if ($this->RelationshipsSoftwaresUsers->save($relationshipsSoftwaresUser)) {
                $this->Flash->success(__('The relationships softwares user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The relationships softwares user could not be saved. Please, try again.'));
            }

            $softwares = $this->RelationshipsSoftwaresUsers->Softwares->find('list', ['limit' => 200]);
            $users = $this->RelationshipsSoftwaresUsers->Users->find('list', ['limit' => 200]);
            $relationships = $this->RelationshipsSoftwaresUsers->Relationships->find('list', ['limit' => 200]);
            $this->set(compact('relationshipsSoftwaresUser', 'softwares', 'users', 'relationships'));
            $this->set('_serialize', ['relationshipsSoftwaresUser']);
        } elseif ($this->request->is(['patch', 'post', 'put']) && $this->request->is('json')) {
            $relationshipsSoftwaresUser = $this->RelationshipsSoftwaresUsers->patchEntity(
                $relationshipsSoftwaresUser,
                $this->request->data
            );
            if ($this->RelationshipsSoftwaresUsers->save($relationshipsSoftwaresUser)) {
                $message = "Success";
            } else {
                $message = "Error";
            }
            $this->set(
                [
                    'message' => $message,
                    'relationshipsSoftwaresUser' => $relationshipsSoftwaresUser,
                    '_serialize' => ['message', 'relationshipsSoftwaresUser']
                ]
            );
        } else {//Pour le template avant le post des données.
            $softwares = $this->RelationshipsSoftwaresUsers->Softwares->find('list', ['limit' => 200]);
            $users = $this->RelationshipsSoftwaresUsers->Users->find('list', ['limit' => 200]);
            $relationships = $this->RelationshipsSoftwaresUsers->Relationships->find('list', ['limit' => 200]);
            $this->set(compact('relationshipsSoftwaresUser', 'softwares', 'users', 'relationships'));
            $this->set('_serialize', ['relationshipsSoftwaresUser']);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Relationships Softwares User id.
     * @return Response|null Redirects to index.
     * @throws NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $relationshipsSoftwaresUser = $this->RelationshipsSoftwaresUsers->get($id);
        if ($this->RelationshipsSoftwaresUsers->delete($relationshipsSoftwaresUser)) {
            $this->Flash->success(__('The relationships softwares user has been deleted.'));
        } else {
            $this->Flash->error(__('The relationships softwares user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
