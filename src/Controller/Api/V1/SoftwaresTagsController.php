<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use App\Model\Table\SoftwaresTagsTable;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Response;

/**
 * SoftwaresTags Controller
 *
 * @property SoftwaresTagsTable $SoftwaresTags
 */
class SoftwaresTagsController extends AppController
{

    /**
     * Index method
     *
     * @return Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Softwares', 'Tags']
        ];
        $softwaresTags = $this->paginate($this->SoftwaresTags);

        $this->set(compact('softwaresTags'));
        $this->set('_serialize', ['softwaresTags']);
    }

    /**
     * View method
     *
     * @param string|null $id Softwares Tag id.
     * @return Response|null
     * @throws RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $softwaresTag = $this->SoftwaresTags->get(
            $id,
            [
                'contain' => ['Softwares', 'Tags']
            ]
        );

        $this->set('softwaresTag', $softwaresTag);
        $this->set('_serialize', ['softwaresTag']);
    }

    /**
     * Add method
     *
     * @return Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $softwaresTag = $this->SoftwaresTags->newEntity();
        if ($this->request->is('post')) {
            $softwaresTag = $this->SoftwaresTags->patchEntity($softwaresTag, $this->request->data);
            if ($this->SoftwaresTags->save($softwaresTag)) {
                $this->Flash->success(__d('Tags', 'The software tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__d('Tags', 'The software tag could not be saved. Please, try again.'));
            }
        }
        $softwares = $this->SoftwaresTags->Softwares->find('list', ['limit' => 200]);
        $tags = $this->SoftwaresTags->Tags->find('list', ['limit' => 200]);
        $this->set(compact('softwaresTag', 'softwares', 'tags'));
        $this->set('_serialize', ['softwaresTag']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Softwares Tag id.
     * @return Response|void Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $softwaresTag = $this->SoftwaresTags->get(
            $id,
            [
                'contain' => []
            ]
        );
        if ($this->request->is(['patch', 'post', 'put'])) {
            $softwaresTag = $this->SoftwaresTags->patchEntity($softwaresTag, $this->request->data);
            if ($this->SoftwaresTags->save($softwaresTag)) {
                $this->Flash->success(__d('Tags', 'The softwares tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__d('Tags', 'The softwares tag could not be saved. Please, try again.'));
            }
        }
        $softwares = $this->SoftwaresTags->Softwares->find('list', ['limit' => 200]);
        $tags = $this->SoftwaresTags->Tags->find('list', ['limit' => 200]);
        $this->set(compact('softwaresTag', 'softwares', 'tags'));
        $this->set('_serialize', ['softwaresTag']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Softwares Tag id.
     * @return Response|null Redirects to index.
     * @throws RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $softwaresTag = $this->SoftwaresTags->get($id);
        if ($this->SoftwaresTags->delete($softwaresTag)) {
            $this->Flash->success(__d('Tags', 'The softwares tag has been deleted.'));
        } else {
            $this->Flash->error(__d('Tags', 'The softwares tag could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
