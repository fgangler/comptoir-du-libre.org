# LANGUAGE translation of CakePHP Application
# Copyright 2018 NAME <EMAIL@ADDRESS>
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-02-04 11:59+0000\n"
"PO-Revision-Date: 2018-05-23 13:36+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: Mailer/CrudMailer.php:16
msgid "New {0} on Comptoir du libre : {1} "
msgstr "New {0} on Comptoir du libre : {1} "

#: Mailer/CrudMailer.php:27;75
msgid "Something happend on Comptoir du Libre : "
msgstr "Something happend on Comptoir du Libre : "

#: Mailer/CrudMailer.php:29;53;77
msgid "DATA : "
msgstr "DATA : "

#: Mailer/CrudMailer.php:39
msgid "A {0} was deleted on {1}"
msgstr "A {0} was deleted on {1}"

#: Mailer/CrudMailer.php:47
msgid "Something happend on Comptoir du Libre on : {0} "
msgstr "Something happend on Comptoir du Libre on : {0} "

#: Mailer/CrudMailer.php:63
msgid "Modified {0}: {1} by {2} on Comptoir du libre : {3} "
msgstr "Modified {0}: {1} by {2} on Comptoir du libre : {3} "

#: Mailer/UserMailer.php:39
msgid "Reset password"
msgstr "[Comptoir du Libre] Reset your password"

#: Mailer/UserMailer.php:45
msgid ""
"Please follow this link to reset your password: \n"
"{0} \n"
msgstr ""
"Please follow this link to reset your password: \n"
"{0} \n"

#: Template/Email/text/contact.ctp:18 Template/Email/text/reset_password.ctp:19
msgid "Hello {0},\n"
msgstr "Hello {0},\n"

#: Template/Email/text/contact.ctp:24
#: Template/Email/text/reset_password.ctp:26
msgid ""
"Regards,\n"
"\n"
"Comptoir du libre team."
msgstr ""
"Regards,\n"
"\n"
"Comptoir du libre team."
"\n\n--\n"
"https://comptoir-du-libre.org \n"

#: Template/Email/text/contact_via_form.ctp:8
msgid "ContactViaForm_header"
msgstr "On your \"Comptoir du Libre\" profile \n"
"a user sent you a message. \n\n"
"From: {0}"

#: Template/Email/text/contact_via_form.ctp:8
msgid "ContactViaForm_footer"
msgstr "\n\n--\n"
"https://comptoir-du-libre.org \n"
