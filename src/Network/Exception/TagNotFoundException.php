<?php

namespace App\Network\Exception;

use Cake\Network\Exception\NotFoundException;

class TagNotFoundException extends NotFoundException
{

}
