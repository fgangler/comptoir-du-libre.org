<?php

namespace App\View\Helper;

use Cake\View\StringTemplateTrait;

/**
 * Class SoftwareHelper
 * Build Html structure for a software item
 *
 * @package App\View\Helper
 */
class SoftwareHelper extends ListItemHelper
{

    use StringTemplateTrait;

    public $helpers = ['Html', 'Text', 'Rating', 'Form'];
    protected $_defaultConfig = [
        'templates' => [
            'software' =>
                '<li {{attrsColumn}}>
                    <div {{attrsBlock}}>
                        {{LinkItem}}
                        <div class="size-logo">
                            {{LinkImage}}
                        </div>
                        {{softwareName}}
                        <p {{attrsDescription}}>{{softwareDescription}}</p>
                        <div class="rating-unit">{{review_average}}</div>
                    </div>
                </li>',
        ]
    ];

    public function display($software, $options = ["limit" => false])
    {
        // This helper is used all the time.
        // But, the $selectedLanguage variable is not available.
        // We force its creation by using a translation string that is always available.
        if (!isset($selectedLanguage)) {
            $selectedLanguage =  __d("default", "lang.id");  // "fr" or "en"
        }

        $result = null;
        if (!empty($software)) {
            $logo = $this->displayLogoLink($software);

            $attrsColumn = $this->templater()->formatAttributes(['class' => 'col-xs-12 col-sm-6 col-md-3 col-lg-3']);
            $attrsBlock = $this->templater()->formatAttributes(['class' => 'software-unit-home backgroundUnit',
                                                                'id'    => 'softwarePreviewCard-'. $software->id]);

            $result .= $this->formatTemplate(
                'software',
                [
                    'attrsColumn' => $attrsColumn,
                    'attrsBlock' => $attrsBlock,
                    'review_average' => $this->Rating->display($software->average_review),
                    'softwareName' =>
                        $this->Html->link(
                            $this->Text->truncate($software->softwarename, 35, ['ellipsis' => '...', 'exact' => false]),
                            [
                                'language' => $selectedLanguage,
                                'prefix' => false,
                                'controller' => 'softwares',
                                "action" => $software->id
                            ],
                            [
                                'escape' => false,
                                'title' => __d("Softwares", "software.softwareName") . $software->softwarename
                            ]
                        ),
                    'LinkImage' => $logo,
                    'attrsDescription' => $this
                        ->templater()
                        ->formatAttributes(['class' => 'text-overflow project-description']),
                    'softwareDescription' => $this->Text->truncate(
                        $software->description,
                        100,
                        ['ellipsis' => '...', 'exact' => false]
                    ),
                    ['class' => 'text-overflow project-description'],
                    'LinkItem' => $this->Html->link(
                        "",
                        [
                            'language' => $selectedLanguage,
                            'prefix' => false,
                            'controller' => 'Softwares',
                            'action' => $software->id
                        ],
                        ["class" => "linkItem", 'escape' => false,]
                    ),
                ]
            );
        }
        return $result;
    }


    /**
     * Return a link with containing the software'logo if it get one, placeholder otherwise
     *
     * @param  $software
     * @return mixed
     */
    public function displayLogoLink($software)
    {
        // This helper is used all the time.
        // But, the $selectedLanguage variable is not available.
        // We force its creation by using a translation string that is always available.
        if (!isset($selectedLanguage)) {
            $selectedLanguage =  __d("default", "lang.id");  // "fr" or "en"
        }

        return !$this->hasLogo($software) || is_null($software->photo) || empty($software->photo) ?

            $this->Html->link(
                $this->Html->image(
                    "logos/placeholder_software.svg",
                    [
                        "alt" => __d("Softwares", "Go to the {0}'s page", $software->softwarename),
                        "class" => "img-responsive",
                        "loading" => "lazy"
                    ]
                ),
                [
                    'language' => $selectedLanguage,
                    'prefix' => false,
                    'controller' => 'softwares',
                    "action" => $software->id
                ],
                ['escape' => false, "title" => __d("Softwares", "software.softwareName") . $software->softwarename]
            )
            :
            $this->Html->link(
                $this->Html->image(
                    $software->logo_directory . DS . $software->photo,
                    [
                        "alt" => $software->softwarename,
                        "class" => "img-responsive",
                        "loading" => "lazy"
                    ]
                ),
                [
                    'language' => $selectedLanguage,
                    'prefix' => false,
                    'controller' => 'softwares',
                    "action" => $software->id
                ],
                ['escape' => false, "title" => __d("Softwares", "software.softwareName") . $software->softwarename]
            );
    }
}
