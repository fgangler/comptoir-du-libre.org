<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View;

use Cake\View\View;

/**
 * Application View
 *
 * Your application’s default view class
 *
 * @link http://book.cakephp.org/3.0/en/views.html#the-app-view
 */
class AppView extends View
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
    public function initialize()
    {
        $this->loadHelper('Gourmet/SocialMeta.Card');
        $this->loadHelper('Gourmet/SocialMeta.OpenGraph');
    }

    public function render($view = null, $layout = null)
    {
        if ($this->request->is('json')) {
            $layout = 'json';
        }

        $result = parent::render($view, $layout);
        if ($this->request->is('json')) {
            $this->response->header('Content-Type', 'application/json; charset=UTF-8');
        } else {
            // Add HTTP security headers
            $this->response->header('X-Frame-Options', 'DENY');
            $this->response->header('X-Content-Type-Options', 'nosniff');
            $this->response->header('X-XSS-Protection', '1; mode=block');
            $this->response->header('Referrer-Policy', 'strict-origin-when-cross-origin');

            // The following HTTP security headers are not yet implemented
                // $this->response->header('Strict-Transport-Security', 'max-age=31536000');
                // $this->response->header('Content-Security-Policy', "default-src 'self' statistiques.adullact.org;");
        }

        return $result;
    }

    /**
     * Test if the current page depend on a controller specified as parameter.
     *
     * @param  String $request the string to test. A controller Name
     * @param String $action the default action, specified action if different
     * @return Boolean Return true if the controller matches with the $request
     */
    public function isHere($request, $action = "index")
    {
        if ($this->request->params['controller'] == ucfirst($request)
            && $this->request->params['action'] == $action
        ) {
            return true;
        } else {
            return false;
        }
    }
}
