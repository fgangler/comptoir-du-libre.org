<?php

namespace App\Cache;

use App\Network\Exception\RelationshipNotFoundException;
use App\Network\Exception\UserTypeNotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * A trait that provides methods for some database cache
 */
trait AppCacheTrait
{
    /**
     * @var array
     */
    private $appCache = [];


    /////////// Taxonomy //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Returns mapping first levels
     * @return array  example: [<taxonId> => <taxonName>, ...]
     */
    final public function getMappingFirstLevels(string $lang = 'en')
    {
        if (!isset($this->appCache['mappingFirstLevels'][$lang])
            || !array($this->appCache['mappingFirstLevels'][$lang])) {
            $this->getMappingFromCache($lang);
        }
        return $this->appCache['mappingFirstLevels'][$lang];
    }

    /**
     * Returns mapping data
     * example:
     *      [ <taxonId_1> => [ 'slug'      => <taxonSlug>,
     *                         'title'     => <taxonTitle>,
     *                         'desc       => <description>,
     *                         'children'  => [<taxonId> => <taxonName>, ...], ]
     *        <taxonId_4> => [ 'slug'      => <taxonSlug>,
     *                         'title'     => <taxonTitle>,
     *                         'desc       => <description>,
     *                         'id_parent' => <taxonParenId> ]
     *
     * @return array
     */
    final public function getMappingTaxons(string $lang = 'en')
    {
        if (!isset($this->appCache['mappingTaxons'][$lang]) || !array($this->appCache['mappingTaxons'][$lang])) {
            $this->getMappingFromCache($lang);
        }
        return $this->appCache['mappingTaxons'][$lang];
    }

    /**
     * Returns mapping data, filtered from provided taxon IDs.
     * Parent taxons are included.
     *
     * example:
     *      [ <taxonId_1> => [ 'slug'      => <taxonSlug>,
     *                         'title'     => <taxonTitle>,
     *                         'desc       => <description>,
     *                         'children'  => [<taxonId> => <taxonName>, ...], ]
     *        <taxonId_4> => [ 'slug'      => <taxonSlug>,
     *                         'title'     => <taxonTitle>,
     *                         'desc       => <description>,
     *                         'id_parent' => <taxonParenId> ]
     *
     * @param array $taxonIds example: [<taxonId_4>, <taxonId_5>, ...]
     * @param string $lang   (optional) language code, by default it's 'en'
     * @return array
     */
    final public function getMappingTaxonsWithTaxonIdsFilter(array $taxonIds, string $lang = 'en')
    {
        $taxonIds = array_flip($taxonIds);
        $data = $this->getMappingTaxons($lang);
        $filteredData = [];
        $parentList = [];

        // Filtring except first level taxons
        foreach ($data as $taxonId => $taxonData) {
            if (!isset($taxonData['id_parent'])) { // First level
                $parentList[$taxonId] = $taxonData;
            } elseif (isset($taxonIds[$taxonId])) {
                $filteredData[$taxonId] = $taxonData;
            }
        }

        // Filtring for first level taxons
        foreach ($parentList as $parentId => $parentData) {
            foreach ($parentData['children'] as $childId => $child) {
                if (!isset($taxonIds[$childId])) {
                    unset($parentList[$parentId]['children'][$childId]);
                }
            }
            if (count($parentList[$parentId]['children']) === 0) {
                unset($parentList[$parentId]);
            }
        }
        $filteredData = $parentList + $filteredData;
        return $filteredData;
    }


    /**
     * Populate:
     * - $this->appCache['mappingTaxons'][$lang]
     * - $this->appCache['mappingFirstLevels'][$lang]
     *
     * @param string $lang
     * @return void
     */
    private function getMappingFromCache(string $lang = 'en')
    {
        $this->initAppCache();
        if (isset($this->appCache['mappingTaxons'][$lang]) && array($this->appCache['mappingTaxons'][$lang])) {
            return;
        }

        $titlePropertie = "title_i18n_$lang";
        $descPropertie = "description_i18n_$lang";
        $mapping = [];
        $mappingFirstLevels = [];

        $registry = TableRegistry::get("Taxonomys");
        $taxonomys = $registry->find('all');
        foreach ($taxonomys as $taxonomy) {
            $id = $taxonomy->id;
            $title = $taxonomy->$titlePropertie; // property "title_i18n_fr" or "title_i18n_en"
            $desc = $taxonomy->$descPropertie;  // property "description_i18n_fr" or "description_i18n_en"

            $mapping[$id]['slug'] = strtolower(Text::slug($title));
            $mapping[$id]['title'] = $title ;
            $mapping[$id]['desc'] = $desc ;
            if (is_null($taxonomy->parent_id)) {
                $mappingFirstLevels[$id] = $title;
            } else {
                $idParent = $taxonomy->parent_id;
                $mapping[$id]['id_parent'] = $idParent ;
                $mapping[$idParent]['children'][$id] = $title;
            }
        }

        ksort($mappingFirstLevels);
        foreach ($mapping as $id => $dataTaxon) {
            if (isset($mapping[$id]['children'])) {
                asort($mapping[$id]['children']);
            }
        }
        $this->appCache['mappingFirstLevels'][$lang] = $mappingFirstLevels;
        $this->appCache['mappingTaxons'][$lang] = $mapping;
    }


    /////////// Relationships //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Get relationships list
     *
     * @return array   [ "<relationship_cd>" => <relationship_ID>, ... ]
     */
    final protected function getRelationshipsListFromCache()
    {
        $this->initAppCache();
        if (!isset($this->appCache['relationships']) || !array($this->appCache['relationships'])) {
            $registry = TableRegistry::get('Relationships');
            $result = $registry->find("all")->toList();
            $data = [];
            foreach ($result as $obj) {
                $data[$obj->cd] = $obj->id;
            }
            $this->appCache['relationships'] = $data;
        }
        return $this->appCache['relationships'];
    }

    /**
     * Get relationship ID by name
     *
     * @param string $name
     * @return int
     */
    final protected function getRelationshipIdByName(string $name)
    {
        $relationships = $this->getRelationshipsListFromCache();
        if (isset($relationships[$name])) {
            return $relationships[$name]; // RelationshipId
        } else {
            throw new RelationshipNotFoundException("The relationship with the cd (nameSlug) [ $name ] does not exist");
        }
    }


    /////////// User type //////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Get user types list
     *
     * @return array   [ "<userType_cd>" => <relationship_ID>, ... ]
     */
    final protected function getUserTypesListFromCache()
    {
        $this->initAppCache();
        if (!isset($this->appCache['userTypes']) || !array($this->appCache['userTypes'])) {
            $registry = TableRegistry::get('userTypes');
            $result = $registry->find("all")->toList();
            $data = [];
            foreach ($result as $obj) {
                $data[$obj->cd] = $obj->id;
            }
            $this->appCache['userTypes'] = $data;
        }
        return $this->appCache['userTypes'];
    }

    /**
     * Get user type ID by name
     *
     * @param string $name
     * @return int
     */
    final protected function getUserTypeIdByName(string $name)
    {
        $userTypes = $this->getUserTypesListFromCache();
        if (isset($userTypes[$name])) {
            return $userTypes[$name]; // userTypesId
        } else {
            throw new UserTypeNotFoundException("The relationship with the cd (nameSlug) [ $name ] does not exist");
        }
    }


    //////////////////////////////////////////////////// ///////////////////////////////////////////////////////////////

    /**
     * Initializes property $this->appCache, if it does not exist
     */
    final protected function initAppCache()
    {
        if (!isset($this->appCache)) {
            $this->appCache = [];
        }
    }
}
