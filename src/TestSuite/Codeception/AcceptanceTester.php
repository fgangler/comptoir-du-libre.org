<?php
/**
 * This file adds custom methods (loginMe, logoutMe, createAccountMinimal) for acceptance test.
 *
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
namespace App\TestSuite\Codeception;

use Codeception\Actor;
use Codeception\Lib\Friend;

/**
 * Inherited Methods
 *
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 *
 * @package App\Test\Acceptance
 * @author  julie gauthier <julie.gauthier@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
class AcceptanceTester extends Actor
{
    use _generated\AcceptanceTesterActions;

    private $lang = 'en';
    private static $random;

    /**
     * Return self::$random (static property)
     * and initialize it if necessary
     *
     * @return string
     */
    public function getCurrentRandom()
    {
        if (is_null(self::$random)) {
            self::$random =  bin2hex(openssl_random_pseudo_bytes(6));
        }
        return self::$random;
    }


    /**
     * With $email as login,
     * $password as password,
     * $userName as name display when you are connected.
     *
     * @param String $email Email of the user account
     * @param String $password Password of the user account
     * @param String $userName Name of the user account
     * @param int|null $userId  expected user ID, by default is null
     *
     * @return void
     */
    public function loginMe(string $email, string $password, string $userName, int $userId = null)
    {
        $lang = $this->lang;
        $I = $this;
        $I->amOnPage("/$lang/");
        $I->click('//nav/ul[2]/li[3]/a');
        $I->seeInCurrentUrl("/$lang/users/login");
        $I->submitForm(
            '#signinform',
            [
                'email' => $email,
                'password' => $password
            ]
        );
        if (is_null($userId)) {
            $I->canSeeLink($userName, ("/$lang/users/"));
        } else {
            $I->canSeeLink($userName, ("/$lang/users/$userId"));
        }
    }

    /**
     * Use the user name to disconnect the user.
     *
     * @param String $userName Identification of the user account
     *
     * @return void
     */
    public function logoutMe($userName)
    {
        $I = $this;
        $I->see($userName);
        $I->click('//li[@id="userProfil-nav"]/ul/li[@id="nav_logout_item"]/a');
        $I->canSeeInCurrentUrl('/users/login');
        $I->dontSee($userName);
    }


    /**
     * Function with all fields to create a user should be added.
     * 18.07.28 we have to release in production so we can't cover all cases.
     *
     * To create a user account with only the required parameters:
     *
     * @param String $userTypeId Must be 2,4,5 or 6
     * @param String $username Name of the user account
     * @param String $email Email of the user account
     * @param String $password Password of the user account
     * @param String $confirmPwd He have to be identical to $password
     *
     * @return void
     */
    public function createAccountMinimal($userTypeId, $username, $email, $password, $confirmPwd)
    {
        $I = $this;
        $I->amOnPage('/');
        $I->click('//nav/ul[2]/li[2]/a');
        $I->seeInCurrentUrl('/users/add?t1=');
        $random = $I->getCurrentRandom();
        $randomUsername = "$username"."_$random";
        $randomEmail = str_replace("@", "$random@", "$email");
        // echo "\n-------------\n$randomUsername\n$randomEmail\n---------\n";

        // Add delay to look like a real user and not a robot
        sleep(TEST_MIN_TIME_TO_SEND_SIGNUP_FORM);

        $I->submitForm(
            '#createAccountForm',
            [
                'user_type_id' => $userTypeId,
                'username' => $randomUsername,
                'email' => $randomEmail,
                'password' => $password,
                'confirm_password' => $confirmPwd,
            ]
        );
    }
}
