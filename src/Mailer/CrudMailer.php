<?php

namespace App\Mailer;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Mailer\Mailer;

class CrudMailer extends Mailer
{
    public function created(Event $event, $className)
    {
        $this
            ->to(Configure::read('Email.to'))
            ->from(Configure::read('Email.from'))
            ->subject(__d(
                "Email",
                "New {0} on Comptoir du libre : {1} ",
                [$className, Configure::read('App.fullBaseUrl')]
            ))
            ->template('default')// Par défaut le template avec le même nom que le nom de la méthode est utilisé.
            ->transport('default');

        $this->viewVars(
            [
                'message' =>
                    __d("Email", "Something happend on Comptoir du Libre : " . Configure::read('App.fullBaseUrl') . " ")
                    .
                    __d("Email", "DATA : " . $event->data)
            ]
        );
    }

    public function deleted(Event $event, $className)
    {
        $this
            ->to(Configure::read('Email.to'))
            ->from(Configure::read('Email.from'))
            ->subject(__d("Email", 'A {0} was deleted on {1}', [$className, Configure::read('App.fullBaseUrl')]))
            ->template('default')// Par défaut le template avec le même nom que le nom de la méthode est utilisé.
            //            ->layout('custom')
            ->transport('default');

        $this->viewVars(
            [
                'message' =>
                    __d(
                        "Email",
                        "Something happend on Comptoir du Libre on : {0} ",
                        [Configure::read('App.fullBaseUrl')]
                    )
                    .
                    __d("Email", "DATA : " . $event->data)
            ]
        );
    }

    public function modifiedSoftware(Event $event, $className, $userName = "a user")
    {
        $this
            ->to(Configure::read('Email.to'))
            ->from(Configure::read('Email.from'))
            ->subject(__d(
                "Email",
                "Modified {0}: {1} by {2} on Comptoir du libre : {3} ",
                [$className, $event->data["softwarename"], $userName, Configure::read('App.fullBaseUrl')]
            ))
            ->template('default')// Par défaut le template avec le même nom que le nom de la méthode est utilisé.
            //            ->layout('custom')
            ->transport('default');

        $this->viewVars(
            [
                'message' =>
                    __d("Email", "Something happend on Comptoir du Libre : " . Configure::read('App.fullBaseUrl') . " ")
                    .
                    __d("Email", "DATA : " . $event->data)
            ]
        );
    }

    public function modifiedUser(Event $event, $className, $userName = "a user")
    {
        $this
            ->to(Configure::read('Email.to'))
            ->from(Configure::read('Email.from'))
            ->subject(__d(
                "Email",
                "Modified {0}: {1} by {2} on Comptoir du libre : {3} ",
                [$className, 'user.ID-'. $event->data["id"], $userName, Configure::read('App.fullBaseUrl')]
            ))
            ->template('default')// Par défaut le template avec le même nom que le nom de la méthode est utilisé.
            //            ->layout('custom')
            ->transport('default');

        $this->viewVars(
            [
                'message' =>
                    __d("Email", "Something happend on Comptoir du Libre : " . Configure::read('App.fullBaseUrl') . " ")
                    .
                    __d("Email", "DATA : " . $event->data)
            ]
        );
    }

    public function modifiedTaxonomySoftware(Event $event, $className, $userName = "a user")
    {
        $userTxt = 'user ['.$event->data['user_id'].'-'. $event->data['user']['username'].']';
        $this
            ->to(Configure::read('Email.to'))
            ->from(Configure::read('Email.from'))
            ->subject(__d(
                "Email",
                "Modified {0}: {1} by {2} on Comptoir du libre : {3} ",
                [$className, 'ID-'. $event->data['id'], $userTxt, Configure::read('App.fullBaseUrl')]
            ))
            ->template('default')// Par défaut le template avec le même nom que le nom de la méthode est utilisé.
            //            ->layout('custom')
            ->transport('default');

        $this->viewVars(
            [
                'message' =>
                    __d("Email", "Something happend on Comptoir du Libre : " . Configure::read('App.fullBaseUrl') . " ")
                    .
                    __d("Email", "DATA : " . $event->data)
            ]
        );
    }
}
