<?php
/**
 * Send emails.
 * Used in case of a lost password or to contact another user.
 *
 * @package App\Mailer
 * @author  mickael pastor <mickael.pastor@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */

namespace App\Mailer;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Mailer\Mailer;

/**
 * Send emails.
 * Used in case of a lost password or to contact another user.
 *
 * @package App\Mailer
 * @author  mickael pastor <mickael.pastor@adullact.org>
 * @license https://spdx.org/licenses/AGPL-3.0-or-later.html Affero General Public License
 */
class UserMailer extends Mailer
{
    /**
     * Reset password of a user who can't login anymore.
     *
     * @param Object $user User
     *
     * @return void
     */
    public function resetPassword($user)
    {
        $this
            ->to($user->email)
            ->from(Configure::read('Email.from'))
            ->subject(__d("Email", 'Reset password'))
            // Par défaut le template avec le même nom que le nom de la méthode est utilisé.
            ->template('reset_password')
            ->set(
                [
                    'userName' => $user->username,
                    'message' => __d(
                        "Email",
                        "Please follow this link to reset your password: \n{0} \n",
                        Configure::read('App.fullBaseUrl') . DS . "users" . DS . "reset-password" . DS . $user->token
                    ),
                ]
            )
            ->transport('default');
    }


    /**
     * Send a email to an user in Comptoir du Libre
     *
     * @param Event $event Event
     *
     * @return void
     */
    public function contactViaForm(Event $event)
    {

        $subject = strip_tags($event->data["subject"]);
        $msg = strip_tags($event->data["text"]);
        $replyTo = strip_tags($event->data['email']);
        $header = __d("Email", "ContactViaForm_header", $replyTo);

        $this
            ->to($event->data['recipient'])
            ->from(Configure::read('Email.from'))
            ->subject(Configure::read("Mail.Contact.UserToUser.Prefix") . $subject)
            ->replyTo($replyTo)
            // Par défaut le template avec le même nom que le nom de la méthode est utilisé.
            ->template('contactViaForm')
            ->set(
                [   'header' => $header,
                    'message' => $msg,
                ]
            )
            ->transport('default');
    }
}
