<?php
$this->layout("base");
$this->assign('title', __d("Forms", 'Sign in {0}', isset($message) ? " - " . $message : ""));
?>


<div class="row">
    <div class="warning-form bg-warning col-xs-offset-3 col-xs-6">
        <?= __d("Forms", "Fields marked with an asterisk ({0}) are required.", '<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-offset-3 col-xs-6">
        <?= $this->Form->create(null, ['id' => "signinform"]) ?>

        <fieldset>
            <legend><?= __d("Forms", 'Login') ?></legend>
            <?= $this->Form->input('email',
                [
                    'type' => "email",
                    "label" => ["class" => "control-form", "text" => __d("Forms", "{0} Email: ", '<span class = "asterisk">*</span>')],
                    "class" => "form-control",
                    "required" => "required",
                    "escape" => false,
                ]) ?>

            <?= $this->Form->input('password',
                [
                    "label" => ["class" => "control-form", "text" => __d("Forms", "{0} Password: ", '<span class = "asterisk">*</span>')],
                    "class" => "form-control",
                    "required" => "required",
                    "escape" => false,
                ])
            ?>
            <?= $this->Form->button(__d("Forms", 'Login'), ["class" => "btn btn-default addmore"]) ?>
            <?= $this->Form->end() ?>
        </fieldset>


        <?=
        $this->Html->link(__d("Users", "Lost your password ?"),
            ['prefix'=>false, 'controller'=>'users', 'action'=>'forgotPassword'],
            ['escape' => false, "class" => "btn btn-default addmore forgotPasswordLink"])
        ?>
    </div>
</div>
