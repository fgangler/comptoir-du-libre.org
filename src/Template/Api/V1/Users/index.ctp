<?php

$this->layout = 'base';
$this->assign('title', __d("Users",'Users.index.title'));
?>

<?php if (!empty( $users ) ): ?>

        <section>
            <?php
            echo $this->Lists->block($users,
                [
                    "type" => "user",
                    "title" => "<h1>" . __d("Users","Users list for Comptoir du libre ({0})",count($users)) ."</h1>",
                    "linkParticipate" => [
                        "id" => null,
                    ],
                    "form" => $this->element("Pages/SearchFormUsers"),
                    "emptyMsg" => __d("Users", "No user matching with your request in Comptoir du libre, yet."),
                ],
                false
            ); ?>
        </section>
<?php endif; ?>
