<?php
$this->layout('base');
$this->assign('title', __d("Users", "User {0}", $user->username));

?>
<section class="row">
    <div class=" col-sm-6 col-md-4 col-lg-4 hidden-xs">
        <div class="size-logo-overview">
            <?php if (!$this->User->hasLogo($user) || is_null($user->photo) || empty($user->photo)) : ?>

                <?php if ($user->user_type->name == 'Administration') : ?>
                      <?= $this->Html->image("logos/placeholder_user.administration.svg",
                        ["alt" => '', "class" => "img-responsive img-placeholder"]) ?>
                <?php elseif ($user->user_type->name == 'Association') : ?>
                    <?= $this->Html->image("logos/placeholder_user.association.svg",
                        ["alt" => '', "class" => "img-responsive img-placeholder"]) ?>
                <?php elseif ($user->user_type->name == 'Company') : ?>
                    <?= $this->Html->image("logos/placeholder_user.company.svg",
                        ["alt" => '', "class" => "img-responsive img-placeholder"]) ?>
                <?php else : ?>
                    <?= $this->Html->image("logos/placeholder_user.person.svg",
                            ["alt" => '', "class" => "img-responsive img-placeholder"]) ?>
                <?php endif; ?>
            <?php else : ?>
                <?= $this->Html->link($this->Html->image($user->logo_directory . DS . $user->photo,
                    [
                        "alt" => $user->username,
                        "class" => "img-responsive",
                        'rel' => 'nofollow noopener noreferrer'
                    ]),
                    isset($user->url) ? $user->url : "#",
                    ['escape' => false]) ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
        <ul class="list-unstyled">
            <li>
                <h1 id="userPageFirstTitle">
                    <?= $user->username ?>
                </h1>
            </li>
            <li>
                <?php
                    if ($user->url != "") {
                        echo $this->Html->link($user->url,
                                                $user->url,
                                                [
                                                    'rel' => 'nofollow noopener noreferrer',
                                                    'escape' => false
                                                ]);
                    }
                ?>
            </li>
            <li>
                <p>
                    <?php
                    echo __d("Users", "TypeOf.User");
                    if ($user->user_type->name == "Administration") {
                        echo __d("Users", "TypeOf.User.Administration");
                    } elseif ($user->user_type->name == "Company") {
                        echo __d("Users", "TypeOf.User.Company");
                    } elseif ($user->user_type->name == "Association") {
                        echo __d("Users", "TypeOf.User.Association");
                    } else {
                        echo __d("Users", "TypeOf.User.Person");
                    }
                    ?>
                </p>
            </li>
            <li>
                <?= $this->Text->autoParagraph($user->description); ?>
            </li>
            <li>
                <?php
                    echo $this->Action->participate([
                        "id" => $user->id,
                        "text" => __d("Users", "Contact"),
                        "action" => "contact",
                        'controller' => "Users",
                        'extraCssClass'  => "contact-user  inline-block",
                    ]);

                    // Add link (edit user profil):
                    // - if ID of connected user is same as ID of the current page user
                    // - if connected user can edit all users (role = admin)
                    if ($this->request->session()->read('Auth.User.id') === $user->id
                        | $this->request->session()->read('Auth.User.role') === 'admin' ) {

                        $userId = $user->id;
                        $url = "/fr/users/edit/$userId";
                        if($this->request->session()->read('Auth.User.id') === $user->id) {
                            // ID of connected user is same as ID of the current page user.
                            $linkText =  __d("Users", "userProfil.editLink");
                            $linkTitle =  __d("Users", "userProfil.editLink.title");
                        }
                        else if ($this->request->session()->read('Auth.User.role') === 'admin' ) {
                            // Connected user can edit all users (role = admin)
                            $linkText =  __d("Users", "admin.userProfil.editLink");
                            $linkTitle =  __d("Users", "admin.userProfil.editLink.title", $userId);
                        }
                        $linkEditUserProfil = $this->Html->link(
                            $linkText,
                            $url,
                            [
                                'class' => 'btn btn-default btn-info addmore link-user-edit-profil',
                                'id' => "link-user-edit-profil-$userId",
                                'title' => $linkTitle,
                            ]
                        );
                        echo $linkEditUserProfil;
                    }
                ?>
            </li>
        </ul>
    </div>
</section>
<!-- END of OVERVIEW-->
<?php /** TODO : Refactor contributor's section
 * <section>
 * <?php echo $this->Lists->softwareBlock(
 * \Cake\Utility\Hash::extract($user->contributionssoftwares, '{n}.software'),
 * [
 * "title" => __d("Users", "Contributor for"),
 * "link" => [
 * "id" => $user->id,
 * "action" => "contributionsSoftwares",
 * "participate" => __d("Users", " Add"),
 * ],
 * "tooManyMsg" => __d("Layout", "See all"),
 * "titleSeeAll" => __d("Users", "See all softwares which {0} has contributed to.", $user->username),
 * "titleAddMore" => __d("Users", "Declare a software that {0} has contributed to.", $user->username),
 * "emptyMsg" => __d("Users", "{0} do not contribute to any project, yet.", $user->username)
 * ]
 * ); ?>
 * </section>
 **/ ?>
<section>
    <?php
    $softwareBlock = "";

    $user->user_type->name != 'Company' ?
        $softwareBlock = $this->Lists->block(
            $user->usedsoftwares,
            [
                "type" => "software",
                "title" => "<h2>" . __d("Users", "User of") . "</h2>",
                "linkSeeAll" => [
                    "url" => "/$selectedLanguage/users/usedSoftwares/$user->id",
                    "title" => __d("Users", "See all softwares used by {0}", $user->username),
                    "tooManyMsg" => __d("Layout", "See all"),
                ],
                "indicator" => [
                    "idTooltip" => "softwareListUserOfId",
                    "indicatorMessage" => __d("Users", "users.softwareListUserOf", $user->username)
                ],
                "titleAddMore" => __d("Users", "Declare a software that is used by {0}.", $user->username),
                "emptyMsg" => __d("Users", "{0} do not used a software, yet.", $user->username)
            ]
        )
        : "";
    echo $softwareBlock;
    ?>

    <?php
        // Mapping display for current user
        if ($user->user_type->name === 'Administration' && count($taxonomiesSoftware) > 0 ) {
            echo $this->element("Users/mappingOnUserPage");
        }
    ?>
</section>
<section>
    <?php
    echo $this->Lists->block(
        $user->providerforsoftwares,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Users", "Services provider for") . "</h2>",
            "linkSeeAll" => [
                "url" => "/$selectedLanguage/users/providerforSoftwares/$user->id",
                "title" => __d("Users", "See all softwares which {0} is a service provider for.", $user->username),
                "tooManyMsg" => __d("Layout", "See all"),
            ],
            'LinkParticipate'=>[],
            "indicator" => [
                "idTooltip" => "ServiceProviderForSoftwareListId",
                "indicatorMessage" => __d("Users", "users.ServiceProviderForSoftwareList", $user->username)
            ],
            "titleAddMore" => __d("Users", "Declare a software that {0} is a service provider for.", $user->username),
            "emptyMsg" => __d("Users", "{0} do not provide a software, yet.", $user->username)
        ],
        true
    ); ?>
</section>
<section class="clearfix">
    <?php
    echo $this->Lists->block(
        $user->reviews,
        [
            "type" => "review",
            "title" => "<h2>" . __d("Users", "Users.View.Reviews.Title", count($user->reviews)) . "</h2>",
            "linkSeeAll" => [
                "url" => "/$selectedLanguage/users/$user->id/reviews",
                "title" => __d("Users", "Users.View.Reviews.Message.SeeAll", $user->username),
                "tooManyMsg" => __d("Layout", "See all ({0} reviews)", count($user->reviews)),
            ],
            'LinkParticipate'=>[],
            "indicator" => [
                "idTooltip" => "reviewsListId",
                "indicatorMessage" => __d("Users", "Users.View.Reviews.IndicatorMessage", $user->username)
            ],
            "titleAddMore" => __d("Users", "Users.View.username", $user->username),
            "emptyMsg" => __d("Users", "Users.View.Reviews.NoOne", $user->username)
        ]
    ); ?>
</section>
