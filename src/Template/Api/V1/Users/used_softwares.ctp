<?php
$this->layout('base');
$this->assign('title', __d("Users", "{0} is user of",$user->username));
?>

<?php echo $this->Lists->block(
    $user->usedsoftwares,
    [
        "type" => "software",
        "title" => "<h1>" . __d("Users", "User of") . "</h1>",
        "link" => [
            "id" => $user->id,
            "action" => "UsedSoftwares",
            "participate" => __d("Users", "Declare a software that is used by {0}.", $user->username),
        ],
        "titleAddMore" => __d("Users", "Declare a software that is used by {0}.", $user->username),
    ],
    false
); ?>
