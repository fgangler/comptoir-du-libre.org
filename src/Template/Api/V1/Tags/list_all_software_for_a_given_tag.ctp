<?php
$this->layout('base');
$this->assign('title', $tag->name);

?>
<section class="clearfix row">
    <ul class="list-unstyled col-xs-12">

        <li>
            <h1><?= __d("Tags", "Tag") . " " . $tag->name ?></h1>
        </li>
        <?php
            if($selectedLanguage === 'fr' && !empty($tag->description_i18n_fr)) {
                echo "<li>$tag->description_i18n_fr</li>";
            } else if($selectedLanguage === 'en' && !empty($tag->description_i18n_en)) {
                echo "<li>$tag->description_i18n_en</li>";
            }
        ?>
    </ul>
</section>

<section class="clearfix">
    <?php
    echo $this->Lists->block(
        $tag->softwares,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Tags", "SoftwareListTag") . $tag->name . " (" . count($tag->softwares) . ")" . "</h2>",
            "titleAddMore" => "",
            "titleSeeAll" => "",
            "emptyMsg" => __d("Tags", "tags.view.error", $tag->name),
        ],
        false

    ); ?>

</section>
