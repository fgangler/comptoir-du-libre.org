<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Softwares Tag'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Softwares'), ['controller' => 'Softwares', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Software'), ['controller' => 'Softwares', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tags'), ['controller' => 'Tags', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tag'), ['controller' => 'Tags', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="softwaresTags index large-9 medium-8 columns content">
    <h3><?= __('Softwares Tags') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('software_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tag_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($softwaresTags as $softwaresTag): ?>
            <tr>
                <td><?= $this->Number->format($softwaresTag->id) ?></td>
                <td><?= $softwaresTag->has('software') ? $this->Html->link($softwaresTag->software->softwarename, ['controller' => 'Softwares', 'action' => 'view', $softwaresTag->software->id]) : '' ?></td>
                <td><?= $softwaresTag->has('tag') ? $this->Html->link($softwaresTag->tag->name, ['controller' => 'Tags', 'action' => 'view', $softwaresTag->tag->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $softwaresTag->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $softwaresTag->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $softwaresTag->id], ['confirm' => __('Are you sure you want to delete # {0}?', $softwaresTag->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
