<?php
$this->layout('base');

$this->assign('title', __d("Softwares", "{0}", $software->softwarename));
?>
<!-- ------------------------------------------ Overview ----------------------------------------------------------- -->
<section class="row clearfix">
    <?php echo $this->element("Softwares/overview") ?>
</section>
<!-- ------------------------------------------------ End overview ------------------------------------------------ -->

<section class="clearfix">
<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///// ADMIN only - External ressources (wikidata, SILL, framalibre, wikipédia, CNLL) ////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // if connected user can display external ressources (role = admin)
        if ($this->request->session()->read('Auth.User.role') === 'admin' ) {

            $linkWikidata = '';
            $linkWikidataJson = '';
            if (!empty($software->wikidata)) {
                $linkWikidata .= '<a id="external-link_wikidata_'.$software->id.'" ';
                $linkWikidata .= '   title="Consulter la fiche WikiData du logiciel '.$software->softwarename.'" ';
                $linkWikidata .= '   href="https://www.wikidata.org/wiki/'.$software->wikidata.'">';
                $linkWikidata .= 'Consulter la fiche  <strong>wikidata.org</strong></a>';

                $linkWikidataJson .= '<a id="external-link_wikidataJson_'.$software->id.'" ';
                $linkWikidataJson .= '   title="Consulter les données WikiData du logiciel '.$software->softwarename.' au format JSON" ';
                $linkWikidataJson .= '   href="https://www.wikidata.org/wiki/Special:EntityData/'.$software->wikidata.'.json">';
                $linkWikidataJson .= $software->wikidata .'</a>';
            }
            $linkSill = '';
            if (!empty($software->sill)) {
                $linkSill .= '<a id="external-link_sill_'.$software->id.'" ';
                $linkSill .= '   title="Consulter la fiche du logiciel '.$software->softwarename.' sur l\'annuaire du SILL" ';
                $linkSill .= '   href="https://code.gouv.fr/sill/fr/software?id='. $software->sill.'">';
                $linkSill .= 'Consulter la fiche <strong>SILL</strong> </a>';
            }
            $linkCnll = '';
            if (!empty($software->cnll)) {
                $linkCnll .= '<a id="external-link_cnll_'.$software->id.'" ';
                $linkCnll .= '   title="Consulter la fiche du logiciel '.$software->softwarename.' sur l\'annuaire du CNLL" ';
                $linkCnll .= '   href="https://annuaire.cnll.fr/solutions/'. $software->cnll.'">';
                $linkCnll .= 'Consulter la fiche <strong>CNLL</strong> </a>';
            }
            $linkFramalibre = '';
            if (!empty($software->framalibre)) {
                $linkFramalibre .= '<a id="external-link_framalibre_'.$software->id.'" ';
                $linkFramalibre .= '   title="Consulter la fiche du logiciel '.$software->softwarename.' sur l\'annuaire du FramLibre" ';
                $linkFramalibre .= '   href="https://framalibre.org/content/'. $software->framalibre.'">';
                $linkFramalibre .= 'Consulter la fiche <strong>FramaLibre</strong> </a>';
            }
            $linkWikipediaEn = '';
            if (!empty($software->wikipedia_en)) {
                $linkWikipediaEn .= '<a id="external-link_wikipedia-en_'.$software->id.'" ';
                $linkWikipediaEn .= '   title="Consulter la page Wikipédia en anglais du logiciel '.$software->softwarename.'" ';
                $linkWikipediaEn .= '   href="https://en.wikipedia.org/wiki/'. $software->wikipedia_en.'">';
                $linkWikipediaEn .= 'Consulter la page <strong>Wikipédia</strong> en <strong>anglais</strong> </a>';
            }
            $linkWikipediaFr = '';
            if (!empty($software->wikipedia_fr)) {
                $linkWikipediaFr .= '<a id="external-link_wikipedia-fr_'.$software->id.'" ';
                $linkWikipediaFr .= '   title="Consulter la page Wikipédia en français du logiciel '.$software->softwarename.'" ';
                $linkWikipediaFr .= '   href="https://fr.wikipedia.org/wiki/'. $software->wikipedia_fr.'">';
                $linkWikipediaFr .= 'Consulter la page <strong>Wikipédia</strong> en <strong>français</strong> </a>';
            }
?>
    <div class="only-for-admin" id="only-for-admin_software-external-ressources">
        <h2>Ressources externes <small>visible uniquement par les admins</small>  </h2>
        <table>
            <tr>
                <th>Ressource</th>
                <th>Identifiant</th>
                <th>URL</th>
            </tr>
            <tr>
                <td>SILL</td>
                <td><?php echo $software->sill; ?></td>
                <td><?php echo $linkSill; ?></td>
            </tr>
            <tr>
                <td>Wikidata</td>
                <td><?php echo $linkWikidataJson; ?></td>
                <td><?php echo $linkWikidata ?></td>
            </tr>
            <tr>
                <td>Wikipédia EN</td>
                <td><?php echo $software->wikipedia_en; ?></td>
                <td><?php echo $linkWikipediaEn ?></td>
            </tr>
            <tr>
                <td>Wikipédia FR</td>
                <td><?php echo $software->wikipedia_fr; ?></td>
                <td><?php echo $linkWikipediaFr ?></td>
            </tr>
            <tr>
                <td>FramaLibre</td>
                <td><?php echo $software->framalibre; ?></td>
                <td><?php echo $linkFramalibre ?></td>
            </tr>
            <tr>
                <td>CNLL</td>
                <td><?php echo $software->cnll; ?></td>
                <td><?php echo $linkCnll ?></td>
            </tr>
        </table>
    </div>
<?php
        }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///// END ---> ADMIN only - External ressources (wikidata, sill, framalibre, wikipédia) /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>


    <section>
    <?php
      $htmlUsedBy =  $this->Lists->block(
            $software->userssoftwares,
            [
                "type" => "user",
                "title" => "<h2>" . __d("Softwares", "Users of {0}", $software->softwarename) . "</h2>",
                "linkSeeAll" => [
                    "url" => "/$selectedLanguage/softwares/usersSoftware/$software->id",
                    "title" => __d("Softwares", "See all declared users of {0}", $software->softwarename),
                    "tooManyMsg" => __d("Layout", "See all ({0} users)", count($software->userssoftwares)),
                ],
                "linkParticipate" => [
                    "id" => $software->id,
                    "action" => "usersSoftware",
                    "text" => __d("Softwares", "Softwares.Users.DeclareAs.user.addMessage", $software->softwarename),
                ],
                "linkFallBack" => [
                    "id" => $software->id,
                    "action" => "deleteUsersSoftware",
                    "text" => __d("Softwares", "Softwares.Users.DeclareAs.user.removeMessage", $software->softwarename),
                ],
                "linkEdit" => [
                    "id" => $software->id,
                    "controller" => "TaxonomysSoftwares",
                    "action" => "mappingForm",
                    "url" => "/$selectedLanguage/mappingForm/". $software->id,
                    "text" => __d("Softwares", "Softwares.Users.DeclareAs.updateTaxonomy"),
                ],
                "indicator" => [
                    "idTooltip" => "usersOfListId",
                    "indicatorMessage" => __d("Softwares", "softwares.usersOfList", $software->softwarename)
                ],
                "titleAddMore" => __d("Softwares", "Softwares.Users.DeclareAs.user.addMessage", $software->softwarename),
                "titleFallBack" => __d("Softwares", "Softwares.Users.DeclareAs.user.removeMessage.", $software->softwarename),
                "emptyMsg" => __d("Softwares", "No user for {0}", $software->softwarename)
            ]
        );
        echo $htmlUsedBy;
    ?>
    </section>

    <?php
        // Extract buttons for mapping section and rename some HTML ids
        // @@@TODO ---> must be refactorized
        $mappingButtons = '';
        if ( 'Administration' === $this->request->session()->read("Auth.User.user_type")) {
            $startPattern = '<div class="container_btn-participate">';
            $endPattern = '</div><!-- END class "container_btn-participate" -->';
            $mappingButtons = strstr($htmlUsedBy, $startPattern);
            $mappingButtons = strstr($mappingButtons, $endPattern, true);
            $mappingButtons = "$mappingButtons $endPattern <div class=\"clearFloat\"></div>";
            $mappingButtons = str_replace('Form_', 'FormMapping_', $mappingButtons);
            $mappingButtons = str_replace('btn_', 'btnMapping_', $mappingButtons);
        }
    ?>
    <section class="clearfix">
        <div class="align">
            <h2> <?= __d("Taxonomy", "Taxonomy.softwarePage.sectionTitle", $software->softwarename); ?></h2>
            <?php echo $mappingButtons; ?>
        </div>
        <?php
            // Mapping display for current software
            $mappingNoRecord = __d("Taxonomy", "Taxonomy.softwarePage.sectionNoRecord", $software->softwarename);
            if (count($taxonomiesSoftware) > 0) {
                echo $this->element("Softwares/mappingOnSoftwarePage");
            } elseif (count($taxonomiesPreSelection) > 0) {
                echo "<p id=\"mappingForSoftware-noRecord\">$mappingNoRecord</p>";
                echo $this->element("Softwares/mappingPreSelectionOnSoftwarePage");
            } else {
                echo "<p id=\"mappingForSoftware-noRecord\">$mappingNoRecord</p>";
            }
        ?>
    </section>

    <section class="clearfix">
        <?php
        echo $this->Lists->block(
            $software->reviews,
            [
                "type" => "review",
                "title" => "<h2>" . __d("Softwares", "Reviews for {0}", $software->softwarename) . "</h2>",
                "linkSeeAll" => [
                    "url" => "/$selectedLanguage/softwares/$software->id/reviews",
                    "title" => __d("Softwares", "See all reviews of {0}", $software->softwarename),
                    "tooManyMsg" => __d("Layout", "See all ({0} reviews)", count(\Cake\Utility\Hash::extract($software->reviews, '{n}'))),
                ],
                "linkParticipate" => [
                    "id" => $software->id,
                    "action" => "addReview",
                    "text" => __d("Softwares", "Sofwares.Review.addMessage"),
                    "method" => "GET",
                ],
                "indicator" => [
                    "idTooltip" => "reviewsListId",
                    "indicatorMessage" => __d("Softwares", "softwares.reviewsList", $software->softwarename)
                ],
                "titleAddMore" => __d("Softwares", "Sofwares.Review.addMessage", $software->softwarename),
                "emptyMsg" => __d("Softwares", "No review for {0}.", $software->softwarename)
            ]
        ); ?>
    </section>

    <section>
        <?php echo $this->Lists->block(
            $software->providerssoftwares,
            [
                "type" => "user",
                "title" => "<h2>" . __d("Softwares", "Service providers for {0}", $software->softwarename) . "</h2>",
                "linkSeeAll" => [
                    "url" => "/$selectedLanguage/softwares/servicesProviders/$software->id",
                    "title" => __d("Softwares", "See all service providers of {0}", $software->softwarename),
                    "tooManyMsg" => __d("Layout", "See all ({0} services providers)", count($software->providerssoftwares)),
                ],
                "linkParticipate" => [
                    "id" => $software->id,
                    "action" => "servicesProviders",
                    "text" => __d("Softwares", "Softwares.Users.DeclareAs.serviceProvider.addMessage", $software->softwarename),
                ],
                "linkFallBack" => [
                    "id" => $software->id,
                    "action" => "deleteServicesProviders",
                    "text" => __d("Softwares", "Softwares.Users.DeclareAs.serviceProvider.removeMessage", $software->softwarename),
                ],
                "indicator" => [
                    "idTooltip" => "serviceProvidersOfListId",
                    "indicatorMessage" => __d("Softwares", "softwares.serviceProvidersOfList", $software->softwarename)
                ],
                "titleAddMore" => __d("Softwares", "Softwares.Users.DeclareAs.serviceProvider.addMessage", $software->softwarename),
                "emptyMsg" => __d("Softwares", "No service provider for {0}.", $software->softwarename)
            ]
        ); ?>
    </section>

    <?php echo $this->Lists->block(
        $software->screenshots,
        [
            "type" => "screenshot",
            "title" => "<h2>" . __d("Softwares", "Screenshots of {0}", $software->softwarename) . "</h2>",
            "linkSeeAll" => [
                "url" => "/$selectedLanguage/softwares/$software->id/screenshots",
                "title" => __d("Softwares", "See all screenshots of {0}", $software->softwarename),
                "tooManyMsg" => __d("Layout", "See all ({0} screenshots)", count($software->screenshots)),
            ],
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "addScreenshot",
                "text" => __d("Softwares", "Add a screenshot for {0}.", $software->softwarename),
                "class" => "btn btn-default inactive-button",
            ],
            "indicator" => [
                "idTooltip" => "sreenshotsListId",
                "indicatorMessage" => __d("Softwares", "softwares.sreenshotsList", $software->softwarename)
            ],
            "titleAddMore" => __d("Softwares", "Softwares.Screenshots.addMessage", $software->softwarename),
            "emptyMsg" => __d("Softwares", "No screenshot for {0}.", $software->softwarename)
        ]
    ); ?>
</section>

<section>
    <?php echo $this->Lists->block(
        $software->workswellsoftwares,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Softwares", "Working well with {0}", $software->softwarename) . "</h2>",
            "linkSeeAll" => [
                "url" => "/$selectedLanguage/softwares/worksWellSoftwares/$software->id",
                "title" => __d("Softwares", "See all softwares working well with {0}", $software->softwarename),
                "tooManyMsg" => __d("Layout", "See all ({0} softwares)", count($software->workswellsoftwares)),
            ],
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "addWorksWellWith",
                "text" => __d("Softwares", "Add a software that works well with {0}.", $software->softwarename),
                "class" => "btn btn-default inactive-button",
            ],
            "indicator" => [
                "idTooltip" => "softwaresWorkingWellWithListId",
                "indicatorMessage" => __d("Softwares", "softwares.softwaresWorkingWellWithList", $software->softwarename)
            ],
            "titleAddMore" => __d("Softwares", "Add a software that works well with {0}.", $software->softwarename),
            "emptyMsg" => __d("Softwares", "There are no project for {0}", $software->softwarename)
        ]
    ); ?>
</section>


<section>
    <?php echo $this->Lists->block(
        $software->alternativeto,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Softwares", "Alternative to {0}", $software->softwarename) . "</h2>",
            "linkSeeAll" => [
                "url" => "/$selectedLanguage/softwares/alternativeTo/$software->id",
                "title" => __d("Softwares", "See all alternatives to {0}", $software->softwarename),
                "tooManyMsg" => __d("Layout", "See all ({0} softwares)", count($software->alternativeto)),
            ],
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "addAlternativeTo",
                "text" => __d("Softwares", "Add a software that is an alternative to {0}.", $software->softwarename),
                "class" => "btn btn-default inactive-button",
            ],
            "indicator" => [
                "idTooltip" => "softwaresAlternativeToListId",
                "indicatorMessage" => __d("Softwares", "softwares.softwaresAlternativeToList", $software->softwarename)
            ],
            "titleAddMore" => __d("Softwares", "Add a software that is an alternative to {0}.", $software->softwarename),
            "emptyMsg" => __d("Softwares", "No alternative to {0}.", $software->softwarename)
        ]
    ); ?>
</section>

