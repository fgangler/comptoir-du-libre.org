<?php
$this->layout('base');

$this->assign('title', __d("Softwares", "softwares.softwaresWorkingWellWithList", $software->softwarename));
?>

<section>
    <?php
//    // TODO  Fil d'ariane
//    $softwareId   = $software->id;
//    $softwareName = $software->softwarename;
//    $breadcrumb = [
//        "/softwares/" => __d("ElementNavigation", "All softwares"),
//        "/softwares/$softwareId" => $softwareName,
//    ];
//    echo '<a href="/">Home</a>';
//    foreach($breadcrumb as $url => $name){
//        echo " > <a href=\"$url\">$name</a>";
//    }

    echo $this->Lists->block(
        $software->workswellsoftwares,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Softwares", "softwares.softwaresWorkingWellWithList", $software->softwarename) . "</h2>",
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "",
                "text" => __d("Softwares", "Add a software that works well with {0}.", $software->softwarename),
                "class" => "btn btn-default inactive-button",
            ],
            "titleAddMore" => __d("Softwares", "Add a software that works well with {0}.", $software->softwarename),
            "emptyMsg" => __d("Softwares", "There are no project for {0}", $software->softwarename)
        ],
        false
    );
    ?>
</section>
