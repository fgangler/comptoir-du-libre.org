<?php
    $this->layout = 'base';
    $this->assign('title', __d("Softwares",'Softwares.index.title'));

    // View debug json only for connected user with role = admin
    if ($this->request->session()->read('Auth.User.role') === 'admin' ) {
        if(isset($export) && isset($exportFile)) {
            echo "<h1 id=\"export-json_displayed\">JSON export</h1>";
            echo "<a id=\"link_export-json_displayed\" href=\"/$exportFile\">JSON export</a><hr>";
            echo "<pre>$export</pre><hr>";
        }
        else {
            echo "<h1 id=\"error_export-json_displayed\">Error: JSON export not available</h1>";
        }
    }
    else {
        if(isset($export) && isset($exportFile)) {
            echo '<div id="export-json_not-displayed"></div>';
        }
        else {
            echo '<div id="error_export-json_not-displayed"></div>';
        }
    }
?>

