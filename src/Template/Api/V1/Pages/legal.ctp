<?php

$this->layout = 'base';
$this->assign('title', __d("Home", "Legal"));

?>
<h1>
    <?= __d("Home", "Legal") ?>
</h1>

<h2>
    <?= __d("Home", "Postal Adress") ?>
</h2>
<p>
    5 rue du plan du palais 34000 Montpellier, FRANCE
</p>

<h3>
    <?= __d("Home", "Editor") ?>
</h3>
<p>
    ADULLACT
</p>

<h3>
    <?= __d("Home", "Publication director") ?>
</h3>
<p>
    <?= __d("Home", "Pascal Kuczynski, General Director of ADULLACT") ?>
</p>

<h3>
    <?= __d("Home", "Hosting") ?>
</h3>
<p>
    OVH : 2 rue Kellermann 59100 Roubaix, FRANCE
</p>

<h3>
    <?= __d("Home", "Contact") ?>
</h3>
<p>
    <?= __d("Home", "Email address :") ?>
    <code>comptoir · adullact.org</code>
</p>

