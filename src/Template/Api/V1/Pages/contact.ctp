<?php

$this->layout = 'base';
$this->assign('title', __d("Home", "Contact the Comptoir"));

?>
<h1><?= __d("Home", "Contact the Comptoir") ?></h1>
<ul class="list-unstyled">
    <li>
        <i class="glyphicon glyphicon-user"></i> Matthieu FAURE<?= __d("Home", " : Project leader") ?>
    </li>
    <li>
        <i class="glyphicon glyphicon-phone-alt"></i><?= __d("Home", "  Phone : +33 4 67 65 05 88 ") ?>
    </li>
    <li>
        <i class="glyphicon glyphicon-envelope"></i><?= __d("Home", " Courriel : ") ?>
        <code>comptoir · adullact.org</code>
    </li>
    <li>
        <i class="glyphicon glyphicon-map-marker"></i><?= __d("Home", " Address : ") ?> Association Adullact, 5 rue du
        plan du palais, 34000 Montpellier
    </li>
</ul>
