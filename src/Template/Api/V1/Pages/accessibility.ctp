<?php
$this->layout = 'base';
$this->assign('title', __d("Home", "page-accessibility-title"));
?>
<h1><?= __d("Home", "page-accessibility-h1") ?></h1>

<?php
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ($selectedLanguage === 'fr') { // French
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>
    <p>Cette déclaration d’accessibilité s’applique au site web <strong>Comptoir du Libre</strong>
        (<code>https://comptoir-du-libre.org</code>).</p>

    <h2>État de conformité</h2>
    <p> Le site web <strong>Comptoir du Libre</strong> est
        <strong>non conforme</strong>
        avec le <abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>.
    </p>
    <p> Le site web n’a encore pas été audité.</p>

    <h2>Établissement de cette déclaration d’accessibilité</h2>
    <p>Cette déclaration a été établie le <span>26 décembre 2022</span>.</p>

    <h2>Résultat des tests</h2>
    <p>En l’absence d’audit de conformité il n’y a pas de résultats de tests.</p>

    <h2>Contenus non accessibles</h2>

    <h3>Non conformité</h3>
    <p> En l’absence d’audit tous les contenus seront considérés comme non accessibles par hypothèse.</p>

    <h3>Dérogations pour charge disproportionnée</h3>
    <p> En l’absence d’audit aucune dérogation n’a été établie.</p>

    <h3>Contenus non soumis à l’obligation d’accessibilité</h3>
    <p> En l’absence d’audit aucun contenu n’a été identifié comme n’entrant
        pas dans le champ de la législation applicable.</p>

    <h3>Agents utilisateurs, technologies d’assistance et outils utilisés pour vérifier l’accessibilité</h3>
    <p> En l’absence d’audit aucun agent utilisateur et aucune technologie d’assistance n’ont été utilisés.</p>

    <h3>Pages du site ayant fait l’objet de la vérification de conformité</h3>
    <p> En l’absence d’audit aucune page n’a fait l’objet de la vérification de conformité.

    <h2>Amélioration et contact</h2>
    <p>Si vous n’arrivez pas à accéder à un contenu ou à un service, vous pouvez
        <a href="/fr/pages/contact">contacter le responsable du <em>Comptoir du Libre</em></a>
        pour être orienté vers une alternative accessible ou obtenir le contenu sous une autre forme.</p>

    <h2>Voie de recours</h2>
    <p>Cette procédure est à utiliser dans le cas suivant&nbsp;: vous avez signalé
        au responsable du site internet un défaut d’accessibilité qui vous empêche d’accéder
        à un contenu ou à un des services du portail et vous n’avez pas obtenu de réponse satisfaisante.</p>
    <p>Vous pouvez&nbsp;:</p>
    <ul>
        <li>Écrire un message au <a rel="nofollow noopener noreferrer"
                                    href="https://formulaire.defenseurdesdroits.fr/">Défenseur des droits</a></li>
        <li>Contacter le <a  rel="nofollow noopener noreferrer"
                          href="https://www.defenseurdesdroits.fr/saisir/delegues">délégué du Défenseur
                des droits dans votre région</a></li>
        <li>Envoyer un courrier par la poste (gratuit, ne pas mettre de timbre)&nbsp;:<br>
            Défenseur des droits <br>
            Libre réponse 71120 <br>
            75342 Paris CEDEX 07</li>
    </ul>

<?php
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
    else { // English
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>
<p>
   <?=
    __d("Home",
        "Accessibility conformance is in progress (<abbr title=\"Web Content Accessibility Guidelines\">WCAG</abbr> / <span lang=\"fr\"><abbr title=\"Référentiel Général d'Accessibilité des Administrations\">RGAA</abbr></span>)")
    ?>
</p>
<?php
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
?>
