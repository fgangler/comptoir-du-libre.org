<?php
/**
 *      URL: /api/v1/taxonomys/mappingTaxonUsersOf/<id>  ---> redirect to '/en/mapping/'
 *      URL: /en/mapping/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId>
 *           /fr/cartographie/<slugPrimaryLevel>/<slugTaxon>/<slugSoftware>/<taxonId>.<softwareId>
 *
 *      ex:  /fr/cartographie/generiques/bureautique/libreoffice/20.33
 *           /en/mapping/generics/office-automation/libreoffice/20.33
 */

use Cake\I18n\I18n;
$this->layout = 'base';

// Prepare view parameters
$software = $data[$softwareId]['software'];
$users = $data[$softwareId]['users'];
$userUpdateDates = $data[$softwareId]['userUpdateDates'];
$lastUpdate = $data[$softwareId]['lastUpdate'];
$numberOfComments = $data[$softwareId]['numberOfComments'];
$comments = [];
if (isset($data[$softwareId]['comments'])) {
    $comments = $data[$softwareId]['comments'];
}

// Format the page update date
$pageUpdateDate = $this->Time->format($lastUpdate, [IntlDateFormatter::LONG, -1], I18n::locale());
        // formats : [IntlDateFormatter::SHORT,  -1]  ---> "24/04/2020"            | "4/24/20"
        //           [IntlDateFormatter::MEDIUM, -1]  ---> "Apr 24, 2020"           | "Apr 24, 2020"
        //           [IntlDateFormatter::LONG,   -1]  ---> "24 avr. 2020"           | "April 24, 2020"
        //           [IntlDateFormatter::FULL,   -1]  ---> "vendredi 24 avril 2020" | "Friday, April 24, 2020"
        //           [-1, IntlDateFormatter::SHORT]   ---> "14:11"                  | "2:11 PM"
        //           [-1, IntlDateFormatter::MEDIUM]  ---> "14:11:04"               | "2:11:04 PM"
        //           [-1, IntlDateFormatter::LONG]    ---> "24 avr. 2020"           | "April 24, 2020"
        //           [-1, IntlDateFormatter::FULL]    ---> "vendredi 24 avril 2020" | "Friday, April 24, 2020"

$softwareName = $software->softwarename;
$softwareLink = $this->Html->link(
    $softwareName,
    "/$selectedLanguage/softwares/". $softwareId,
    ["title" => __d("Softwares", "software.softwareName") . $software->softwarename]
);
$headTitle = __d("Taxonomy", "TaxonomySoftware.UserOf.Software_forOneTaxon.Title", $softwareName, $taxonName);
$h1 =  __d("Taxonomy", "TaxonomySoftware.UserOf.Software_forOneTaxon.H1", $softwareLink, $taxonName);
$subTitle =  __d("Taxonomy", "TaxonomySoftware.UserOf.Software_forOneTaxon.subTitle", $softwareName, $taxonName);
$attId = "mappingTaxon$taxonId-UsersOf$softwareId";

$this->assign('title', $headTitle);
?>
<section id="<?php echo $attId; ?>" class="mappingTaxonUsersOfPage taxonomyPages clearfix">
    <h1> <?php echo $h1; ?> </h1>
    <p>  <?php echo $subTitle; ?> </p>

    <?php echo $this->Lists->block(
        $users,
        [
            "type" => "user",
            "title" => '',
//            "linkParticipate" => [
//                "id" => $software->id,
//                "action" => "usersSoftware",
//                "text" => __d("Softwares", "Softwares.Users.DeclareAs.user.addMessage", $software->softwarename),
//            ],
//            "linkFallBack" => [
//                "id" => $software->id,
//                "action" => "fallBackusersSoftware",
//                "text" => __d("Softwares", "Softwares.Users.DeclareAs.user.removeMessage", $software->softwarename),
//            ],
            "titleAddMore" => "",
//            "titleFallBack" => __d("Softwares", "Roll back from users' list of {0}.", $software->softwarename),
        ],
        false
    ); ?>


    <?php
// Show TaxonomySoftwareComments ----> to be reviewed (refs. z881)
 /////////////////////////////////////////////////////////////////
//        if ($numberOfComments > 0) {
//            echo "<h2>".__d("Taxonomy", "TaxonomySoftware.UserOf.comments.title", $softwareName)."</h2>";
//            echo "<p>".__d("Taxonomy", "TaxonomySoftware.UserOf.comments.subtitle", $softwareName, $taxonName)."</p>";
//            foreach($userUpdateDates as $userId => $userUpdate) {
//                if (isset($comments[$userId])) {
//                    $txt = $this->Text->autoParagraph(h($comments[$userId]));
//                    $userName = $users[$userId]->username;
//                    $userUrl = "/$selectedLanguage/users/$userId";
//                    $userLink = $this->Html->link("$userName", $userUrl);
//                    $commentDate = $this->Time->format($userUpdate, [IntlDateFormatter::LONG, -1], I18n::locale());
//                    echo " <div id=\"$attId-comment-user$userId\" class=\"taxonomyComment\">
//                                $commentDate, <strong>$userLink</strong>
//                                <blockquote>  $txt </blockquote>
//                           </div>";
//                }
//            }
//        }
    ?>

    <div class="pageUpdate">
        <?= __d("Default", "updateOn", $pageUpdateDate);?>
    </div>
</section>
