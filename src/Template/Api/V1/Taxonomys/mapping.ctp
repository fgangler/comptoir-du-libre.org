<?php
    /**
     *      URL: /api/v1/taxonomys/mapping  ---> redirect to '/en/mapping/ '
     *           /fr/cartographie/
     *           /en/mapping/
     */
    $this->layout = 'base';
    $this->assign('title',  __d("Taxonomy", "Taxonomy.Index.Title"));
?>


<section id="mappingRoot" class="taxonomyPages taxonomyRootPage clearfix">
    <h1>
        <?= __d("Taxonomy", "Taxonomy.Index.H1")?>
     </h1>

    <?php
        $html = "";
        $list = [];
        if(count($mappingHead) > 0){
            foreach ($mappingHead as $primaryId => $primaryLevel) {
                $primaryName =  $mappingTaxons[$primaryId]['title'];
                $primarySlug =  $mappingTaxons[$primaryId]['slug'];
                $url = "$mappingBaseUrl/$primarySlug/";
                $linkPrimaryLevel =  $this->Html->link($primaryName, $url, ['class' => 'button']);
                $subLlist = [];
                $htmlSubLlist = "";
                if(isset($mappingTaxons[$primaryId]['children'])){
                    foreach ($mappingTaxons[$primaryId]['children'] as $subId => $subName) {
                        $subSlug = $mappingTaxons[$subId]['slug'];
                        $url = "$mappingBaseUrl/$primarySlug/$subSlug/$subId";
                        $subLlist[] = $this->Html->link($subName, $url);
                    }
                    $htmlSubLlist  = $this->Html->nestedList($subLlist);
                    $list[] = $linkPrimaryLevel .$htmlSubLlist ;
                }
            }
        }
        echo $this->Html->nestedList(
            $list,
            [ 'id' => 'taxonomy_primaryLevel'],
            ['class' => 'navHead']
        );
    ?>
</section>
