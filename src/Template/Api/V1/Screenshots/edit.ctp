<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $screenshot->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $screenshot->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Screenshots'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Softwares'), ['controller' => 'Softwares', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Software'), ['controller' => 'Softwares', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="screenshots form large-9 medium-8 columns content">
    <?= $this->Form->create($screenshot) ?>
    <fieldset>
        <legend><?= __('Edit Screenshot') ?></legend>
        <?php
            echo $this->Form->input('software_id', ['options' => $softwares, 'empty' => true]);
            echo $this->Form->input('name');
            echo $this->Form->input('photo');
            echo $this->Form->input('url_directory');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
