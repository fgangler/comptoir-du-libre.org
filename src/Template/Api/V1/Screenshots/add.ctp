<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Screenshots'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Softwares'), ['controller' => 'Softwares', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Software'), ['controller' => 'Softwares', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="screenshots form large-9 medium-8 columns content">
    <?= $this->Form->create($screenshot,['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Add Screenshot') ?></legend>
        <?php
            echo $this->Form->input('software_id', ['options' => $softwares, 'empty' => true]);
            //echo $this->Form->input('name');
            //echo $this->Form->input('photo');
            //echo $this->Form->input('url_directory');
            echo $this->Form->input('photo', ['type' => 'file',"label"=>"screenshots"]);

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
