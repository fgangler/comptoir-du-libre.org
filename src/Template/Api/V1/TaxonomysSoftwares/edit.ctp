<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $taxonomysSoftware->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $taxonomysSoftware->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Taxonomys Softwares'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Taxonomys'), ['controller' => 'Taxonomys', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Taxonomy'), ['controller' => 'Taxonomys', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Softwares'), ['controller' => 'Softwares', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Software'), ['controller' => 'Softwares', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="taxonomysSoftwares form large-9 medium-8 columns content">
    <?= $this->Form->create($taxonomysSoftware) ?>
    <fieldset>
        <legend><?= __('Edit Taxonomys Software') ?></legend>
        <?php
            echo $this->Form->input('taxonomy_id', ['options' => $taxonomys]);
            echo $this->Form->input('software_id', ['options' => $softwares]);
            echo $this->Form->input('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('comment', ['textarea' => h($taxonomysSoftware->comment), 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
