<?php
    /**
     *      URL: /api/v1/taxonomys-softwares/mappingForm/<softwareId>  ---> redirect to '/<lang>/mappingForm/<softwareId> '
     *           /fr/mappingForm/<softwareId>
     *           /en/mappingForm/<softwareId>
     */
    $this->layout = 'base';
    $softwareName = $software->softwarename;
    $userName = $user->username;
    $headTitle = __d("Taxonomy", "TaxonomySoftware.Form.Add.Title", h($softwareName));
    $htmlH1 = __d("Taxonomy", "TaxonomySoftware.Form.Add.H1", h($softwareName), h($userName));
    $formHelp1 = __d("Taxonomy", "TaxonomySoftware.Form.Add.Help", h($softwareName));
    $formHelp2 = __d("Taxonomy", "TaxonomySoftware.Form.Add.Help2");
    $formHelp3 = __d("Taxonomy", "TaxonomySoftware.Form.Add.Help3");
    $this->assign('title',  $headTitle);

    // Build the form
    $list = [];
    $htmlForm = '';
    foreach ($mappingHead as $primaryId => $primaryLevel) {
        $primaryName =  $mappingTaxons[$primaryId]['title'];
        $htmlSubLlist = "";
        if(isset($mappingTaxons[$primaryId]['children'])){
            foreach ($mappingTaxons[$primaryId]['children'] as $taxonId => $taxonName) {
                $checkBoxData = [
                    'type' => 'checkbox',
                    'value' => $taxonId,
                    'id' => "checkbox_$taxonId",
                    'label' => $taxonName,
                    'hiddenField' => false,
                ];
                if (isset($existingEntries[$taxonId])) { // existing entry for current user and this software
                    $checkBoxData['checked'] = 'checked';
                }
                $htmlSubLlist .= $this->Form->input('taxonCheckboxes[]', $checkBoxData);
            }
            $htmlForm .= "<fieldset id=\"\" class=\"form_fieldsetRadio\">
                                <legend>$primaryName</legend>
                                $htmlSubLlist
                          </fieldset>";
        }
    }
?>

<section id="mappingForm" class="taxonomyPages clearfix">
    <h1> <?php   echo $htmlH1;   ?>   </h1>
    <br>
    <p>  <?php   echo $formHelp1;   ?> </p>
    <?= $this->Form->create($software, [
        'type' => 'file',
        "enctype" => "multipart/form-data",
        'id' => "taxonomyForm_step1"
    ]) ?>
        <div class="taxonomyForm_step1_content">
            <?php echo $htmlForm; ?>
        </div>
        <div class="taxonomyForm_step1_footer">
            <?php
                // Additional form ----> to be reviewed (refs. z881)
                //////////////////////////////////////////////////////
                //    $commentLabel = "Informations complémentaires
                //            <br> <small style=\"font-weight: 100;\">Vous pouvez donner des précisions
                //            sur les utilisations
                //            <br> du logiciel  <strong>$softwareName</strong> par votre organisation.</small>";
                //    echo $this->Form->input(
                //        'comment', [
                //            "type"=>"textarea",
                //            "class" => "form-control",
                //            "label" => [
                //                "class"=>"control-label",
                //                "text"=> "$commentLabel" ,
                //                "escape"=>false,
                //            ],
                //        ]
                //    )
            ?>
            <?= $this->Form->hidden('id') ?>
            <?= $this->Form->button(
                __d("Taxonomy",'TaxonomySoftware.Form.Submit'),
                [
                    "class"=>"btn btn-default btn-mapping",
                    "id"=>"taxonomyFormButton",
                ]
            ) ?>

            <p>  <?php   echo $formHelp2;   ?> </p>
            <p>  <?php   echo $formHelp3;   ?> </p>
        </div>
    <?= $this->Form->end() ?>
</section>
