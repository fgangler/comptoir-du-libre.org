<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Taxonomys Software'), ['action' => 'edit', $taxonomysSoftware->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Taxonomys Software'), ['action' => 'delete', $taxonomysSoftware->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxonomysSoftware->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Taxonomys Softwares'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxonomys Software'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Taxonomys'), ['controller' => 'Taxonomys', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxonomy'), ['controller' => 'Taxonomys', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Softwares'), ['controller' => 'Softwares', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Software'), ['controller' => 'Softwares', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="taxonomysSoftwares view large-9 medium-8 columns content">
    <h3><?= h($taxonomysSoftware->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Taxonomy') ?></th>
            <td><?= $taxonomysSoftware->has('taxonomy') ? $this->Html->link($taxonomysSoftware->taxonomy->id .' -  '. $taxonomysSoftware->taxonomy->title_i18n_en, ['controller' => 'Taxonomys', 'action' => 'view', $taxonomysSoftware->taxonomy->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Software') ?></th>
            <td><?= $taxonomysSoftware->has('software') ? $this->Html->link($taxonomysSoftware->software->softwarename, ['controller' => 'Softwares', 'action' => 'view', $taxonomysSoftware->software->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $taxonomysSoftware->has('user') ? $this->Html->link($taxonomysSoftware->user->username, ['controller' => 'Users', 'action' => 'view', $taxonomysSoftware->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($taxonomysSoftware->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($taxonomysSoftware->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($taxonomysSoftware->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Comment') ?></th>
            <td><?= h($taxonomysSoftware->comment) ?></td>
        </tr>
    </table>
</div>
