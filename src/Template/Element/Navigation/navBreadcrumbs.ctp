<?php
if(isset($breadcrumbs)){
    $nb = count($breadcrumbs);
    if($nb > 1){
        $htmlBreadcrumbs  = '';
        $htmlBreadcrumbs .= '<nav aria-label="Vous êtes ici" >';
        $htmlBreadcrumbs .= '<ol class="breadcrumb">';

        $ListJsonBreadcrumb = [];
        foreach ($breadcrumbs as $key => $data) {
            $name = $data['name'];

            $linkActive = '';
            $liActive = '';
            if(($nb -1) === $key){
                $linkActive = ' aria-current="page" ';
                $liActive = '  class="active" ';
            }
            $htmlBreadcrumbs .= "<li $liActive>";

            $htmlTitle = '';
            if(strlen($name) > 35){
                $htmlTitle = " title=\"$name\" ";
                $name  = $this->Text->truncate($name, 35, ['ellipsis' => '…', 'exact' => false]);
            }

            if(isset($data['url'])) {
                $url = $data['url'];
                $htmlBreadcrumbs .= "<a $htmlTitle $linkActive href=\"$url\">$name</a>";
                $ListJsonBreadcrumb[] = [
                        "@type" => "ListItem",
                        "position" => $key + 1,
                        "name" => "$name",
                        "item" => $appFullBaseUrl ."$url"
                ];
                //  $appFullBaseUrl à pour valeur : 
                //      $_SERVER['REQUEST_SCHEME'] .'://' .  $_SERVER['HTTP_HOST']
            }
            else {
                $htmlBreadcrumbs .= "<spam $htmlTitle $linkActive>$name</spam>";
            }
            $htmlBreadcrumbs .= '</li>';
        }
        $htmlBreadcrumbs .= '</ol>';
        $htmlBreadcrumbs .= '</nav>';
        echo $htmlBreadcrumbs;

        $dataJsonBreadcrumb = [
            "@context" => "https://schema.org",
            "@type" => "BreadcrumbList",
            "itemListElement" => [$ListJsonBreadcrumb]
        ];
        $jsonBreadcrumb = json_encode($dataJsonBreadcrumb,  JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);
        if($jsonBreadcrumb !== FALSE){
            echo "\n<script type=\"application/ld+json\">\n$jsonBreadcrumb\n</script>\n";
        }
    }
}

?>
