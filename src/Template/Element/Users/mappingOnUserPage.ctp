<?php
    ///////////////////////////////////////////////////////////////
    // Mapping display for current user
    //
    // Used by Users/view.ctp
    ///////////////////////////////////////////////////////////////

    $list = [];
    $userName = $user->username;
    $userId = $user->id;
    foreach ($mappingFirstLevels as $firsLevelId => $firsLevelName) {
        if(isset($mappingTaxons[$firsLevelId]['children'])) {
            foreach ($mappingTaxons[$firsLevelId]['children'] as $taxonId => $taxonName) {
                if (isset($mappingTaxons[$taxonId]) && $taxonomiesSoftware[$taxonId]) {
                    $name = $mappingTaxons[$taxonId]['title'];
                    $slug = $mappingTaxons[$taxonId]['slug'];
                    $parentId = $mappingTaxons[$taxonId]['id_parent'];
                    $parentName = $mappingTaxons[$parentId]['title'];
                    $parentSlug = $mappingTaxons[$parentId]['slug'];

                    // Prepare software list for current taxon
                    $softwareList = [];
                    foreach ($taxonomiesSoftware[$taxonId] as $item) {
                        $softwareId = $item->software_id;
                        $usedSoftwareKey = $usedSoftwareKeys[$softwareId];
                        $software = $user->usedsoftwares[$usedSoftwareKey]->software;
                        $softwareSlug = $software->slugName;
                        $softwareName = $software->softwarename;
                        $linkSoftwareOptions = [
                            'title' => __d(
                                "Taxonomy",
                                "Taxonomy.userPage.taxonSoftwareLink.title",
                                [$software->softwarename, $parentName, $name]
                            ),
                            'id' => "linkMappingTaxon-$taxonId-Software-$softwareId",
                            'class' => "linkMappingTaxonSoftware",
                            'escape' => true
                        ];
                        // $htmlSoftwareImg = $this->Software->displayLogoLink($software);
                        $softwareUrl = "$mappingBaseUrl/$parentSlug/$slug/$softwareSlug/$taxonId.$softwareId";
                        $softwareList[] = $this->Html->link($softwareName, $softwareUrl, $linkSoftwareOptions);
                    }
                    $htmlSoftwareList = $this->Html->nestedList($softwareList);

                    // Display current taxon
                    $displayName = "$parentName ·  <strong>$name</strong>";
                    $linkOptions = [
                        'title' => __d("Taxonomy", "Taxonomy.userPage.taxonLink.title", [$parentName, $name]),
                        'id' => "linkMappingTaxon-$taxonId",
                        'class' => "linkMappingTaxon",
                        'escape' => false
                    ];
                    $url = "$mappingBaseUrl/$parentSlug/$slug/$taxonId";
                    $list[] =  $this->Html->link($displayName, $url, $linkOptions) . $htmlSoftwareList;
                }
            }
        }
    }

    $nbOfUseCases = count($list);
    if ($nbOfUseCases > 0) {
        $s = ''; if($nbOfUseCases > 1) { $s = 's'; }
        $mappingTxt =  __d("Taxonomy", "Taxonomy.userPage.description", [$s, $userName,]);
        $mappingTitle =  __d("Taxonomy", "Taxonomy.userPage.title", [$s,]);
        $htmlList = $this->Html->nestedList($list);
        echo "<div id=\"mappingForUser-$userId\" class=\"userPage_mapping\">
                <h2>$mappingTitle</h2>
                <p>$mappingTxt</p>
                $htmlList
          <div>";
    }

