<div class="flashMsgWrapper flashMsgWrapperSuccess" onclick="this.classList.add('hidden')">
    <div class="message success msgOnlyBefore"></div>
    <div class="message success msgNoBefore">
        <?php
            // autoriser uniquement les balises <br>
            echo str_replace('&lt;br&gt;','<br>', h($message));
        ?>
    </div>
</div>
