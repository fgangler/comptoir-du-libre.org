<?php

/**
 * This template conatins all informations for the header
 */

    // This template is used all the time (even on error pages).
    // But, on error pages, the $appVersion variable is not available.
    // We force its creation that is always available.
    if (!isset($appVersion)) {
        $appVersion = Cake\Core\Configure::read("VERSION.footer");
    }
?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- ****** faviconit.com favicons ****** -->
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.ico?$appVersion", ['type' => 'icon', 'rel' => 'shortcut icon']); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.16_16.ico?$appVersion", ['type' => 'icon', "sizes" => "16x16 32x32 64x64"]); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.196_196.png?$appVersion", ['type' => 'icon', "sizes" => "196x196"]); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.160_160.png?$appVersion", ['type' => 'icon', "sizes" => "160x160"]); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.96_96.png?$appVersion", ['type' => 'icon', "sizes" => "96x96"]); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.64_64.png?$appVersion", ['type' => 'icon', "sizes" => "64x64"]); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.32_32.png?$appVersion", ['type' => 'icon', "sizes" => "32x32"]); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.16_16.png?$appVersion", ['type' => 'icon', "sizes" => "16x16"]); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.svg?$appVersion", ['type' => 'icon', 'rel' => 'apple-touch-icon']); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.114_114.png?$appVersion", ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '114x114']); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.72_72.png?$appVersion", ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '72x72']); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.144_144.png?$appVersion", ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '144x144']); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.60_60.png?$appVersion", ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '60x60']); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.120_120.png?$appVersion", ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '120x120']); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.76_76.png?$appVersion", ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '76x76']); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.152_152.png?$appVersion", ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '152x152']); ?>
    <?= $this->Html->meta('favicon.ico', "img/favicon/CDL-Favicon.180_180.png?$appVersion", ['type' => 'icon', 'rel' => 'apple-touch-icon', 'sizes' => '180x180']); ?>
<?php
//    <meta name="msapplication-TileColor" content="#FFFFFF">
//    <meta name="msapplication-TileImage" content="favicon/favicon-144.png">
//    <meta name="msapplication-config" content="favicon/browserconfig.xml">
?>

    <?= $this->fetch('title') ?  '<title>' . $this->fetch('title') . '</title>' : '<title>' . __d("Layout","Comptoir du libre")  . '</title>'  ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php // OPEN GRAPH ?>
    <?php if (isset($openGraph)): ?>
        <?= $this->OpenGraph
        ->setTitle($openGraph["title"])
        ->setDescription($openGraph["description"])
        ->setImage($openGraph["image"]."?$appVersion", $openGraph["imageSize"])
        ->render(); ?>
    <?php endif ; ?>

    <?php if (isset($card)): ?>
    <?= $this->Card
        ->setTitle($card["title"])
        ->setCard(\Cake\Core\Configure::read("OpenGraph.card"))
        ->setDescription($card["description"])
        ->setImage($card["image"]."?$appVersion", $card["imageSize"])
        ->render(); ?>
    <?php endif ; ?>

    <?= $this->Html->css('bootstrap/css/bootstrap.min.css?'. $appVersion) ?>
    <?= $this->Html->css('font-awesome-4.7.0/css/font-awesome.min.css?'. $appVersion) ?>
    <?= $this->Html->css('comptoir.css?'. $appVersion) ?>

    <?= $this->Html->script('jquery/jquery.min.js?'. $appVersion) ?>
    <?= $this->Html->script('bootstrap/js/bootstrap.min.js?'. $appVersion) ?>
    <?= $this->Html->script('bootstrap/js/simpleAccessible.js?'. $appVersion) ?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
