<?= $this->Form->create(null, ['class' => "form-inline", 'title' => __d("Forms", "Search for softwares and users"),'method'=>"GET"]) ?>

<?php
echo '<div class="input-group">';
echo '<label class = "control-label" for="sofwareFilter_reviewed">' . __d('Forms', "Search.Label.Review") . '</label>';
echo $this->Form->select('reviewed', $reviewed, ["id" => "sofwareFilter_reviewed", "class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="sofwareFilter_screenCaptured">' . __d('Forms', "Search.Label.Screenshot") . '</label>';
echo $this->Form->select('screenCaptured', $screenCaptured, ["id" => "sofwareFilter_screenCaptured", "class" => 'form-control']);
echo '</div>';
echo '<div class="input-group">';
echo '<label class = "control-label" for="sofwareFilter_hasUser">' . __d('Forms', "Search.Label.User") . '</label>';
echo $this->Form->select('hasUser', $used, ["id" => "sofwareFilter_hasUser", "name" => "hasUser", "class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="sofwareFilter_hasServiceProvider">' . __d('Forms', "Search.Label.ServiceProvider") . '</label>';
echo $this->Form->select('hasServiceProvider', $hasServiceProvider, ["id" => "sofwareFilter_hasServiceProvider", "name" => "hasServiceProvider", "class" => 'form-control']);
echo '</div>';


echo '<div class="input-group">';
echo '<label class = "control-label" for="sofwareFilter_order">' . __d('Forms', "Search.Label.Sort") . '</label>';
echo $this->Form->select('order', $order, ["id" => "sofwareFilter_order", "class" => 'form-control']);
echo '</div>';
?>

<div class="input-group">
    <?= $this->Form->input('search', [
        'id'    => 'sofwareFilter',
        'title' => 'search',
        'label' => false,
        "type" => "text",
        "class" => "hidden"]) ?>
</div>

<div class="form-group">
    <?= $this->Form->button(__d("ElementNavigation", "Filter"), ['class' => 'btn btn-default submit-form filter']) ?>
</div>

<?= $this->Form->end() ?>

