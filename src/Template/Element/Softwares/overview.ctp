<div class="col-sm-6 col-md-4 col-lg-4 hidden-xs">
    <div class="size-logo-overview">
        <?php if (!$this->Software->hasLogo($software) || is_null($software->photo) || empty($software->photo)) : ?>
            <?= $this->Html->image("logos/placeholder_software.svg",
                ["alt" => $software->softwarename, "class" => "img-responsive img-placeholder"]) ?>
        <?php else : ?>
            <?= $this->Html->link($this->Html->image($software->logo_directory . DS . $software->photo,
                ["alt" => $software->softwarename, "class" => "img-responsive"]),
                "/$selectedLanguage/softwares/$software->id",
                ['escape' => false]) ?>
        <?php endif; ?>
    </div>

</div>

<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
    <ul class="list-unstyled metric-score col-xs-12">

        <li>

            <div class="align">
                <h1>
                    <?= $software->softwarename ?>
                </h1>
                <div class="rating">
                    <?= $this->Rating->display($software->average_review,
                        [
                            "indicator"=>__d("Softwares","Software.rating.indicatorMessage",[count($software->reviews)])
                        ])?>
                </div>

            </div>

        </li>
        <li>
            <?php if ($software->url_website) : ?>
            <?= __d("Forms","Official web site:") .
                $this->Html->link($software->url_website,
                                    $software->url_website,
                                    [
                                        'rel' => 'noopener noreferrer',
                                        'escape' => false
                                    ])
                ?>

            <?php else :?>

            <?= __d("Forms","Official web site:") . __d("Forms","NA") ?>

            <?php endif; ?>

        </li>
        <li>
            <?= __d("Forms","Source code:") .
                    $this->Html->link($software->url_repository,
                                        $software->url_repository,
                                        [
                                            'rel' => 'noopener noreferrer',
                                            'escape' => false
                                        ])
            ?>
        </li>
        <li class="software-license">
            <?= __d("Softwares","software.license") ?>
            <?php if ($software->license) : ?>
                <strong><?= $software->license->name ?> </strong>
            <?php endif; ?>
        </li>
        <li>
            <?= $this->Text->autoParagraph($software->description); ?>
        </li>
        <li class="">
            <div class="tagsPlace">
                <?= __d("Tags", "Tags : ") ?>
            </div>

            <ul class="list-inline tagsPlace">
                <?= $this->Tag->display($software->tags) ?>
            </ul>
        </li>
        <li>
            <?= $this->Html->link(__d("Softwares","Edit"),
                ['prefix'=>false,'controller' => $this->request->controller, 'action' => "edit",$software->id],
                ['class' => 'btn btn-default edit-software inline-block', ],
                ['escape' => false]) ?>
        </li>
    </ul>
</div>
