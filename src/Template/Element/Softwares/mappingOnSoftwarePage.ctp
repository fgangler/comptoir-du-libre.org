<?php
    ///////////////////////////////////////////////////////////////
    // Mapping display for current software
    //
    // Used by Softwares/view.ctp
    ///////////////////////////////////////////////////////////////

    // Mapping for current software
    $list = [];
    $softwareName = $software->softwarename;
    $softwareSlug = $software->slugName;
    $softwareId = $software->id;
    foreach ($mappingFirstLevels as $firsLevelId => $firsLevelName) {
        if(isset($mappingTaxons[$firsLevelId]['children'])) {
            foreach ($mappingTaxons[$firsLevelId]['children'] as $taxonId => $taxonName) {
                if (isset($mappingTaxons[$taxonId]) && $taxonomiesSoftware[$taxonId]) {
                    $name = $mappingTaxons[$taxonId]['title'];
                    $slug = $mappingTaxons[$taxonId]['slug'];
                    $parentId = $mappingTaxons[$taxonId]['id_parent'];
                    $parentName = $mappingTaxons[$parentId]['title'];
                    $parentSlug = $mappingTaxons[$parentId]['slug'];
                    $url = "$mappingBaseUrl/$parentSlug/$slug/$softwareSlug/$taxonId.$softwareId";
                    $linkOptions = [
                        'title' => __d(
                            "Taxonomy",
                            "Taxonomy.softwarePage.taxonSoftwareLink.title",
                            [$software->softwarename, $parentName, $name]
                        ),
                        'id' => "linkMapping-taxon-$taxonId-Software-$softwareId",
                        'class' => "linkMapping-taxonSoftware",
                        'escape' => false
                    ];
                    $list[] =  $this->Html->link("$parentName · <strong>$name</strong>", $url, $linkOptions);
                }
            }
        }
    }

    $nbOfUseCases = count($list);
    if ($nbOfUseCases > 0) {
        $s = ''; if($nbOfUseCases > 1) { $s = 's'; }
        $mappingTxt =  __d(
            "Taxonomy",
            "Taxonomy.softwarePage.description",
            [$s, $software->softwarename,]
        );
        $htmlList = $this->Html->nestedList($list);
        echo "<div id=\"mappingForSoftware-$softwareId\">
                   <p>$mappingTxt</p>
                   $htmlList
              <div>";
    }
