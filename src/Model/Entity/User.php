<?php
namespace App\Model\Entity;

use App\Cache\AppCacheTrait as AppCacheTrait;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;
use Cake\ORM\Entity;

/**
 * User Entity.
 *
 * @property int $id
 * @property string $username
 * @property Time $created
 * @property string $logo_directory
 * @property string $url
 * @property int $user_type_id
 * @property UserType $user_type
 * @property string $description
 * @property Time $modified
 * @property string $role
 * @property string $password
 * @property string $email
 * @property $photo
 */
class User extends Entity
{
    use AppCacheTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'digest_hash',
        'email',
        'token',
        'active',
        '_joinData',
        '_matchingData'
    ];

    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher())->hash($password);
        }
    }


    /**
     * Is the current user an "$allowedUserTypeName" user type?
     * - returns TRUE if the user as know as "$allowedUserTypeName" FALSE otherwise
     * - use only the current properties of the current instance
     *
     * @param string $allowedUserTypeName
     * @return bool
     */
    public function isAllowedUserType(string $allowedUserTypeName)
    {
        if (!is_null($this->user_type)) {
            if ($this->user_type->cd === $allowedUserTypeName) {
                return true;
            }
        } else {
            $allowedUserTypeId = $this->getUserTypeIdByName($allowedUserTypeName);
            if ($this->get('user_type_id') === $allowedUserTypeId) {
                return true;
            }
        }
        return false;
    }

    /**
     * Is the current user an "Administration" user type?
     * - returns TRUE if the user as know as "Administration" FALSE otherwise
     * - use only the current properties of the current instance
     *
     * @return bool
     */
    public function isAdministrationType()
    {
        return $this->isAllowedUserType('Administration');
    }

    /**
     * Is the current user a "Person" user type?
     * - returns TRUE if the user as know as "Person" FALSE otherwise
     * - use only the current properties of the current instance
     *
     * @return bool
     */
    public function isPersonType()
    {
        return $this->isAllowedUserType('Person');
    }

    /**
     * Is the current user a "Company" user type?
     * - returns TRUE if the user as know as "Company" FALSE otherwise
     * - use only the current properties of the current instance
     *
     * @return bool
     */
    public function isCompanyType()
    {
        return $this->isAllowedUserType('Company');
    }

    /**
     * Is the current user an "Association" user type?
     * - returns TRUE if the user as know as "Association" FALSE otherwise
     * - use only the current properties of the current instance
     *
     * @return bool
     */
    public function isAssociationType()
    {
        return $this->isAllowedUserType('Association');
    }

    /**
     * Is the current user is a admin user? (role = "admin")
     * - returns TRUE if the user have the "admin" role FALSE otherwise
     * - use only the current properties of the current instance (no database request is made)
     *
     * @return bool
     */
    public function isAdminUser()
    {
        if ($this->get('role') === 'admin') {
            return true;
        }
        return false;
    }

    /**
     * Is the current user is a classic user? (role = "User")
     * - returns TRUE if the user have not the "admin" role FALSE otherwise
     * - use only the current properties of the current instance (no database request is made)
     *
     * @return bool
     */
    public function isClassicUser()
    {
        return !$this->isAdminRole();
    }
}
