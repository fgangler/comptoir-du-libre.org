<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StatisticsAverage Entity
 *
 * @property int $id
 * @property float $delta_commit_one_month
 * @property float $delta_commit_twelve_month
 * @property float $number_of_contributors
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class StatisticsAverage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
