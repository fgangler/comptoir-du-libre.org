<?php
namespace App\Model\Entity;

use Cake\I18n\Time;
use Cake\ORM\Entity;

/**
 * TaxonomysSoftware Entity
 *
 * @property int $id
 * @property int $taxonomy_id
 * @property int $software_id
 * @property int $user_id
 * @property Time $created
 * @property Time $modified
 * @property string $comment
 *
 * @property \App\Model\Entity\Taxonomy $taxonomy
 * @property \App\Model\Entity\Software $software
 * @property \App\Model\Entity\User $user
 */
class TaxonomysSoftware extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
