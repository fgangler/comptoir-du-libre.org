<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RawMetricsSoftware Entity.
 *
 * @property int $id
 * @property int $software_id
 * @property \App\Model\Entity\Software $software
 * @property string $name
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $last_commit_age
 * @property float $high_committer_percent
 * @property int $number_of_contributors
 * @property int $declared_users
 * @property float $average_review_score
 * @property int $screenshots
 * @property int $code_gouv_label
 */
class RawMetricsSoftware extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
