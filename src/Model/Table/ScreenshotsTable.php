<?php

namespace App\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Josegonzalez\Upload\Validation\DefaultValidation;

/**
 * Screenshots Model
 *
 * @property BelongsTo $Softwares
 */
class ScreenshotsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('screenshots');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        //For upload file -> avatar
        $this->addBehavior(
            'Josegonzalez/Upload.Upload',
            [
                'photo' => [
                    'fields' => [
                        'dir' => 'url_directory',
                    ],
                    'filesystem' => [
                        'root' => WWW_ROOT . 'img' . DS,
                    ],
                    'path' => 'files{DS}{model}',
                ],
            ]
        );


        $this->belongsTo(
            'Softwares',
            [
                'foreignKey' => 'software_id'
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'created');

        $validator
            ->allowEmpty('name');

        //        $validator
        //            ->requirePresence('photo', 'create')
        //            ->notEmpty('photo');

        $validator
            ->allowEmpty('url_directory');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('photo');

        $validator->provider('upload', new DefaultValidation());


        $validator->add(
            'photo',
            'file',
            [
                'rule' => ['mimeType', ['image/jpeg', 'image/png', 'image/gif', 'image/svg+xml']],
                'message' => __d(
                    "Forms",
                    "This format is not allowed. Please use one in this list: JPEG, PNG, GIF, SVG."
                )
            ]
        );


        $validator->add(
            'photo',
            'fileBelowMaxSize',
            [
                'rule' => ['isBelowMaxSize', 1048576],
                'message' => __d("Forms", 'The maximum weight allowed is 1Mb, please use a file less than 1Mb.'),
                'provider' => 'upload'
            ]
        );

        //Check that the file is below the maximum height requirement (checked in pixels)
        $validator->add(
            'photo',
            'fileBelowMaxHeight',
            [
                'rule' => ['isBelowMaxHeight', 1080],
                'message' => __d(
                    "Forms",
                    'The size of your image is too big, please use an image fitting a 1080x1920px size.'
                ),
                'provider' => 'upload',
                'on' => function ($context) {
                    return in_array($context['data']['photo']['type'], ['image/jpeg', 'image/png', 'image/gif']);
                },
                'last' => true,
            ]
        );

        //Check that the file is below the maximum height requirement (checked in pixels)
        $validator->add(
            'photo',
            'fileBelowMaxWidth',
            [
                'rule' => ['isBelowMaxHeight', 1920],
                'message' => __d(
                    "Forms",
                    'The size of your image is too big, please use an image fitting a 1080x1920px size.'
                ),
                'provider' => 'upload',
                'on' => function ($context) {
                    return in_array($context['data']['photo']['type'], ['image/jpeg', 'image/png', 'image/gif']);
                },
                'last' => true,
            ]
        );
        return $validator;
    }


    public function afterSave(Event $created, $options = array())
    {

        if ($options->isNew()) {
            $event = new Event('Model.Screenshot.created', $this, $options);

            $this->eventManager()->dispatch($event);

            return true;
        }
        return false;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     *
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['software_id'], 'Softwares'));
        return $rules;
    }
}
