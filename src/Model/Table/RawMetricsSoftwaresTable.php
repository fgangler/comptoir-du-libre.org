<?php

namespace App\Model\Table;

use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RawMetricsSoftwares Model
 *
 * @property BelongsTo $Softwares
 */
class RawMetricsSoftwaresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('raw_metrics_softwares');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Softwares',
            [
                'foreignKey' => 'software_id'
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->integer('last_commit_age')
            ->requirePresence('last_commit_age', 'create')
            ->notEmpty('last_commit_age');

        $validator
            ->numeric('high_committer_percent')
            ->requirePresence('high_committer_percent', 'create')
            ->notEmpty('high_committer_percent');

        $validator
            ->integer('number_of_contributors')
            ->requirePresence('number_of_contributors', 'create')
            ->notEmpty('number_of_contributors');

        $validator
            ->integer('declared_users')
            ->allowEmpty('declared_users');

        $validator
            ->decimal('average_review_score')
            ->allowEmpty('average_review_score');

        $validator
            ->integer('screenshots')
            ->allowEmpty('screenshots');

        $validator
            ->integer('code_gouv_label')
            ->allowEmpty('code_gouv_label');

        $validator
            ->dateTime('metrics_date')
            ->allowEmpty('metrics_date');

        $validator
            ->integer('project_age')
            ->allowEmpty('project_age');

        $validator
            ->integer('delta_commit_one_month')
            ->allowEmpty('delta_commit_one_month');

        $validator
            ->integer('delta_commit_twelve_month')
            ->allowEmpty('delta_commit_twelve_month');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['software_id'], 'Softwares'));
        return $rules;
    }
}
