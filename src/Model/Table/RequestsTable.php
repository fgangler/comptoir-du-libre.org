<?php

namespace App\Model\Table;

use Cake\ORM\Association\HasMany;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Requests Model
 *
 * @property HasMany $Panels
 */
class RequestsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('requests');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany(
            'Panels',
            [
                'foreignKey' => 'request_id'
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('url', 'create')
            ->notEmpty('url');

        $validator
            ->allowEmpty('content_type');

        $validator
            ->integer('status_code')
            ->allowEmpty('status_code');

        $validator
            ->allowEmpty('method');

        $validator
            ->dateTime('requested_at')
            ->requirePresence('requested_at', 'create')
            ->notEmpty('requested_at');

        return $validator;
    }
}
