<?php
use Migrations\AbstractMigration;

/**
 * Alter TaxonomysSoftwares table
 * -----------------------------------
 * Add "comment" field
 */
class AlterTaxonomysSoftwaresAddCommentField extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('taxonomys_softwares');
        // --------------------------------------
        $table->addColumn('comment', 'text', [
            'default' => null,
            'null' => true,
        ]);
        // --------------------------------------
        $table->update();
    }
}
