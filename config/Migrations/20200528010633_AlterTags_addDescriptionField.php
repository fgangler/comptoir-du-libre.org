<?php
use Migrations\AbstractMigration;

/**
 * Alter Tags table
 * -----------------------------------
 * Add "description" field
 */
class AlterTagsAddDescriptionField extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tags');
        // --------------------------------------
        $table->addColumn('description_i18n_fr', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('description_i18n_en', 'text', [
            'default' => null,
            'null' => true,
        ]);
        // --------------------------------------
        $table->update();
    }
}
