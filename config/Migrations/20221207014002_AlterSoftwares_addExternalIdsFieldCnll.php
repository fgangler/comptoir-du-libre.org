<?php
use Migrations\AbstractMigration;

/**
 * Alter Tags table
 * -----------------------------------
 * Add "external Id" field (CNLL)
 */

class AlterSoftwaresAddExternalIdsFieldCnll extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('softwares');
        // --------------------------------------
        $table->addColumn('cnll', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        // --------------------------------------
        $table->update();
    }
}
