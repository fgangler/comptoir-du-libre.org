<?php
use Migrations\AbstractMigration;

class CreateTaxonomys extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {

        $table = $this->table('taxonomys');
        // --------------------------------------
        $table->addColumn('parent_id', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('title_i18n_en', 'string', [
            'default' => null,
            'limit' => 250,
            'null' => false,
        ]);
        $table->addColumn('title_i18n_fr', 'string', [
            'default' => null,
            'limit' => 250,
            'null' => false,
        ]);
        $table->addColumn('description_i18n_en', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('description_i18n_fr', 'text', [
            'default' => null,
            'null' => true,
        ]);
        // --------------------------------------
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        // --------------------------------------
        $table->addForeignKey('parent_id', 'taxonomys', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);
        // --------------------------------------
        $table->create();
    }
}
