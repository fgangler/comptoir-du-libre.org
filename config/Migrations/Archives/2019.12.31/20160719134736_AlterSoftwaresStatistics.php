<?php
use Migrations\AbstractMigration;

class AlterSoftwaresStatistics extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * 20160719134736
     * @return void
     */
    public function change()
    {
        $table = $this->table('softwares_statistics');
        $table->renameColumn('contributors_code_percent', 'high_committer_percent');
        $table->renameColumn('nb_contributors', 'number_of_contributors');
        $table->renameColumn('codegouv_label', 'code_gouv_label');
        $table->renameColumn('delta_commit_one_year', 'delta_commit_twelve_month');
        $table->update();
    }
}
