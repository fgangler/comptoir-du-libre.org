<?php
use Migrations\AbstractMigration;

class CreateStatisticsAverages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('statistics_averages');
        $table->addColumn('delta_commit_one_month', 'float', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('delta_commit_twelve_month', 'float', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('number_of_contributors', 'float', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
