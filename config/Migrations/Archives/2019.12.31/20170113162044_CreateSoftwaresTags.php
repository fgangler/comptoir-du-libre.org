<?php
use Migrations\AbstractMigration;

class CreateSoftwaresTags extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('softwares_tags');
        $table->addColumn('software_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('tag_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        // Add a foreign key
        $table->addForeignKey('tag_id', 'tags', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ])
            ->addForeignKey('software_id', 'softwares', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);

        $table->create();
    }
}
