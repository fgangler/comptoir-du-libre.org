--
-- PostgreSQL database dump
--

-- Exported by MFAURE on 2018-09-05

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: calculate_score_for_softwares_statistics(); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.calculate_score_for_softwares_statistics()
    RETURNS void
LANGUAGE plpgsql
AS $$DECLARE calculated_score REAL;

    DECLARE  stats_line       softwares_statistics%ROWTYPE;
    DECLARE  stats            softwares_statistics.software_id%TYPE;

    DECLARE  total_softwares  INTEGER;

BEGIN

    SELECT count(id)
    INTO total_softwares
    FROM softwares_statistics;

    FOR stats_line IN SELECT *
                      FROM softwares_statistics LOOP

        /* Get points */
        calculated_score :=
        (stats_line.last_commit_age +
         stats_line.project_age +
         stats_line.delta_commit_one_month +
         stats_line.delta_commit_one_year +
         stats_line.nb_contributors +
         stats_line.contributors_code_percent +
         stats_line.declared_users +
         stats_line.average_review_score +
         stats_line.screenshots) / 29 * 100;

        /* Update the statistics table */
        UPDATE softwares_statistics
        SET score = calculated_score
        WHERE software_id = stats_line.software_id;

    END LOOP;
END;
$$;


ALTER FUNCTION public.calculate_score_for_softwares_statistics()
OWNER TO comptoir;

--
-- Name: calculated_points_from_manivelle(); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.calculated_points_from_manivelle()
    RETURNS void
LANGUAGE plpgsql
AS $$
DECLARE     point_reviews      INTEGER;
    DECLARE points_screenshots INTEGER;
    DECLARE point_users_of     INTEGER;

    DECLARE stats_line         softwares_statistics%ROWTYPE;
    DECLARE stats              softwares_statistics.software_id%TYPE;


BEGIN
    FOR stats_line IN SELECT *
                      FROM softwares_statistics LOOP

        /*RAISE NOTICE  'test %' ,stats_line.software_id;*/
        /* Get points */
        point_reviews := points_for_reviews(stats_line.software_id);
        points_screenshots := points_for_screenshots(stats_line.software_id);
        point_users_of := points_for_users_of(stats_line.software_id);

        /* Update the statistics table */
        UPDATE softwares_statistics
        SET average_review_score = point_reviews,
            screenshots          = points_screenshots,
            declared_users       = point_users_of
        WHERE software_id = stats_line.software_id;

    END LOOP;
END
$$;


ALTER FUNCTION public.calculated_points_from_manivelle()
OWNER TO comptoir;

--
-- Name: calculated_points_from_server(); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.calculated_points_from_server()
    RETURNS void
LANGUAGE plpgsql
AS $$DECLARE total_softwares                  REAL;
    DECLARE  raw_line                         raw_metrics_softwares%ROWTYPE;
    DECLARE  avg_delta_commit_1m              REAL;
    DECLARE  avg_delta_commit_12m             REAL;
    DECLARE  avg_hight_commiter_percent       REAL;
    DECLARE  avg_number_of_contributors       REAL;
    DECLARE  final_avg_delta_commit_1m        REAL;
    DECLARE  final_avg_delta_commit_12m       REAL;
    DECLARE  final_avg_hight_commiter_percent REAL;
    DECLARE  final_avg_number_of_contributors REAL;

BEGIN
    SELECT COUNT(*)
    INTO total_softwares
    FROM (SELECT DISTINCT software_id
          FROM raw_metrics_softwares) AS count;

    avg_delta_commit_1m := 0;
    avg_delta_commit_12m := 0;
    avg_hight_commiter_percent := 0;
    avg_number_of_contributors := 0;

    FOR raw_line IN (WITH grouped_softwares AS (SELECT
                                                    *,
                                                    ROW_NUMBER()
                                                    OVER (PARTITION BY software_id
                                                        ORDER BY id DESC) AS rk
                                                FROM raw_metrics_softwares) SELECT g.*
                                                                            FROM grouped_softwares g
                                                                            WHERE g.rk = 1) LOOP

        /* Get sum */
        avg_delta_commit_1m := avg_delta_commit_1m + (raw_line.delta_commit_one_month);
        avg_delta_commit_12m := avg_delta_commit_12m + (raw_line.delta_commit_twelve_month);
        avg_hight_commiter_percent := avg_hight_commiter_percent + (raw_line.high_committer_percent);
        avg_number_of_contributors := avg_number_of_contributors + (raw_line.number_of_contributors);

    END LOOP;

    /* Get average */
    avg_delta_commit_1m         := avg_delta_commit_1m / total_softwares;
    avg_delta_commit_12m        := avg_delta_commit_12m / total_softwares;
    avg_hight_commiter_percent  := avg_hight_commiter_percent / total_softwares;
    avg_number_of_contributors  := avg_number_of_contributors / total_softwares;


    FOR raw_line IN (WITH grouped_softwares AS (SELECT
                                                    *,
                                                    ROW_NUMBER()
                                                    OVER (PARTITION BY software_id
                                                        ORDER BY id DESC) AS rk
                                                FROM raw_metrics_softwares) SELECT g.*
                                                                            FROM grouped_softwares g
                                                                            WHERE g.rk = 1) LOOP

        final_avg_delta_commit_1m := (raw_line.delta_commit_one_month - avg_delta_commit_1m) / avg_delta_commit_1m *
                                     100;
        final_avg_delta_commit_12m := (raw_line.delta_commit_twelve_month - avg_delta_commit_12m) / avg_delta_commit_12m
                                      * 100;
        final_avg_hight_commiter_percent := raw_line.high_committer_percent;
        /*(raw_line.high_committer_percent-avg_hight_commiter_percent)/avg_hight_commiter_percent*100;*/
        final_avg_number_of_contributors :=
        (raw_line.number_of_contributors - avg_number_of_contributors) / avg_number_of_contributors * 100;

        PERFORM score_age(raw_line.project_age, raw_line.last_commit_age, raw_line.software_id);
        PERFORM score_delta_commit_1m(final_avg_delta_commit_1m, raw_line.software_id);
        PERFORM score_delta_commit_12m(final_avg_delta_commit_12m, raw_line.software_id);
        PERFORM score_hight_commiter_percent(final_avg_hight_commiter_percent, raw_line.software_id);
        PERFORM score_number_of_contributors(final_avg_number_of_contributors, raw_line.software_id);


    END LOOP;

END;$$;


ALTER FUNCTION public.calculated_points_from_server()
OWNER TO comptoir;

--
-- Name: points_for_reviews(integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.points_for_reviews(stats_id integer)
    RETURNS real
LANGUAGE plpgsql
AS $$DECLARE total_reviews REAL;
    DECLARE  points        REAL;
BEGIN
    points :=0;
    SELECT avg(reviews.evaluation)
    INTO total_reviews
    FROM reviews
    WHERE reviews.software_id = stats_id;

    IF total_reviews > 2.5
    THEN points := 2;
    ELSIF total_reviews <= 2.5
        THEN points := 1;
    ELSIF total_reviews = 0
        THEN points := 0;
    END IF;
    RETURN points;
END;
$$;


ALTER FUNCTION public.points_for_reviews(stats_id integer )
OWNER TO comptoir;

--
-- Name: points_for_screenshots(integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.points_for_screenshots(stats_id integer)
    RETURNS integer
LANGUAGE plpgsql
AS $$

DECLARE     total_screenshots INTEGER;
    DECLARE points            INTEGER;

BEGIN
    SELECT count(screenshots.id)
    INTO total_screenshots
    FROM screenshots
    WHERE screenshots.software_id = stats_id;

    IF total_screenshots > 2
    THEN points := 2;
    ELSIF total_screenshots BETWEEN 0 AND 2
        THEN points = 1;
    ELSIF total_screenshots = 0
        THEN points := 0;
    END IF;
    RETURN points;
END;
$$;


ALTER FUNCTION public.points_for_screenshots(stats_id integer )
OWNER TO comptoir;

--
-- Name: points_for_users_of(integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.points_for_users_of(stats_id integer)
    RETURNS integer
LANGUAGE plpgsql
AS $$DECLARE total_users_of INTEGER;
    DECLARE  points         INTEGER;
BEGIN
    SELECT count(relationships_softwares_users.id)
    INTO total_users_of
    FROM relationships_softwares_users
    WHERE relationships_softwares_users.software_id = stats_id;

    IF total_users_of > 5
    THEN points := 2;
    ELSIF total_users_of BETWEEN 0 AND 5
        THEN points = 1;
    ELSIF total_users_of = 0
        THEN points := 0;
    END IF;
    RETURN points;
END;
$$;


ALTER FUNCTION public.points_for_users_of(stats_id integer )
OWNER TO comptoir;

--
-- Name: raw_metrics_from_manivelle(); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.raw_metrics_from_manivelle()
    RETURNS void
LANGUAGE plpgsql
AS $$DECLARE avg_reviews       DECIMAL;
    DECLARE  total_screenshots INTEGER;
    DECLARE  total_users_of    INTEGER;

    DECLARE  stats_line        raw_metrics_softwares%ROWTYPE;
    DECLARE  id_software       raw_metrics_softwares.software_id%TYPE;


BEGIN
    FOR stats_line IN SELECT *
                      FROM raw_metrics_softwares LOOP

        /*RAISE NOTICE  'test %' ,stats_line.software_id;*/
        /* Get points */
        SELECT avg(reviews.evaluation)
        INTO avg_reviews
        FROM reviews
        WHERE reviews.software_id = stats_line.software_id;

        SELECT count(screenshots.id)
        INTO total_screenshots
        FROM screenshots
        WHERE screenshots.software_id = stats_line.software_id;

        SELECT count(relationships_softwares_users.id)
        INTO total_users_of
        FROM relationships_softwares_users
        WHERE relationships_softwares_users.software_id = stats_line.software_id;

        /* Update the statistics table */
        UPDATE raw_metrics_softwares
        SET average_review_score = avg_reviews,
            screenshots          = total_screenshots,
            declared_users       = total_users_of
        WHERE software_id = stats_line.software_id;

    END LOOP;
END

$$;


ALTER FUNCTION public.raw_metrics_from_manivelle()
OWNER TO comptoir;

--
-- Name: score_age(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.score_age(p_project_age integer, p_commit_age integer, p_id integer)
    RETURNS void
LANGUAGE plpgsql
AS $$

DECLARE     d_score_project INTEGER;
    DECLARE d_score_commit  INTEGER;

BEGIN

    IF p_project_age > 365 * 5
    THEN d_score_project := 2;
    ELSIF p_project_age > 365 * 3
        THEN d_score_project := 1;
    ELSE d_score_project :=0;
    END IF;

    IF p_commit_age > 365 * 3
    THEN d_score_commit := 0;
    ELSIF p_commit_age > 365 * 2
        THEN d_score_commit := 1;
    ELSIF p_commit_age > 90
        THEN d_score_commit := 2;
    ELSIF p_commit_age > 30
        THEN d_score_commit := 3;
    ELSE d_score_commit :=4;
    END IF;

    IF EXISTS(SELECT id
              FROM softwares_statistics
              WHERE software_id = p_id)
    THEN UPDATE softwares_statistics
    SET project_age = d_score_project, last_commit_age = d_score_commit
    WHERE software_id = p_id;
    ELSE INSERT INTO softwares_statistics (software_id, project_age, last_commit_age)
    VALUES (p_id, d_score_project, d_score_commit);
    END IF;

END;$$;


ALTER FUNCTION public.score_age(p_project_age integer, p_commit_age integer, p_id integer )
OWNER TO comptoir;

--
-- Name: FUNCTION score_age(p_project_age integer, p_commit_age integer, p_id integer); Type: COMMENT; Schema: public; Owner: comptoir
--

COMMENT ON FUNCTION public.score_age(p_project_age integer, p_commit_age integer, p_id integer)
IS 'updates the scores based on age

NOTE: this needs to be the first function called to avoid updating a row that does not exist!';


--
-- Name: score_delta_commit_12m(real, integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.score_delta_commit_12m(p_pourcentage_delta_12m real, p_id integer)
    RETURNS void
LANGUAGE plpgsql
AS $$

DECLARE     d_score        INTEGER;
    DECLARE d_score_commit INTEGER;

BEGIN

    IF p_pourcentage_delta_12m > 50
    THEN d_score := 4;
    ELSIF p_pourcentage_delta_12m > 25
        THEN d_score := 3;
    ELSIF p_pourcentage_delta_12m > -25
        THEN d_score := 1;
    ELSE d_score :=0;
    END IF;

    UPDATE softwares_statistics
    SET delta_commit_one_year = d_score
    WHERE software_id = p_id;

END;$$;


ALTER FUNCTION public.score_delta_commit_12m(p_pourcentage_delta_12m real, p_id integer )
OWNER TO comptoir;

--
-- Name: score_delta_commit_1m(real, integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.score_delta_commit_1m(p_pourcentage_delta_1m real, p_id integer)
    RETURNS void
LANGUAGE plpgsql
AS $$

DECLARE d_score INTEGER;

BEGIN

    IF p_pourcentage_delta_1m > 50
    THEN d_score := 4;
    ELSIF p_pourcentage_delta_1m > 25
        THEN d_score := 3;
    ELSIF p_pourcentage_delta_1m > -25
        THEN d_score := 1;
    ELSE d_score :=0;
    END IF;

    UPDATE softwares_statistics
    SET delta_commit_one_month = d_score
    WHERE software_id = p_id;

END;$$;


ALTER FUNCTION public.score_delta_commit_1m(p_pourcentage_delta_1m real, p_id integer )
OWNER TO comptoir;

--
-- Name: score_hight_commiter_percent(real, integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.score_hight_commiter_percent(p_pourcentage_high real, p_id integer)
    RETURNS void
LANGUAGE plpgsql
AS $$

DECLARE d_score INTEGER;

BEGIN

    IF p_pourcentage_high > 90
    THEN d_score := 0;
    ELSIF p_pourcentage_high > 50
        THEN d_score := 2;
    ELSIF p_pourcentage_high > 10
        THEN d_score := 3;
    ELSE d_score :=4;
    END IF;

    UPDATE softwares_statistics
    SET contributors_code_percent = d_score
    WHERE software_id = p_id;

END;$$;


ALTER FUNCTION public.score_hight_commiter_percent(p_pourcentage_high real, p_id integer )
OWNER TO comptoir;

--
-- Name: score_number_of_contributors(real, integer); Type: FUNCTION; Schema: public; Owner: comptoir
--

CREATE FUNCTION public.score_number_of_contributors(pourcentage_nb_contributors real, p_id integer)
    RETURNS void
LANGUAGE plpgsql
AS $$

DECLARE d_score INTEGER;

BEGIN

    IF pourcentage_nb_contributors > 50
    THEN d_score := 4;
    ELSIF pourcentage_nb_contributors > 25
        THEN d_score := 3;
    ELSIF pourcentage_nb_contributors > 0
        THEN d_score := 1;
    ELSE d_score :=0;
    END IF;

    UPDATE softwares_statistics
    SET nb_contributors = d_score
    WHERE software_id = p_id;

END;
$$;


ALTER FUNCTION public.score_number_of_contributors(pourcentage_nb_contributors real, p_id integer )
OWNER TO comptoir;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: licence_types; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.licence_types (
    id       bigint NOT NULL,
    name     text,
    created  timestamp with time zone,
    modified timestamp with time zone,
    cd       text
);


ALTER TABLE public.licence_types
    OWNER TO comptoir;

--
-- Name: licence_types_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.licence_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.licence_types_id_seq
    OWNER TO comptoir;

--
-- Name: licence_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.licence_types_id_seq
OWNED BY public.licence_types.id;


--
-- Name: licenses; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.licenses (
    id              bigint NOT NULL,
    name            text   NOT NULL,
    license_type_id bigint NOT NULL,
    created         timestamp with time zone,
    modified        timestamp with time zone
);


ALTER TABLE public.licenses
    OWNER TO comptoir;

--
-- Name: licenses_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.licenses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.licenses_id_seq
    OWNER TO comptoir;

--
-- Name: licenses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.licenses_id_seq
OWNED BY public.licenses.id;


--
-- Name: panels; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.panels (
    id         uuid NOT NULL,
    request_id uuid NOT NULL,
    panel      character varying,
    title      character varying,
    element    character varying,
    summary    character varying,
    content    bytea
);


ALTER TABLE public.panels
    OWNER TO comptoir;

--
-- Name: phinxlog; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.phinxlog (
    version        bigint                      NOT NULL,
    start_time     timestamp without time zone NOT NULL,
    end_time       timestamp without time zone NOT NULL,
    migration_name character varying(100)
);


ALTER TABLE public.phinxlog
    OWNER TO comptoir;

--
-- Name: raw_metrics_softwares; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.raw_metrics_softwares (
    id                           integer NOT NULL,
    software_id                  integer,
    name                         character varying(255),
    created                      timestamp without time zone,
    modified                     timestamp without time zone,
    last_commit_age              integer NOT NULL,
    high_committer_percent       real    NOT NULL,
    number_of_contributors       integer NOT NULL,
    declared_users               integer,
    average_review_score         numeric,
    screenshots                  integer,
    code_gouv_label              integer,
    metrics_date                 timestamp without time zone,
    project_age                  integer,
    delta_commit_one_month       integer,
    delta_commit_twelve_month    integer,
    commit_activity_one_month    real,
    commit_activity_twelve_month real
);


ALTER TABLE public.raw_metrics_softwares
    OWNER TO comptoir;

--
-- Name: raw_metrics_softwares_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.raw_metrics_softwares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.raw_metrics_softwares_id_seq
    OWNER TO comptoir;

--
-- Name: raw_metrics_softwares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.raw_metrics_softwares_id_seq
OWNED BY public.raw_metrics_softwares.id;


--
-- Name: relationship_types; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.relationship_types (
    id       bigint NOT NULL,
    name     text,
    created  timestamp with time zone,
    modified timestamp with time zone,
    cd       text
);


ALTER TABLE public.relationship_types
    OWNER TO comptoir;

--
-- Name: relationship_types_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.relationship_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relationship_types_id_seq
    OWNER TO comptoir;

--
-- Name: relationship_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.relationship_types_id_seq
OWNED BY public.relationship_types.id;


--
-- Name: relationships; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.relationships (
    id                   bigint NOT NULL,
    name                 text   NOT NULL,
    created              timestamp with time zone,
    modified             timestamp with time zone,
    cd                   text,
    relationship_type_id bigint
);


ALTER TABLE public.relationships
    OWNER TO comptoir;

--
-- Name: relationships_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.relationships_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relationships_id_seq
    OWNER TO comptoir;

--
-- Name: relationships_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.relationships_id_seq
OWNED BY public.relationships.id;


--
-- Name: relationships_softwares; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.relationships_softwares (
    id              bigint NOT NULL,
    software_id     bigint NOT NULL,
    relationship_id bigint NOT NULL,
    recipient_id    bigint NOT NULL,
    created         timestamp with time zone,
    modified        timestamp with time zone
);


ALTER TABLE public.relationships_softwares
    OWNER TO comptoir;

--
-- Name: relationships_softwares_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.relationships_softwares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relationships_softwares_id_seq
    OWNER TO comptoir;

--
-- Name: relationships_softwares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.relationships_softwares_id_seq
OWNED BY public.relationships_softwares.id;


--
-- Name: relationships_softwares_users; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.relationships_softwares_users (
    id              bigint NOT NULL,
    software_id     bigint NOT NULL,
    user_id         bigint NOT NULL,
    relationship_id bigint NOT NULL,
    created         timestamp with time zone,
    modified        timestamp with time zone
);


ALTER TABLE public.relationships_softwares_users
    OWNER TO comptoir;

--
-- Name: relationships_softwares_users_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.relationships_softwares_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relationships_softwares_users_id_seq
    OWNER TO comptoir;

--
-- Name: relationships_softwares_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.relationships_softwares_users_id_seq
OWNED BY public.relationships_softwares_users.id;


--
-- Name: relationships_users; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.relationships_users (
    id              bigint                   NOT NULL,
    user_id         bigint                   NOT NULL,
    recipient_id    bigint                   NOT NULL,
    relationship_id bigint                   NOT NULL,
    created         timestamp with time zone NOT NULL,
    modified        timestamp with time zone
);


ALTER TABLE public.relationships_users
    OWNER TO comptoir;

--
-- Name: relationships_users_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.relationships_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relationships_users_id_seq
    OWNER TO comptoir;

--
-- Name: relationships_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.relationships_users_id_seq
OWNED BY public.relationships_users.id;


--
-- Name: requests; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.requests (
    id           uuid                        NOT NULL,
    url          character varying           NOT NULL,
    content_type character varying,
    status_code  integer,
    method       character varying,
    requested_at timestamp without time zone NOT NULL
);


ALTER TABLE public.requests
    OWNER TO comptoir;

--
-- Name: reviews; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.reviews (
    id          bigint                   NOT NULL,
    comment     text                     NOT NULL,
    title       text                     NOT NULL,
    created     timestamp with time zone NOT NULL,
    user_id     bigint,
    software_id bigint,
    evaluation  bigint,
    modified    timestamp with time zone
);


ALTER TABLE public.reviews
    OWNER TO comptoir;

--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_id_seq
    OWNER TO comptoir;

--
-- Name: reviews_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.reviews_id_seq
OWNED BY public.reviews.id;

--
-- Name: screenshots; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.screenshots (
    id            bigint NOT NULL,
    software_id   bigint,
    url_directory text,
    created       timestamp with time zone,
    modified      timestamp with time zone,
    name          text,
    photo         text
);


ALTER TABLE public.screenshots
    OWNER TO comptoir;

--
-- Name: screenshots_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.screenshots_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.screenshots_id_seq
    OWNER TO comptoir;

--
-- Name: screenshots_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.screenshots_id_seq
OWNED BY public.screenshots.id;


--
-- Name: softwares; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.softwares (
    id             bigint NOT NULL,
    softwarename   text,
    url_repository text   NOT NULL,
    description    text,
    licence_id     bigint,
    created        timestamp with time zone,
    modified       timestamp with time zone,
    logo_directory text,
    photo          text,
    url_website    character varying(255)
);


ALTER TABLE public.softwares
    OWNER TO comptoir;

--
-- Name: softwares_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.softwares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.softwares_id_seq
    OWNER TO comptoir;

--
-- Name: softwares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.softwares_id_seq
OWNED BY public.softwares.id;

--
-- Name: softwares_statistics; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.softwares_statistics (
    id                        integer NOT NULL,
    software_id               integer,
    last_commit_age           integer,
    project_age               integer,
    delta_commit_one_month    integer,
    delta_commit_twelve_month integer,
    number_of_contributors    integer,
    high_committer_percent    numeric,
    declared_users            integer,
    average_review_score      numeric,
    screenshots               integer,
    code_gouv_label           integer,
    score                     numeric
);


ALTER TABLE public.softwares_statistics
    OWNER TO comptoir;

--
-- Name: softwares_tags; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.softwares_tags (
    id integer NOT NULL,
    software_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.softwares_tags
    OWNER TO comptoir;

--
-- Name: softwares_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.softwares_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.softwares_tags_id_seq
    OWNER TO comptoir;

--
-- Name: softwares_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.softwares_tags_id_seq
OWNED BY public.softwares_tags.id;


--
-- Name: statistics_averages; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.statistics_averages (
    id                        integer                     NOT NULL,
    delta_commit_one_month    real                        NOT NULL,
    delta_commit_twelve_month real                        NOT NULL,
    number_of_contributors    real                        NOT NULL,
    created                   timestamp without time zone NOT NULL,
    modified                  timestamp without time zone NOT NULL
);


ALTER TABLE public.statistics_averages
    OWNER TO comptoir;

--
-- Name: statistics_averages_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.statistics_averages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.statistics_averages_id_seq
    OWNER TO comptoir;

--
-- Name: statistics_averages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.statistics_averages_id_seq
OWNED BY public.statistics_averages.id;


--
-- Name: statistics_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.statistics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.statistics_id_seq
    OWNER TO comptoir;

--
-- Name: statistics_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.statistics_id_seq
OWNED BY public.softwares_statistics.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.tags (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


ALTER TABLE public.tags
    OWNER TO comptoir;

--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tags_id_seq
    OWNER TO comptoir;

--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.tags_id_seq
OWNED BY public.tags.id;


--
-- Name: user_types; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.user_types (
    id       bigint NOT NULL,
    name     text,
    created  timestamp with time zone,
    modified timestamp with time zone,
    cd       text
);


ALTER TABLE public.user_types
    OWNER TO comptoir;

--
-- Name: user_types_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.user_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_types_id_seq
    OWNER TO comptoir;

--
-- Name: user_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.user_types_id_seq
OWNED BY public.user_types.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: comptoir
--

CREATE TABLE public.users (
    id             bigint                   NOT NULL,
    username       text,
    created        timestamp with time zone NOT NULL,
    logo_directory text,
    url            text,
    user_type_id   bigint                   NOT NULL,
    description    text,
    modified       timestamp with time zone NOT NULL,
    role           text                     NOT NULL,
    password       text                     NOT NULL,
    email          text,
    photo          text,
    active         boolean DEFAULT false,
    digest_hash    text,
    token          uuid
);


ALTER TABLE public.users
    OWNER TO comptoir;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: comptoir
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq
    OWNER TO comptoir;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: comptoir
--

ALTER SEQUENCE public.users_id_seq
OWNED BY public.users.id;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.licence_types
    ALTER COLUMN id SET DEFAULT nextval('public.licence_types_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.licenses
    ALTER COLUMN id SET DEFAULT nextval('public.licenses_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.raw_metrics_softwares
    ALTER COLUMN id SET DEFAULT nextval('public.raw_metrics_softwares_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationship_types
    ALTER COLUMN id SET DEFAULT nextval('public.relationship_types_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships
    ALTER COLUMN id SET DEFAULT nextval('public.relationships_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares
    ALTER COLUMN id SET DEFAULT nextval('public.relationships_softwares_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares_users
    ALTER COLUMN id SET DEFAULT nextval('public.relationships_softwares_users_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_users
    ALTER COLUMN id SET DEFAULT nextval('public.relationships_users_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.reviews
    ALTER COLUMN id SET DEFAULT nextval('public.reviews_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.screenshots
    ALTER COLUMN id SET DEFAULT nextval('public.screenshots_id_seq' :: regclass);

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares
    ALTER COLUMN id SET DEFAULT nextval('public.softwares_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_statistics
    ALTER COLUMN id SET DEFAULT nextval('public.statistics_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_tags
    ALTER COLUMN id SET DEFAULT nextval('public.softwares_tags_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.statistics_averages
    ALTER COLUMN id SET DEFAULT nextval('public.statistics_averages_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.tags
    ALTER COLUMN id SET DEFAULT nextval('public.tags_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.user_types
    ALTER COLUMN id SET DEFAULT nextval('public.user_types_id_seq' :: regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.users
    ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq' :: regclass);


--
-- Name: idx_26468_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.licence_types
    ADD CONSTRAINT idx_26468_primary PRIMARY KEY (id);


--
-- Name: idx_26477_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.licenses
    ADD CONSTRAINT idx_26477_primary PRIMARY KEY (id);


--
-- Name: idx_26486_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships
    ADD CONSTRAINT idx_26486_primary PRIMARY KEY (id);


--
-- Name: idx_26495_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares
    ADD CONSTRAINT idx_26495_primary PRIMARY KEY (id);


--
-- Name: idx_26501_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares_users
    ADD CONSTRAINT idx_26501_primary PRIMARY KEY (id);


--
-- Name: idx_26507_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_users
    ADD CONSTRAINT idx_26507_primary PRIMARY KEY (id);


--
-- Name: idx_26513_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationship_types
    ADD CONSTRAINT idx_26513_primary PRIMARY KEY (id);


--
-- Name: idx_26522_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT idx_26522_primary PRIMARY KEY (id);


--
-- Name: idx_26531_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.screenshots
    ADD CONSTRAINT idx_26531_primary PRIMARY KEY (id);


--
-- Name: idx_26540_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares
    ADD CONSTRAINT idx_26540_primary PRIMARY KEY (id);


--
-- Name: idx_26549_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT idx_26549_primary PRIMARY KEY (id);


--
-- Name: idx_26558_primary; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.user_types
    ADD CONSTRAINT idx_26558_primary PRIMARY KEY (id);


--
-- Name: panels_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.panels
    ADD CONSTRAINT panels_pkey PRIMARY KEY (id);


--
-- Name: phinxlog_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.phinxlog
    ADD CONSTRAINT phinxlog_pkey PRIMARY KEY (version);


--
-- Name: raw_metrics_softwares_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.raw_metrics_softwares
    ADD CONSTRAINT raw_metrics_softwares_pkey PRIMARY KEY (id);


--
-- Name: requests_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.requests
    ADD CONSTRAINT requests_pkey PRIMARY KEY (id);


--
-- Name: softwares_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_tags
    ADD CONSTRAINT softwares_tags_pkey PRIMARY KEY (id);


--
-- Name: statistics_averages_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.statistics_averages
    ADD CONSTRAINT statistics_averages_pkey PRIMARY KEY (id);


--
-- Name: statistics_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_statistics
    ADD CONSTRAINT statistics_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: unique_panel; Type: CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.panels
    ADD CONSTRAINT unique_panel UNIQUE (request_id, panel);


--
-- Name: idx_26477_fk_license_licence_type_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26477_fk_license_licence_type_idx
    ON public.licenses
    USING btree (license_type_id);


--
-- Name: idx_26486_fk_relationships_relationship_types_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26486_fk_relationships_relationship_types_idx
    ON public.relationships
    USING btree (relationship_type_id);

--
-- Name: idx_26495_fk_software_relationship_relation_type_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26495_fk_software_relationship_relation_type_idx
    ON public.relationships_softwares
    USING btree (relationship_id);


--
-- Name: idx_26495_fk_software_relationship_software_from; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26495_fk_software_relationship_software_from
    ON public.relationships_softwares
    USING btree (software_id);


--
-- Name: idx_26495_fk_software_relationship_to_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26495_fk_software_relationship_to_idx
    ON public.relationships_softwares
    USING btree (recipient_id);


--
-- Name: idx_26501_fk_entity_software_relationship_entity_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26501_fk_entity_software_relationship_entity_idx
    ON public.relationships_softwares_users
    USING btree (user_id);


--
-- Name: idx_26501_fk_entity_software_relationship_relationship_type_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26501_fk_entity_software_relationship_relationship_type_idx
    ON public.relationships_softwares_users
    USING btree (relationship_id);


--
-- Name: idx_26501_fk_user_software_relationship_software; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26501_fk_user_software_relationship_software
    ON public.relationships_softwares_users
    USING btree (software_id);


--
-- Name: idx_26507_fk_entities_relationships_entities_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26507_fk_entities_relationships_entities_idx
    ON public.relationships_users
    USING btree (user_id);


--
-- Name: idx_26507_fk_entities_relationships_relationtion_types_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26507_fk_entities_relationships_relationtion_types_idx
    ON public.relationships_users
    USING btree (relationship_id);


--
-- Name: idx_26507_fk_entities_relationships_users_recipient_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26507_fk_entities_relationships_users_recipient_idx
    ON public.relationships_users
    USING btree (recipient_id);


--
-- Name: idx_26513_cd_unique; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE UNIQUE INDEX idx_26513_cd_unique
    ON public.relationship_types
    USING btree (cd);

--
-- Name: idx_26513_name_unique; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE UNIQUE INDEX idx_26513_name_unique
    ON public.relationship_types
    USING btree (name);


--
-- Name: idx_26522_fk_review_software_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26522_fk_review_software_idx
    ON public.reviews
    USING btree (software_id);


--
-- Name: idx_26522_fk_review_user_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26522_fk_review_user_idx
    ON public.reviews
    USING btree (user_id);


--
-- Name: idx_26531_fk_screens_software_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26531_fk_screens_software_idx
    ON public.screenshots
    USING btree (software_id);


--
-- Name: idx_26540_fk_software_licence_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26540_fk_software_licence_idx
    ON public.softwares
    USING btree (licence_id);


--
-- Name: idx_26540_software_url_repository_unique; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE UNIQUE INDEX idx_26540_software_url_repository_unique
    ON public.softwares
    USING btree (url_repository);


--
-- Name: idx_26540_softwarename_unique; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE UNIQUE INDEX idx_26540_softwarename_unique
    ON public.softwares
    USING btree (softwarename);


--
-- Name: idx_26549_fk_users_user_types_idx; Type: INDEX; Schema: public; Owner: comptoir
--

CREATE INDEX idx_26549_fk_users_user_types_idx
    ON public.users
    USING btree (user_type_id);


--
-- Name: fk_entities_relationships_relationtion_types; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_users
    ADD CONSTRAINT fk_entities_relationships_relationtion_types FOREIGN KEY (relationship_id) REFERENCES public.relationships (id);


--
-- Name: fk_entities_relationships_users; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_users
    ADD CONSTRAINT fk_entities_relationships_users FOREIGN KEY (user_id) REFERENCES public.users (id);

--
-- Name: fk_entities_relationships_users_recipient; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_users
    ADD CONSTRAINT fk_entities_relationships_users_recipient FOREIGN KEY (recipient_id) REFERENCES public.users (id);


--
-- Name: fk_entity_software_relationship_entity; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares_users
    ADD CONSTRAINT fk_entity_software_relationship_entity FOREIGN KEY (user_id) REFERENCES public.users (id);


--
-- Name: fk_license_licence_type; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.licenses
    ADD CONSTRAINT fk_license_licence_type FOREIGN KEY (license_type_id) REFERENCES public.licence_types (id);


--
-- Name: fk_relationships_relationship_types; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships
    ADD CONSTRAINT fk_relationships_relationship_types FOREIGN KEY (relationship_type_id) REFERENCES public.relationship_types (id);


--
-- Name: fk_review_software; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT fk_review_software FOREIGN KEY (software_id) REFERENCES public.softwares (id);


--
-- Name: fk_review_user; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT fk_review_user FOREIGN KEY (user_id) REFERENCES public.users (id);


--
-- Name: fk_screens_software; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.screenshots
    ADD CONSTRAINT fk_screens_software FOREIGN KEY (software_id) REFERENCES public.softwares (id);


--
-- Name: fk_software_licence; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares
    ADD CONSTRAINT fk_software_licence FOREIGN KEY (licence_id) REFERENCES public.licenses (id);


--
-- Name: fk_software_relationship_relation_type; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares
    ADD CONSTRAINT fk_software_relationship_relation_type FOREIGN KEY (relationship_id) REFERENCES public.relationships (id);


--
-- Name: fk_software_relationship_software_from; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares
    ADD CONSTRAINT fk_software_relationship_software_from FOREIGN KEY (software_id) REFERENCES public.softwares (id);

--
-- Name: fk_software_relationship_to; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares
    ADD CONSTRAINT fk_software_relationship_to FOREIGN KEY (recipient_id) REFERENCES public.softwares (id);


--
-- Name: fk_softwares; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_statistics
    ADD CONSTRAINT fk_softwares FOREIGN KEY (software_id) REFERENCES public.softwares (id);


--
-- Name: fk_user_software_relationship_relationship_type; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares_users
    ADD CONSTRAINT fk_user_software_relationship_relationship_type FOREIGN KEY (relationship_id) REFERENCES public.relationships (id);


--
-- Name: fk_user_software_relationship_software; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.relationships_softwares_users
    ADD CONSTRAINT fk_user_software_relationship_software FOREIGN KEY (software_id) REFERENCES public.softwares (id);


--
-- Name: fk_user_user_type; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_user_user_type FOREIGN KEY (user_type_id) REFERENCES public.user_types (id);


--
-- Name: raw_metrics_softwares_software_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.raw_metrics_softwares
    ADD CONSTRAINT raw_metrics_softwares_software_id_fkey FOREIGN KEY (software_id) REFERENCES public.softwares (id);


--
-- Name: softwares_tags_software_id; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_tags
    ADD CONSTRAINT softwares_tags_software_id FOREIGN KEY (software_id) REFERENCES public.softwares (id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: softwares_tags_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: comptoir
--

ALTER TABLE ONLY public.softwares_tags
    ADD CONSTRAINT softwares_tags_tag_id FOREIGN KEY (tag_id) REFERENCES public.tags (id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

