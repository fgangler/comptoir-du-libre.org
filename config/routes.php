<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    mickael pastor <mickael.pastor@adullact.org>
 * @copyright Copyright (c) 2016, Cake Software Foundation Inc. (http://cakefoundation.org)
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @link      http://cakephp.org CakePHP(tm) Project
 */

use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 */
Router::defaultRouteClass('DashedRoute');

Router::addUrlFilter(
    function ($params, $request) {
        if (isset($request->params['language']) && !isset($params['language'])) {
            $params['language'] = $request->params['language'];
        }
        return $params;
    }
);

Router::scope(
    '/',
    function ($routes) {
    }
);

$basicRoutes = function (RouteBuilder $routes) {

    $routes->connect(
        '/softwares/add-review/:software_id',
        [
            'controller' => 'Reviews',
            'action' => 'add',
            "pass" => ["software_id"],
            "prefix" => Configure::read("DEFAULT_PREFIX")
        ]
    );

    // Pages ==================================================================
    $routes->resources(
        'Pages',
        [
            "prefix" => Configure::read("DEFAULT_PREFIX"),
            "map" => [
                "search/:search" => [
                    'action' => 'search',
                    'method' => ['GET'],
                ],
                "search/" => [
                    'action' => 'search',
                    'method' => ['POST', 'GET'],
                ],
                "contact/" => [
                    'action' => 'contact',
                    'method' => ['GET'],
                ],
                "opendata/" => [
                    'action' => 'opendata',
                    'method' => ['GET'],
                ],
                "legal/" => [
                    'action' => 'legal',
                    'method' => ['GET'],
                ],
                "accessibility/" => [
                    'action' => 'accessibility',
                    'method' => ['GET'],
                ],
                "index/" => [
                    'action' => 'index',
                    'method' => ['GET'],
                ]
            ]
        ]
    );


    // Taxonomys ==================================================================
    $langs = [
        'en' => 'mapping',
        'fr' => 'cartographie'
    ];
    foreach ($langs as $lang => $mappingLanguageSlug) {
        $routes->connect(
            "$mappingLanguageSlug",
            [
                'controller' => 'Taxonomys',
                'action' => 'mapping',
                'method' => ['GET'],
                "prefix" => Configure::read("DEFAULT_PREFIX"),
            ]
        );
        $routes->connect(
            "$mappingLanguageSlug/:primary_slug/",
            [
                'controller' => 'Taxonomys',
                'action' => 'mappingPrimaryLevel',
                'method' => ['GET'],
                "pass" => ["primary_slug"],
                "prefix" => Configure::read("DEFAULT_PREFIX"),
            ]
        );
        $routes->connect(
            "$mappingLanguageSlug/:primary_slug/:taxon_slug/:taxon_id",
            [
                'controller' => 'Taxonomys',
                'action' => 'mappingTaxon',
                'method' => ['GET'],
                "pass" => ["primary_slug"],
                "pass" => ["taxon_slug"],
                "pass" => ["taxon_id"],
                "prefix" => Configure::read("DEFAULT_PREFIX"),
            ]
        );
        $routes->connect(
            "$mappingLanguageSlug/:primary_slug/:taxon_slug/:software_slug/:taxon_id".".:software_id",
            [
                'controller' => 'Taxonomys',
                'action' => 'mappingTaxonUsersOf',
                'method' => ['GET'],
                "pass" => ["primary_slug"],
                "pass" => ["taxon_slug"],
                "pass" => ["software_slug"],
                "pass" => ["taxon_id"],
                "pass" => ["software_id"],
                "prefix" => Configure::read("DEFAULT_PREFIX"),
            ]
        );
    }
    $routes->connect(
        "/mappingForm/:softwareId",
        [
            'controller' => 'TaxonomysSoftwares',
            'action' => 'mappingForm',
            'method' => ['GET', 'POST'],
            "pass" => ["softwareId"],
            "prefix" => Configure::read("DEFAULT_PREFIX"),
        ]
    );

    // Software ===============================================================
    $routes->resources(
        'Softwares',
        [
            "prefix" => Configure::read("DEFAULT_PREFIX"),
            'map' => [
                // MAP FOR ADDITIONAL ACTION
                'services-providers/:id' => [
                    'action' => 'servicesProviders',
                    'method' => ['GET', 'POST']
                ],
                'alternative-to/:id' => [
                    'action' => 'alternativeTo',
                    'method' => 'GET'
                ],
                'works-well-softwares/:id' => [
                    'action' => 'workswellSoftwares',
                    'method' => 'GET'
                ],
                'delete-services-providers/:id' => [
                    'action' => 'deleteServicesProviders',
                    'method' => 'DELETE'
                ],
                'delete-users-software/:id' => [
                    'action' => 'deleteUsersSoftware',
                    'method' => 'DELETE'
                ],
                'servicesProviders/:id' => [
                    'action' => 'servicesProviders',
                    'method' => ['GET', 'POST']
                ],
                'users-software/:id' => [
                    'action' => 'usersSoftware',
                    'method' => ['GET', 'POST']
                ],
                'usersSoftware/:id' => [
                    'action' => 'usersSoftware',
                    'method' => 'GET'
                ],
                'alternativeTo/:id' => [
                    'action' => 'alternativeTo',
                    'method' => 'GET'
                ],
                'worksWellSoftwares/:id' => [
                    'action' => 'workswellSoftwares',
                    'method' => 'GET'
                ],
                'deleteServicesProviders/:id' => [
                    'action' => 'deleteServicesProviders',
                    'method' => 'DELETE'
                ],
                'deleteUsersSoftware/:id' => [
                    'action' => 'deleteUsersSoftware',
                    'method' => 'DELETE'
                ],
                'edit/:id' => [
                    'action' => 'edit',
                    'method' => ['GET', 'PUT']
                ],
                'view/:id' => [
                    'action' => 'view',
                    'method' => ['GET']
                ],
                'add' => [
                    'action' => 'add',
                    'method' => ['GET', 'POST']
                ],
                "index/" => [
                    'action' => 'index',
                    'method' => ['GET'],
                ],
                "export" => [
                    'action' => 'export',
                    'method' => ['GET'],
                ]
            ]
        ]
    );

    /**
     * Software routing
     */
    $routes->resources(
        'Softwares',
        function (RouteBuilder $routes) {
            $routes->resources('Reviews', ["prefix" => Configure::read("DEFAULT_PREFIX")]);
            $routes->resources('Screenshots', ["prefix" => Configure::read("DEFAULT_PREFIX")]);
        }
    );

    $routes->connect(
        '/softwares/add-review/:software_id',
        [
            'controller' => 'Reviews',
            'action' => 'add',
            "pass" => ["software_id"]
        ]
    );

    // Users ==================================================================
    $routes->resources(
        'Users',
        [
            "prefix" => Configure::read("DEFAULT_PREFIX"),
            "map" => [
                // MAP FOR ADDITIONAL ACTION
                ':id/reviews' => [
                    'action' => 'reviewsforSoftware',
                    'method' => 'GET'
                ],
                'providerforSoftwares/:id' => [
                    'action' => 'providerforSoftwares',
                    'method' => 'GET'
                ],
                'usedSoftwares/:id' => [
                    'action' => 'usedSoftwares',
                    'method' => 'GET'
                ],
                'providers' => [
                    'action' => 'providers',
                    'method' => 'GET'
                ],
                'add' => [
                    'action' => 'add',
                    'method' => ['GET', 'POST']
                ],
                'edit/:id' => [
                    'action' => 'edit',
                    'method' => ['GET', 'PUT']
                ],
                'login' => [
                    'action' => 'login',
                    'method' => ['GET', 'POST']
                ],
                'logout' => [
                    'action' => 'logout',
                    'method' => 'GET'
                ],
                'forgot-password' => [
                    'action' => 'forgotPassword',
                    'method' => ['GET', 'POST']
                ],
                'reset-password/*' => [
                    'action' => 'resetPassword',
                    'method' => ['GET', 'POST']
                ],
                'change-password' => [
                    'action' => 'changePassword',
                    'method' => ['GET']
                ],
                'change-password/*' => [
                    'action' => 'changePassword',
                    'method' => ['GET', "POST"]
                ],
                'view/:id' => [
                    'action' => 'view',
                    'method' => ['GET']
                ],
                'contact/:id' => [
                    'action' => 'contact',
                    'method' => ['GET', 'POST']
                ],
                "index/" => [
                    'action' => 'index',
                    'method' => ['GET'],
                ],
                "export" => [
                    'action' => 'export',
                    'method' => ['GET'],
                ]
            ]
        ]
    );

    // Tags ===================================================================
    $routes->resources(
        'Tags',
        [
            "prefix" => Configure::read("DEFAULT_PREFIX"),
            "map" => [
                'view/:id' => [
                    'action' => 'view',
                    'method' => ['GET']
                ],
                ':id/software' => [
                    'action' => 'listAllSoftwareForAGivenTag',
                    'method' => ['GET']
                ],
            ]
        ]
    );

    /*
     * SoftwaresTags routing
     */
    $routes->resources(
        'SoftwaresTags',
        [
            "prefix" => Configure::read("DEFAULT_PREFIX"),
            "map" => [

            ]
        ]
    );

    $routes->connect(
        "/",
        [
            "controller" => "Pages",
            "action" => "index",
            "prefix" => Configure::read("DEFAULT_PREFIX")
        ]
    );

    $routes->fallbacks('DashedRoute');
};

Router::prefix(
    'api/v1',
    function ($routes) {
        $routes->extensions(['json']);

    /**
     * Softwares routing
     */
        $routes->resources(
            'Softwares',
            function (RouteBuilder $routes) {
                $routes->resources('Reviews');
                $routes->resources('Screenshots');
            }
        );

        $routes->connect(
            '/softwares/add-review/:software_id',
            [
                'controller' => 'Reviews',
                'action' => 'add',
                "pass" => ["software_id"]
            ]
        );


    /**
     * Users routing
     */
        $routes->resources('Users');
        $routes->connect(
            '/users/servicesproviderUsers',
            [
                'controller' => 'Users',
                'action' => 'servicesproviderUsers'
            ]
        );
        $routes->connect('/users/add', ['controller' => 'Users', 'action' => 'add', '_method' => 'GET']);
        $routes->connect('/users/logout', ['controller' => 'Users', 'action' => 'logout']);


        $routes->resources('Reviews');
        $routes->resources('UserTypes');
        $routes->resources('Tags');
        $routes->resources('Tags');

        $routes->fallbacks('DashedRoute');
    }
);


$realRoutes = function ($routes) use ($basicRoutes) {
    $routes->scope('/', $basicRoutes);

    return $routes;
};

Router::scope('/fr', ['language' => 'fr'], $realRoutes);
Router::scope('/en', ['language' => 'en'], $realRoutes);
Router::scope('/', $realRoutes);

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
