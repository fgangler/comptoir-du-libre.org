#!/usr/bin/env bash


set -o errexit

# #############################################################################
# Option management
# #############################################################################

TEMP=`getopt -o d: -- "$@"`

if [[ $? != 0 ]] ; then echo "Terminating..." >&2 ; exit 1 ; fi

usage () {
    echo 'Re-generate .pot files and update all .po files.'
    echo ''
    echo 'usage: ./COMPTOIR_i18n_update.sh [[OPTIONS]...'
    echo ''
    echo '  -d <comptoir-srv-dir>   (MANDATORY) Absolute directory to Comptoir-srv, *without* trailing slash, eg "/home/comptoir/comptoir-srv"'
    echo ''
    exit 2
}

# Note the double quotes around $TEMP: they are essential!
eval set -- "${TEMP}"

declare COMPTOIR_SRV_DIR=

while true; do
    case "$1" in
        -d ) COMPTOIR_SRV_DIR="$2"; shift 2 ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

# check mandatory options
if [[ "${COMPTOIR_SRV_DIR}" = "" ]]
then
    echo ''
    echo 'Mandatory option is missing'
    echo ''
    usage
fi

# #############################################################################
# Variables
# #############################################################################
MSGMERGE="/usr/bin/msgmerge"
MSGMERGE_PKG="gettext"
LOCALE_DIR="${COMPTOIR_SRV_DIR}/src/Locale"

# #############################################################################
# Checks
# #############################################################################

if [[ ! -f /usr/bin/msgmerge ]]
then
    echo "${MSGMERGE} is missing, please install package ${MSGMERGE_PKG}"
    exit 2
fi

# #############################################################################
# Actual job
# #############################################################################

# #############################################################################
# Part 1: re-generate .pot files

echo "Part 1: re-generate .pot files"

"${COMPTOIR_SRV_DIR}/bin/cake" i18n extract \
    --output "${COMPTOIR_SRV_DIR}/src/Locale/" \
    --paths "${COMPTOIR_SRV_DIR}/src/" \
    --extract-core no \
    --exclude Test,vendor \
    --merge no

# #############################################################################
# Part 2: update .po files from .pot files

echo "Part 2: update .po files from .pot files"

for j in fr en ;
# loop over our 2 locales
do
    for i in $(ls ${LOCALE_DIR}/*.pot);
    # loop over each .pot file generated by `cake i18n extract`
    do
        MY_FILE=$(basename "${i}" .pot)
        echo "=== File: ${MY_FILE} ==="
        if [[ ! -f "${LOCALE_DIR}/${j}/${MY_FILE}.po" ]];
        then
            # .po file does *not* exist; let's create it
            msginit \
                --input "${LOCALE_DIR}/${MY_FILE}.pot" \
                --output "${LOCALE_DIR}/${j}/${MY_FILE}.po" \
                --no-translator
        else
            # .po file exists; let's update it
            msgmerge \
                "${LOCALE_DIR}/${j}/${MY_FILE}.po" \
                "${LOCALE_DIR}/${MY_FILE}.pot" \
                --update \
                --verbose \
                --force-po
        fi
    done
done
