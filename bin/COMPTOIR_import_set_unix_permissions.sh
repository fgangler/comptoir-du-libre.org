#!/usr/bin/env bash

set -o errexit

# #############################################################################
# Option management
# #############################################################################

TEMP=`getopt -o d:t: --long no-sudo -- "$@"`

if [[ $? != 0 ]] ; then echo "Terminating..." >&2 ; exit 1 ; fi

usage () {
    echo 'Set suitable UNIX permissions to binary files imported into Comptoir'
    echo 'NOTE: this must be run as a user with *sudo* privileges'
    echo ''
    echo 'usage: $0 [OPTIONS]...'
    echo ''
    echo '  -d <comptoir-srv-dir>   (MANDATORY) Absolute directory to Comptoir-srv, *without* trailing slash, eg "/home/comptoir/comptoir-srv"'
    echo '  --no-sudo               Do not use SUDO (useful for Docker containers)'
    echo ''
    exit 2
}

# Note the double quotes around $TEMP: they are essential!
eval set -- "${TEMP}"

declare COMPTOIR_ROOT=
declare SUDO=sudo

while true; do
    case "$1" in
        -d )        COMPTOIR_ROOT="$2"; shift 2 ;;
        --no-sudo ) SUDO="" ;           shift ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

# check mandatory options
if [[ "${COMPTOIR_ROOT}" = "" ]]
then
    echo ''
    echo 'Mandatory option is missing'
    echo ''
    usage
fi

# #############################################################################
# Actual action
# #############################################################################

${SUDO} groupadd comptoir
${SUDO} chmod -R o-w "${COMPTOIR_ROOT}"
for i in webroot logs tmp; do
    ${SUDO} chown -R www-data:comptoir "${COMPTOIR_ROOT}/${i}"
    find "${COMPTOIR_ROOT}/${i}" -type d -exec ${SUDO} chmod 775 {} \;
    find "${COMPTOIR_ROOT}/${i}" -type f -exec ${SUDO} chmod 664 {} \;
done
